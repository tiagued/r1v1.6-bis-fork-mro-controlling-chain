﻿Public Class CMatMarkupRowValidator
    Public Shared instance As CMatMarkupRowValidator
    Public Shared Function getInstance() As CMatMarkupRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CMatMarkupRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(mkup As CMatMarkup)
        mkup.calc_is_in_error_state = False
    End Sub

    Public Function percent(mkup As CMatMarkup) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If mkup.is_editable Then
            If mkup.percent < 0 OrElse mkup.percent > 1 Then
                err = "Markupt Rate shall be between 0 and 1 (0% and 100%)"
                res = False
                mkup.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class
