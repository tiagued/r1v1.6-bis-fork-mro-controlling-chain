﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CTaskRate
    Implements IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer

    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_TASK_RATE_LOADED Then
            CTaskRate.currentMaxID = CTaskRate.currentMaxID + 1
            _curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _is_editable = True
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_TASK_RATE_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_TASKRATE_DB_READ
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""

        Try
            If Not _is_editable Then
                res = False
                mess = "Item is not editable by end user so cannot be deleted"
            Else
                Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isTaskRateUsed(Me)
                If isUsed.Key Then
                    res = False
                    mess = "Item " & Me.tostring & " Cannot be deleted because it is used in " & isUsed.Value.Count & " CC Sold Hours " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value)
                Else
                    Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                    MConstants.GLB_MSTR_CTRL.lab_rate_controller.deleteObjectTaskRate(Me)
                    res = True
                End If
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Public Shared Function getEmptyLabRate() As CTaskRate
        Dim currState As Boolean
        currState = MConstants.GLOB_TASK_RATE_LOADED
        MConstants.GLOB_TASK_RATE_LOADED = False
        Dim empty As New CTaskRate
        empty.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if label nothing it will cause issue in combobox selection
        empty.label = " "
        empty.curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        MConstants.GLOB_TASK_RATE_LOADED = currState
        Return empty
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_TASK_RATE_LOADED Then
                    CTaskRate.currentMaxID = Math.Max(CTaskRate.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _value As Double
    Public Property value() As Double
        Get
            Return _value
        End Get
        Set(ByVal value As Double)
            If Not _value = value Then
                _value = value
                If MConstants.GLOB_TASK_RATE_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _curr As String
    Public Property curr() As String
        Get
            Return _curr
        End Get
        Set(ByVal value As String)
            If Not _curr = value Then
                _curr = value
                If MConstants.GLOB_TASK_RATE_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_editable As Boolean
    Public Property is_editable() As Boolean
        Get
            Return _is_editable
        End Get
        Set(ByVal value As Boolean)
            If Not _is_editable = value Then
                _is_editable = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "Task: " & _label & ",  rate: " & _value & " " & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_curr).value
    End Function
End Class

