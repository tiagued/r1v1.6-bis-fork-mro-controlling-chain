﻿Imports System.Threading

Public Class CMasterController
    'reference to the main form
    Public mainForm As Form1
    'customer statement handler
    Public csDB As CDBHandler
    Public csFile As CExcelInteropHandler
    Public file_path As String
    'conn string to cs file
    Public cs_conn As String
    'project controller
    Public project_controller As CProjectDelegate
    Public billing_cond_controller As CBillingConditionDelegate
    Public lab_rate_controller As CLaborRateDelegate
    Public ini_scope_controller As CInitScopeDelegate
    Public add_scope_controller As CAddScopeDelegate
    Public init_scope_hours_edit_ctrl As CInitScopeEditDelegate
    Public add_scope_hours_edit_ctrl As CAddScopeEditActivityDelegate
    Public material_controller As CMaterialInitAddScopeDelegate
    Public material_edit_ctrl As CMaterialIniAddEditDelegate
    Public admin_conf_controller As CAdminViewDelegate
    Public awq_controller As CAWQuotationDelegate
    Public awq_edit_ctrl As CAWQuotationEditDelegate
    Public save_controller As CSaveBackGroundWorker
    Public calculation_controller As CRefreshCalculationBackGroundWorker
    Public lov_edit_delegate As CListOfValuesDelegate
    Public proj_calculator As CProjectCalculator
    Public notifHelper As CNotificationHelper
    Public labor_controlling_controller As CLaborControllingDelegate
    Public labor_controlling_controller_edit As CLaborControllingEditDelegate
    Public labor_controlling_cost_node_ctrl As CLaborControllingCostNodeViewDelegate
    Public sys_data_ctrl As CDBSystemHandler
    'model view mapping
    Public modelViewMap As Dictionary(Of String, CViewModelMapList)
    'status bar view
    Public statusBar As CStatusStripView
    'save handler
    Public Shared saverDoneEvent As New AutoResetEvent(False)

    'delegate used to store functions to save sheets
    Delegate Sub saveSheetData()

    Public Sub New(form As Form1)
        Try

            'init splash screen handler
            MSpashScreenHandler.init()

            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("Starting application loading")
            MSpashScreenHandler.setProgressBarValue(0)

            MConstants.GLB_MSTR_CTRL = Me
            'get main form object
            mainForm = form
            'initialize status bar
            statusBar = New CStatusStripView(Me)

            'get file path
            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("Waiting for user input : Customer Statement File Path...")
            file_path = getDBFile()

            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("Getting file " & file_path)

            If String.IsNullOrWhiteSpace(file_path) Then
                Throw New Exception("No file selected !!")
            End If
            If file_path.ToLower.EndsWith(".accdb") Then
                cs_conn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & file_path & ";Persist Security Info=False;"
                If file_path.Contains("oleacedriver_15") Then
                    cs_conn = "Provider=Microsoft.ACE.OLEDB.15.0;Data Source=" & file_path & ";Persist Security Info=False;"
                ElseIf file_path.Contains("oleacedriver_16") Then
                    cs_conn = "Provider=Microsoft.ACE.OLEDB.16.0;Data Source=" & file_path & ";Persist Security Info=False;"
                End If
            Else
                Throw New Exception("Unrecognized file extension. Please choose a .accdb file")
            End If

            'inti delegate
            admin_conf_controller = New CAdminViewDelegate
            csDB = New CDBHandler(cs_conn, file_path)
            csFile = New CExcelInteropHandler(file_path)

            project_controller = New CProjectDelegate(Me)
            billing_cond_controller = New CBillingConditionDelegate(Me)
            lab_rate_controller = New CLaborRateDelegate(Me)
            ini_scope_controller = New CInitScopeDelegate(Me)
            add_scope_controller = New CAddScopeDelegate(Me)
            init_scope_hours_edit_ctrl = New CInitScopeEditDelegate
            add_scope_hours_edit_ctrl = New CAddScopeEditActivityDelegate
            material_controller = New CMaterialInitAddScopeDelegate
            material_edit_ctrl = New CMaterialIniAddEditDelegate
            awq_controller = New CAWQuotationDelegate
            awq_edit_ctrl = New CAWQuotationEditDelegate
            save_controller = New CSaveBackGroundWorker
            calculation_controller = New CRefreshCalculationBackGroundWorker
            lov_edit_delegate = New CListOfValuesDelegate
            labor_controlling_controller = New CLaborControllingDelegate
            labor_controlling_controller_edit = New CLaborControllingEditDelegate
            labor_controlling_cost_node_ctrl = New CLaborControllingCostNodeViewDelegate
            sys_data_ctrl = New CDBSystemHandler

            CViewRefreshHandler.initiateList()

            CGridViewRowValidationHandler.handler_list = New Dictionary(Of String, CGridViewRowValidationHandler)
            CGridViewRowValidationHandler.current_handled_grid_list = New List(Of String)
            CEditPanelViewRowValidationHandler.handler_list = New Dictionary(Of String, CEditPanelViewRowValidationHandler)
        Catch ex As Exception
            Throw New Exception("Loading Application" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub

    Public Sub initializeForm()
        Try
            statusBar.initialize()
            statusBar.status_strip_main_label = "Load customer statement configuration"

            Try
                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("Openning connection " & csDB.getConn)

                csDB.openConn()

                'bring to front before reading db
                MSpashScreenHandler.bringSplashFront()

                'init calculation worker
                calculation_controller.init()

                'get data model definition
                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("Loading Model and View Configuration")

                getModelViewMapping()

                'get constants
                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("Initialize Constants, List of Values, Payers and Cost Center Lists")

                MConstants.initialize(Me)

                'load report conf
                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("Load Reporting Configuration")

                CReportConfObject.loadReportConfObject()

                statusBar.status_strip_main_label = "Load project details"

                'log splashscreen
                'bring to front before reading db
                MSpashScreenHandler.bringSplashFront()
                MSpashScreenHandler.setCurrentTaskLabel("Start reading customer statement file tables")

                'read and disply project, exchange rates
                MSpashScreenHandler.setCurrentTaskLabel("Reading Project General Info Table")
                project_controller.model_readProjectDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Billing Condition Tables : Exchange rate, Freight, Markup,Surcharges, Discount, NTE, DownPayments")
                billing_cond_controller.model_read_BillingConditionDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Labor Rate Tables : Standard Labor rates, OEM Payer Rates, Task Rates")
                lab_rate_controller.model_read_labor_ratingDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Initial Scope Tables : Quote, Quote Summary")
                ini_scope_controller.model_read_init_scopeDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Additional Scope Tables : Activities, Labor and Operations")
                add_scope_controller.model_read_add_scopeDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Material Table")
                material_controller.model_read_material_relatedDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Additional Work Quotation Table")
                awq_controller.model_read_awqDB()
                MSpashScreenHandler.setCurrentTaskLabel("Reading Labor Controlling Tables")
                labor_controlling_controller.model_read_cc_transfer()

                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("End reading customer statement file tables")

                'notif helper
                notifHelper = New CNotificationHelper(mainForm)

                'close any remaing connection wb
                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("Closing OLEDB Opened Excel File")
                csFile.closeUserAppReadOnly()

                'link tables objects
                'log splashscreen
                MSpashScreenHandler.setCurrentTaskLabel("Building Relationships between objects")
                loadLinkedIDObjects()
            Catch ex As Exception
                Try
                    MConstants.GLB_MSTR_CTRL.csDB.closeConn()
                Catch ex2 As Exception
                End Try
                Throw New Exception("Reading File Data" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
            End Try

            'init context Menus
            MContextMenu.initializeContextMenu()

            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("Start Loading Standard User Interface Views")

            'bind view control to underlying data model
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface : Project General Info")
            project_controller.bind_project_view()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Billing Conditions: Exchange rate, Freight, Markup,Surcharges, Discount, NTE, DownPayments")
            billing_cond_controller.bind_billing_view()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Labor Rates:  Standard Labor rates, OEM Payer Rates, Task Rates")
            lab_rate_controller.bind_lab_rate_views()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Initial Scope: Quote, Quote Summary")
            ini_scope_controller.bind_init_scope_views()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Additional Scope:  Activities, Labor and Operations")
            add_scope_controller.bind_add_scope_views()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Materials")
            material_controller.bind_material_related_views()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Additional Work Quotations")
            awq_controller.bind_awq_views()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Editable List Of Values")
            lov_edit_delegate.bind_lov_views()
            MSpashScreenHandler.setCurrentTaskLabel("Loading User Interface Editable List Of Values")
            labor_controlling_controller.bind_controlling_view()

            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("End Loading Standard User Interface Views")

            'calculate project
            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("Calculating The Project")
            proj_calculator = New CProjectCalculator
            refreshCalculation()

            'init saver
            save_controller.init()

            MConstants.GLOB_APP_LOADED = True
            statusBar.initialize()

            'handle backup
            'log splashscreen
            MSpashScreenHandler.setCurrentTaskLabel("Saving project Backup Files And Logging session data to DB")

            MBackupHandler.backupFile(file_path)
            sys_data_ctrl.db_log_latest_open_user_time()

            MSpashScreenHandler.setCurrentTaskLabel("Application Ready !!")
            MSpashScreenHandler.setProgressBarValue(100)
        Catch ex As Exception
            Try
                MConstants.GLB_MSTR_CTRL.csDB.closeConn()
            Catch ex2 As Exception
            End Try
            Throw New Exception("Loading Application" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub

    'replace all ID links with the real object
    Private Sub loadLinkedIDObjects()
        Dim listTempAct As New List(Of String)

        'link on sold hours
        For Each soldH As CSoldHours In add_scope_controller.dbSoldHrsList
            soldH.act_object = add_scope_controller.getActivityObject(soldH.act_id)
            soldH.act_object.sold_hour_list.Add(soldH)
            If soldH.awq_id > 0 Then
                soldH.awq_obj = awq_controller.getAWQObject(soldH.awq_id)
                soldH.awq_obj.sold_hour_list.Add(soldH)
            End If

            If soldH.remove_awq_id > 0 Then
                soldH.remove_awq_obj = awq_controller.getAWQObject(soldH.remove_awq_id)
                soldH.awq_obj.sold_hour_removed_list.Add(soldH)
            End If
        Next
        MConstants.GLOB_SOLDHRS_LOADED = True

        'fill quote activities
        For Each act As CScopeActivity In add_scope_controller.scopeList.DataSource

            If act.is_fake_activity Then
                listTempAct.Add(act.act)
            End If

            'act link to awq
            If act.awq_master_id > 0 Then
                act.awq_master_obj = awq_controller.getAWQObject(act.awq_master_id)
                act.awq_master_obj.dependantActList.Add(act)
            End If
        Next
        MConstants.GLOB_ACTIVITY_LOADED = True

        'link between activities and quote entries
        For Each saleActLk As CSaleQuoteEntryToActLink In ini_scope_controller.quote_entries_to_act_link_list
            If saleActLk.act_id > 0 AndAlso saleActLk.init_quote_entry_id > 0 Then
                'get objects
                saleActLk.act_obj = add_scope_controller.getActivityObject(saleActLk.act_id)
                saleActLk.act_obj.quote_entry_link_list.Add(saleActLk)

                saleActLk.init_quote_entry_obj = ini_scope_controller.getQuoteEntryObject(saleActLk.init_quote_entry_id)
                saleActLk.init_quote_entry_obj.quote_entry_link_list.Add(saleActLk)
            Else
                ini_scope_controller.deleteQuoteEntryToActLink(saleActLk)
            End If
        Next
        MConstants.GLOB_SALEENTRY_TO_ACT_LK_LOADED = True

        'set activity to material
        For Each material As CMaterial In material_controller.matList.DataSource
            material.activity_obj = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(material.activity_id)
            material.activity_obj.material_list.Add(material)
            'awq
            If material.awq_id > 0 Then
                material.awq_obj = awq_controller.getAWQObject(material.awq_id)
                material.awq_obj.material_list.Add(material)
            End If

            If material.remove_awq_id > 0 Then
                material.remove_awq_obj = awq_controller.getAWQObject(material.remove_awq_id)
                material.awq_obj.material_removed_list.Add(material)
            End If
        Next

        MConstants.GLOB_MATERIAL_LOADED = True

        'build the awr structure and related objects
        For Each awq As CAWQuotation In awq_controller.awq_list.DataSource

            If Not String.IsNullOrWhiteSpace(awq.temp_reference) AndAlso Not listTempAct.Contains(awq.temp_reference) Then
                listTempAct.Add(awq.temp_reference)
            End If

            awq.master_obj = awq_controller.getAWQObject(awq.master_id)
            awq.master_obj.revision_list.Add(awq)

            If IsNothing(awq.master_obj.latest_REV) Then
                awq.master_obj.latest_REV = awq
            Else
                If awq.revision > awq.master_obj.latest_REV.revision Then
                    awq.master_obj.latest_REV = awq
                End If
            End If

            If awq.main_activity_id > 0 Then
                awq.main_activity_obj = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(awq.main_activity_id)
            End If
        Next

        MConstants.GLOB_AWQ_LOADED = True

        'fake activities counter
        CScopeActivity.currentMaxFakeAct = listTempAct.Count
        For Each tmp As String In listTempAct
            Dim act_cnt_str As String = ""
            'if awq
            Dim tmp_arr As String() = tmp.Split("-")
            If tmp_arr.Length >= 2 Then
                act_cnt_str = tmp_arr(1).Replace(MConstants.constantConf(MConstants.CONST_CONF_FAKE_ACT_PREFIX).value, "")
            Else
                'if act
                act_cnt_str = tmp.Replace(MConstants.constantConf(MConstants.CONST_CONF_FAKE_ACT_PREFIX).value, "")
            End If
            Dim act_cnt As Integer
            If Integer.TryParse(act_cnt_str, act_cnt) Then
                CScopeActivity.currentMaxFakeAct = Math.Max(CScopeActivity.currentMaxFakeAct, act_cnt)
            Else
                'if awq tmp ref
            End If
        Next
    End Sub

    Public Sub refreshCalculation()
        Try
            calculation_controller.startAsync()
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Calculation Error!", "An error occured while calculating the whole project" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'retrieve the configuration table that define the mapping between excel DB columns, objects properties and view controls
    Private Sub getModelViewMapping()
        Try
            modelViewMap = New Dictionary(Of String, CViewModelMapList)
            Dim modelViewMapSelf As CViewModelMapList
            Dim mapList As List(Of Object)

            modelViewMapSelf = MConstants.getMapMaping()
            mapList = csDB.performSelectObject(modelViewMapSelf.getDefaultSelect, modelViewMapSelf, True)
            modelViewMap = MGeneralFunctionsModelViewMap.fillModelViewDict(mapList)
        Catch ex As Exception
            Throw New Exception("An error occured while loading the mapping conf between DB, Business Objects and View Controls" & Chr(10) & ex.Message, ex)
        End Try
    End Sub

    'get file path either form argument list or from user input
    Public Function getDBFile() As String
        'check if any argument as parameter
        Try
            Dim skipfirst As Boolean = True
            For Each arg As String In Environment.GetCommandLineArgs()
                If skipfirst Then
                    skipfirst = False
                    Continue For
                End If
                If Not String.IsNullOrWhiteSpace(arg) AndAlso System.IO.File.Exists(arg.Trim) Then
                    Return arg
                End If
            Next
        Catch ex As Exception
            Throw New Exception("Checking if any file path was passed as an argument" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try

        'open file dialog
        Dim dialogRes As DialogResult
        Dim filepath As String = ""
        Try
            statusBar.status_strip_sub_label = "Select CS file..."
            mainForm.open_file_dialog.AddExtension = True
            mainForm.open_file_dialog.CheckFileExists = True
            mainForm.open_file_dialog.CheckPathExists = True
            mainForm.open_file_dialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            mainForm.open_file_dialog.Multiselect = False
            mainForm.open_file_dialog.DereferenceLinks = True
            'filter file types
            mainForm.open_file_dialog.Filter = "Customer Statement Access DB|*accdb"

            dialogRes = mainForm.open_file_dialog.ShowDialog()

            If dialogRes = DialogResult.OK Then
                'check file extention
                filepath = mainForm.open_file_dialog.FileName
            Else
                Return ""
            End If
        Catch ex As Exception
            Throw New Exception("Picking File Dialog loading" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        Return filepath
    End Function

    Public Function closeApp() As Boolean
        Dim cancel As Boolean = False
        Try
            If MConstants.GLOB_APP_LOADED Then
                Dim dialogRes As DialogResult
                dialogRes = MessageBox.Show("Do you want to close the application ?", "Close ?", MessageBoxButtons.OKCancel)
                If dialogRes = DialogResult.OK Then
                    'block auto save
                    save_controller.auto_save_timer_enable = False
                    'log session closure data to db
                    sys_data_ctrl.db_log_latest_close_user_time()
                    saveApp()
                    'check that any saving operation is ongoing
                    save_controller.startAsync(CSaveWorkerArgs.getDoneEventArg())

                    While save_controller.is_worker_busy
                        Application.DoEvents()
                        Thread.Sleep(TimeSpan.FromSeconds(1))
                    End While

                    While Not CMasterController.saverDoneEvent.WaitOne()
                        Thread.Sleep(TimeSpan.FromSeconds(1))
                    End While

                    csDB.closeConn()
                    ElseIf dialogRes = DialogResult.Cancel Then
                        cancel = True
                End If
            End If
        Catch ex As Exception
        End Try
        Return cancel
    End Function

    Public Sub saveApp()
        Try
            If MConstants.GLOB_APP_LOADED Then
                save_controller.startAsync(CSaveWorkerArgs.getInsertAll)
                save_controller.startAsync(CSaveWorkerArgs.getUpdateDeleteAll)
            End If
        Catch ex As Exception
        End Try
    End Sub
End Class
