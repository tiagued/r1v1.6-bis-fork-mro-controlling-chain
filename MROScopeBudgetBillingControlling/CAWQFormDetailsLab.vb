﻿Public Class CAWQFormDetailsLab
    Private _skill As String
    Public Property skill() As String
        Get
            Return _skill
        End Get
        Set(ByVal value As String)
            _skill = value
        End Set
    End Property

    Private _skill_key As String
    Public Property skill_key() As String
        Get
            Return _skill_key
        End Get
        Set(ByVal value As String)
            _skill_key = value
        End Set
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            _qty = value
        End Set
    End Property

    Private _UoM As String
    Public Property UoM() As String
        Get
            Return _UoM
        End Get
        Set(ByVal value As String)
            _UoM = value
        End Set
    End Property

    Private _price As Double
    Public Property price() As Double
        Get
            Return _price
        End Get
        Set(ByVal value As Double)
            _price = value
        End Set
    End Property

    Private _price_str As String
    Public Property price_str() As String
        Get
            Return _price_str
        End Get
        Set(ByVal value As String)
            _price_str = value
        End Set
    End Property

    Public ReadOnly Property price_view() As String
        Get
            If _price <> 0 Then
                Return _price.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
            ElseIf Not String.IsNullOrWhiteSpace(_price_str) Then
                Return _price_str
            Else
                Return 0.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
            End If
        End Get
    End Property

    Private _approved_price As Double
    Public Property approved_price() As Double
        Get
            Return _approved_price
        End Get
        Set(ByVal value As Double)
            _approved_price = value
        End Set
    End Property

    Private _approved_price_str As String
    Public Property approved_price_str() As String
        Get
            Return _approved_price_str
        End Get
        Set(ByVal value As String)
            _approved_price_str = value
        End Set
    End Property

    Public ReadOnly Property approved_price_view() As String
        Get
            If _approved_price <> 0 Then
                Return _approved_price.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
            ElseIf Not String.IsNullOrWhiteSpace(_approved_price_str) Then
                Return _approved_price_str
            Else
                Return 0.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATAGRIDVIEW_DEFAULT_DOUBLE_FORMAT).value)
            End If
        End Get
    End Property

    Public Sub addApprLabor(soldH As CSoldHours, isNegative As Boolean)
        If isNegative Then
            _qty = _qty - soldH.sold_hrs
            _approved_price = _approved_price - soldH.calc_price_before_disc
            If soldH.isRedMarkAndReportableToCustomer Then
                _approved_price_str = "(-)" & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_LABOR_REDMARK_LIST)(soldH.red_mark).value
            End If
        Else
            _qty = _qty + soldH.sold_hrs
            _approved_price = _approved_price + soldH.calc_price_before_disc
            If soldH.isRedMarkAndReportableToCustomer Then
                _approved_price_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_LABOR_REDMARK_LIST)(soldH.red_mark).value
            End If
        End If
    End Sub

    Public Sub addApprLaborDiscount(soldH As CSoldHours, isNegative As Boolean)
        If isNegative Then
            _approved_price = _approved_price - soldH.calc_discount
        Else
            _approved_price = _approved_price + soldH.calc_discount
        End If
    End Sub

    Public Sub addLabor(soldH As CSoldHours, isNegative As Boolean)
        If isNegative Then
            _qty = _qty - soldH.sold_hrs
            _price = _price - soldH.calc_price_before_disc
            If soldH.isRedMarkAndReportableToCustomer Then
                _price_str = "(-)" & MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_LABOR_REDMARK_LIST)(soldH.red_mark).value
            End If
        Else
            _qty = _qty + soldH.sold_hrs
            _price = _price + soldH.calc_price_before_disc
            If soldH.isRedMarkAndReportableToCustomer Then
                _price_str = MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_LABOR_REDMARK_LIST)(soldH.red_mark).value
            End If
        End If
    End Sub

    Public Sub addLaborDiscount(soldH As CSoldHours, isNegative As Boolean)
        If isNegative Then
            _price = _price - soldH.calc_discount
        Else
            _price = _price + soldH.calc_discount
        End If
    End Sub

End Class
