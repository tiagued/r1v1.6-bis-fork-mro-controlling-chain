﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SplashScreen
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ApplicationTitle = New System.Windows.Forms.Label()
        Me.Copyright = New System.Windows.Forms.Label()
        Me.Version = New System.Windows.Forms.Label()
        Me.loading_logs_list_b = New System.Windows.Forms.ListBox()
        Me.progressBar = New System.Windows.Forms.ProgressBar()
        Me.starting_lbl = New System.Windows.Forms.Label()
        Me.loading_gif_pb = New System.Windows.Forms.PictureBox()
        Me.app_logo_pb = New System.Windows.Forms.PictureBox()
        Me.jet_values_pb = New System.Windows.Forms.PictureBox()
        Me.jet_logo_pb = New System.Windows.Forms.PictureBox()
        Me.current_task_lbl = New System.Windows.Forms.Label()
        Me.app_info_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.copy_pictureBox = New System.Windows.Forms.PictureBox()
        Me.copy_splash_logs_tooltip = New System.Windows.Forms.ToolTip(Me.components)
        CType(Me.loading_gif_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.app_logo_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.jet_values_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.jet_logo_pb, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.app_info_layout.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        CType(Me.copy_pictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ApplicationTitle
        '
        Me.ApplicationTitle.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.ApplicationTitle.BackColor = System.Drawing.Color.Transparent
        Me.ApplicationTitle.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ApplicationTitle.Location = New System.Drawing.Point(6, 10)
        Me.ApplicationTitle.Name = "ApplicationTitle"
        Me.ApplicationTitle.Size = New System.Drawing.Size(542, 56)
        Me.ApplicationTitle.TabIndex = 0
        Me.ApplicationTitle.Text = "Application Title"
        Me.ApplicationTitle.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Copyright
        '
        Me.Copyright.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Copyright.BackColor = System.Drawing.Color.Transparent
        Me.Copyright.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Copyright.Location = New System.Drawing.Point(8, 66)
        Me.Copyright.Name = "Copyright"
        Me.Copyright.Size = New System.Drawing.Size(539, 30)
        Me.Copyright.TabIndex = 2
        Me.Copyright.Text = "Copyright"
        '
        'Version
        '
        Me.Version.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.Version.BackColor = System.Drawing.Color.Transparent
        Me.Version.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Version.Location = New System.Drawing.Point(6, 96)
        Me.Version.Name = "Version"
        Me.Version.Size = New System.Drawing.Size(542, 20)
        Me.Version.TabIndex = 1
        Me.Version.Text = "Version {0}.{1:00}"
        '
        'loading_logs_list_b
        '
        Me.loading_logs_list_b.BackColor = System.Drawing.SystemColors.ControlLight
        Me.loading_logs_list_b.FormattingEnabled = True
        Me.loading_logs_list_b.Location = New System.Drawing.Point(12, 153)
        Me.loading_logs_list_b.Name = "loading_logs_list_b"
        Me.loading_logs_list_b.Size = New System.Drawing.Size(711, 277)
        Me.loading_logs_list_b.TabIndex = 6
        '
        'progressBar
        '
        Me.progressBar.Location = New System.Drawing.Point(12, 478)
        Me.progressBar.Name = "progressBar"
        Me.progressBar.Size = New System.Drawing.Size(711, 23)
        Me.progressBar.TabIndex = 7
        '
        'starting_lbl
        '
        Me.starting_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.starting_lbl.BackColor = System.Drawing.Color.Transparent
        Me.starting_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.starting_lbl.Location = New System.Drawing.Point(736, 180)
        Me.starting_lbl.Name = "starting_lbl"
        Me.starting_lbl.Size = New System.Drawing.Size(153, 23)
        Me.starting_lbl.TabIndex = 8
        Me.starting_lbl.Text = "Starting ..."
        Me.starting_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'loading_gif_pb
        '
        Me.loading_gif_pb.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.LoadingGif
        Me.loading_gif_pb.Location = New System.Drawing.Point(739, 215)
        Me.loading_gif_pb.Name = "loading_gif_pb"
        Me.loading_gif_pb.Size = New System.Drawing.Size(150, 150)
        Me.loading_gif_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.loading_gif_pb.TabIndex = 9
        Me.loading_gif_pb.TabStop = False
        '
        'app_logo_pb
        '
        Me.app_logo_pb.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.application_logo
        Me.app_logo_pb.Location = New System.Drawing.Point(739, 12)
        Me.app_logo_pb.Name = "app_logo_pb"
        Me.app_logo_pb.Size = New System.Drawing.Size(150, 124)
        Me.app_logo_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.app_logo_pb.TabIndex = 5
        Me.app_logo_pb.TabStop = False
        '
        'jet_values_pb
        '
        Me.jet_values_pb.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.JetValues
        Me.jet_values_pb.Location = New System.Drawing.Point(739, 401)
        Me.jet_values_pb.Name = "jet_values_pb"
        Me.jet_values_pb.Size = New System.Drawing.Size(150, 100)
        Me.jet_values_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.jet_values_pb.TabIndex = 4
        Me.jet_values_pb.TabStop = False
        '
        'jet_logo_pb
        '
        Me.jet_logo_pb.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.jetLogo
        Me.jet_logo_pb.Location = New System.Drawing.Point(12, 12)
        Me.jet_logo_pb.Name = "jet_logo_pb"
        Me.jet_logo_pb.Size = New System.Drawing.Size(150, 50)
        Me.jet_logo_pb.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.jet_logo_pb.TabIndex = 3
        Me.jet_logo_pb.TabStop = False
        '
        'current_task_lbl
        '
        Me.current_task_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.current_task_lbl.BackColor = System.Drawing.Color.Transparent
        Me.current_task_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.current_task_lbl.Location = New System.Drawing.Point(12, 444)
        Me.current_task_lbl.Name = "current_task_lbl"
        Me.current_task_lbl.Size = New System.Drawing.Size(710, 23)
        Me.current_task_lbl.TabIndex = 10
        Me.current_task_lbl.Text = "Starting ..."
        Me.current_task_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'app_info_layout
        '
        Me.app_info_layout.AutoSize = True
        Me.app_info_layout.ColumnCount = 1
        Me.app_info_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.app_info_layout.Controls.Add(Me.ApplicationTitle, 0, 0)
        Me.app_info_layout.Controls.Add(Me.Copyright, 0, 1)
        Me.app_info_layout.Controls.Add(Me.Version, 0, 2)
        Me.app_info_layout.Location = New System.Drawing.Point(175, 12)
        Me.app_info_layout.Name = "app_info_layout"
        Me.app_info_layout.Padding = New System.Windows.Forms.Padding(0, 10, 0, 10)
        Me.app_info_layout.RowCount = 3
        Me.app_info_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.app_info_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.app_info_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.app_info_layout.Size = New System.Drawing.Size(555, 126)
        Me.app_info_layout.TabIndex = 11
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.copy_pictureBox)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(726, 158)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(25, 45)
        Me.FlowLayoutPanel1.TabIndex = 12
        '
        'copy_pictureBox
        '
        Me.copy_pictureBox.Cursor = System.Windows.Forms.Cursors.Hand
        Me.copy_pictureBox.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.copy_icon
        Me.copy_pictureBox.Location = New System.Drawing.Point(0, 0)
        Me.copy_pictureBox.Margin = New System.Windows.Forms.Padding(0)
        Me.copy_pictureBox.Name = "copy_pictureBox"
        Me.copy_pictureBox.Size = New System.Drawing.Size(25, 21)
        Me.copy_pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.copy_pictureBox.TabIndex = 6
        Me.copy_pictureBox.TabStop = False
        Me.copy_pictureBox.Tag = ""
        Me.copy_splash_logs_tooltip.SetToolTip(Me.copy_pictureBox, "Copy Logs")
        '
        'SplashScreen
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(901, 513)
        Me.ControlBox = False
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.app_info_layout)
        Me.Controls.Add(Me.current_task_lbl)
        Me.Controls.Add(Me.loading_gif_pb)
        Me.Controls.Add(Me.starting_lbl)
        Me.Controls.Add(Me.progressBar)
        Me.Controls.Add(Me.loading_logs_list_b)
        Me.Controls.Add(Me.app_logo_pb)
        Me.Controls.Add(Me.jet_values_pb)
        Me.Controls.Add(Me.jet_logo_pb)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "SplashScreen"
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        CType(Me.loading_gif_pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.app_logo_pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.jet_values_pb, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.jet_logo_pb, System.ComponentModel.ISupportInitialize).EndInit()
        Me.app_info_layout.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        CType(Me.copy_pictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Copyright As Label
    Friend WithEvents Version As Label
    Friend WithEvents ApplicationTitle As Label
    Friend WithEvents jet_logo_pb As PictureBox
    Friend WithEvents jet_values_pb As PictureBox
    Friend WithEvents loading_logs_list_b As ListBox
    Friend WithEvents progressBar As ProgressBar
    Friend WithEvents starting_lbl As Label
    Friend WithEvents loading_gif_pb As PictureBox
    Public WithEvents app_logo_pb As PictureBox
    Friend WithEvents current_task_lbl As Label
    Friend WithEvents app_info_layout As TableLayoutPanel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents copy_pictureBox As PictureBox
    Friend WithEvents copy_splash_logs_tooltip As ToolTip
End Class
