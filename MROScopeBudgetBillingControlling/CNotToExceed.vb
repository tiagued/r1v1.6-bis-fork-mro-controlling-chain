﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CNotToExceed
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable
    'used to created new ids
    Public Shared currentMaxID As Integer


    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_NTE_LOADED Then
            Me.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value
            'do it on internal private property , cause the setter will alterate the values
            _value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _nte_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_TYPE_NTEY_KEY).value
            _payer = MConstants.constantConf(MConstants.CONST_CONF_CUSTOMER_AS_DEFAULT_PAYER).value
            _payer_obj = MConstants.listOfPayer(_payer)
            _is_editable = True
        End If
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Sub setID()
        If MConstants.GLOB_NTE_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
        MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshNTEDependantViews()
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_NTE_DB_READ
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""

        Try
            If Not _is_editable Then
                res = False
                mess = "Item is not editable by end user so cannot be deleted"
            Else
                Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isNTEUsed(Me)
                If isUsed.Key Then
                    res = False
                    mess = "Item " & Me.ToString & " Cannot be deleted because it is used in " & isUsed.Value.Count & " Activity(ies)/Labor CC(s) / Material(s) " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value)
                Else
                    Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.deleteObjectNTE(Me)
                    res = True
                End If
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.ToString & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        If Not _is_editable Then
            res = False
            mess = "Cannot be modified"
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Shared Function getEmptyNTE() As CNotToExceed
        Dim currState As Boolean
        currState = MConstants.GLOB_NTE_LOADED
        MConstants.GLOB_NTE_LOADED = False
        Dim empty As New CNotToExceed
        empty.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if label nothing it will cause issue in combobox selection
        empty.label = " "
        MConstants.GLOB_NTE_LOADED = currState
        Return empty
    End Function

    Public Shared Function getNTENNoNote() As CNotToExceed
        Dim currState As Boolean
        currState = MConstants.GLOB_NTE_LOADED
        MConstants.GLOB_NTE_LOADED = False
        Dim nonote As New CNotToExceed
        nonote.id = -2
        'if label nothing it will cause issue in combobox selection
        nonote.label = "NTE Exclusion With No Exclusion Note"
        nonote.description = "NTE Exclusion With No Exclusion Note"
        nonote.lab_mat_applicability = MConstants.constantConf(MConstants.CONST_CONF_LOV_LABMAT_APP_LAB_MAT_REP_FRGH_SERV_KEY).value
        MConstants.GLOB_NTE_LOADED = currState
        Return nonote
    End Function

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_in_error_state = value Then
                _calc_is_in_error_state = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_NTE_LOADED Then
                    CNotToExceed.currentMaxID = Math.Max(CNotToExceed.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                If MConstants.GLOB_NTE_LOADED Then
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshNTEDependantViews()
                End If
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _lab_mat_applicability As String
    Public Property lab_mat_applicability() As String
        Get
            Return _lab_mat_applicability
        End Get
        Set(ByVal value As String)
            If Not _lab_mat_applicability = value Then
                _lab_mat_applicability = value
                If MConstants.GLOB_NTE_LOADED Then
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshNTEDependantViews()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property lab_mat_applicability_view
        Get
            If MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_APPLICABILITY_LIST).ContainsKey(_lab_mat_applicability) Then
                Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_LAB_MAP_APPLICABILITY_LIST)(_lab_mat_applicability).value
            Else
                Return ""
            End If
        End Get
    End Property

    Private _scope_applicability As String
    Public Property scope_applicability() As String
        Get
            Return _scope_applicability
        End Get
        Set(ByVal value As String)
            If Not _scope_applicability = value Then

                'check if used before changing scope applicability
                If MConstants.GLOB_NTE_LOADED Then
                    Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isNTEUsed(Me)
                    If isUsed.Key Then
                        MGeneralFuntionsViewControl.displayMessage("NTE is used", "Cannot change NTE scope cause it's already used by the following items " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value), MsgBoxStyle.Critical)
                        Exit Property
                    End If
                    _scope_applicability = value
                    MConstants.GLB_MSTR_CTRL.billing_cond_controller.refreshNTEDependantViews()
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                    NotifyPropertyChanged()
                Else
                    _scope_applicability = value
                End If

                _scope_applicability = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _nte_value As Double
    Public Property nte_value() As Double
        Get
            Return _nte_value
        End Get
        Set(ByVal value As Double)
            If Not _nte_value = value Then
                _nte_value = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_default As Boolean
    Public Property is_default() As Boolean
        Get
            Return _is_default
        End Get
        Set(ByVal value As Boolean)
            If Not _is_default = value Then
                _is_default = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _value_curr As String
    Public Property value_curr() As String
        Get
            Return _value_curr
        End Get
        Set(ByVal value As String)
            If Not _value_curr = value Then
                _value_curr = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'use for view
    Public ReadOnly Property value_curr_view() As String
        Get
            If MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR).ContainsKey(_value_curr) Then
                Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_value_curr).value
            Else
                Return ""
            End If
        End Get
    End Property

    Private _nte_type As String
    Public Property nte_type() As String
        Get
            Return _nte_type
        End Get
        Set(ByVal value As String)
            If Not _nte_type = value Then
                _nte_type = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                If MConstants.listOfPayer.ContainsKey(_payer) Then
                    Me.payer_obj = MConstants.listOfPayer(_payer)
                Else
                    Me.payer_obj = Nothing
                    Throw New Exception("Payer does not exist in payer list : " & _payer)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property
    Private _payer_obj As CPayer
    Public Property payer_obj() As CPayer
        Get
            Return _payer_obj
        End Get
        Set(ByVal value As CPayer)
            If Not Object.Equals(_payer_obj, value) Then
                _payer_obj = value
                If IsNothing(_payer_obj) Then
                    'use Me to force a new value of payer_obj
                    Me.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _payer = _payer_obj.payer
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _is_editable As Boolean
    Public Property is_editable() As Boolean
        Get
            Return _is_editable
        End Get
        Set(ByVal value As Boolean)
            If Not _is_editable = value Then
                _is_editable = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'get NTE for reporting
    Public Function getNTEAsCalEntry(soldH As CSoldHours) As CScopeAtivityCalcEntry
        Try
            Dim res As New CScopeAtivityCalcEntry(_id)
            If MUtils.isLaborApplicableLabMat(_lab_mat_applicability) Then
                If IsNothing(soldH.discount_obj) Then
                    res.labor_sold_calc = soldH.calc_price_before_disc
                Else
                    res.labor_sold_calc = soldH.calc_price
                End If
            End If
            Return res
        Catch ex As Exception
            Throw New Exception("An error occured while getting NTE on item labor" & soldH.tostring & Chr(10) & ex.StackTrace)
        End Try
    End Function

    'get NTE 
    Public Function getNTEAsCalEntry(mat As CMaterial, Optional exclusionEntry As CScopeAtivityCalcEntry = Nothing) As CScopeAtivityCalcEntry
        Try

            Dim res As New CScopeAtivityCalcEntry(_id)

            If MUtils.isFreightApplicableLabMat(_lab_mat_applicability) Then
                res.freight_sold_calc = res.freight_sold_calc + mat.calc_total_freight_in_freight_curr
            End If

            If MUtils.isServApplicableLabMat(_lab_mat_applicability) Then
                res.serv_sold_calc = res.serv_sold_calc + mat.calc_handling_zcus_value_in_mat_curr
            End If

            If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_MAT_KEY).value AndAlso
                MUtils.isMaterialApplicableLabMat(_lab_mat_applicability) Then
                res.mat_sold_calc = res.mat_sold_calc + mat.calc_total_material_in_mat_curr
            ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_REP_KEY).value AndAlso
                MUtils.isRepairApplicableLabMat(_lab_mat_applicability) Then
                res.rep_sold_calc = res.rep_sold_calc + mat.calc_total_material_in_mat_curr
            ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_FREIGHT_KEY).value AndAlso
                MUtils.isFreightApplicableLabMat(_lab_mat_applicability) Then
                res.freight_sold_calc = res.freight_sold_calc + mat.calc_total_material_in_mat_curr
            ElseIf mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_MAT_TYPE_3RD_P_KEY).value AndAlso
                MUtils.isServApplicableLabMat(_lab_mat_applicability) Then
                res.serv_sold_calc = res.serv_sold_calc + mat.calc_total_material_in_mat_curr
            End If

            If Not IsNothing(exclusionEntry) Then
                If MUtils.isMaterialApplicableLabMat(_lab_mat_applicability) Then
                    exclusionEntry.mat_sold_calc = 0
                End If
                If MUtils.isRepairApplicableLabMat(_lab_mat_applicability) Then
                    exclusionEntry.rep_sold_calc = 0
                End If
                If MUtils.isFreightApplicableLabMat(_lab_mat_applicability) Then
                    exclusionEntry.freight_sold_calc = 0
                End If
                If MUtils.isServApplicableLabMat(_lab_mat_applicability) Then
                    exclusionEntry.serv_sold_calc = 0
                End If
            End If

            Return res
        Catch ex As Exception
            Throw New Exception("An error occured while getting NTE on item Material" & mat.tostring & Chr(10) & ex.StackTrace)
        End Try
    End Function

    Public Function isNTEYes() As Boolean
        Return _nte_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_TYPE_NTEY_KEY).value
    End Function
    Public Function isNTEExclusion() As Boolean
        Return _nte_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_NTE_TYPE_NTEN_KEY).value
    End Function
    'to string
    Public Overrides Function tostring() As String
        Return "NTE : " & _label & ", Desc: " & _description
    End Function

End Class

