﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices
Imports System.Text.RegularExpressions

Public Class CActualHours
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Shared currentMaxID As Integer
    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_ACTUAL_HRS_LOADED Then
            'set combo box
            'set first cc in list
            'use Me. to trigger object retrieval from id
            _cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _posting_date = MConstants.APP_NULL_DATE
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_ACTUAL_HRS_LOADED Then
            CActualHours.currentMaxID = CActualHours.currentMaxID + 1
            Me.id = CActualHours.currentMaxID
        End If
    End Sub

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_ACTUAL_LABOR_DB_READ
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        res = False
        mess = "Cannot be modified"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = "It is not allowed to delete Actual Hours objects"
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_ACTUAL_HRS_LOADED Then
                    CActualHours.currentMaxID = Math.Max(CActualHours.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _cost_object As String
    Public Property cost_object() As String
        Get
            Return _cost_object
        End Get
        Set(ByVal value As String)
            If Not _cost_object = value Then
                _cost_object = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_id As Long
    Public Property act_id() As Long
        Get
            Return _act_id
        End Get
        Set(ByVal value As Long)
            If Not _act_id = value Then
                _act_id = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _act_obj As CScopeActivity
    Public Property act_obj() As CScopeActivity
        Get
            Return _act_obj
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(value, _act_obj) Then
                _act_obj = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _personal_number As String
    Public Property personal_number() As String
        Get
            Return _personal_number
        End Get
        Set(ByVal value As String)
            If Not _personal_number = value Then
                _personal_number = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _wbs_element As String
    Public Property wbs_element() As String
        Get
            Return _wbs_element
        End Get
        Set(ByVal value As String)
            If Not _wbs_element = value Then
                _wbs_element = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property


    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value Then
                _qty = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _posting_number As Long
    Public Property posting_number() As Long
        Get
            Return _posting_number
        End Get
        Set(ByVal value As Long)
            If Not _posting_number = value Then
                _posting_number = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _posting_date As Date
    Public Property posting_date() As Date
        Get
            Return _posting_date
        End Get
        Set(ByVal value As Date)
            If Not _posting_date = value Then
                _posting_date = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'network
    Private _calc_network As String
    Public Property calc_network() As String
        Get
            Return _calc_network
        End Get
        Set(ByVal value As String)
            If Not _calc_network = value Then
                _calc_network = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'activity
    Private _calc_activity As String
    Public Property calc_activity() As String
        Get
            Return _calc_activity
        End Get
        Set(ByVal value As String)
            If Not _calc_activity = value Then
                _calc_activity = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'notif
    Private _calc_notfication As String
    Public Property calc_notfication() As String
        Get
            Return _calc_notfication
        End Get
        Set(ByVal value As String)
            If Not _calc_notfication = value Then
                _calc_notfication = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'is prodo
    Private _is_prodo As Boolean
    Public Property is_prodo() As Boolean
        Get
            Return _is_prodo
        End Get
        Set(ByVal value As Boolean)
            If Not _is_prodo = value Then
                _is_prodo = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'ist cost collector
    Private _is_cost_collector As Boolean
    Public Property is_cost_collector() As Boolean
        Get
            Return _is_cost_collector
        End Get
        Set(ByVal value As Boolean)
            If Not _is_cost_collector = value Then
                _is_cost_collector = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Sub populateCalc()
        'get network=> all cases
        If Not String.IsNullOrWhiteSpace(_wbs_element) Then
            _calc_network = Trim(_wbs_element.Split(MConstants.constantConf(MConstants.CONST_CONF_WBS_SAP_SEPARATOR).value)(0))
        Else
            _calc_network = ""
        End If

        'if prodo, cost_obj is numeric,
        If IsNumeric(_cost_object) Then
            _is_prodo = True
        Else
            'if not prodo, cost_obj is Network Act (for MRO and non MRO)
            _calc_activity = Trim(Strings.Replace(_cost_object, _calc_network, ""))
            If IsNumeric(_calc_activity) Then
                Dim act_lng = CLng(_calc_activity)
                If act_lng >= 1000 AndAlso act_lng <= 6999 Then
                    _is_cost_collector = False
                Else
                    _is_cost_collector = True
                End If
            Else
                'if not numeric => cost collector
                _is_cost_collector = True
            End If
        End If

        'notification (for prodo and imro tasks)
        If is_prodo OrElse Not _is_cost_collector Then
            Dim wbs_split As String() = _wbs_element.Split(MConstants.constantConf(MConstants.CONST_CONF_WBS_SAP_SEPARATOR).value)
            _calc_notfication = Trim(wbs_split(wbs_split.Count - 1))
            'check if end by a lettere
            _calc_notfication = Regex.Replace(_calc_notfication, "\D+", "")
        End If
    End Sub
End Class
