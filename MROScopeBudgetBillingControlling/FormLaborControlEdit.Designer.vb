﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FormLaborControlEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.top_panel_activity = New System.Windows.Forms.Panel()
        Me.activity_info_grp = New System.Windows.Forms.GroupBox()
        Me.activity_details_tab = New System.Windows.Forms.TabControl()
        Me.act_details_general_page = New System.Windows.Forms.TabPage()
        Me.act_details_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.act_just_overshoot_lbl = New System.Windows.Forms.Label()
        Me.act_dev_after_just_lbl = New System.Windows.Forms.Label()
        Me.act_just_gain_val = New System.Windows.Forms.TextBox()
        Me.act_just_overshoot_val = New System.Windows.Forms.TextBox()
        Me.act_dev_after_just_val = New System.Windows.Forms.TextBox()
        Me.act_actl_adjusted_lbl = New System.Windows.Forms.Label()
        Me.act_actl_received_lbl = New System.Windows.Forms.Label()
        Me.act_actl_received_val = New System.Windows.Forms.TextBox()
        Me.act_actl_adjusted_val = New System.Windows.Forms.TextBox()
        Me.act_actl_sent_lbl = New System.Windows.Forms.Label()
        Me.act_actual_val = New System.Windows.Forms.TextBox()
        Me.act_actual_lbl = New System.Windows.Forms.Label()
        Me.cc_desc_lbl = New System.Windows.Forms.Label()
        Me.cc_val = New System.Windows.Forms.TextBox()
        Me.cc_lbl = New System.Windows.Forms.Label()
        Me.act_notif_val = New System.Windows.Forms.TextBox()
        Me.act_notif_lbl = New System.Windows.Forms.Label()
        Me.act_jc_val = New System.Windows.Forms.TextBox()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.act_proj_val = New System.Windows.Forms.TextBox()
        Me.act_just_gain_lbl = New System.Windows.Forms.Label()
        Me.act_devition_val = New System.Windows.Forms.TextBox()
        Me.act_devition_lbl = New System.Windows.Forms.Label()
        Me.act_budget_sent_val = New System.Windows.Forms.TextBox()
        Me.act_budget_sent_lbl = New System.Windows.Forms.Label()
        Me.act_budget_val = New System.Windows.Forms.TextBox()
        Me.act_budget_lbl = New System.Windows.Forms.Label()
        Me.act_status_lbl = New System.Windows.Forms.Label()
        Me.act_status_val = New System.Windows.Forms.TextBox()
        Me.act_proj_lbl = New System.Windows.Forms.Label()
        Me.act_activity_lbl = New System.Windows.Forms.Label()
        Me.act_activity_val = New System.Windows.Forms.TextBox()
        Me.act_desc_lbl = New System.Windows.Forms.Label()
        Me.act_desc_val = New System.Windows.Forms.TextBox()
        Me.cc_desc_val = New System.Windows.Forms.TextBox()
        Me.act_budget_received_lbl = New System.Windows.Forms.Label()
        Me.act_budget_received_val = New System.Windows.Forms.TextBox()
        Me.act_budget_adj_lbl = New System.Windows.Forms.Label()
        Me.act_budget_adj_val = New System.Windows.Forms.TextBox()
        Me.act_actl_sent_val = New System.Windows.Forms.TextBox()
        Me.act_details_other_page = New System.Windows.Forms.TabPage()
        Me.product_details_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.act_budget_add_foc_val = New System.Windows.Forms.TextBox()
        Me.act_budget_add_foc = New System.Windows.Forms.Label()
        Me.act_budget_awq_appr_lbl = New System.Windows.Forms.Label()
        Me.act_budget_awq_pmgo_val = New System.Windows.Forms.TextBox()
        Me.act_budget_awq_pmgo_lbl = New System.Windows.Forms.Label()
        Me.act_budget_awq_sent_val = New System.Windows.Forms.TextBox()
        Me.act_budget_awq_sent_lbl = New System.Windows.Forms.Label()
        Me.act_budget_blw_thr_val = New System.Windows.Forms.TextBox()
        Me.act_budget_blw_thr_lbl = New System.Windows.Forms.Label()
        Me.act_budget_init_val = New System.Windows.Forms.TextBox()
        Me.act_budget_awq_inwk_lbl = New System.Windows.Forms.Label()
        Me.act_budget_awq_inwk_val = New System.Windows.Forms.TextBox()
        Me.act_budget_init_lbl = New System.Windows.Forms.Label()
        Me.act_budget_init_foc_lbl = New System.Windows.Forms.Label()
        Me.act_budget_init_foc_val = New System.Windows.Forms.TextBox()
        Me.act_budget_awq_total_lbl = New System.Windows.Forms.Label()
        Me.act_budget_awq_total_val = New System.Windows.Forms.TextBox()
        Me.act_budget_awq_appr_val = New System.Windows.Forms.TextBox()
        Me.select_activity_grp = New System.Windows.Forms.GroupBox()
        Me.act_cc_error_state_pic = New System.Windows.Forms.PictureBox()
        Me.act_cc_select_right_pic = New System.Windows.Forms.PictureBox()
        Me.act_cc_select_left_pic = New System.Windows.Forms.PictureBox()
        Me.cb_act_cc = New System.Windows.Forms.ComboBox()
        Me.ctrl_operations_panel = New System.Windows.Forms.Panel()
        Me.ctrl_operations_tab = New System.Windows.Forms.TabControl()
        Me.ctrl_op_deviation_just_page = New System.Windows.Forms.TabPage()
        Me.dev_just_savings_grp = New System.Windows.Forms.GroupBox()
        Me.dev_just_savings_dgrid = New System.Windows.Forms.DataGridView()
        Me.dev_just_overshoot_grp = New System.Windows.Forms.GroupBox()
        Me.dev_just_overshoot_dgrid = New System.Windows.Forms.DataGridView()
        Me.ctrl_op_budget_transfer_page = New System.Windows.Forms.TabPage()
        Me.budget_received_grp = New System.Windows.Forms.GroupBox()
        Me.budget_received_dgrid = New System.Windows.Forms.DataGridView()
        Me.budget_transfer_to_grp = New System.Windows.Forms.GroupBox()
        Me.budget_transfer_to_dgrid = New System.Windows.Forms.DataGridView()
        Me.ctrl_op_actual_transfer_page = New System.Windows.Forms.TabPage()
        Me.actual_received_grp = New System.Windows.Forms.GroupBox()
        Me.actual_received_dgrid = New System.Windows.Forms.DataGridView()
        Me.actual_transfer_to_grp = New System.Windows.Forms.GroupBox()
        Me.actual_transfer_to_dgrid = New System.Windows.Forms.DataGridView()
        Me.ctrl_op_actuals_page = New System.Windows.Forms.TabPage()
        Me.cij3_booking_grp = New System.Windows.Forms.GroupBox()
        Me.cij3_booking_dgrid = New System.Windows.Forms.DataGridView()
        Me.controling_operation_grp = New System.Windows.Forms.GroupBox()
        Me.main_panel = New System.Windows.Forms.Panel()
        Me.top_panel_activity.SuspendLayout()
        Me.activity_info_grp.SuspendLayout()
        Me.activity_details_tab.SuspendLayout()
        Me.act_details_general_page.SuspendLayout()
        Me.act_details_lay.SuspendLayout()
        Me.act_details_other_page.SuspendLayout()
        Me.product_details_lay.SuspendLayout()
        Me.select_activity_grp.SuspendLayout()
        CType(Me.act_cc_error_state_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.act_cc_select_right_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.act_cc_select_left_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctrl_operations_panel.SuspendLayout()
        Me.ctrl_operations_tab.SuspendLayout()
        Me.ctrl_op_deviation_just_page.SuspendLayout()
        Me.dev_just_savings_grp.SuspendLayout()
        CType(Me.dev_just_savings_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.dev_just_overshoot_grp.SuspendLayout()
        CType(Me.dev_just_overshoot_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctrl_op_budget_transfer_page.SuspendLayout()
        Me.budget_received_grp.SuspendLayout()
        CType(Me.budget_received_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.budget_transfer_to_grp.SuspendLayout()
        CType(Me.budget_transfer_to_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctrl_op_actual_transfer_page.SuspendLayout()
        Me.actual_received_grp.SuspendLayout()
        CType(Me.actual_received_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.actual_transfer_to_grp.SuspendLayout()
        CType(Me.actual_transfer_to_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ctrl_op_actuals_page.SuspendLayout()
        Me.cij3_booking_grp.SuspendLayout()
        CType(Me.cij3_booking_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.controling_operation_grp.SuspendLayout()
        Me.main_panel.SuspendLayout()
        Me.SuspendLayout()
        '
        'top_panel_activity
        '
        Me.top_panel_activity.Controls.Add(Me.activity_info_grp)
        Me.top_panel_activity.Controls.Add(Me.select_activity_grp)
        Me.top_panel_activity.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel_activity.Location = New System.Drawing.Point(0, 0)
        Me.top_panel_activity.Name = "top_panel_activity"
        Me.top_panel_activity.Size = New System.Drawing.Size(1132, 276)
        Me.top_panel_activity.TabIndex = 1
        '
        'activity_info_grp
        '
        Me.activity_info_grp.Controls.Add(Me.activity_details_tab)
        Me.activity_info_grp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.activity_info_grp.Location = New System.Drawing.Point(0, 59)
        Me.activity_info_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.activity_info_grp.Name = "activity_info_grp"
        Me.activity_info_grp.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.activity_info_grp.Size = New System.Drawing.Size(1132, 217)
        Me.activity_info_grp.TabIndex = 1
        Me.activity_info_grp.TabStop = False
        Me.activity_info_grp.Text = "Activity-Cost Center Controlling Details"
        '
        'activity_details_tab
        '
        Me.activity_details_tab.Controls.Add(Me.act_details_general_page)
        Me.activity_details_tab.Controls.Add(Me.act_details_other_page)
        Me.activity_details_tab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.activity_details_tab.Location = New System.Drawing.Point(7, 19)
        Me.activity_details_tab.Name = "activity_details_tab"
        Me.activity_details_tab.SelectedIndex = 0
        Me.activity_details_tab.Size = New System.Drawing.Size(1118, 192)
        Me.activity_details_tab.TabIndex = 2
        '
        'act_details_general_page
        '
        Me.act_details_general_page.AutoScroll = True
        Me.act_details_general_page.Controls.Add(Me.act_details_lay)
        Me.act_details_general_page.Location = New System.Drawing.Point(4, 22)
        Me.act_details_general_page.Name = "act_details_general_page"
        Me.act_details_general_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_details_general_page.Size = New System.Drawing.Size(1110, 166)
        Me.act_details_general_page.TabIndex = 0
        Me.act_details_general_page.Text = "General"
        Me.act_details_general_page.UseVisualStyleBackColor = True
        '
        'act_details_lay
        '
        Me.act_details_lay.ColumnCount = 8
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.act_details_lay.Controls.Add(Me.act_just_overshoot_lbl, 4, 4)
        Me.act_details_lay.Controls.Add(Me.act_dev_after_just_lbl, 6, 4)
        Me.act_details_lay.Controls.Add(Me.act_just_gain_val, 3, 4)
        Me.act_details_lay.Controls.Add(Me.act_just_overshoot_val, 5, 4)
        Me.act_details_lay.Controls.Add(Me.act_dev_after_just_val, 7, 4)
        Me.act_details_lay.Controls.Add(Me.act_actl_adjusted_lbl, 6, 3)
        Me.act_details_lay.Controls.Add(Me.act_actl_received_lbl, 4, 3)
        Me.act_details_lay.Controls.Add(Me.act_actl_received_val, 5, 3)
        Me.act_details_lay.Controls.Add(Me.act_actl_adjusted_val, 7, 3)
        Me.act_details_lay.Controls.Add(Me.act_actl_sent_lbl, 2, 3)
        Me.act_details_lay.Controls.Add(Me.act_actual_val, 1, 3)
        Me.act_details_lay.Controls.Add(Me.act_actual_lbl, 0, 3)
        Me.act_details_lay.Controls.Add(Me.cc_desc_lbl, 6, 1)
        Me.act_details_lay.Controls.Add(Me.cc_val, 5, 1)
        Me.act_details_lay.Controls.Add(Me.cc_lbl, 4, 1)
        Me.act_details_lay.Controls.Add(Me.act_notif_val, 3, 1)
        Me.act_details_lay.Controls.Add(Me.act_notif_lbl, 2, 1)
        Me.act_details_lay.Controls.Add(Me.act_jc_val, 5, 0)
        Me.act_details_lay.Controls.Add(Me.Label31, 4, 0)
        Me.act_details_lay.Controls.Add(Me.act_proj_val, 1, 0)
        Me.act_details_lay.Controls.Add(Me.act_just_gain_lbl, 2, 4)
        Me.act_details_lay.Controls.Add(Me.act_devition_val, 1, 4)
        Me.act_details_lay.Controls.Add(Me.act_devition_lbl, 0, 4)
        Me.act_details_lay.Controls.Add(Me.act_budget_sent_val, 3, 2)
        Me.act_details_lay.Controls.Add(Me.act_budget_sent_lbl, 2, 2)
        Me.act_details_lay.Controls.Add(Me.act_budget_val, 1, 2)
        Me.act_details_lay.Controls.Add(Me.act_budget_lbl, 0, 2)
        Me.act_details_lay.Controls.Add(Me.act_status_lbl, 0, 1)
        Me.act_details_lay.Controls.Add(Me.act_status_val, 1, 1)
        Me.act_details_lay.Controls.Add(Me.act_proj_lbl, 0, 0)
        Me.act_details_lay.Controls.Add(Me.act_activity_lbl, 2, 0)
        Me.act_details_lay.Controls.Add(Me.act_activity_val, 3, 0)
        Me.act_details_lay.Controls.Add(Me.act_desc_lbl, 6, 0)
        Me.act_details_lay.Controls.Add(Me.act_desc_val, 7, 0)
        Me.act_details_lay.Controls.Add(Me.cc_desc_val, 7, 1)
        Me.act_details_lay.Controls.Add(Me.act_budget_received_lbl, 4, 2)
        Me.act_details_lay.Controls.Add(Me.act_budget_received_val, 5, 2)
        Me.act_details_lay.Controls.Add(Me.act_budget_adj_lbl, 6, 2)
        Me.act_details_lay.Controls.Add(Me.act_budget_adj_val, 7, 2)
        Me.act_details_lay.Controls.Add(Me.act_actl_sent_val, 3, 3)
        Me.act_details_lay.Location = New System.Drawing.Point(6, 6)
        Me.act_details_lay.Name = "act_details_lay"
        Me.act_details_lay.RowCount = 5
        Me.act_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.act_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.act_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.act_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.act_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.act_details_lay.Size = New System.Drawing.Size(1101, 125)
        Me.act_details_lay.TabIndex = 0
        '
        'act_just_overshoot_lbl
        '
        Me.act_just_overshoot_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_just_overshoot_lbl.AutoSize = True
        Me.act_just_overshoot_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_just_overshoot_lbl.Location = New System.Drawing.Point(354, 106)
        Me.act_just_overshoot_lbl.Name = "act_just_overshoot_lbl"
        Me.act_just_overshoot_lbl.Size = New System.Drawing.Size(124, 13)
        Me.act_just_overshoot_lbl.TabIndex = 9
        Me.act_just_overshoot_lbl.Text = "Justified Overshoot :"
        '
        'act_dev_after_just_lbl
        '
        Me.act_dev_after_just_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_dev_after_just_lbl.AutoSize = True
        Me.act_dev_after_just_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_dev_after_just_lbl.Location = New System.Drawing.Point(577, 106)
        Me.act_dev_after_just_lbl.Name = "act_dev_after_just_lbl"
        Me.act_dev_after_just_lbl.Size = New System.Drawing.Size(131, 13)
        Me.act_dev_after_just_lbl.TabIndex = 10
        Me.act_dev_after_just_lbl.Text = "Deviation After Just. :"
        '
        'act_just_gain_val
        '
        Me.act_just_gain_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_just_gain_val.Location = New System.Drawing.Point(265, 103)
        Me.act_just_gain_val.Name = "act_just_gain_val"
        Me.act_just_gain_val.Size = New System.Drawing.Size(83, 20)
        Me.act_just_gain_val.TabIndex = 3
        '
        'act_just_overshoot_val
        '
        Me.act_just_overshoot_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_just_overshoot_val.Location = New System.Drawing.Point(484, 103)
        Me.act_just_overshoot_val.Name = "act_just_overshoot_val"
        Me.act_just_overshoot_val.Size = New System.Drawing.Size(87, 20)
        Me.act_just_overshoot_val.TabIndex = 4
        '
        'act_dev_after_just_val
        '
        Me.act_dev_after_just_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_dev_after_just_val.Location = New System.Drawing.Point(714, 103)
        Me.act_dev_after_just_val.Name = "act_dev_after_just_val"
        Me.act_dev_after_just_val.Size = New System.Drawing.Size(83, 20)
        Me.act_dev_after_just_val.TabIndex = 5
        '
        'act_actl_adjusted_lbl
        '
        Me.act_actl_adjusted_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actl_adjusted_lbl.AutoSize = True
        Me.act_actl_adjusted_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_actl_adjusted_lbl.Location = New System.Drawing.Point(577, 81)
        Me.act_actl_adjusted_lbl.Name = "act_actl_adjusted_lbl"
        Me.act_actl_adjusted_lbl.Size = New System.Drawing.Size(104, 13)
        Me.act_actl_adjusted_lbl.TabIndex = 12
        Me.act_actl_adjusted_lbl.Text = "Actual Adjusted :"
        '
        'act_actl_received_lbl
        '
        Me.act_actl_received_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actl_received_lbl.AutoSize = True
        Me.act_actl_received_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_actl_received_lbl.Location = New System.Drawing.Point(354, 81)
        Me.act_actl_received_lbl.Name = "act_actl_received_lbl"
        Me.act_actl_received_lbl.Size = New System.Drawing.Size(99, 13)
        Me.act_actl_received_lbl.TabIndex = 11
        Me.act_actl_received_lbl.Text = "Actual Receiv. :"
        '
        'act_actl_received_val
        '
        Me.act_actl_received_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actl_received_val.Location = New System.Drawing.Point(484, 78)
        Me.act_actl_received_val.Name = "act_actl_received_val"
        Me.act_actl_received_val.Size = New System.Drawing.Size(87, 20)
        Me.act_actl_received_val.TabIndex = 9
        '
        'act_actl_adjusted_val
        '
        Me.act_actl_adjusted_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actl_adjusted_val.Location = New System.Drawing.Point(714, 78)
        Me.act_actl_adjusted_val.Name = "act_actl_adjusted_val"
        Me.act_actl_adjusted_val.Size = New System.Drawing.Size(83, 20)
        Me.act_actl_adjusted_val.TabIndex = 10
        '
        'act_actl_sent_lbl
        '
        Me.act_actl_sent_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actl_sent_lbl.AutoSize = True
        Me.act_actl_sent_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_actl_sent_lbl.Location = New System.Drawing.Point(167, 81)
        Me.act_actl_sent_lbl.Name = "act_actl_sent_lbl"
        Me.act_actl_sent_lbl.Size = New System.Drawing.Size(81, 13)
        Me.act_actl_sent_lbl.TabIndex = 14
        Me.act_actl_sent_lbl.Text = "Actual Sent :"
        '
        'act_actual_val
        '
        Me.act_actual_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actual_val.Location = New System.Drawing.Point(78, 78)
        Me.act_actual_val.Name = "act_actual_val"
        Me.act_actual_val.Size = New System.Drawing.Size(83, 20)
        Me.act_actual_val.TabIndex = 26
        '
        'act_actual_lbl
        '
        Me.act_actual_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actual_lbl.AutoSize = True
        Me.act_actual_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_actual_lbl.Location = New System.Drawing.Point(3, 81)
        Me.act_actual_lbl.Name = "act_actual_lbl"
        Me.act_actual_lbl.Size = New System.Drawing.Size(51, 13)
        Me.act_actual_lbl.TabIndex = 25
        Me.act_actual_lbl.Text = "Actual :"
        '
        'cc_desc_lbl
        '
        Me.cc_desc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cc_desc_lbl.AutoSize = True
        Me.cc_desc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cc_desc_lbl.Location = New System.Drawing.Point(577, 31)
        Me.cc_desc_lbl.Name = "cc_desc_lbl"
        Me.cc_desc_lbl.Size = New System.Drawing.Size(68, 13)
        Me.cc_desc_lbl.TabIndex = 24
        Me.cc_desc_lbl.Text = "CC Desc. :"
        '
        'cc_val
        '
        Me.cc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cc_val.Location = New System.Drawing.Point(484, 28)
        Me.cc_val.Name = "cc_val"
        Me.cc_val.Size = New System.Drawing.Size(87, 20)
        Me.cc_val.TabIndex = 22
        '
        'cc_lbl
        '
        Me.cc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cc_lbl.AutoSize = True
        Me.cc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cc_lbl.Location = New System.Drawing.Point(354, 31)
        Me.cc_lbl.Name = "cc_lbl"
        Me.cc_lbl.Size = New System.Drawing.Size(81, 13)
        Me.cc_lbl.TabIndex = 21
        Me.cc_lbl.Text = "Cost Center :"
        '
        'act_notif_val
        '
        Me.act_notif_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_notif_val.Location = New System.Drawing.Point(265, 28)
        Me.act_notif_val.Name = "act_notif_val"
        Me.act_notif_val.Size = New System.Drawing.Size(83, 20)
        Me.act_notif_val.TabIndex = 20
        '
        'act_notif_lbl
        '
        Me.act_notif_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_notif_lbl.AutoSize = True
        Me.act_notif_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_notif_lbl.Location = New System.Drawing.Point(167, 31)
        Me.act_notif_lbl.Name = "act_notif_lbl"
        Me.act_notif_lbl.Size = New System.Drawing.Size(46, 13)
        Me.act_notif_lbl.TabIndex = 19
        Me.act_notif_lbl.Text = "Notif. :"
        '
        'act_jc_val
        '
        Me.act_jc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_jc_val.Location = New System.Drawing.Point(484, 3)
        Me.act_jc_val.Name = "act_jc_val"
        Me.act_jc_val.Size = New System.Drawing.Size(87, 20)
        Me.act_jc_val.TabIndex = 17
        '
        'Label31
        '
        Me.Label31.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(354, 6)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(65, 13)
        Me.Label31.TabIndex = 16
        Me.Label31.Text = "Job Card :"
        '
        'act_proj_val
        '
        Me.act_proj_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_proj_val.Location = New System.Drawing.Point(78, 3)
        Me.act_proj_val.Name = "act_proj_val"
        Me.act_proj_val.Size = New System.Drawing.Size(83, 20)
        Me.act_proj_val.TabIndex = 15
        '
        'act_just_gain_lbl
        '
        Me.act_just_gain_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_just_gain_lbl.AutoSize = True
        Me.act_just_gain_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_just_gain_lbl.Location = New System.Drawing.Point(167, 106)
        Me.act_just_gain_lbl.Name = "act_just_gain_lbl"
        Me.act_just_gain_lbl.Size = New System.Drawing.Size(92, 13)
        Me.act_just_gain_lbl.TabIndex = 8
        Me.act_just_gain_lbl.Text = "Justified Gain :"
        '
        'act_devition_val
        '
        Me.act_devition_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_devition_val.Location = New System.Drawing.Point(78, 103)
        Me.act_devition_val.Name = "act_devition_val"
        Me.act_devition_val.Size = New System.Drawing.Size(83, 20)
        Me.act_devition_val.TabIndex = 4
        '
        'act_devition_lbl
        '
        Me.act_devition_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_devition_lbl.AutoSize = True
        Me.act_devition_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_devition_lbl.Location = New System.Drawing.Point(3, 106)
        Me.act_devition_lbl.Name = "act_devition_lbl"
        Me.act_devition_lbl.Size = New System.Drawing.Size(69, 13)
        Me.act_devition_lbl.TabIndex = 9
        Me.act_devition_lbl.Text = "Deviation :"
        '
        'act_budget_sent_val
        '
        Me.act_budget_sent_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_sent_val.Location = New System.Drawing.Point(265, 53)
        Me.act_budget_sent_val.Name = "act_budget_sent_val"
        Me.act_budget_sent_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_sent_val.TabIndex = 8
        '
        'act_budget_sent_lbl
        '
        Me.act_budget_sent_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_sent_lbl.AutoSize = True
        Me.act_budget_sent_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_sent_lbl.Location = New System.Drawing.Point(167, 56)
        Me.act_budget_sent_lbl.Name = "act_budget_sent_lbl"
        Me.act_budget_sent_lbl.Size = New System.Drawing.Size(85, 13)
        Me.act_budget_sent_lbl.TabIndex = 13
        Me.act_budget_sent_lbl.Text = "Budget Sent :"
        '
        'act_budget_val
        '
        Me.act_budget_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_val.Location = New System.Drawing.Point(78, 53)
        Me.act_budget_val.Name = "act_budget_val"
        Me.act_budget_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_val.TabIndex = 9
        '
        'act_budget_lbl
        '
        Me.act_budget_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_lbl.AutoSize = True
        Me.act_budget_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_lbl.Location = New System.Drawing.Point(3, 56)
        Me.act_budget_lbl.Name = "act_budget_lbl"
        Me.act_budget_lbl.Size = New System.Drawing.Size(55, 13)
        Me.act_budget_lbl.TabIndex = 5
        Me.act_budget_lbl.Text = "Budget :"
        '
        'act_status_lbl
        '
        Me.act_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_status_lbl.AutoSize = True
        Me.act_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_status_lbl.Location = New System.Drawing.Point(3, 31)
        Me.act_status_lbl.Name = "act_status_lbl"
        Me.act_status_lbl.Size = New System.Drawing.Size(51, 13)
        Me.act_status_lbl.TabIndex = 4
        Me.act_status_lbl.Text = "Status :"
        '
        'act_status_val
        '
        Me.act_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_status_val.Location = New System.Drawing.Point(78, 28)
        Me.act_status_val.Name = "act_status_val"
        Me.act_status_val.Size = New System.Drawing.Size(83, 20)
        Me.act_status_val.TabIndex = 14
        '
        'act_proj_lbl
        '
        Me.act_proj_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_proj_lbl.AutoSize = True
        Me.act_proj_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_proj_lbl.Location = New System.Drawing.Point(3, 6)
        Me.act_proj_lbl.Name = "act_proj_lbl"
        Me.act_proj_lbl.Size = New System.Drawing.Size(55, 13)
        Me.act_proj_lbl.TabIndex = 3
        Me.act_proj_lbl.Text = "Project :"
        '
        'act_activity_lbl
        '
        Me.act_activity_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_activity_lbl.AutoSize = True
        Me.act_activity_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_activity_lbl.Location = New System.Drawing.Point(167, 6)
        Me.act_activity_lbl.Name = "act_activity_lbl"
        Me.act_activity_lbl.Size = New System.Drawing.Size(57, 13)
        Me.act_activity_lbl.TabIndex = 0
        Me.act_activity_lbl.Text = "Activity :"
        '
        'act_activity_val
        '
        Me.act_activity_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_activity_val.Location = New System.Drawing.Point(265, 3)
        Me.act_activity_val.Name = "act_activity_val"
        Me.act_activity_val.Size = New System.Drawing.Size(83, 20)
        Me.act_activity_val.TabIndex = 1
        '
        'act_desc_lbl
        '
        Me.act_desc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_desc_lbl.AutoSize = True
        Me.act_desc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_desc_lbl.Location = New System.Drawing.Point(577, 6)
        Me.act_desc_lbl.Name = "act_desc_lbl"
        Me.act_desc_lbl.Size = New System.Drawing.Size(75, 13)
        Me.act_desc_lbl.TabIndex = 18
        Me.act_desc_lbl.Text = "Act. Desc. :"
        '
        'act_desc_val
        '
        Me.act_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_desc_val.Location = New System.Drawing.Point(714, 3)
        Me.act_desc_val.Name = "act_desc_val"
        Me.act_desc_val.Size = New System.Drawing.Size(201, 20)
        Me.act_desc_val.TabIndex = 12
        '
        'cc_desc_val
        '
        Me.cc_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cc_desc_val.Location = New System.Drawing.Point(714, 28)
        Me.cc_desc_val.Name = "cc_desc_val"
        Me.cc_desc_val.Size = New System.Drawing.Size(201, 20)
        Me.cc_desc_val.TabIndex = 23
        '
        'act_budget_received_lbl
        '
        Me.act_budget_received_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_received_lbl.AutoSize = True
        Me.act_budget_received_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_received_lbl.Location = New System.Drawing.Point(354, 56)
        Me.act_budget_received_lbl.Name = "act_budget_received_lbl"
        Me.act_budget_received_lbl.Size = New System.Drawing.Size(103, 13)
        Me.act_budget_received_lbl.TabIndex = 10
        Me.act_budget_received_lbl.Text = "Budget Receiv. :"
        '
        'act_budget_received_val
        '
        Me.act_budget_received_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_received_val.Location = New System.Drawing.Point(484, 53)
        Me.act_budget_received_val.Name = "act_budget_received_val"
        Me.act_budget_received_val.Size = New System.Drawing.Size(87, 20)
        Me.act_budget_received_val.TabIndex = 4
        '
        'act_budget_adj_lbl
        '
        Me.act_budget_adj_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_adj_lbl.AutoSize = True
        Me.act_budget_adj_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_adj_lbl.Location = New System.Drawing.Point(577, 56)
        Me.act_budget_adj_lbl.Name = "act_budget_adj_lbl"
        Me.act_budget_adj_lbl.Size = New System.Drawing.Size(108, 13)
        Me.act_budget_adj_lbl.TabIndex = 11
        Me.act_budget_adj_lbl.Text = "Budget Adjusted :"
        '
        'act_budget_adj_val
        '
        Me.act_budget_adj_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_adj_val.Location = New System.Drawing.Point(714, 53)
        Me.act_budget_adj_val.Name = "act_budget_adj_val"
        Me.act_budget_adj_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_adj_val.TabIndex = 5
        '
        'act_actl_sent_val
        '
        Me.act_actl_sent_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_actl_sent_val.Location = New System.Drawing.Point(265, 78)
        Me.act_actl_sent_val.Name = "act_actl_sent_val"
        Me.act_actl_sent_val.Size = New System.Drawing.Size(83, 20)
        Me.act_actl_sent_val.TabIndex = 2
        '
        'act_details_other_page
        '
        Me.act_details_other_page.Controls.Add(Me.product_details_lay)
        Me.act_details_other_page.Location = New System.Drawing.Point(4, 22)
        Me.act_details_other_page.Name = "act_details_other_page"
        Me.act_details_other_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_details_other_page.Size = New System.Drawing.Size(1110, 166)
        Me.act_details_other_page.TabIndex = 1
        Me.act_details_other_page.Text = "Others"
        Me.act_details_other_page.UseVisualStyleBackColor = True
        '
        'product_details_lay
        '
        Me.product_details_lay.ColumnCount = 10
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_appr_lbl, 6, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_pmgo_val, 5, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_pmgo_lbl, 4, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_sent_val, 3, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_sent_lbl, 2, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_blw_thr_val, 5, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_blw_thr_lbl, 4, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_init_val, 1, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_inwk_lbl, 0, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_inwk_val, 1, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_init_lbl, 0, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_init_foc_lbl, 2, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_init_foc_val, 3, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_total_lbl, 6, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_total_val, 7, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_awq_appr_val, 7, 1)
        Me.product_details_lay.Controls.Add(Me.act_budget_add_foc, 8, 0)
        Me.product_details_lay.Controls.Add(Me.act_budget_add_foc_val, 9, 0)
        Me.product_details_lay.Location = New System.Drawing.Point(3, 3)
        Me.product_details_lay.Name = "product_details_lay"
        Me.product_details_lay.RowCount = 5
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.Size = New System.Drawing.Size(1086, 123)
        Me.product_details_lay.TabIndex = 1
        '
        'act_budget_add_foc_val
        '
        Me.act_budget_add_foc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_add_foc_val.Location = New System.Drawing.Point(1006, 3)
        Me.act_budget_add_foc_val.Name = "act_budget_add_foc_val"
        Me.act_budget_add_foc_val.Size = New System.Drawing.Size(91, 20)
        Me.act_budget_add_foc_val.TabIndex = 14
        '
        'act_budget_add_foc
        '
        Me.act_budget_add_foc.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_add_foc.AutoSize = True
        Me.act_budget_add_foc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_add_foc.Location = New System.Drawing.Point(887, 5)
        Me.act_budget_add_foc.Name = "act_budget_add_foc"
        Me.act_budget_add_foc.Size = New System.Drawing.Size(113, 13)
        Me.act_budget_add_foc.TabIndex = 20
        Me.act_budget_add_foc.Text = "Budget Add. FOC :"
        '
        'act_budget_awq_appr_lbl
        '
        Me.act_budget_awq_appr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_appr_lbl.AutoSize = True
        Me.act_budget_awq_appr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_awq_appr_lbl.Location = New System.Drawing.Point(675, 29)
        Me.act_budget_awq_appr_lbl.Name = "act_budget_awq_appr_lbl"
        Me.act_budget_awq_appr_lbl.Size = New System.Drawing.Size(122, 13)
        Me.act_budget_awq_appr_lbl.TabIndex = 24
        Me.act_budget_awq_appr_lbl.Text = "Budget AWQ Appr. :"
        '
        'act_budget_awq_pmgo_val
        '
        Me.act_budget_awq_pmgo_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_pmgo_val.Location = New System.Drawing.Point(592, 27)
        Me.act_budget_awq_pmgo_val.Name = "act_budget_awq_pmgo_val"
        Me.act_budget_awq_pmgo_val.Size = New System.Drawing.Size(77, 20)
        Me.act_budget_awq_pmgo_val.TabIndex = 22
        '
        'act_budget_awq_pmgo_lbl
        '
        Me.act_budget_awq_pmgo_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_pmgo_lbl.AutoSize = True
        Me.act_budget_awq_pmgo_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_awq_pmgo_lbl.Location = New System.Drawing.Point(456, 29)
        Me.act_budget_awq_pmgo_lbl.Name = "act_budget_awq_pmgo_lbl"
        Me.act_budget_awq_pmgo_lbl.Size = New System.Drawing.Size(130, 13)
        Me.act_budget_awq_pmgo_lbl.TabIndex = 21
        Me.act_budget_awq_pmgo_lbl.Text = "Budget AWQ PM Go :"
        '
        'act_budget_awq_sent_val
        '
        Me.act_budget_awq_sent_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_sent_val.Location = New System.Drawing.Point(367, 27)
        Me.act_budget_awq_sent_val.Name = "act_budget_awq_sent_val"
        Me.act_budget_awq_sent_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_awq_sent_val.TabIndex = 20
        '
        'act_budget_awq_sent_lbl
        '
        Me.act_budget_awq_sent_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_sent_lbl.AutoSize = True
        Me.act_budget_awq_sent_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_awq_sent_lbl.Location = New System.Drawing.Point(223, 29)
        Me.act_budget_awq_sent_lbl.Name = "act_budget_awq_sent_lbl"
        Me.act_budget_awq_sent_lbl.Size = New System.Drawing.Size(138, 13)
        Me.act_budget_awq_sent_lbl.TabIndex = 19
        Me.act_budget_awq_sent_lbl.Text = "Budget AWQ Pending :"
        '
        'act_budget_blw_thr_val
        '
        Me.act_budget_blw_thr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_blw_thr_val.Location = New System.Drawing.Point(592, 3)
        Me.act_budget_blw_thr_val.Name = "act_budget_blw_thr_val"
        Me.act_budget_blw_thr_val.Size = New System.Drawing.Size(77, 20)
        Me.act_budget_blw_thr_val.TabIndex = 17
        '
        'act_budget_blw_thr_lbl
        '
        Me.act_budget_blw_thr_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_blw_thr_lbl.AutoSize = True
        Me.act_budget_blw_thr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_blw_thr_lbl.Location = New System.Drawing.Point(456, 5)
        Me.act_budget_blw_thr_lbl.Name = "act_budget_blw_thr_lbl"
        Me.act_budget_blw_thr_lbl.Size = New System.Drawing.Size(110, 13)
        Me.act_budget_blw_thr_lbl.TabIndex = 16
        Me.act_budget_blw_thr_lbl.Text = "Budget Blw. Thr. :"
        '
        'act_budget_init_val
        '
        Me.act_budget_init_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_init_val.Location = New System.Drawing.Point(134, 3)
        Me.act_budget_init_val.Name = "act_budget_init_val"
        Me.act_budget_init_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_init_val.TabIndex = 15
        '
        'act_budget_awq_inwk_lbl
        '
        Me.act_budget_awq_inwk_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_inwk_lbl.AutoSize = True
        Me.act_budget_awq_inwk_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_awq_inwk_lbl.Location = New System.Drawing.Point(3, 29)
        Me.act_budget_awq_inwk_lbl.Name = "act_budget_awq_inwk_lbl"
        Me.act_budget_awq_inwk_lbl.Size = New System.Drawing.Size(125, 13)
        Me.act_budget_awq_inwk_lbl.TabIndex = 4
        Me.act_budget_awq_inwk_lbl.Text = "Budget AWQ INWK :"
        '
        'act_budget_awq_inwk_val
        '
        Me.act_budget_awq_inwk_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_inwk_val.Location = New System.Drawing.Point(134, 27)
        Me.act_budget_awq_inwk_val.Name = "act_budget_awq_inwk_val"
        Me.act_budget_awq_inwk_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_awq_inwk_val.TabIndex = 14
        '
        'act_budget_init_lbl
        '
        Me.act_budget_init_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_init_lbl.AutoSize = True
        Me.act_budget_init_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_init_lbl.Location = New System.Drawing.Point(3, 5)
        Me.act_budget_init_lbl.Name = "act_budget_init_lbl"
        Me.act_budget_init_lbl.Size = New System.Drawing.Size(81, 13)
        Me.act_budget_init_lbl.TabIndex = 3
        Me.act_budget_init_lbl.Text = "Budget Init. :"
        '
        'act_budget_init_foc_lbl
        '
        Me.act_budget_init_foc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_init_foc_lbl.AutoSize = True
        Me.act_budget_init_foc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_init_foc_lbl.Location = New System.Drawing.Point(223, 5)
        Me.act_budget_init_foc_lbl.Name = "act_budget_init_foc_lbl"
        Me.act_budget_init_foc_lbl.Size = New System.Drawing.Size(109, 13)
        Me.act_budget_init_foc_lbl.TabIndex = 0
        Me.act_budget_init_foc_lbl.Text = "Budget Init. FOC :"
        '
        'act_budget_init_foc_val
        '
        Me.act_budget_init_foc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_init_foc_val.Location = New System.Drawing.Point(367, 3)
        Me.act_budget_init_foc_val.Name = "act_budget_init_foc_val"
        Me.act_budget_init_foc_val.Size = New System.Drawing.Size(83, 20)
        Me.act_budget_init_foc_val.TabIndex = 1
        '
        'act_budget_awq_total_lbl
        '
        Me.act_budget_awq_total_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_total_lbl.AutoSize = True
        Me.act_budget_awq_total_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_budget_awq_total_lbl.Location = New System.Drawing.Point(675, 5)
        Me.act_budget_awq_total_lbl.Name = "act_budget_awq_total_lbl"
        Me.act_budget_awq_total_lbl.Size = New System.Drawing.Size(121, 13)
        Me.act_budget_awq_total_lbl.TabIndex = 18
        Me.act_budget_awq_total_lbl.Text = "Budget AWQ Total :"
        '
        'act_budget_awq_total_val
        '
        Me.act_budget_awq_total_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_total_val.Location = New System.Drawing.Point(803, 3)
        Me.act_budget_awq_total_val.Name = "act_budget_awq_total_val"
        Me.act_budget_awq_total_val.Size = New System.Drawing.Size(78, 20)
        Me.act_budget_awq_total_val.TabIndex = 12
        '
        'act_budget_awq_appr_val
        '
        Me.act_budget_awq_appr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_budget_awq_appr_val.Location = New System.Drawing.Point(803, 27)
        Me.act_budget_awq_appr_val.Name = "act_budget_awq_appr_val"
        Me.act_budget_awq_appr_val.Size = New System.Drawing.Size(78, 20)
        Me.act_budget_awq_appr_val.TabIndex = 23
        '
        'select_activity_grp
        '
        Me.select_activity_grp.Controls.Add(Me.act_cc_error_state_pic)
        Me.select_activity_grp.Controls.Add(Me.act_cc_select_right_pic)
        Me.select_activity_grp.Controls.Add(Me.act_cc_select_left_pic)
        Me.select_activity_grp.Controls.Add(Me.cb_act_cc)
        Me.select_activity_grp.Dock = System.Windows.Forms.DockStyle.Top
        Me.select_activity_grp.Location = New System.Drawing.Point(0, 0)
        Me.select_activity_grp.Margin = New System.Windows.Forms.Padding(5, 5, 5, 6)
        Me.select_activity_grp.Name = "select_activity_grp"
        Me.select_activity_grp.Padding = New System.Windows.Forms.Padding(5)
        Me.select_activity_grp.Size = New System.Drawing.Size(1132, 59)
        Me.select_activity_grp.TabIndex = 0
        Me.select_activity_grp.TabStop = False
        Me.select_activity_grp.Text = "Select Activity - Cost Center"
        '
        'act_cc_error_state_pic
        '
        Me.act_cc_error_state_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.act_cc_error_state_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.no_errors
        Me.act_cc_error_state_pic.Location = New System.Drawing.Point(5, 13)
        Me.act_cc_error_state_pic.Name = "act_cc_error_state_pic"
        Me.act_cc_error_state_pic.Size = New System.Drawing.Size(36, 32)
        Me.act_cc_error_state_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.act_cc_error_state_pic.TabIndex = 4
        Me.act_cc_error_state_pic.TabStop = False
        Me.act_cc_error_state_pic.Tag = ""
        '
        'act_cc_select_right_pic
        '
        Me.act_cc_select_right_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.act_cc_select_right_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.act_cc_select_right_pic.Location = New System.Drawing.Point(1048, 13)
        Me.act_cc_select_right_pic.Name = "act_cc_select_right_pic"
        Me.act_cc_select_right_pic.Size = New System.Drawing.Size(36, 32)
        Me.act_cc_select_right_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.act_cc_select_right_pic.TabIndex = 2
        Me.act_cc_select_right_pic.TabStop = False
        '
        'act_cc_select_left_pic
        '
        Me.act_cc_select_left_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.act_cc_select_left_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.act_cc_select_left_pic.Location = New System.Drawing.Point(47, 13)
        Me.act_cc_select_left_pic.Name = "act_cc_select_left_pic"
        Me.act_cc_select_left_pic.Size = New System.Drawing.Size(36, 32)
        Me.act_cc_select_left_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.act_cc_select_left_pic.TabIndex = 1
        Me.act_cc_select_left_pic.TabStop = False
        '
        'cb_act_cc
        '
        Me.cb_act_cc.FormattingEnabled = True
        Me.cb_act_cc.Location = New System.Drawing.Point(88, 21)
        Me.cb_act_cc.Name = "cb_act_cc"
        Me.cb_act_cc.Size = New System.Drawing.Size(942, 21)
        Me.cb_act_cc.TabIndex = 0
        '
        'ctrl_operations_panel
        '
        Me.ctrl_operations_panel.AutoScroll = True
        Me.ctrl_operations_panel.Controls.Add(Me.ctrl_operations_tab)
        Me.ctrl_operations_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ctrl_operations_panel.Location = New System.Drawing.Point(5, 18)
        Me.ctrl_operations_panel.Name = "ctrl_operations_panel"
        Me.ctrl_operations_panel.Size = New System.Drawing.Size(1122, 604)
        Me.ctrl_operations_panel.TabIndex = 2
        '
        'ctrl_operations_tab
        '
        Me.ctrl_operations_tab.Controls.Add(Me.ctrl_op_deviation_just_page)
        Me.ctrl_operations_tab.Controls.Add(Me.ctrl_op_budget_transfer_page)
        Me.ctrl_operations_tab.Controls.Add(Me.ctrl_op_actual_transfer_page)
        Me.ctrl_operations_tab.Controls.Add(Me.ctrl_op_actuals_page)
        Me.ctrl_operations_tab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ctrl_operations_tab.Location = New System.Drawing.Point(0, 0)
        Me.ctrl_operations_tab.Name = "ctrl_operations_tab"
        Me.ctrl_operations_tab.SelectedIndex = 0
        Me.ctrl_operations_tab.Size = New System.Drawing.Size(1122, 604)
        Me.ctrl_operations_tab.TabIndex = 3
        '
        'ctrl_op_deviation_just_page
        '
        Me.ctrl_op_deviation_just_page.Controls.Add(Me.dev_just_savings_grp)
        Me.ctrl_op_deviation_just_page.Controls.Add(Me.dev_just_overshoot_grp)
        Me.ctrl_op_deviation_just_page.Location = New System.Drawing.Point(4, 22)
        Me.ctrl_op_deviation_just_page.Name = "ctrl_op_deviation_just_page"
        Me.ctrl_op_deviation_just_page.Padding = New System.Windows.Forms.Padding(3)
        Me.ctrl_op_deviation_just_page.Size = New System.Drawing.Size(1114, 578)
        Me.ctrl_op_deviation_just_page.TabIndex = 0
        Me.ctrl_op_deviation_just_page.Text = "Deviation Justifications"
        Me.ctrl_op_deviation_just_page.UseVisualStyleBackColor = True
        '
        'dev_just_savings_grp
        '
        Me.dev_just_savings_grp.AutoSize = True
        Me.dev_just_savings_grp.Controls.Add(Me.dev_just_savings_dgrid)
        Me.dev_just_savings_grp.Location = New System.Drawing.Point(22, 289)
        Me.dev_just_savings_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.dev_just_savings_grp.Name = "dev_just_savings_grp"
        Me.dev_just_savings_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.dev_just_savings_grp.Size = New System.Drawing.Size(835, 255)
        Me.dev_just_savings_grp.TabIndex = 2
        Me.dev_just_savings_grp.TabStop = False
        Me.dev_just_savings_grp.Text = "Savings Justifications"
        '
        'dev_just_savings_dgrid
        '
        Me.dev_just_savings_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dev_just_savings_dgrid.Location = New System.Drawing.Point(5, 27)
        Me.dev_just_savings_dgrid.Name = "dev_just_savings_dgrid"
        Me.dev_just_savings_dgrid.Size = New System.Drawing.Size(821, 210)
        Me.dev_just_savings_dgrid.TabIndex = 0
        '
        'dev_just_overshoot_grp
        '
        Me.dev_just_overshoot_grp.AutoSize = True
        Me.dev_just_overshoot_grp.Controls.Add(Me.dev_just_overshoot_dgrid)
        Me.dev_just_overshoot_grp.Location = New System.Drawing.Point(22, 19)
        Me.dev_just_overshoot_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.dev_just_overshoot_grp.Name = "dev_just_overshoot_grp"
        Me.dev_just_overshoot_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.dev_just_overshoot_grp.Size = New System.Drawing.Size(847, 261)
        Me.dev_just_overshoot_grp.TabIndex = 1
        Me.dev_just_overshoot_grp.TabStop = False
        Me.dev_just_overshoot_grp.Text = "Overshoot Justifications"
        '
        'dev_just_overshoot_dgrid
        '
        Me.dev_just_overshoot_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dev_just_overshoot_dgrid.Location = New System.Drawing.Point(5, 29)
        Me.dev_just_overshoot_dgrid.Name = "dev_just_overshoot_dgrid"
        Me.dev_just_overshoot_dgrid.Size = New System.Drawing.Size(837, 214)
        Me.dev_just_overshoot_dgrid.TabIndex = 0
        '
        'ctrl_op_budget_transfer_page
        '
        Me.ctrl_op_budget_transfer_page.Controls.Add(Me.budget_received_grp)
        Me.ctrl_op_budget_transfer_page.Controls.Add(Me.budget_transfer_to_grp)
        Me.ctrl_op_budget_transfer_page.Location = New System.Drawing.Point(4, 22)
        Me.ctrl_op_budget_transfer_page.Name = "ctrl_op_budget_transfer_page"
        Me.ctrl_op_budget_transfer_page.Padding = New System.Windows.Forms.Padding(3)
        Me.ctrl_op_budget_transfer_page.Size = New System.Drawing.Size(1114, 578)
        Me.ctrl_op_budget_transfer_page.TabIndex = 1
        Me.ctrl_op_budget_transfer_page.Text = "Budget Transfers"
        Me.ctrl_op_budget_transfer_page.UseVisualStyleBackColor = True
        '
        'budget_received_grp
        '
        Me.budget_received_grp.AutoSize = True
        Me.budget_received_grp.Controls.Add(Me.budget_received_dgrid)
        Me.budget_received_grp.Location = New System.Drawing.Point(18, 296)
        Me.budget_received_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.budget_received_grp.Name = "budget_received_grp"
        Me.budget_received_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.budget_received_grp.Size = New System.Drawing.Size(835, 265)
        Me.budget_received_grp.TabIndex = 4
        Me.budget_received_grp.TabStop = False
        Me.budget_received_grp.Text = "Budget Received From Others Controlling Object"
        '
        'budget_received_dgrid
        '
        Me.budget_received_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.budget_received_dgrid.Location = New System.Drawing.Point(5, 27)
        Me.budget_received_dgrid.Name = "budget_received_dgrid"
        Me.budget_received_dgrid.Size = New System.Drawing.Size(821, 220)
        Me.budget_received_dgrid.TabIndex = 0
        '
        'budget_transfer_to_grp
        '
        Me.budget_transfer_to_grp.AutoSize = True
        Me.budget_transfer_to_grp.Controls.Add(Me.budget_transfer_to_dgrid)
        Me.budget_transfer_to_grp.Location = New System.Drawing.Point(18, 18)
        Me.budget_transfer_to_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.budget_transfer_to_grp.Name = "budget_transfer_to_grp"
        Me.budget_transfer_to_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.budget_transfer_to_grp.Size = New System.Drawing.Size(835, 248)
        Me.budget_transfer_to_grp.TabIndex = 3
        Me.budget_transfer_to_grp.TabStop = False
        Me.budget_transfer_to_grp.Text = "Budget Transfers To Others Controlling Object"
        '
        'budget_transfer_to_dgrid
        '
        Me.budget_transfer_to_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.budget_transfer_to_dgrid.Location = New System.Drawing.Point(5, 29)
        Me.budget_transfer_to_dgrid.Name = "budget_transfer_to_dgrid"
        Me.budget_transfer_to_dgrid.Size = New System.Drawing.Size(821, 201)
        Me.budget_transfer_to_dgrid.TabIndex = 0
        '
        'ctrl_op_actual_transfer_page
        '
        Me.ctrl_op_actual_transfer_page.Controls.Add(Me.actual_received_grp)
        Me.ctrl_op_actual_transfer_page.Controls.Add(Me.actual_transfer_to_grp)
        Me.ctrl_op_actual_transfer_page.Location = New System.Drawing.Point(4, 22)
        Me.ctrl_op_actual_transfer_page.Margin = New System.Windows.Forms.Padding(2)
        Me.ctrl_op_actual_transfer_page.Name = "ctrl_op_actual_transfer_page"
        Me.ctrl_op_actual_transfer_page.Padding = New System.Windows.Forms.Padding(2)
        Me.ctrl_op_actual_transfer_page.Size = New System.Drawing.Size(1114, 578)
        Me.ctrl_op_actual_transfer_page.TabIndex = 3
        Me.ctrl_op_actual_transfer_page.Text = "Actual Hours Transfers"
        Me.ctrl_op_actual_transfer_page.UseVisualStyleBackColor = True
        '
        'actual_received_grp
        '
        Me.actual_received_grp.AutoSize = True
        Me.actual_received_grp.Controls.Add(Me.actual_received_dgrid)
        Me.actual_received_grp.Location = New System.Drawing.Point(18, 304)
        Me.actual_received_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.actual_received_grp.Name = "actual_received_grp"
        Me.actual_received_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.actual_received_grp.Size = New System.Drawing.Size(835, 265)
        Me.actual_received_grp.TabIndex = 6
        Me.actual_received_grp.TabStop = False
        Me.actual_received_grp.Text = "Actual Received From Others Controlling Object"
        '
        'actual_received_dgrid
        '
        Me.actual_received_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.actual_received_dgrid.Location = New System.Drawing.Point(5, 27)
        Me.actual_received_dgrid.Name = "actual_received_dgrid"
        Me.actual_received_dgrid.Size = New System.Drawing.Size(821, 220)
        Me.actual_received_dgrid.TabIndex = 0
        '
        'actual_transfer_to_grp
        '
        Me.actual_transfer_to_grp.AutoSize = True
        Me.actual_transfer_to_grp.Controls.Add(Me.actual_transfer_to_dgrid)
        Me.actual_transfer_to_grp.Location = New System.Drawing.Point(18, 26)
        Me.actual_transfer_to_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.actual_transfer_to_grp.Name = "actual_transfer_to_grp"
        Me.actual_transfer_to_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.actual_transfer_to_grp.Size = New System.Drawing.Size(835, 248)
        Me.actual_transfer_to_grp.TabIndex = 5
        Me.actual_transfer_to_grp.TabStop = False
        Me.actual_transfer_to_grp.Text = "Actual Hour Transfers To Others Controlling Object"
        '
        'actual_transfer_to_dgrid
        '
        Me.actual_transfer_to_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.actual_transfer_to_dgrid.Location = New System.Drawing.Point(5, 29)
        Me.actual_transfer_to_dgrid.Name = "actual_transfer_to_dgrid"
        Me.actual_transfer_to_dgrid.Size = New System.Drawing.Size(821, 201)
        Me.actual_transfer_to_dgrid.TabIndex = 0
        '
        'ctrl_op_actuals_page
        '
        Me.ctrl_op_actuals_page.Controls.Add(Me.cij3_booking_grp)
        Me.ctrl_op_actuals_page.Location = New System.Drawing.Point(4, 22)
        Me.ctrl_op_actuals_page.Name = "ctrl_op_actuals_page"
        Me.ctrl_op_actuals_page.Padding = New System.Windows.Forms.Padding(3)
        Me.ctrl_op_actuals_page.Size = New System.Drawing.Size(1114, 578)
        Me.ctrl_op_actuals_page.TabIndex = 2
        Me.ctrl_op_actuals_page.Text = "CJI3 Actual Hours"
        Me.ctrl_op_actuals_page.UseVisualStyleBackColor = True
        '
        'cij3_booking_grp
        '
        Me.cij3_booking_grp.AutoSize = True
        Me.cij3_booking_grp.Controls.Add(Me.cij3_booking_dgrid)
        Me.cij3_booking_grp.Location = New System.Drawing.Point(13, 15)
        Me.cij3_booking_grp.Margin = New System.Windows.Forms.Padding(2)
        Me.cij3_booking_grp.Name = "cij3_booking_grp"
        Me.cij3_booking_grp.Padding = New System.Windows.Forms.Padding(2)
        Me.cij3_booking_grp.Size = New System.Drawing.Size(923, 575)
        Me.cij3_booking_grp.TabIndex = 4
        Me.cij3_booking_grp.TabStop = False
        Me.cij3_booking_grp.Text = "CJI3 Bookings"
        '
        'cij3_booking_dgrid
        '
        Me.cij3_booking_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.cij3_booking_dgrid.Location = New System.Drawing.Point(5, 29)
        Me.cij3_booking_dgrid.Name = "cij3_booking_dgrid"
        Me.cij3_booking_dgrid.Size = New System.Drawing.Size(913, 528)
        Me.cij3_booking_dgrid.TabIndex = 0
        '
        'controling_operation_grp
        '
        Me.controling_operation_grp.Controls.Add(Me.ctrl_operations_panel)
        Me.controling_operation_grp.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.controling_operation_grp.Location = New System.Drawing.Point(0, 276)
        Me.controling_operation_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.controling_operation_grp.Name = "controling_operation_grp"
        Me.controling_operation_grp.Padding = New System.Windows.Forms.Padding(5)
        Me.controling_operation_grp.Size = New System.Drawing.Size(1132, 627)
        Me.controling_operation_grp.TabIndex = 3
        Me.controling_operation_grp.TabStop = False
        Me.controling_operation_grp.Text = "Controlling Operations"
        '
        'main_panel
        '
        Me.main_panel.AutoScroll = True
        Me.main_panel.Controls.Add(Me.top_panel_activity)
        Me.main_panel.Controls.Add(Me.controling_operation_grp)
        Me.main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.main_panel.Location = New System.Drawing.Point(3, 3)
        Me.main_panel.Margin = New System.Windows.Forms.Padding(2)
        Me.main_panel.Name = "main_panel"
        Me.main_panel.Size = New System.Drawing.Size(1149, 676)
        Me.main_panel.TabIndex = 7
        '
        'FormLaborControlEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.ClientSize = New System.Drawing.Size(1155, 682)
        Me.Controls.Add(Me.main_panel)
        Me.Name = "FormLaborControlEdit"
        Me.Padding = New System.Windows.Forms.Padding(3)
        Me.Text = "Edit Labor Controlling Data"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.top_panel_activity.ResumeLayout(False)
        Me.activity_info_grp.ResumeLayout(False)
        Me.activity_details_tab.ResumeLayout(False)
        Me.act_details_general_page.ResumeLayout(False)
        Me.act_details_lay.ResumeLayout(False)
        Me.act_details_lay.PerformLayout()
        Me.act_details_other_page.ResumeLayout(False)
        Me.product_details_lay.ResumeLayout(False)
        Me.product_details_lay.PerformLayout()
        Me.select_activity_grp.ResumeLayout(False)
        CType(Me.act_cc_error_state_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.act_cc_select_right_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.act_cc_select_left_pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctrl_operations_panel.ResumeLayout(False)
        Me.ctrl_operations_tab.ResumeLayout(False)
        Me.ctrl_op_deviation_just_page.ResumeLayout(False)
        Me.ctrl_op_deviation_just_page.PerformLayout()
        Me.dev_just_savings_grp.ResumeLayout(False)
        CType(Me.dev_just_savings_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.dev_just_overshoot_grp.ResumeLayout(False)
        CType(Me.dev_just_overshoot_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctrl_op_budget_transfer_page.ResumeLayout(False)
        Me.ctrl_op_budget_transfer_page.PerformLayout()
        Me.budget_received_grp.ResumeLayout(False)
        CType(Me.budget_received_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.budget_transfer_to_grp.ResumeLayout(False)
        CType(Me.budget_transfer_to_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctrl_op_actual_transfer_page.ResumeLayout(False)
        Me.ctrl_op_actual_transfer_page.PerformLayout()
        Me.actual_received_grp.ResumeLayout(False)
        CType(Me.actual_received_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.actual_transfer_to_grp.ResumeLayout(False)
        CType(Me.actual_transfer_to_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ctrl_op_actuals_page.ResumeLayout(False)
        Me.ctrl_op_actuals_page.PerformLayout()
        Me.cij3_booking_grp.ResumeLayout(False)
        CType(Me.cij3_booking_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.controling_operation_grp.ResumeLayout(False)
        Me.main_panel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents top_panel_activity As Panel
    Friend WithEvents activity_info_grp As GroupBox
    Friend WithEvents act_details_lay As TableLayoutPanel
    Friend WithEvents act_status_lbl As Label
    Friend WithEvents act_activity_lbl As Label
    Friend WithEvents select_activity_grp As GroupBox
    Friend WithEvents act_cc_select_right_pic As PictureBox
    Friend WithEvents act_cc_select_left_pic As PictureBox
    Friend WithEvents cb_act_cc As ComboBox
    Friend WithEvents act_budget_lbl As Label
    Friend WithEvents act_budget_sent_lbl As Label
    Friend WithEvents act_just_gain_lbl As Label
    Friend WithEvents act_budget_received_lbl As Label
    Friend WithEvents act_budget_adj_lbl As Label
    Friend WithEvents act_devition_lbl As Label
    Friend WithEvents activity_details_tab As TabControl
    Friend WithEvents act_details_general_page As TabPage
    Friend WithEvents ctrl_operations_panel As Panel
    Friend WithEvents controling_operation_grp As GroupBox
    Friend WithEvents ctrl_operations_tab As TabControl
    Friend WithEvents ctrl_op_deviation_just_page As TabPage
    Friend WithEvents ctrl_op_budget_transfer_page As TabPage
    Friend WithEvents ctrl_op_actuals_page As TabPage
    Friend WithEvents dev_just_overshoot_dgrid As DataGridView
    Friend WithEvents act_activity_val As TextBox
    Friend WithEvents act_actl_sent_val As TextBox
    Friend WithEvents act_devition_val As TextBox
    Friend WithEvents act_budget_received_val As TextBox
    Friend WithEvents act_budget_sent_val As TextBox
    Friend WithEvents act_budget_val As TextBox
    Friend WithEvents act_desc_val As TextBox
    Friend WithEvents act_status_val As TextBox
    Friend WithEvents act_budget_adj_val As TextBox
    Friend WithEvents act_cc_error_state_pic As PictureBox
    Friend WithEvents act_proj_lbl As Label
    Friend WithEvents main_panel As Panel
    Friend WithEvents act_jc_val As TextBox
    Friend WithEvents Label31 As Label
    Friend WithEvents act_proj_val As TextBox
    Friend WithEvents act_desc_lbl As Label
    Friend WithEvents cc_desc_lbl As Label
    Friend WithEvents cc_val As TextBox
    Friend WithEvents cc_lbl As Label
    Friend WithEvents act_notif_val As TextBox
    Friend WithEvents act_notif_lbl As Label
    Friend WithEvents cc_desc_val As TextBox
    Friend WithEvents act_actl_adjusted_lbl As Label
    Friend WithEvents act_actl_received_lbl As Label
    Friend WithEvents act_actl_received_val As TextBox
    Friend WithEvents act_actl_adjusted_val As TextBox
    Friend WithEvents act_actl_sent_lbl As Label
    Friend WithEvents act_actual_val As TextBox
    Friend WithEvents act_actual_lbl As Label
    Friend WithEvents act_just_overshoot_lbl As Label
    Friend WithEvents act_dev_after_just_lbl As Label
    Friend WithEvents act_just_gain_val As TextBox
    Friend WithEvents act_just_overshoot_val As TextBox
    Friend WithEvents act_dev_after_just_val As TextBox
    Friend WithEvents act_details_other_page As TabPage
    Friend WithEvents product_details_lay As TableLayoutPanel
    Friend WithEvents act_budget_awq_appr_lbl As Label
    Friend WithEvents act_budget_awq_pmgo_val As TextBox
    Friend WithEvents act_budget_awq_pmgo_lbl As Label
    Friend WithEvents act_budget_awq_sent_val As TextBox
    Friend WithEvents act_budget_awq_sent_lbl As Label
    Friend WithEvents act_budget_blw_thr_val As TextBox
    Friend WithEvents act_budget_blw_thr_lbl As Label
    Friend WithEvents act_budget_init_val As TextBox
    Friend WithEvents act_budget_awq_inwk_lbl As Label
    Friend WithEvents act_budget_awq_inwk_val As TextBox
    Friend WithEvents act_budget_init_lbl As Label
    Friend WithEvents act_budget_init_foc_lbl As Label
    Friend WithEvents act_budget_init_foc_val As TextBox
    Friend WithEvents act_budget_awq_total_lbl As Label
    Friend WithEvents act_budget_awq_total_val As TextBox
    Friend WithEvents act_budget_awq_appr_val As TextBox
    Friend WithEvents dev_just_savings_grp As GroupBox
    Friend WithEvents dev_just_savings_dgrid As DataGridView
    Friend WithEvents dev_just_overshoot_grp As GroupBox
    Friend WithEvents budget_received_grp As GroupBox
    Friend WithEvents budget_received_dgrid As DataGridView
    Friend WithEvents budget_transfer_to_grp As GroupBox
    Friend WithEvents budget_transfer_to_dgrid As DataGridView
    Friend WithEvents cij3_booking_grp As GroupBox
    Friend WithEvents cij3_booking_dgrid As DataGridView
    Friend WithEvents ctrl_op_actual_transfer_page As TabPage
    Friend WithEvents actual_received_grp As GroupBox
    Friend WithEvents actual_received_dgrid As DataGridView
    Friend WithEvents actual_transfer_to_grp As GroupBox
    Friend WithEvents actual_transfer_to_dgrid As DataGridView
    Friend WithEvents act_budget_add_foc_val As TextBox
    Friend WithEvents act_budget_add_foc As Label
End Class
