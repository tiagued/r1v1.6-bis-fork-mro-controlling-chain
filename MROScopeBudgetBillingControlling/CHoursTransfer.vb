﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CHoursTransfer
    Implements INotifyPropertyChanged, IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    Public Shared currentMaxID As Integer
    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_TRANSFER_HRS_LOADED Then
            _from_act_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _to_act_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
            _from_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _to_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _from_cc_obj = MConstants.listOfCC(_from_cc)
            _to_cc_obj = MConstants.listOfCC(_to_cc)
            _scope = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        End If
    End Sub

    Public Sub setID()
        If MConstants.GLOB_TRANSFER_HRS_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub

    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_HOURS_TRANSFER_DB_READ
    End Function

    Public Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged
    Private Sub NotifyPropertyChanged(<CallerMemberName()> Optional ByVal propertyName As String = Nothing)
        RaiseEvent PropertyChanged(Me, New PropertyChangedEventArgs(propertyName))
    End Sub

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Dim res As Boolean = True
        Dim mess As String = ""
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res = False
        Dim mess = ""

        Try
            If Not IsNothing(_to_ctrl_obj) AndAlso _to_ctrl_obj.hoursTransfert.Contains(Me) Then
                _to_ctrl_obj.hoursTransfert.Remove(Me)
                _to_ctrl_obj.update()
                If _to_ctrl_obj.isZero Then
                    MConstants.GLB_MSTR_CTRL.labor_controlling_controller.unLoadActCCCtrl(_to_ctrl_obj)
                End If
            End If
            If Not IsNothing(_from_ctrl_obj) AndAlso _from_ctrl_obj.hoursTransfert.Contains(Me) Then
                _from_ctrl_obj.hoursTransfert.Remove(Me)
                _from_ctrl_obj.update()
            End If
            Me.system_status = MConstants.CONST_SYS_ROW_DELETED
            MConstants.GLB_MSTR_CTRL.labor_controlling_controller.deleteObjectHoursTransfer(Me)
            res = True
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message & Chr(10) & ex.StackTrace
        End Try

        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_TRANSFER_HRS_LOADED Then
                    CHoursTransfer.currentMaxID = Math.Max(CHoursTransfer.currentMaxID, _id)
                End If
            End If
        End Set
    End Property


    Private _from_act_id As Long
    Public Property from_act_id() As Long
        Get
            Return _from_act_id
        End Get
        Set(ByVal value As Long)
            If Not _from_act_id = value Then
                _from_act_id = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _to_act_id As Long
    Public Property to_act_id() As Long
        Get
            Return _to_act_id
        End Get
        Set(ByVal value As Long)
            If Not _to_act_id = value Then
                _to_act_id = value
                'fetch contains update
                fetch_to_ctrl_obj()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _to_cc As String
    Public Property to_cc() As String
        Get
            Return _to_cc
        End Get
        Set(ByVal value As String)
            If Not _to_cc = value Then
                _to_cc = value
                If MConstants.listOfCC.ContainsKey(_to_cc) Then
                    Me.to_cc_obj = MConstants.listOfCC(_to_cc)
                Else
                    Me.to_cc_obj = Nothing
                    Throw New Exception("Cost center does not exist in cc list : " & _to_cc)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _to_cc_obj As CCostCenter
    Public Property to_cc_obj() As CCostCenter
        Get
            Return _to_cc_obj
        End Get
        Set(ByVal value As CCostCenter)
            If Not Object.Equals(value, _to_cc_obj) Then
                _to_cc_obj = value
                If IsNothing(_to_cc_obj) Then
                    'use Me.cc to force obj value
                    Me.to_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _to_cc = _to_cc_obj.cc
                End If
                'fetch contains update
                fetch_to_ctrl_obj()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property to_cc_desc As String
        Get
            If Not IsNothing(_to_cc_obj) Then
                Return _to_cc_obj.cc_desc_view
            Else
                Return ""
            End If
        End Get

    End Property
    Private _from_cc As String
    Public Property from_cc() As String
        Get
            Return _from_cc
        End Get
        Set(ByVal value As String)
            If Not _from_cc = value Then
                _from_cc = value
                If MConstants.listOfCC.ContainsKey(_from_cc) Then
                    Me.from_cc_obj = MConstants.listOfCC(_from_cc)
                Else
                    Me.from_cc_obj = Nothing
                    Throw New Exception("Cost center does not exist in cc list : " & _from_cc)
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _from_cc_obj As CCostCenter
    Public Property from_cc_obj() As CCostCenter
        Get
            Return _from_cc_obj
        End Get
        Set(ByVal value As CCostCenter)
            If Not Object.Equals(value, _from_cc_obj) Then
                _from_cc_obj = value
                If IsNothing(_from_cc_obj) Then
                    'use Me.cc to force obj value
                    Me.from_cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _from_cc = _from_cc_obj.cc
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public ReadOnly Property from_cc_desc As String
        Get
            If Not IsNothing(_from_cc_obj) Then
                Return _from_cc_obj.cc_desc_view
            Else
                Return ""
            End If
        End Get
    End Property

    Private _qty As Double
    Public Property qty() As Double
        Get
            Return _qty
        End Get
        Set(ByVal value As Double)
            If Not _qty = value AndAlso value > 0 Then
                _qty = value
                update_ctrl_obj()
                'simulate qty update
                Me.budget_available_to_transfer = 0
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _hours_type As String
    Public Property hours_type() As String
        Get
            Return _hours_type
        End Get
        Set(ByVal value As String)
            If Not _hours_type = value Then
                _hours_type = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                NotifyPropertyChanged()
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _from_ctrl_obj As CLaborControlMainViewObj
    Public Property from_ctrl_obj() As CLaborControlMainViewObj
        Get
            Return _from_ctrl_obj
        End Get
        Set(ByVal value As CLaborControlMainViewObj)
            _from_ctrl_obj = value
        End Set
    End Property

    Private _to_ctrl_obj As CLaborControlMainViewObj
    Public Property to_ctrl_obj() As CLaborControlMainViewObj
        Get
            Return _to_ctrl_obj
        End Get
        Set(ByVal value As CLaborControlMainViewObj)
            _to_ctrl_obj = value
        End Set
    End Property

    Public Property budget_available_to_transfer As Double
        Get
            If Not IsNothing(_from_ctrl_obj) Then
                If _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ODI Then
                    Return _from_ctrl_obj.budget_odi_qty - _from_ctrl_obj.sent_item.budget_odi_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_INIT Then
                    Return _from_ctrl_obj.budget_init_quoted_qty - _from_ctrl_obj.sent_item.budget_init_quoted_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_BLW_THR Then
                    Return _from_ctrl_obj.budget_add_below_thr_qty - _from_ctrl_obj.sent_item.budget_add_below_thr_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_APPR Then
                    Return _from_ctrl_obj.budget_add_awq_approved_qty - _from_ctrl_obj.sent_item.budget_add_awq_approved_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_PEND Then
                    Return _from_ctrl_obj.budget_add_awq_sent_qty - _from_ctrl_obj.sent_item.budget_add_awq_sent_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_ADD_INWK Then
                    Return _from_ctrl_obj.budget_add_awq_inwork_qty - _from_ctrl_obj.sent_item.budget_add_awq_inwork_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_INIT Then
                    Return _from_ctrl_obj.budget_foc_init_qty - _from_ctrl_obj.sent_item.budget_foc_init_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_BLW_THR Then
                    Return _from_ctrl_obj.budget_foc_add_blw_qty - _from_ctrl_obj.sent_item.budget_foc_add_blw_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_APPR Then
                    Return _from_ctrl_obj.budget_foc_add_appr_qty - _from_ctrl_obj.sent_item.budget_foc_add_appr_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_PEND Then
                    Return _from_ctrl_obj.budget_foc_add_pending_qty - _from_ctrl_obj.sent_item.budget_foc_add_pending_qty
                ElseIf _scope = MConstants.CONST_CONF_LAB_CTRL_SCOPE_FOC_ADD_INWK Then
                    Return _from_ctrl_obj.budget_foc_add_inwork_qty - _from_ctrl_obj.sent_item.budget_foc_add_inwork_qty
                Else
                    Return 0
                End If

            Else
                Return 0
            End If
        End Get
        Set(value As Double)
            NotifyPropertyChanged()
        End Set
    End Property

    Public Property actual_available_to_transfer As Double
        Get
            If Not IsNothing(_from_ctrl_obj) Then
                Return _from_ctrl_obj.actual_booked_qty - _from_ctrl_obj.sent_item.actual_booked_qty
            Else
                Return 0
            End If
        End Get
        Set(value As Double)
        End Set
    End Property

    Public ReadOnly Property from_act_str As String
        Get
            If Not IsNothing(_from_ctrl_obj) AndAlso Not IsNothing(_from_ctrl_obj.act_obj) Then
                Return _from_ctrl_obj.act_obj.tostring
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property from_cc_str As String
        Get
            If Not IsNothing(_from_cc_obj) Then
                Return _from_cc_obj.cc_desc_view
            Else
                Return ""
            End If
        End Get
    End Property

    'set to ctrl obj
    Public Sub fetch_to_ctrl_obj()
        Try
            If MConstants.GLOB_TRANSFER_HRS_LOADED Then
                'remove in list of current object
                If Not IsNothing(_to_ctrl_obj) AndAlso _to_ctrl_obj.hoursTransfert.Contains(Me) Then
                    _to_ctrl_obj.hoursTransfert.Remove(Me)
                    _to_ctrl_obj.update()
                    If _to_ctrl_obj.isZero Then
                        MConstants.GLB_MSTR_CTRL.labor_controlling_controller.unLoadActCCCtrl(_to_ctrl_obj)
                    End If
                End If
                'get new object
                Dim to_act As CScopeActivity = MConstants.GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_to_act_id)
                _to_ctrl_obj = MConstants.GLB_MSTR_CTRL.labor_controlling_controller.loadActCCCtrl(to_act.id, to_act, _to_cc_obj)
                'add to list of to
                If Not IsNothing(_to_ctrl_obj) AndAlso Not _to_ctrl_obj.hoursTransfert.Contains(Me) Then
                    'check that object are not equals, cause if they are equals (when creating the transfert object) the item will be added to the to_object by the GUI
                    If Not _to_ctrl_obj.hoursTransfert.Contains(Me) AndAlso Not Object.Equals(_from_ctrl_obj, _to_ctrl_obj) Then
                        _to_ctrl_obj.hoursTransfert.Add(Me)
                    End If
                End If
            End If
            'must update
            update_ctrl_obj()
        Catch ex As Exception
            _calc_error_text = _calc_error_text & Chr(10) & "Error occured while retrieving Labor controlling object from act " & _to_act_id & " and cc " & _to_cc & ", error details " & ex.Message & Chr(10) & ex.StackTrace
            _calc_is_in_error_state = True
        End Try
    End Sub

    'update calculations
    Public Sub update_ctrl_obj()
        If Not IsNothing(_to_ctrl_obj) AndAlso Not IsNothing(_from_ctrl_obj) Then
            'When updating transferHour object, from object shall always be updated before to object, to calculate if item is ignored or not
            _from_ctrl_obj.update()
            _to_ctrl_obj.update()
        End If
    End Sub

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    'text to display any error
    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
        End Set
    End Property

    Private _scope As String
    Public Property scope() As String
        Get
            Return _scope
        End Get
        Set(ByVal value As String)
            If Not _scope = value Then
                _scope = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
                update_ctrl_obj()
            End If
        End Set
    End Property

    Public ReadOnly Property scope_str As String
        Get
            If MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST).ContainsKey(_scope) Then
                Return MConstants.listOfValueConf(CONST_CONF_LOV_LAB_CTRL_DEV_JUST_SCOPE_LIST)(_scope).value
            Else
                Return ""
            End If
        End Get
    End Property

    Private _calc_is_ignored As Boolean
    Public Property calc_is_ignored() As Boolean
        Get
            Return _calc_is_ignored
        End Get
        Set(ByVal value As Boolean)
            If Not _calc_is_ignored = value Then
                _calc_is_ignored = value
                Me.budget_available_to_transfer = 0
                Me.actual_available_to_transfer = 0
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "Qty: " & _qty & ",From Act: " & If(IsNothing(_from_ctrl_obj) OrElse IsNothing(_from_ctrl_obj.act_obj), "-1", _from_ctrl_obj.act_obj.tostring) & ", from CC: " & _from_cc & ",  scope: " & _scope & ", to act " & If(IsNothing(_to_ctrl_obj) OrElse IsNothing(_to_ctrl_obj.act_obj), "-1", _to_ctrl_obj.act_obj.tostring) & ", tp CC " & _to_cc & ", type " & _hours_type
    End Function
End Class
