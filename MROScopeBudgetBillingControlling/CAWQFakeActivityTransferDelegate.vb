﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CAWQFakeActivityTransferDelegate
    Public form As FormTransferLabMat
    Private activity As CScopeActivity

    Private allActSelectedItems As List(Of CScopeActivity)
    Private depActSelectedItems As List(Of CScopeActivity)

    Private depAct As List(Of CScopeActivity)
    Private mainAct As CScopeActivity

    Private fakeAWQLaborList As List(Of CAWQActivityTransferViewObject)
    Private fakeAWQMaterialList As List(Of CAWQActivityTransferViewObject)

    Private potentialActivityLabor As Dictionary(Of CScopeActivity, List(Of CAWQActivityTransferViewObject))
    Private potentialActivityMaterial As Dictionary(Of CScopeActivity, List(Of CAWQActivityTransferViewObject))

    Private forceClose As Boolean


    'launch form
    Public Sub launchEditForm(_act As CScopeActivity)
        Try
            forceClose = False

            potentialActivityLabor = New Dictionary(Of CScopeActivity, List(Of CAWQActivityTransferViewObject))
            potentialActivityMaterial = New Dictionary(Of CScopeActivity, List(Of CAWQActivityTransferViewObject))

            activity = _act
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "preparing view to transfer awq data..."

            allActSelectedItems = New List(Of CScopeActivity)
            depActSelectedItems = New List(Of CScopeActivity)
            depAct = New List(Of CScopeActivity)

            form = New FormTransferLabMat

            form.ctrl = Me

            'draw hours grid views.
            drawGrid(form.awq_labor_dgrid, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_LABOR_TRANSFER_VIEW_DISPLAY))
            drawGrid(form.awq_material_dgrid, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_MATERIAL_TRANSFER_VIEW_DISPLAY))
            drawGrid(form.labor_map_dgrid, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_LABOR_TRANSFERED_VIEW_DISPLAY))
            drawGrid(form.material_map_dgrid, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_MATERIAL_TRANSFERED_VIEW_DISPLAY))

            fakeAWQLaborList = New List(Of CAWQActivityTransferViewObject)
            For Each soldH As CSoldHours In activity.sold_hour_list
                If soldH.system_status = MConstants.CONST_SYS_ROW_DELETED OrElse Not IsNothing(soldH.init_quote_entry_obj) Then
                    Continue For
                End If
                Dim viewO As New CAWQActivityTransferViewObject
                viewO.labor = soldH
                viewO.activity = activity
                fakeAWQLaborList.Add(viewO)
            Next
            Dim awqLabView As New BindingListView(Of CAWQActivityTransferViewObject)(fakeAWQLaborList)
            awqLabView.ApplyFilter(AddressOf filterAddScope)
            form.awq_labor_dgrid.DataSource = awqLabView

            fakeAWQMaterialList = New List(Of CAWQActivityTransferViewObject)
            For Each material As CMaterial In activity.material_list
                If material.system_status = MConstants.CONST_SYS_ROW_DELETED OrElse Not IsNothing(material.init_quote_entry_obj) Then
                    Continue For
                End If
                Dim viewO As New CAWQActivityTransferViewObject
                viewO.material = material
                viewO.activity = activity
                fakeAWQMaterialList.Add(viewO)
            Next
            Dim awqMatView As New BindingListView(Of CAWQActivityTransferViewObject)(fakeAWQMaterialList)
            awqMatView.ApplyFilter(AddressOf filterAddMat)
            form.awq_material_dgrid.DataSource = awqMatView

            'fill data in views
            updateView()

            form.ShowDialog()
            form.BringToFront()

            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End preparing view to edit awq"
        Catch ex As Exception
            MsgBox("An error occured while preparing the windows to edit awq " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'Update page
    Public Sub updateView()
        Try
            form.temp_act_val.Enabled = False
            form.temp_act_val.Text = activity.tostring
            updateDependantActivitiesView()

        Catch ex As Exception
            MsgBox("An error occured while loading data" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Function close() As Boolean
        Try
            If forceClose Then
                Return False
            End If
            Dim dialogRes As DialogResult
            dialogRes = MessageBox.Show("This action will close the current windows and cancel the mapping. Are you sure you want to cancel ? " & Chr(10) & "Click Yes to cancel, or No to continue the transfer.", "Cancel ?", MessageBoxButtons.YesNo)
            If dialogRes = DialogResult.Yes Then
                Return False
            Else
                Return True
            End If
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Sub perform_transfer()
        Try
            'check if all items have been transfered
            Dim notMappedList As New List(Of String)

            'check if items are not mapped
            For Each viewO As CAWQActivityTransferViewObject In fakeAWQLaborList
                If Not IsNothing(viewO.labor.awq_obj) Then
                    If IsNothing(viewO.linked_view_object) Then
                        notMappedList.Add(viewO.ToString)
                    End If
                End If
            Next

            For Each viewO As CAWQActivityTransferViewObject In fakeAWQMaterialList
                If Not IsNothing(viewO.material.awq_obj) Then
                    If IsNothing(viewO.linked_view_object) Then
                        notMappedList.Add(viewO.ToString)
                    End If
                End If
            Next

            If notMappedList.Count > 0 Then
                MsgBox("Cannot perform transfer cause the following items have not been mapped" & Chr(10) & "-" & String.Join(Chr(10) & "-", notMappedList), MsgBoxStyle.Critical, "All items are not mapped")
                Exit Sub
            ElseIf IsNothing(mainAct) Then
                MsgBox("Cannot perform transfer cause No Main activity was selected" & Chr(10) & "-" & String.Join(Chr(10) & "-", notMappedList), MsgBoxStyle.Critical, "All items are not mapped")
                Exit Sub
            End If

            'start transfer

            'transfer labor
            For Each viewO As CAWQActivityTransferViewObject In fakeAWQLaborList
                If Not IsNothing(viewO.linked_view_object) Then
                    If Not IsNothing(viewO.linked_view_object.labor) Then
                        viewO.labor.transferDataTo(viewO.linked_view_object.labor)
                        viewO.linked_view_object.labor.updateCal(False)

                        'remove from old activity list
                        If viewO.labor.act_object.sold_hour_list.Contains(viewO.labor) Then
                            viewO.labor.act_object.sold_hour_list.Remove(viewO.labor)
                        End If

                        'delete
                        MConstants.GLB_MSTR_CTRL.add_scope_controller.deleteObjectSoldHours(viewO.labor)
                    Else
                        'remove from old activity list.remove first
                        If viewO.labor.act_object.sold_hour_list.Contains(viewO.labor) Then
                            viewO.labor.act_object.sold_hour_list.Remove(viewO.labor)
                        End If
                        viewO.labor.act_object = viewO.linked_view_object.activity
                        If Not viewO.linked_view_object.activity.sold_hour_list.Contains(viewO.labor) Then
                            viewO.linked_view_object.activity.sold_hour_list.Add(viewO.labor)
                        End If
                    End If
                Else
                    'delete not mapped
                    MConstants.GLB_MSTR_CTRL.add_scope_controller.deleteObjectSoldHours(viewO.labor)
                End If
            Next

            'transfer material
            For Each viewO As CAWQActivityTransferViewObject In fakeAWQMaterialList
                If Not IsNothing(viewO.linked_view_object) Then
                    If Not IsNothing(viewO.linked_view_object.material) Then
                        viewO.material.transferDataTo(viewO.linked_view_object.material)
                        viewO.linked_view_object.material.updateCal(False)

                        'remove from old activity list
                        If viewO.material.activity_obj.material_list.Contains(viewO.material) Then
                            viewO.material.activity_obj.material_list.Remove(viewO.material)
                        End If
                        'delete
                        MConstants.GLB_MSTR_CTRL.material_controller.deleteObjectMaterial(viewO.material)
                    Else
                        'remove from old activity list.remove first
                        If viewO.material.activity_obj.material_list.Contains(viewO.material) Then
                            viewO.material.activity_obj.material_list.Remove(viewO.material)
                        End If

                        viewO.material.activity_obj = viewO.linked_view_object.activity
                        If Not viewO.linked_view_object.activity.material_list.Contains(viewO.material) Then
                            viewO.linked_view_object.activity.material_list.Add(viewO.material)
                        End If
                    End If
                Else
                    'if an item is not transfered delete it
                    'remove from old activity list
                    If viewO.material.activity_obj.material_list.Contains(viewO.material) Then
                        viewO.material.activity_obj.material_list.Remove(viewO.material)
                    End If
                    'delete
                    MConstants.GLB_MSTR_CTRL.material_controller.deleteObjectMaterial(viewO.material)
                End If
            Next

            'transfer dependant activities
            Dim masterAWQ As CAWQuotation = activity.awq_master_obj
            masterAWQ.dependantActList.Clear()
            activity.awq_master_obj = Nothing
            masterAWQ.main_activity_obj = mainAct

            'check if has been sent
            Dim hasBeenSent As Boolean = False
            For Each awq_rev As CAWQuotation In masterAWQ.revision_list
                If awq_rev.has_been_sent Then
                    hasBeenSent = True
                    Exit For
                End If
            Next
            'set reference before calcul as reference is used as scope identifier in calculations
            'new AWQ Reference
            For Each awq_rev As CAWQuotation In masterAWQ.revision_list
                If hasBeenSent Then
                    awq_rev.temp_reference = awq_rev.reference
                End If
                awq_rev.setReference()
            Next

            For Each act As CScopeActivity In depAct
                act.awq_master_obj = masterAWQ
                masterAWQ.dependantActList.Add(act)
                act.updateMatLabCal()
                act.updateCal()
            Next


            'new AWQ Reference
            For Each awq_rev As CAWQuotation In masterAWQ.revision_list
                'with new ref
                awq_rev.updateCal()
            Next

            'delete fake activity
            MConstants.GLB_MSTR_CTRL.add_scope_controller.deleteObjectActivity(activity)

            'refresh Material and Labor Views
            MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.Refresh()
            MConstants.GLB_MSTR_CTRL.material_controller.matList.Refresh()

            MsgBox("The Data transfer has been successfully performed !!")

            forceClose = True
            form.Close()

            'launch refresher of DB
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getAutomaticUpdateDeleteAll)

            'check if activity form opened
            If Application.OpenForms().OfType(Of FormAddScopeActivityEdit).Any Then
                'if awq form opened from activity, close
                If Application.OpenForms().OfType(Of FormAWQEdit).Any Then
                    Application.OpenForms().OfType(Of FormAWQEdit).ToList(0).Close()
                End If
                Application.OpenForms().OfType(Of FormAddScopeActivityEdit).ToList(0).Close()
            Else
                'if awq form opened from activity, close
                If Application.OpenForms().OfType(Of FormAWQEdit).Any Then
                    Application.OpenForms().OfType(Of FormAWQEdit).ToList(0).Close()
                End If
            End If

        Catch ex As Exception
            MsgBox("An error occured while performing transfer" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub cancelItem(dgrid As DataGridView, rowIdx As Integer)
        Try
            Dim objView As CAWQActivityTransferViewObject = CType(dgrid.Rows(rowIdx).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            'ALREADY empty
            If IsNothing(objView.linked_view_object) Then
                Exit Sub
            End If
            'remove from list if new item
            If dgrid.Equals(form.awq_labor_dgrid) Then
                'if new item delete it
                If IsNothing(objView.linked_view_object.labor) Then
                    If potentialActivityLabor.ContainsKey(objView.linked_view_object.activity) AndAlso potentialActivityLabor(objView.linked_view_object.activity).Contains(objView.linked_view_object) Then
                        potentialActivityLabor(objView.linked_view_object.activity).Remove(objView.linked_view_object)
                    End If
                    CType(form.labor_map_dgrid.DataSource, AggregateBindingListView(Of CAWQActivityTransferViewObject)).Refresh()
                End If
            ElseIf dgrid.Equals(form.labor_map_dgrid) Then
                If IsNothing(objView.labor) Then
                    If potentialActivityLabor.ContainsKey(objView.activity) AndAlso potentialActivityLabor(objView.activity).Contains(objView) Then
                        potentialActivityLabor(objView.activity).Remove(objView)
                    End If
                    CType(form.labor_map_dgrid.DataSource, AggregateBindingListView(Of CAWQActivityTransferViewObject)).Refresh()
                End If
            ElseIf dgrid.Equals(form.awq_material_dgrid) Then
                'if new item delete it
                If IsNothing(objView.linked_view_object.material) Then
                    If potentialActivityMaterial.ContainsKey(objView.linked_view_object.activity) AndAlso potentialActivityMaterial(objView.linked_view_object.activity).Contains(objView.linked_view_object) Then
                        potentialActivityMaterial(objView.linked_view_object.activity).Remove(objView.linked_view_object)
                    End If
                    CType(form.material_map_dgrid.DataSource, AggregateBindingListView(Of CAWQActivityTransferViewObject)).Refresh()
                End If
            ElseIf dgrid.Equals(form.material_map_dgrid) Then
                If IsNothing(objView.material) Then
                    If potentialActivityMaterial.ContainsKey(objView.activity) AndAlso potentialActivityMaterial(objView.activity).Contains(objView) Then
                        potentialActivityMaterial(objView.activity).Remove(objView)
                    End If
                    CType(form.material_map_dgrid.DataSource, AggregateBindingListView(Of CAWQActivityTransferViewObject)).Refresh()
                End If
            End If

            If Not IsNothing(objView.linked_view_object) Then
                objView.linked_view_object.linked_view_object = Nothing
                objView.linked_view_object = Nothing
            End If
        Catch ex As Exception
            MsgBox("An error occured while cancelling mapping on the item" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub addNewLabor(rowIdx As Integer)
        Try
            Dim objView As CAWQActivityTransferViewObject = CType(form.awq_labor_dgrid.Rows(rowIdx).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            If Not IsNothing(objView.linked_view_object) Then
                MsgBox("Cannot Perform Action, Item has already been transfered to " & objView.linked_view_object.ToString, MsgBoxStyle.Critical)
                Exit Sub
            End If
            If form.act_dep_selected_listb.SelectedItems.Count = 0 Then
                MsgBox("Cannot Perform Action, No Activity Is selected !!", MsgBoxStyle.Critical)
                Exit Sub
            End If
            Dim activity As CScopeActivity = CType(form.act_dep_selected_listb.SelectedItems(0), CScopeActivity)
            'create new obj 
            Dim objViewNew As New CAWQActivityTransferViewObject
            objViewNew.activity = activity
            If Not potentialActivityLabor.ContainsKey(activity) Then
                potentialActivityLabor.Add(activity, New List(Of CAWQActivityTransferViewObject))
            End If
            potentialActivityLabor(activity).Add(objViewNew)
            'set links
            objViewNew.linked_view_object = objView
            objView.linked_view_object = objViewNew

            CType(form.labor_map_dgrid.DataSource, AggregateBindingListView(Of CAWQActivityTransferViewObject)).Refresh()
        Catch ex As Exception
            MsgBox("An error occured while mapping the item" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub addExistingLabor(rowIdx As Integer)
        Try
            Dim objView As CAWQActivityTransferViewObject = CType(form.awq_labor_dgrid.Rows(rowIdx).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            If Not IsNothing(objView.linked_view_object) Then
                MsgBox("Cannot Perform Action, Item has already been transfered to " & objView.linked_view_object.ToString, MsgBoxStyle.Critical)
                Exit Sub
            End If

            If form.labor_map_dgrid.SelectedRows.Count <> 1 Then
                MsgBox("Cannot Perform Action, Select One Entire Row As The Labor Line to Be Updated", MsgBoxStyle.Critical)
                Exit Sub
            End If

            Dim objViewExist As CAWQActivityTransferViewObject = CType(form.labor_map_dgrid.SelectedRows(0).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            'set links
            objViewExist.linked_view_object = objView
            objView.linked_view_object = objViewExist
        Catch ex As Exception
            MsgBox("An error occured while mapping the item" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub addNewMaterial(rowIdx As Integer)
        Try
            Dim objView As CAWQActivityTransferViewObject = CType(form.awq_material_dgrid.Rows(rowIdx).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            If Not IsNothing(objView.linked_view_object) Then
                MsgBox("Cannot Perform Action, Item has already been transfered to " & objView.linked_view_object.ToString, MsgBoxStyle.Critical)
                Exit Sub
            End If
            If form.act_dep_selected_listb.SelectedItems.Count = 0 Then
                MsgBox("Cannot Perform Action, No Activity Is selected !!", MsgBoxStyle.Critical)
                Exit Sub
            End If
            Dim activity As CScopeActivity = CType(form.act_dep_selected_listb.SelectedItems(0), CScopeActivity)
            'create new obj 
            Dim objViewNew As New CAWQActivityTransferViewObject
            objViewNew.activity = activity
            If Not potentialActivityMaterial.ContainsKey(activity) Then
                potentialActivityMaterial.Add(activity, New List(Of CAWQActivityTransferViewObject))
            End If
            potentialActivityMaterial(activity).Add(objViewNew)
            'set links
            objViewNew.linked_view_object = objView
            objView.linked_view_object = objViewNew

            CType(form.material_map_dgrid.DataSource, AggregateBindingListView(Of CAWQActivityTransferViewObject)).Refresh()
        Catch ex As Exception
            MsgBox("An error occured while mapping the item" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub addExistingMaterial(rowIdx As Integer)
        Try
            Dim objView As CAWQActivityTransferViewObject = CType(form.awq_material_dgrid.Rows(rowIdx).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            If Not IsNothing(objView.linked_view_object) Then
                MsgBox("Cannot Perform Action, Item has already been transfered to " & objView.linked_view_object.ToString, MsgBoxStyle.Critical)
                Exit Sub
            End If

            If form.material_map_dgrid.SelectedRows.Count <> 1 Then
                MsgBox("Cannot Perform Action, Select One Entire Row As The Labor Line to Be Updated", MsgBoxStyle.Critical)
                Exit Sub
            End If

            Dim objViewExist As CAWQActivityTransferViewObject = CType(form.material_map_dgrid.SelectedRows(0).DataBoundItem, ObjectView(Of CAWQActivityTransferViewObject)).Object
            'set links
            objViewExist.linked_view_object = objView
            objView.linked_view_object = objViewExist
        Catch ex As Exception
            MsgBox("An error occured while mapping the item" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub updateDependantActivitiesView()
        Dim bds As New BindingSource
        Dim sortHandler As BindingListView(Of CScopeActivity)
        Try
            'potentials act
            'fill all activities view
            sortHandler = New BindingListView(Of CScopeActivity)(GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource)
            bds = New BindingSource
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterAllPotentialDependantAct)
            sortHandler.ApplySort("act_notif_opcode ASC")
            bds.DataSource = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            form.act_dep_all_listb.DataSource = bds
            form.act_dep_all_listb.SelectionMode = SelectionMode.MultiExtended
            form.act_dep_all_listb.HorizontalScrollbar = True
            form.act_dep_all_listb.SelectedItems.Clear()
            'pre select act
            For Each act As CScopeActivity In allActSelectedItems
                form.act_dep_all_listb.SelectedItems.Add(act)
            Next

            allActSelectedItems.Clear()
            form.act_dep_all_listb.Refresh()

            'current selected act
            sortHandler = New BindingListView(Of CScopeActivity)(depAct)
            bds = New BindingSource
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplySort("act_notif_opcode ASC")
            Dim listO As List(Of CScopeActivity) = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            bds.DataSource = listO
            form.act_dep_selected_listb.DataSource = bds
            form.act_dep_selected_listb.SelectionMode = SelectionMode.MultiExtended
            form.act_dep_selected_listb.HorizontalScrollbar = True
            form.act_dep_selected_listb.SelectedItems.Clear()

            'pre select act
            For Each act As CScopeActivity In depActSelectedItems
                form.act_dep_selected_listb.SelectedItems.Add(act)
            Next
            depActSelectedItems.Clear()
            form.act_dep_selected_listb.Refresh()
            'deblock row refresh

            'primary activity
            sortHandler.RemoveFilter()
            sortHandler.RemoveSort()
            sortHandler.ApplyFilter(AddressOf filterCurrentDependantActPotentialMain)
            sortHandler.ApplySort("act_notif_opcode ASC")

            form.select_main_act_cb.Text = ""
            bds = New BindingSource
            bds.DataSource = MGeneralFuntionsViewControl.getActFromListView(sortHandler)
            form.select_main_act_cb.DataSource = bds
            form.select_main_act_cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            'set current value. if null put first list value
            If IsNothing(mainAct) Then
                'if no main activity set, take first one

            Else
                form.select_main_act_cb.SelectedItem = mainAct
            End If

            'update hours view
            updateHoursAndMaterialsViews()

        Catch ex As Exception
            MsgBox("An error occured while loading data of dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'filter potential dependant activities
    Private Function filterAllPotentialDependantAct(act As CScopeActivity) As Boolean
        If act.system_status <> MConstants.CONST_SYS_ROW_DELETED And Not act.is_fake_activity Then
            'case main activitity has been set 
            If IsNothing(act.awq_master_obj) Then
                Return True
            Else
                Return False
            End If
        Else
            Return False
        End If
    End Function

    'filter potential primary activities activities
    Private Function filterCurrentDependantActPotentialMain(act As CScopeActivity) As Boolean
        If filterAllPotentialDependantAct(act) AndAlso Not act.is_cost_collector Then
            Return True
        Else
            Return False
        End If
    End Function

    'add activities to the quote
    Public Sub addDependantActivities()
        Try
            For Each act As CScopeActivity In form.act_dep_all_listb.SelectedItems
                'if it is the first time that activities are binded to the quote, define a primary activity to avoid error in activity price caluclation (seting the product sum will triger calculation on act that need primary act)
                If IsNothing(mainAct) AndAlso Not act.is_cost_collector Then
                    mainAct = act
                End If
                If Not depAct.Contains(act) Then
                    depAct.Add(act)
                    depActSelectedItems.Add(act)
                    'add prepare activity labor and material views
                    'labor
                    Dim itemList As List(Of CAWQActivityTransferViewObject)
                    If Not potentialActivityLabor.ContainsKey(act) Then
                        potentialActivityLabor.Add(act, New List(Of CAWQActivityTransferViewObject))
                    End If
                    itemList = potentialActivityLabor(act)
                    itemList.Clear()
                    For Each soldH As CSoldHours In act.sold_hour_list
                        If soldH.system_status = MConstants.CONST_SYS_ROW_DELETED OrElse Not IsNothing(soldH.init_quote_entry_obj) Then
                            Continue For
                        End If
                        Dim viewO As New CAWQActivityTransferViewObject
                        viewO.labor = soldH
                        viewO.activity = act
                        itemList.Add(viewO)
                    Next

                    'mat
                    If Not potentialActivityMaterial.ContainsKey(act) Then
                        potentialActivityMaterial.Add(act, New List(Of CAWQActivityTransferViewObject))
                    End If
                    itemList = potentialActivityMaterial(act)
                    itemList.Clear()
                    For Each material As CMaterial In act.material_list
                        If material.system_status = MConstants.CONST_SYS_ROW_DELETED OrElse material.is_scope_init Then
                            Continue For
                        End If
                        Dim viewO As New CAWQActivityTransferViewObject
                        viewO.activity = act
                        viewO.material = material
                        itemList.Add(viewO)
                    Next
                End If
            Next
            updateDependantActivitiesView()
        Catch ex As Exception
            MsgBox("An error occured while adding data to dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'remove activities from the quote
    Public Sub RemoveDependantActivities()
        Try
            For Each act As CScopeActivity In form.act_dep_selected_listb.SelectedItems
                If Not IsNothing(mainAct) AndAlso mainAct.id = act.id Then
                    MsgBox("Cannot remove activity " & act.tostring & " because it is set as primary activity")
                Else
                    If depAct.Contains(act) Then
                        depAct.Remove(act)
                        allActSelectedItems.Add(act)
                    End If
                    'remove  activity labor and material views
                    'labor
                    If potentialActivityLabor.ContainsKey(act) Then
                        For Each viewO As CAWQActivityTransferViewObject In potentialActivityLabor(act)
                            'check if linked to awq object if yes break link
                            If Not IsNothing(viewO.linked_view_object) Then
                                viewO.linked_view_object = Nothing
                            End If
                        Next
                        potentialActivityLabor(act).Clear()
                        potentialActivityLabor.Remove(act)
                    End If
                    'mat
                    If potentialActivityMaterial.ContainsKey(act) Then
                        For Each viewO As CAWQActivityTransferViewObject In potentialActivityMaterial(act)
                            'check if linked to awq object if yes break link
                            If Not IsNothing(viewO.linked_view_object) Then
                                viewO.linked_view_object = Nothing
                            End If
                        Next
                        potentialActivityMaterial(act).Clear()
                        potentialActivityMaterial.Remove(act)
                    End If
                End If
            Next
            updateDependantActivitiesView()
        Catch ex As Exception
            MsgBox("An error occured while removing data from dependant activities " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'update calculation view
    Public Sub updateHoursAndMaterialsViews()
        Try
            'if no act selected block view edit
            If form.act_dep_selected_listb.SelectedItems.Count = 0 Then
                'reset views
                form.labor_map_dgrid.DataSource = Nothing
                form.material_map_dgrid.DataSource = Nothing
                Exit Sub
            End If
            Dim selectedAct As CScopeActivity = form.act_dep_selected_listb.SelectedItems(0)

            Dim LabView As New BindingListView(Of CAWQActivityTransferViewObject)(potentialActivityLabor(selectedAct))
            LabView.ApplyFilter(AddressOf filterAddScope)
            form.labor_map_dgrid.DataSource = LabView

            Dim MatView As New BindingListView(Of CAWQActivityTransferViewObject)(potentialActivityMaterial(selectedAct))
            MatView.ApplyFilter(AddressOf filterAddMat)
            form.material_map_dgrid.DataSource = MatView

        Catch ex As Exception
            MsgBox("An error occured while loading labor and materials of the selected activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'filter add mat
    Private Function filterAddMat(matView As CAWQActivityTransferViewObject) As Boolean
        If IsNothing(matView.material) Then
            Return True
        Else
            Dim mat As CMaterial = matView.material
            Return mat.system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso IsNothing(mat.init_quote_entry_obj)
        End If
    End Function
    'filter add scope
    Private Function filterAddScope(sold_hrView As CAWQActivityTransferViewObject) As Boolean
        If IsNothing(sold_hrView.labor) Then
            Return True
        Else
            Dim sold_hr As CSoldHours = sold_hrView.labor
            Return sold_hr.system_status <> MConstants.CONST_SYS_ROW_DELETED AndAlso IsNothing(sold_hr.init_quote_entry_obj)
        End If
    End Function


    'main activity changed
    Public Sub main_activity_changed()
        Try
            Dim act As CScopeActivity = form.select_main_act_cb.SelectedItem
            mainAct = act
            form.act_dep_selected_listb.SelectedItems.Clear()
            'will trigger hours grid refresh
            form.act_dep_selected_listb.SelectedItem = act
        Catch ex As Exception
            MsgBox("An error occured while setting the new primary activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub drawGrid(dgrid As DataGridView, mapList As CViewModelMapList)

        dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(dgrid)

        For Each map As CViewModelMap In mapList.list.Values

            If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                Dim dgcol As New DataGridViewTextBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = mapList.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgcol.ReadOnly = Not map.col_editable
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                Dim dgcol As New DataGridViewComboBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = mapList.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                'values
                Dim bds As New BindingSource
                If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                    bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                    bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_QUOTE_PAYER.ToLower Then
                    bds.DataSource = MConstants.listOfPayer.Values.ToList
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "payer"
                Else
                    Throw New Exception("Error occured when binding quote view. List " & map.view_list_name & " does not exist in LOV")
                End If
                dgcol.ReadOnly = Not map.col_editable
                dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                Dim dgcol As New DataGridViewCheckBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgcol.ReadOnly = Not map.col_editable
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                Dim dgcol As New DataGridViewButtonColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = ""
                dgcol.Text = map.view_col
                dgcol.UseColumnTextForButtonValue = True
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgrid.Columns.Add(dgcol)
            End If
        Next
    End Sub

End Class
