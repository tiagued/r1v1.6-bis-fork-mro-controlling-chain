﻿Public Class CScopeActivityRowValidator
    Private Shared instance As CScopeActivityRowValidator
    Public Shared Function getInstance() As CScopeActivityRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CScopeActivityRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(act As CScopeActivity)
        act.calc_is_in_error_state = False
    End Sub

    'check calculation ran without error
    Public Function errorText(act As CScopeActivity) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not String.IsNullOrWhiteSpace(act.calc_error_text_thrown) Then
            err = act.calc_error_text_thrown
            res = False
        Else
            res = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'value analyis cannot be empty 
    Public Function value_analysis(act As CScopeActivity) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not act.is_cost_collector AndAlso act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or String.IsNullOrWhiteSpace(act.value_analysis) Then
            err = "Value Analysis cannot be empty"
            res = False
            act.calc_is_in_error_state = True
        End If

        'if there is at least one released iAWQ it's value added ???

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'proj not empty is fake activity
    Public Function proj(act As CScopeActivity) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If act.is_fake_activity AndAlso act.proj = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or String.IsNullOrWhiteSpace(act.proj) Then
            err = "Project cannot be empty"
            res = False
            act.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'waste cause 
    Public Function waste_cause(act As CScopeActivity) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If act.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_WAST_KEY).value Then
            If act.waste_cause = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Or String.IsNullOrWhiteSpace(act.waste_cause) Then
                err = "Waste Cause is mandatory for if Value Analysis is Waste"
                res = False
                act.calc_is_in_error_state = True
            End If
        Else
            If act.waste_cause <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value And Not String.IsNullOrWhiteSpace(act.waste_cause) Then
                err = "Waste Cause shall be empty if Value Analysis is different from Waste"
                res = False
                act.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class