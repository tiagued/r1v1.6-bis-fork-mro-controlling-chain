﻿Public Class CNTEDetailsReportObj

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not value = _label Then
                _label = value
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not value = _description Then
                _description = value
            End If
        End Set
    End Property

    Private _labor_sold_calc As Double
    Public Property labor_sold_calc() As Double
        Get
            Return _labor_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_calc Then
                _labor_sold_calc = value
            End If
        End Set
    End Property


    Private _mat_sold_calc As Double
    Public Property mat_sold_calc() As Double
        Get
            Return _mat_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_calc Then
                _mat_sold_calc = value
            End If
        End Set
    End Property


    Private _rep_sold_calc As Double
    Public Property rep_sold_calc() As Double
        Get
            Return _rep_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_calc Then
                _rep_sold_calc = value
            End If
        End Set
    End Property

    Private _serv_sold_calc As Double
    Public Property serv_sold_calc() As Double
        Get
            Return _serv_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_calc Then
                _serv_sold_calc = value
            End If
        End Set
    End Property

    Private _freight_sold_calc As Double
    Public Property freight_sold_calc() As Double
        Get
            Return _freight_sold_calc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_calc Then
                _freight_sold_calc = value
            End If
        End Set
    End Property

    Private _labor_sold_calc_disc As Double
    Public Property labor_sold_calc_disc() As Double
        Get
            Return _labor_sold_calc_disc
        End Get
        Set(ByVal value As Double)
            If Not value = _labor_sold_calc_disc Then
                _labor_sold_calc_disc = value
            End If
        End Set
    End Property


    Private _mat_sold_calc_disc As Double
    Public Property mat_sold_calc_disc() As Double
        Get
            Return _mat_sold_calc_disc
        End Get
        Set(ByVal value As Double)
            If Not value = _mat_sold_calc_disc Then
                _mat_sold_calc_disc = value
            End If
        End Set
    End Property


    Private _rep_sold_calc_disc As Double
    Public Property rep_sold_calc_disc() As Double
        Get
            Return _rep_sold_calc_disc
        End Get
        Set(ByVal value As Double)
            If Not value = _rep_sold_calc_disc Then
                _rep_sold_calc_disc = value
            End If
        End Set
    End Property

    Private _serv_sold_calc_disc As Double
    Public Property serv_sold_calc_disc() As Double
        Get
            Return _serv_sold_calc_disc
        End Get
        Set(ByVal value As Double)
            If Not value = _serv_sold_calc_disc Then
                _serv_sold_calc_disc = value
            End If
        End Set
    End Property

    Private _freight_sold_calc_disc As Double
    Public Property freight_sold_calc_disc() As Double
        Get
            Return _freight_sold_calc_disc
        End Get
        Set(ByVal value As Double)
            If Not value = _freight_sold_calc_disc Then
                _freight_sold_calc_disc = value
            End If
        End Set
    End Property

    Public ReadOnly Property labor_sold_calc_charg() As Double
        Get
            Return _labor_sold_calc + _labor_sold_calc_disc
        End Get
    End Property


    Public ReadOnly Property mat_sold_calc_charg() As Double
        Get
            Return _mat_sold_calc + _mat_sold_calc_disc
        End Get
    End Property


    Public ReadOnly Property rep_sold_calc_charg() As Double
        Get
            Return _rep_sold_calc + _rep_sold_calc_disc
        End Get
    End Property

    Public ReadOnly Property serv_sold_calc_charg() As Double
        Get
            Return _serv_sold_calc + _serv_sold_calc_disc
        End Get
    End Property

    Public ReadOnly Property freight_sold_calc_charg() As Double
        Get
            Return _freight_sold_calc + _freight_sold_calc_disc
        End Get
    End Property

    Public Sub setGrossValues(entry As CScopeAtivityCalcEntry)
        Try
            _labor_sold_calc = entry.labor_sold_calc
            _mat_sold_calc = entry.mat_sold_calc
            _rep_sold_calc = entry.rep_sold_calc
            _freight_sold_calc = entry.freight_sold_calc
            _serv_sold_calc = entry.serv_sold_calc
        Catch ex As Exception
            Throw New Exception("An error occured while adding gross NTE data to reporting object" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub

    Public Sub setDiscountValues(entry As CScopeAtivityCalcEntry)
        Try
            _labor_sold_calc_disc = entry.labor_sold_calc
            _mat_sold_calc_disc = entry.mat_sold_calc
            _rep_sold_calc_disc = entry.rep_sold_calc
            _freight_sold_calc_disc = entry.freight_sold_calc
            _serv_sold_calc_disc = entry.serv_sold_calc
        Catch ex As Exception
            Throw New Exception("An error occured while adding Discount NTE data to reporting object" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
    End Sub
End Class
