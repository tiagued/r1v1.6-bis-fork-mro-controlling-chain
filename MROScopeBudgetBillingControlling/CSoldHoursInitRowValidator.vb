﻿
Public Class CSoldHoursInitRowValidator
    Public Shared instance As CSoldHoursInitRowValidator
    Public Shared Function getInstance() As CSoldHoursInitRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CSoldHoursInitRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(soldH As CSoldHours)
        soldH.calc_is_in_error_state = False
    End Sub

    ' Name of methods in this class should be the same as the property (class_field) binded to the datagridview in the applicable object
    'validate cost center.
    Public Function cc(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value OrElse IsNothing(soldH.cc_obj) Then
            err = "Cost Center Cannot be empty"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate hours
    Public Function sold_hrs(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.sold_hrs <= 0 Then
            err = "Hours Shall be greather than 0"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate hours adj
    Public Function sold_hrs_adj(soldH As CSoldHours) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not soldH.is_reported Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If soldH.sold_hrs_adj > 0 Then
            err = "Hours Adjustment must be negative"
            res = False
        ElseIf soldH.sold_hrs + soldH.sold_hrs_adj < 0 Then
            err = "Hours Adjustment must not be greater than the original amount"
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class
