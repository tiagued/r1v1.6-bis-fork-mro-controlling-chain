﻿
Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CSalesQuoteEntry
    Implements IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable

    'used to created new ids
    Public Shared currentMaxID As Integer

    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then

        End If
        'no error state at the creation
        _calc_is_in_error_state = False
        _quote_entry_link_list = New List(Of CSaleQuoteEntryToActLink)
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
        For Each lk As CSaleQuoteEntryToActLink In _quote_entry_link_list
            lk.init_quote_entry_id = _id
        Next
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_QUOTE_ENTRIES_DB_READ
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""

        Try
            Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isSalesEntryUsed(Me)
            If isUsed.Key Then
                res = False
                mess = "Item " & Me.tostring & " Cannot be deleted because it is linked activities with that contains " & isUsed.Value.Count & " initial sold hours " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value)
            Else
                For Each lk As CSaleQuoteEntryToActLink In _quote_entry_link_list
                    lk.deleteItem()
                Next
                Me.main_activity_obj = Nothing
                Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                MConstants.GLB_MSTR_CTRL.ini_scope_controller.deleteObjectQuoteEntry(Me)
                res = True
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message
        End Try

        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Sub setID()
        If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            _calc_is_in_error_state = value
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set

    End Property
    Private _quote_entry_link_list As List(Of CSaleQuoteEntryToActLink)
    Public Property quote_entry_link_list() As List(Of CSaleQuoteEntryToActLink)
        Get
            Return _quote_entry_link_list
        End Get
        Set(ByVal value As List(Of CSaleQuoteEntryToActLink))
            _quote_entry_link_list = value
        End Set
    End Property

    Private _id As Integer
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    CSalesQuoteEntry.currentMaxID = Math.Max(CSalesQuoteEntry.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _quote_id As Integer
    Public Property quote_id() As Integer
        Get
            Return _quote_id
        End Get
        Set(ByVal value As Integer)
            If Not _quote_id = value Then
                _quote_id = value
                _quote_obj = MConstants.GLB_MSTR_CTRL.ini_scope_controller.getQuoteObject(_quote_id)
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _quote_obj As CSalesQuote
    Public ReadOnly Property quote_obj() As CSalesQuote
        Get
            Return _quote_obj
        End Get
    End Property

    Private _product As String
    Public Property product() As String
        Get
            Return _product
        End Get
        Set(ByVal value As String)
            If Not _product = value Then
                _product = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property
    'use as scope value in calculations entries in cscopeactivitycalculator for init scope items
    Public ReadOnly Property calc_entry_scope As String
        Get
            Dim res As String = ""
            If Not IsNothing(_quote_obj) Then
                res = _quote_obj.number & "/"
            End If
            Return res & _product
        End Get
    End Property

    Public ReadOnly Property calc_entry_scope_label As String
        Get
            Return Me.calc_entry_scope
        End Get
    End Property

    'use as scope value in calculations entries in cscopeactivitycalculator for init scope items
    Public ReadOnly Property calc_entry_scope_adj As String
        Get
            Dim res As String = ""
            If Not IsNothing(_quote_obj) Then
                res = _quote_obj.number & "/"
            End If
            Return "(ADJ.) " & res & _product
        End Get
    End Property

    Public ReadOnly Property calc_entry_scope_label_adj As String
        Get
            Return Me.calc_entry_scope
        End Get
    End Property

    Private _labor_price As Double
    Public Property labor_price() As Double
        Get
            Return _labor_price
        End Get
        Set(ByVal value As Double)
            If Not _labor_price = value Then
                _labor_price = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _labor_price_adj As Double
    Public Property labor_price_adj() As Double
        Get
            Return _labor_price_adj
        End Get
        Set(ByVal value As Double)
            If Not _labor_price_adj = value Then
                _labor_price_adj = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Property lab_curr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_quote_obj.lab_curr).value
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private _mat_price As Double
    Public Property mat_price() As Double
        Get
            Return _mat_price
        End Get
        Set(ByVal value As Double)
            If Not _mat_price = value Then
                _mat_price = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _mat_price_adj As Double
    Public Property mat_price_adj() As Double
        Get
            Return _mat_price_adj
        End Get
        Set(ByVal value As Double)
            If Not _mat_price_adj = value Then
                _mat_price_adj = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _repair_price As Double
    Public Property repair_price() As Double
        Get
            Return _repair_price
        End Get
        Set(ByVal value As Double)
            If Not _repair_price = value Then
                _repair_price = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _repair_price_adj As Double
    Public Property repair_price_adj() As Double
        Get
            Return _repair_price_adj
        End Get
        Set(ByVal value As Double)
            If Not _repair_price_adj = value Then
                _repair_price_adj = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _third_party_price As Double
    Public Property third_party_price() As Double
        Get
            Return _third_party_price
        End Get
        Set(ByVal value As Double)
            If Not _third_party_price = value Then
                _third_party_price = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _third_party_price_adj As Double
    Public Property third_party_price_adj() As Double
        Get
            Return _third_party_price_adj
        End Get
        Set(ByVal value As Double)
            If Not _third_party_price_adj = value Then
                _third_party_price_adj = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_price As Double
    Public Property freight_price() As Double
        Get
            Return _freight_price
        End Get
        Set(ByVal value As Double)
            If Not _freight_price = value Then
                _freight_price = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _freight_price_adj As Double
    Public Property freight_price_adj() As Double
        Get
            Return _freight_price_adj
        End Get
        Set(ByVal value As Double)
            If Not _freight_price_adj = value Then
                _freight_price_adj = value
                If MConstants.GLOB_QUOTE_ENTRIES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _comment As String
    Public Property comment() As String
        Get
            Return _comment
        End Get
        Set(ByVal value As String)
            If Not _comment = value Then
                _comment = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _adj_comment As String
    Public Property adj_comment() As String
        Get
            Return _adj_comment
        End Get
        Set(ByVal value As String)
            If Not _adj_comment = value Then
                _adj_comment = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Property mat_curr() As String
        Get
            Return MConstants.listOfValueConf(MConstants.CONST_CONF_LOV_CURR)(_quote_obj.mat_curr).value
        End Get
        Set(ByVal value As String)

        End Set
    End Property

    Private _main_activity_id As Long
    Public Property main_activity_id() As Long
        Get
            'if id is null, check if object exist
            If _main_activity_id <= 0 Then
                If Not IsNothing(_main_activity_obj) Then
                    _main_activity_id = _main_activity_obj.id
                End If
            End If
            Return _main_activity_id
        End Get
        Set(ByVal value As Long)
            If Not _main_activity_id = value Then
                _main_activity_id = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _main_activity_obj As CScopeActivity
    Public Property main_activity_obj() As CScopeActivity
        Get
            If IsNothing(_main_activity_obj) Then
                If Not _main_activity_id <= 0 Then
                    _main_activity_obj = GLB_MSTR_CTRL.add_scope_controller.getActivityObject(_main_activity_id)
                End If
            End If
            Return _main_activity_obj
        End Get
        Set(ByVal value As CScopeActivity)
            If Not Object.Equals(_main_activity_obj, value) Then
                _main_activity_obj = value
                If IsNothing(value) Then
                    _main_activity_id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
                Else
                    _main_activity_id = value.id
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'display in gridview
    Public ReadOnly Property main_activity_str() As String
        Get
            If IsNothing(Me.main_activity_obj) Then
                Return ""
            Else
                Return Me.main_activity_obj.proj & "-" & Me.main_activity_obj.act
            End If

        End Get
    End Property

    Public Function getDependantActivitiesList() As List(Of CScopeActivity)
        Dim res As New List(Of CScopeActivity)
        For Each lk As CSaleQuoteEntryToActLink In _quote_entry_link_list
            If Not IsNothing(lk.act_obj) AndAlso Not res.Contains(lk.act_obj) Then
                res.Add(lk.act_obj)
            End If
        Next
        Return res
    End Function

    Public ReadOnly Property dependant_activities_str() As String
        Get
            Dim res As String = ""
            For Each lk As CSaleQuoteEntryToActLink In _quote_entry_link_list
                If Not IsNothing(lk.act_obj) Then
                    If res = "" Then
                        res = lk.act_obj.proj & "-" & lk.act_obj.act
                    Else
                        res = res & ", " & lk.act_obj.proj & "-" & lk.act_obj.act
                    End If
                End If
            Next
            Return res
        End Get
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Public Function isZero() As Boolean
        Return _labor_price = 0 AndAlso _mat_price = 0 AndAlso _repair_price = 0 AndAlso _third_party_price = 0 AndAlso _freight_price = 0
    End Function

    Public Function isInitCancelled() As Boolean
        Dim res As Boolean = False
        If IsNothing(_main_activity_obj) Then
            If _labor_price + _labor_price_adj = 0 AndAlso _mat_price + _mat_price_adj = 0 AndAlso _repair_price + _repair_price_adj = 0 AndAlso _third_party_price + _third_party_price_adj = 0 AndAlso _freight_price + _freight_price_adj = 0 Then
                res = True
            End If
        End If
            Return res
    End Function
    'use to calculate init and adj init in case of cancelled quote entry
    Public Function getInitCancelledEntry() As CScopeAtivityCalcEntry
        Dim res As New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
        res.labor_sold_calc = MCalc.toLabCurr(_labor_price, _quote_obj.lab_curr)
        res.mat_sold_calc = MCalc.toMatCurr(_mat_price, _quote_obj.mat_curr)
        res.rep_sold_calc = MCalc.toMatCurr(_repair_price, _quote_obj.mat_curr)
        res.serv_sold_calc = MCalc.toMatCurr(_third_party_price, _quote_obj.mat_curr)
        res.freight_sold_calc = MCalc.toMatCurr(_freight_price, _quote_obj.mat_curr)
        Return res
    End Function
    Public Function getInitAdjCancelledEntry() As CScopeAtivityCalcEntry
        Dim res As New CScopeAtivityCalcEntry(CScopeActivityCalculator.EMPTY_LINE)
        res.labor_sold_calc = MCalc.toLabCurr(_labor_price_adj, _quote_obj.lab_curr)
        res.mat_sold_calc = MCalc.toMatCurr(_mat_price_adj, _quote_obj.mat_curr)
        res.rep_sold_calc = MCalc.toMatCurr(_repair_price_adj, _quote_obj.mat_curr)
        res.serv_sold_calc = MCalc.toMatCurr(_third_party_price_adj, _quote_obj.mat_curr)
        res.freight_sold_calc = MCalc.toMatCurr(_freight_price_adj, _quote_obj.mat_curr)
        Return res
    End Function

    'to string
    Public Overrides Function tostring() As String
        Return "Quote Entry: " & _product & ", from quote: " & _quote_obj.number
    End Function
End Class

