﻿Public Class CDataGridViewDateEditControl
    Inherits DateTimePicker

    Public Sub New()
        Me.Format = DateTimePickerFormat.Custom
        Me.Anchor = AnchorStyles.None
        Me.Visible = False
        AddHandler Me.CloseUp, AddressOf Me.DateTimePickerCloseUp
    End Sub

    Private _current_cell As DataGridViewCell
    Public Property current_cell() As DataGridViewCell
        Get
            Return _current_cell
        End Get
        Set(ByVal value As DataGridViewCell)
            MConstants.DATE_PICKER.Visible = True
            _current_cell = value
            MyBase.OnDropDown(New EventArgs)
        End Set
    End Property

    Private Sub DateTimePickerCloseUp(sender As Object, e As EventArgs)
        If Not IsNothing(_current_cell) Then
            If Me.Checked Then
                _current_cell.Value = Me.Value
            End If
        End If
        Me.Hide()
        _current_cell = Nothing

    End Sub

End Class
