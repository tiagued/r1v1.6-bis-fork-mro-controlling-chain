﻿Public Class CImportScopeDelegate
    Private Shared instance As CImportScopeDelegate

    Public Shared Function getInstance() As CImportScopeDelegate
        If IsNothing(instance) Then
            instance = New CImportScopeDelegate
        End If
        Return instance
    End Function
    'list of items
    Private ac_reg As List(Of String)
    Private ac_master As List(Of String)
    Private ac_group As List(Of String)
    Private proj_list As List(Of String)
    Private proj_desc As List(Of String)
    Private actList As Dictionary(Of String, CImportScopeObject)

    Private cnt_zimro_lines As Long
    Private cnt_added As Long
    Private cnt_updated As Long
    Private cnt_deleted As Long
    Private cnt_constant As Long
    Private cnt_initial As Long

    Private cnt_zimro_lines_op As Long
    Private cnt_added_op As Long
    Private cnt_updated_op As Long
    Private cnt_deleted_op As Long
    Private cnt_constant_op As Long
    Private cnt_initial_op As Long

    Dim newActListToInsert As List(Of CScopeActivity)
    Dim newSoldHrsListToInsert As List(Of CSoldHours)

    Dim progressForm As FormProgressBar

    Public activitiesToUpdateList As New List(Of CScopeActivity)

    'log message
    Delegate Sub safeCallDelegate(message As String)
    Private Sub appendLogMessage(message As String)
        Try
            If progressForm.final_message_txt.InvokeRequired Then
                Dim deleg As New safeCallDelegate(AddressOf appendLogMessage)
                progressForm.final_message_txt.Invoke(deleg, New Object() {message})
            Else
                progressForm.final_message_txt.AppendText(message & Environment.NewLine & Environment.NewLine)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub launchScopeImport()
        Dim dialogRes As DialogResult
        Dim filepath As String = ""
        activitiesToUpdateList.Clear()
        cnt_zimro_lines = 0
        cnt_added = 0
        cnt_updated = 0
        cnt_deleted = 0
        cnt_constant = 0
        cnt_zimro_lines_op = 0
        cnt_added_op = 0
        cnt_updated_op = 0
        cnt_deleted_op = 0
        cnt_constant_op = 0
        cnt_initial = 0
        cnt_initial_op = 0

        newActListToInsert = New List(Of CScopeActivity)
        newSoldHrsListToInsert = New List(Of CSoldHours)
        Try

            GLB_MSTR_CTRL.statusBar.status_strip_main_label = "Import SAP Scope..."
            'open file dialog
            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Select ZIMRO file..."
            GLB_MSTR_CTRL.mainForm.open_file_dialog.AddExtension = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckFileExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckPathExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Multiselect = False
            GLB_MSTR_CTRL.mainForm.open_file_dialog.DereferenceLinks = True
            'filter file types
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Filter = "Excel Documents|*" & MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value

            dialogRes = GLB_MSTR_CTRL.mainForm.open_file_dialog.ShowDialog()

            If dialogRes = DialogResult.OK Then
                'check file extention
                filepath = GLB_MSTR_CTRL.mainForm.open_file_dialog.FileName
                'if file is mhtml
                If Not filepath.EndsWith(MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value, StringComparison.OrdinalIgnoreCase) Then
                    MsgBox("The file you selected is not a " & MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value & " file", MsgBoxStyle.Critical, "Wrong file type")
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("An error occured when intializing the import" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical, "Error when picking ZIMRO file")
            Exit Sub
        End Try

        Dim xlHandler As CDBHandler = Nothing
        Try
            Dim conn As String
            Dim listO As List(Of Object)
            Dim warnMess As String = ""
            'block calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = True

            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Load ZIMRO file..."
            'get file and query data
            conn = MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_CONN_STR).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, filepath)
            xlHandler = New CDBHandler(conn, filepath)
            listO = xlHandler.performSelectObject(GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_SCOPE_IMPORT).getExcelDefaultSelect, GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_SCOPE_IMPORT), True)

            'check data consistency
            Dim checkResult As KeyValuePair(Of Boolean, String)
            checkResult = checkDataConsistency(listO)
            If checkResult.Key Then
                MsgBox(checkResult.Value, MsgBoxStyle.Critical, "Cannot load data from the file")
                Exit Sub
            End If

            'if first load skip, if not and if a new project is added, ask user to confirm.
            'use ac_reg not project for this check
            If Not String.IsNullOrWhiteSpace(GLB_MSTR_CTRL.project_controller.project.ac_reg) Then
                For Each proj As String In proj_list
                    If Not (GLB_MSTR_CTRL.project_controller.project.project_list.Contains(proj)) Then
                        warnMess = warnMess & Chr(10) & proj
                    End If
                Next
            End If
            If Not String.IsNullOrWhiteSpace(warnMess) Then
                dialogRes = MessageBox.Show("The following below listed projects are not part of current customer statement. Do you want to add them ? " & Chr(10) & "Click OK to add, or Cancel to leave the import." & warnMess, "Confirm new project import", MessageBoxButtons.OKCancel)
                If dialogRes = DialogResult.Cancel Then
                    Exit Sub
                End If
            End If
            'new form
            progressForm = New FormProgressBar

            'if ok, continue import
            loadData()

            'add items to list. activity first the hours
            MConstants.GLB_MSTR_CTRL.csDB.items_to_be_inserted.AddRange(newActListToInsert)
            MConstants.GLB_MSTR_CTRL.csDB.items_to_be_inserted.AddRange(newSoldHrsListToInsert)

            'save in db
            Dim arg_save As CSaveWorkerArgs
            arg_save = CSaveWorkerArgs.getInsertAll
            arg_save.progBar = progressForm.progress_bar_new
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)
            'updated items.
            arg_save = CSaveWorkerArgs.getUpdateDeleteAll
            arg_save.progBar = progressForm.progress_bar_update
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)

            appendLogMessage("ZIMRO Import successfully done !" & Chr(10) & Chr(10))
            appendLogMessage("ZIMRO File_ Activities=" & cnt_zimro_lines & ", Operations=" & cnt_zimro_lines_op)
            appendLogMessage("Initial_______ Activities=" & cnt_initial & ", Operations=" & cnt_initial_op)
            appendLogMessage("Added______ Activities=" & cnt_added & ", Operations=" & cnt_added_op)
            appendLogMessage("Updated____ Activities=" & cnt_updated & ", Operations=" & cnt_updated_op)
            appendLogMessage("Unchanged_ Activities=" & cnt_constant & ", Operations=" & cnt_constant_op)
            appendLogMessage("Not in SAP__ Activities=" & (cnt_initial - cnt_updated - cnt_constant) & ", Operations=" & (cnt_initial_op - cnt_updated_op - cnt_constant_op))

            progressForm.ShowDialog()
            progressForm.BringToFront()

            'refresh view
            GLB_MSTR_CTRL.add_scope_controller.scopeList.Refresh()

            'update lists
            'first set sap import as done
            MConstants.GLOB_SAP_IMPOT_RUNNING = False
            For Each my_act As CScopeActivity In activitiesToUpdateList
                my_act.updateCal()
            Next
            activitiesToUpdateList.Clear()
        Catch ex As Exception
            MsgBox("An error occured when reading data from ZIMRO file. The file might not be in the right template, please check and try again" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical, "Error when loading ZIMRO file")
        Finally
            'allow calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = False
            If Not IsNothing(xlHandler) Then
                Try
                    xlHandler.closeConn()
                Catch ex As Exception
                End Try
            End If
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_main_label = "END Import SAP Scope"
    End Sub

    Private Function checkDataConsistency(listO As List(Of Object)) As KeyValuePair(Of Boolean, String)

        Dim errMessage As String
        Dim is_error As Boolean = False
        Dim main_act_imp As CImportScopeObject
        Dim main_soldHrs_imp As CImportScopeObject

        actList = New Dictionary(Of String, CImportScopeObject)
        ac_reg = New List(Of String)
        ac_master = New List(Of String)
        ac_group = New List(Of String)
        proj_list = New List(Of String)
        proj_desc = New List(Of String)
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Check SAP Data consistency ..."

        Try
            For Each act_imp As CImportScopeObject In listO
                'get ac reg
                If Not ac_reg.Contains(act_imp.ac_reg) Then
                    ac_reg.Add(act_imp.ac_reg)
                End If
                'get ac master
                If Not ac_master.Contains(act_imp.ac_master) Then
                    ac_master.Add(act_imp.ac_master)
                End If
                'get ac master
                If Not ac_group.Contains(act_imp.manufacturer) Then
                    ac_group.Add(act_imp.manufacturer)
                End If
                'get proj
                If Not proj_list.Contains(act_imp.network) Then
                    proj_list.Add(act_imp.network)
                    proj_desc.Add(act_imp.network_description)
                End If

                'convert to Hierarchy structure

                'handle activity
                If Not actList.ContainsKey(act_imp.getActKey) Then
                    actList.Add(act_imp.getActKey, act_imp)
                    main_act_imp = act_imp
                    cnt_zimro_lines = cnt_zimro_lines + 1
                Else
                    main_act_imp = actList(act_imp.getActKey)
                End If

                'corrective actions
                Dim corrAct As String = act_imp.getCorrectiveAction
                If Not main_act_imp.correctiveActionList.Contains(corrAct) Then
                    main_act_imp.correctiveActionList.Add(corrAct)
                End If

                'handle cc
                If Not main_act_imp.cc_list.ContainsKey(act_imp.work_center) Then
                    act_imp.calculated_planned_hrs = act_imp.work
                    act_imp.calculated_operation_desc = act_imp.getOperationDesc
                    main_act_imp.cc_list.Add(act_imp.work_center, act_imp)
                    'count
                    cnt_zimro_lines_op = cnt_zimro_lines_op + 1
                Else
                    'add data
                    main_soldHrs_imp = main_act_imp.cc_list(act_imp.work_center)
                    main_soldHrs_imp.calculated_planned_hrs = main_soldHrs_imp.calculated_planned_hrs + act_imp.work
                    main_soldHrs_imp.calculated_operation_desc = main_soldHrs_imp.calculated_operation_desc & Chr(10) & act_imp.getOperationDesc
                End If
            Next

            'free memory
            listO.Clear()
            listO = Nothing
            errMessage = "Scope upload failed. Error details : "

            'check that there is only one a/c registration
            If ac_reg.Count <> 1 Then
                errMessage = errMessage & Chr(10) & "-The SAP extract file shall contains only 1 A/C Registration. The current file contains " & ac_reg.Count & "values: " & String.Join(MConstants.SEP_COMMA, ac_reg.ToArray)
                is_error = True
            Else
                If Not ac_reg.Item(0) = GLB_MSTR_CTRL.project_controller.project.ac_reg And Not String.IsNullOrWhiteSpace(GLB_MSTR_CTRL.project_controller.project.ac_reg) Then
                    Dim dialogRes As DialogResult
                    dialogRes = MessageBox.Show("The A/C Registration in the SAP extract file (" & ac_reg.Item(0) & ") is different from the A/C Registration in the current customer statement (" & GLB_MSTR_CTRL.project_controller.project.ac_reg & ")" & Chr(10) & "Do you want to overwrite the A/C Registration Value ?", "Confirm overwrite of A/C Registration ", MessageBoxButtons.YesNo)
                    If dialogRes = DialogResult.Yes Then
                        GLB_MSTR_CTRL.project_controller.project.ac_reg = ac_reg.Item(0)
                    Else
                        errMessage = errMessage & Chr(10) & "-The A/C Registration in the SAP extract file (" & ac_reg.Item(0) & ") is different from the A/C Registration in the current customer statement (" & GLB_MSTR_CTRL.project_controller.project.ac_reg & ")"
                        is_error = True
                    End If
                End If
            End If

            'check that there is ony only a/c master
            If ac_master.Count <> 1 Then
                errMessage = errMessage & Chr(10) & "-The SAP extract file shall contains only 1 A/C Master. The current file contains " & ac_master.Count & "values: " & String.Join(MConstants.SEP_COMMA, ac_master.ToArray)
                is_error = True
            Else
                If Not ac_master.Item(0) = GLB_MSTR_CTRL.project_controller.project.ac_master And Not String.IsNullOrWhiteSpace(GLB_MSTR_CTRL.project_controller.project.ac_master) Then
                    errMessage = errMessage & Chr(10) & "-The A/C Master in the SAP extract file (" & ac_master.Item(0) & ") is different from the A/C Master in the current customer statement (" & GLB_MSTR_CTRL.project_controller.project.ac_master & ")"
                    is_error = True
                End If
            End If

            'check that there is ony only a/c group
            If ac_group.Count <> 1 Then
                errMessage = errMessage & Chr(10) & "-The SAP extract file shall contains only 1 A/C Group. The current file contains " & ac_group.Count & "values: " & String.Join(MConstants.SEP_COMMA, ac_group.ToArray)
                is_error = True
            Else
                If Not ac_group.Item(0) = GLB_MSTR_CTRL.project_controller.project.ac_group And Not String.IsNullOrWhiteSpace(GLB_MSTR_CTRL.project_controller.project.ac_group) Then
                    errMessage = errMessage & Chr(10) & "-The A/C Group in the SAP extract file (" & ac_group.Item(0) & ") is different from the A/C Group in the current customer statement (" & GLB_MSTR_CTRL.project_controller.project.ac_group & ")"
                    is_error = True
                End If
            End If

            ''check that there is at least one project
            If Not (proj_list.Count > 0 AndAlso Not String.IsNullOrWhiteSpace(proj_list.Item(0))) Then
                errMessage = errMessage & Chr(10) & "-No network (or Project) were found in the SAP Extract"
                is_error = True
            End If

        Catch ex As Exception
            Throw New Exception("An error occured when checking ZIMRO data consistency" & Chr(10) & ex.Message & ex.StackTrace)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Check SAP Data consistency"

        Return New KeyValuePair(Of Boolean, String)(is_error, errMessage)
    End Function

    Public Sub loadData()
        'proj ref
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Merging SAP data with current customer statement data..."
        Try
            Dim proj As CProjectModel = GLB_MSTR_CTRL.project_controller.project

            'if first load, initialize
            If String.IsNullOrWhiteSpace(proj.ac_reg) Then
                proj.ac_reg = ac_reg.Item(0)
                proj.ac_master = ac_master.Item(0)
                proj.ac_group = ac_group.Item(0)
                proj.project_def = proj_list.Item(0)
                proj.project_desc = proj_desc.Item(0)
            End If
            'add projects
            For Each pro_str As String In proj_list
                If Not proj.project_list.Contains(pro_str) Then
                    'use add item function to trigger view update
                    proj.projectListAddItem(pro_str)
                End If
            Next

            'Merge data
            'organize activities in key object mode to ease comparison
            Dim activities As New Dictionary(Of String, CScopeActivity)
            Dim act_not_in_sap_list As New List(Of CScopeActivity)

            'add default activities if not exists
            Dim listDef As Dictionary(Of String, CScopeActivity) = getDefaultActivities(proj_list.Item(0))

            For Each act_ As CScopeActivity In GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource
                'handle items only concerned with the ongoing import
                If proj_list.Contains(act_.proj) AndAlso Not act_.is_cost_collector Then
                    If Not activities.ContainsKey(act_.getActKey) Then
                        activities.Add(act_.getActKey, act_)
                        'log count
                        cnt_initial = cnt_initial + 1
                        cnt_initial_op = cnt_initial_op + act_.sold_hour_list.Count
                        act_not_in_sap_list.Add(act_)
                        'act_.reset_revision_control()
                    End If
                ElseIf act_.is_cost_collector AndAlso listDef.ContainsKey(act_.getActKey) Then
                    listDef.Remove(act_.getActKey)
                End If
            Next

            'add default activities if not exists
            For Each _act As CScopeActivity In listDef.Values
                If Not activities.ContainsKey(_act.getActKey) Then
                    GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource.Add(_act)
                    cnt_added = cnt_added + 1
                    'add to list of items to be created in db
                    If Not newActListToInsert.Contains(_act) Then
                        newActListToInsert.Add(_act)
                    End If
                End If
            Next

            Dim act As CScopeActivity
            Dim soldH As CSoldHours
            Dim sldHrsList As Dictionary(Of String, CSoldHours)
            For Each import As CImportScopeObject In actList.Values
                Dim isNewAct As Boolean = False

                'handle activity update
                If Not activities.ContainsKey(import.getActKey) Then
                    act = import.getActivity
                    act.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NEW
                    act.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & act.revision_control_status
                    'add to view
                    GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource.Add(act)
                    cnt_added = cnt_added + 1
                    isNewAct = True
                    'add to list of items to be created in db
                    If Not newActListToInsert.Contains(act) Then
                        newActListToInsert.Add(act)
                    End If
                Else
                    act = activities(import.getActKey)
                    'update only items that have not yet been updated, because one activity is repeated if there are several cost centers
                    If (act.UpdateFromSAP(import)) Then
                        cnt_updated = cnt_updated + 1
                    Else
                        cnt_constant = cnt_constant + 1
                    End If

                    'remove from items not in sap
                    If act_not_in_sap_list.Contains(act) Then
                        act_not_in_sap_list.Remove(act)
                    End If
                End If

                sldHrsList = act.getSAPActivities()

                'list to track items not in sap
                Dim soldHoursNotInSAP As New List(Of CSoldHours)(sldHrsList.Values.ToList)

                'new cc to add
                Dim newCCAddList As New List(Of CImportScopeObject)
                'cc not in sap
                Dim ccNotInSAP As List(Of CSoldHours) = getUnusedSoldHr(sldHrsList.Values.ToList)

                'handle work centers
                For Each hrsimport As CImportScopeObject In import.cc_list.Values
                    If sldHrsList.ContainsKey(hrsimport.work_center) Then
                        soldH = sldHrsList(hrsimport.work_center)
                        If soldH.UpdateFromSAP(hrsimport) Then
                            cnt_updated_op = cnt_updated_op + 1
                        Else
                            cnt_constant_op = cnt_constant_op + 1
                        End If
                        'to keep only items not in sap
                        If ccNotInSAP.Contains(soldH) Then
                            ccNotInSAP.Remove(soldH)
                        End If

                        'remove from hours not in sap
                        If soldHoursNotInSAP.Contains(soldH) Then
                            soldHoursNotInSAP.Remove(soldH)
                        End If
                    Else
                        'will be added after loop end
                        newCCAddList.Add(hrsimport)
                    End If
                Next

                'handle new hs import
                'really new item or has just been replaced
                For Each hrsimport As CImportScopeObject In newCCAddList
                    'check if free cc not in sap and unused
                    If ccNotInSAP.Count > 0 Then
                        soldH = ccNotInSAP(0)
                        soldH.UpdateCCReplaceFromSAP(hrsimport)
                        cnt_updated_op = cnt_updated_op + 1
                        ccNotInSAP.Remove(soldH)
                        'remove from items not in sap
                        If soldHoursNotInSAP.Contains(soldH) Then
                            soldHoursNotInSAP.Remove(soldH)
                        End If
                    Else
                        'new item
                        soldH = hrsimport.getSoldHour(act)
                        soldH.is_sap_import = True
                        soldH.is_reported = False
                        soldH.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NEW
                        soldH.revision_control_txt = DateTime.Now.ToString("dd/MM/yy hh:mm:ss") & " " & soldH.revision_control_status

                        'log at activity level
                        act.logRevisionControlText("New CC Added " & soldH.cc)
                        If Not isNewAct Then
                            act.revision_control_status = CONST_REV_CONTROL_STATUS_UPDATED
                        End If

                        act.sold_hour_list.Add(soldH)
                        cnt_added_op = cnt_added_op + 1
                        'add to list of items to be created in db
                        If Not newSoldHrsListToInsert.Contains(soldH) Then
                            newSoldHrsListToInsert.Add(soldH)
                        End If
                    End If
                Next
                'log items not in sap
                For Each noSAPSoldH As CSoldHours In soldHoursNotInSAP
                    noSAPSoldH.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
                    noSAPSoldH.logRevisionControlText("Cost center " & noSAPSoldH.cc & " Removed from SAP")
                    noSAPSoldH.act_object.logRevisionControlText("Cost center " & noSAPSoldH.cc & " Removed from SAP")
                Next
                If isNewAct Then
                    activitiesToUpdateList.Add(act)
                End If
            Next

            'log items not in sap
            For Each noSAPAct As CScopeActivity In act_not_in_sap_list
                noSAPAct.revision_control_status = MConstants.CONST_REV_CONTROL_STATUS_NOSAP
                noSAPAct.logRevisionControlText("Actitity Not Found in SAP")
            Next

        Catch ex As Exception
            Throw New Exception("An error occured while mergin SAP data with customer statement data" & Chr(10) & ex.Message & ex.StackTrace)
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "END Merging SAP data with current customer statement data"
    End Sub

    Public Function getUnusedSoldHr(list As List(Of CSoldHours)) As List(Of CSoldHours)
        Dim res As New List(Of CSoldHours)
        For Each item In list
            If Not item.is_reported Then
                res.Add(item)
            End If
        Next
        Return res
    End Function

    'during the first load create some standard activities
    Public Function getDefaultActivities(proj As String) As Dictionary(Of String, CScopeActivity)
        Dim listA As New Dictionary(Of String, CScopeActivity)
        'supervisor hours
        Dim supAct As New CScopeActivity
        supAct.proj = proj
        supAct.status = "CLSD"
        supAct.job_card = "0000"
        supAct.act = MConstants.SUPV_ACT_NUMBER
        supAct.act_desc = "SPV Hrs budget iaw SGG Quote"
        supAct.act_long_desc = "Cost collector used to control Supervision hours"
        supAct.is_cost_collector = True
        supAct.notification = MConstants.SUPV_ACT_NUMBER
        supAct.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY).value
        listA.Add(supAct.getActKey, supAct)

        'all project hours
        Dim allAct As New CScopeActivity
        allAct.proj = proj
        allAct.status = "CLSD"
        allAct.job_card = "0000"
        allAct.act = MConstants.ALL_PROJ_ACT_NUMBER
        allAct.act_desc = "Project level actual hours collector"
        allAct.act_long_desc = "Cost collector used to control any other hours not part of the WBS structure"
        allAct.is_cost_collector = True
        allAct.notification = MConstants.ALL_PROJ_ACT_NUMBER
        allAct.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_NOT_ADD_V_KEY).value
        listA.Add(allAct.getActKey, allAct)
        Return listA
    End Function

End Class
