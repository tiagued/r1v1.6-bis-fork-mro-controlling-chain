﻿Public Class CLaborControlActFilterObject

    Private _act_obj As CScopeActivity
    Public Property act_obj() As CScopeActivity
        Get
            Return _act_obj
        End Get
        Set(ByVal value As CScopeActivity)
            _act_obj = value
        End Set
    End Property

    Private _is_virtual As Boolean
    Public Property is_virtual() As Boolean
        Get
            Return _is_virtual
        End Get
        Set(ByVal value As Boolean)
            _is_virtual = value
        End Set
    End Property

    Public ReadOnly Property display As String
        Get
            If _is_virtual Then
                Return "*" & _act_obj.tostring()
            ElseIf Object.Equals(_act_obj, MConstants.LABOR_CTRL_SELECT_ALL_ACT) Then
                'space is the first ever ansi char in filtering order
                Return "* " & _act_obj.act
            Else
                Return _act_obj.tostring()
            End If
        End Get
    End Property

    Public Function getKey() As String
        If Object.Equals(_act_obj, MConstants.LABOR_CTRL_SELECT_ALL_ACT) Then
            Return "all_act"
        Else
            Return _act_obj.id & MConstants.SEP_DEFAULT & _is_virtual
        End If
    End Function

    Public Shared Function getKey(ctrlObj As CLaborControlMainViewObj) As String

        If Object.Equals(ctrlObj.act_obj, MConstants.LABOR_CTRL_SELECT_ALL_ACT) Then
            Return "all_act"
        Else
            Return ctrlObj.act_obj.id & MConstants.SEP_DEFAULT & ctrlObj.is_virtual_cost_node
        End If
    End Function

    Public Shared Function getAllSelected() As CLaborControlActFilterObject
        Dim res As New CLaborControlActFilterObject
        res.act_obj = MConstants.LABOR_CTRL_SELECT_ALL_ACT
        Return res
    End Function

    Public Shared Function getNewObj(ctrlObj As CLaborControlMainViewObj) As CLaborControlActFilterObject
        Dim res As New CLaborControlActFilterObject
        res.act_obj = ctrlObj.act_obj
        res.is_virtual = ctrlObj.is_virtual_cost_node
        Return res
    End Function

End Class
