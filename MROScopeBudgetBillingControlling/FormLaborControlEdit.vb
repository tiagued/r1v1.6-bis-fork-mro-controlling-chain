﻿Public Class FormLaborControlEdit
    'navigate with arrows
    Private Sub act_cc_select_left_pic_Click(sender As Object, e As EventArgs) Handles act_cc_select_left_pic.Click
        GLB_MSTR_CTRL.labor_controlling_controller_edit.selectedObjectBackWard()
    End Sub
    'navigate with arrows
    Private Sub act_cc_select_right_pic_Click(sender As Object, e As EventArgs) Handles act_cc_select_right_pic.Click
        GLB_MSTR_CTRL.labor_controlling_controller_edit.selectedObjectForward()
    End Sub

    'object change event
    Private Sub cb_act_cc_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_act_cc.SelectedIndexChanged
        GLB_MSTR_CTRL.labor_controlling_controller_edit.selectedCtrObjChanged()
    End Sub

    'activity error text
    Private Sub activity_error_state_pic_Click(sender As Object, e As EventArgs) Handles act_cc_error_state_pic.Click
        MsgBox(act_cc_error_state_pic.Tag)
    End Sub

    Private Sub add_scope_main_datagrid_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles budget_transfer_to_dgrid.DataError
        sender = sender
        e = e
    End Sub
End Class