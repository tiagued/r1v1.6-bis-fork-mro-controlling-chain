﻿Module MGeneralFunctionsModelViewMap

    Public Function fillModelViewDict(mapList As List(Of Object)) As Dictionary(Of String, CViewModelMapList)
        Dim mapO As CViewModelMap
        Dim res As New Dictionary(Of String, CViewModelMapList)
        Dim mapOList As CViewModelMapList
        Dim key As String
        Try
            For Each obj As Object In mapList
                mapO = CType(obj, CViewModelMap)
                If Not res.Keys.Contains(mapO.func) Then
                    mapOList = New CViewModelMapList(mapO.func, mapO.key, mapO.db_table, mapO.class_name)
                    res.Add(mapOList.func, mapOList)
                Else
                    mapOList = res.Item(mapO.func)
                End If
                key = CallByName(mapO, mapOList.list_key, CallType.Get)
                mapOList.list.Add(key, mapO)
            Next
        Catch ex As Exception
            Throw New Exception("Error occured while organizing view model map from lst view to dictionnary view" & Chr(10) & ex.Message & ex.StackTrace)
        End Try

        Return res
    End Function

End Module
