﻿Public Class CCostCenter
    Implements IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewRecordable

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _cc = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
    End Function

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return True
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_CC_DB_READ
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(False, "")
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
    End Sub

    'get all cc,use for sum
    Public Shared Function getAllCC() As CCostCenter
        'all cc
        Dim all_cc As New CCostCenter
        all_cc.cc = "All CC"
        all_cc.description = "Sum of all cost centers"
        all_cc.friendly_description = "Sum of all cost centers"
        Return all_cc
    End Function

    'get all cc, use for GUI 
    Public Shared Function getSelectAllCC() As CCostCenter
        'all cc
        Dim all_cc As New CCostCenter
        all_cc.cc = "*Select All"
        Return all_cc
    End Function

    Private _id As Long
    Public Property id() As Long
        Get
            Return _id
        End Get
        Set(ByVal value As Long)
            If Not _id = value Then
                _id = value
            End If
        End Set
    End Property

    Private _cc As String
    Public Property cc() As String
        Get
            Return _cc
        End Get
        Set(ByVal value As String)
            If Not _cc = value Then
                _cc = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _label As String
    Public Property label() As String
        Get
            Return _label
        End Get
        Set(ByVal value As String)
            If Not _label = value Then
                _label = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _friendly_description As String
    Public Property friendly_description() As String
        Get
            Return _friendly_description
        End Get
        Set(ByVal value As String)
            If Not _friendly_description = value Then
                _friendly_description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _grouping As String
    Public Property grouping() As String
        Get
            Return _grouping
        End Get
        Set(ByVal value As String)
            If Not _grouping = value Then
                _grouping = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _sap_id As String
    Public Property sap_id() As String
        Get
            Return _sap_id
        End Get
        Set(ByVal value As String)
            If Not _sap_id = value Then
                _sap_id = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _fr_id As String
    Public Property fr_id() As String
        Get
            Return _fr_id
        End Get
        Set(ByVal value As String)
            If Not _fr_id = value Then
                _fr_id = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "CC: " & _label & ", SAP ID: " & _sap_id & ", FR ID: " & _fr_id
    End Function

    'to string
    Public ReadOnly Property cc_desc_view() As String
        Get
            Return _cc & " " & _friendly_description
        End Get
    End Property
End Class

