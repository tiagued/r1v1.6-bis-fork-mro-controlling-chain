﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.pan_mainf_top_menu = New System.Windows.Forms.Panel()
        Me.top_menu_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.top_menu_tab_ctrl = New System.Windows.Forms.TabControl()
        Me.top_menu_tab_home_page = New System.Windows.Forms.TabPage()
        Me.top_menu_tab_home_page_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.top_menu_home_tab_info_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_home_tab_info_lbl = New System.Windows.Forms.Label()
        Me.top_menu_tab_import_page = New System.Windows.Forms.TabPage()
        Me.top_menu_import_page_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.top_menu_import_zimro_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_import_zimro_lbl = New System.Windows.Forms.Label()
        Me.top_menu_import_CN47N_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_import_zmel_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_item_import_cost_coll = New System.Windows.Forms.Label()
        Me.top_menu_import_zmel_lbl = New System.Windows.Forms.Label()
        Me.top_menu_import_CIJ3_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_import_actuals = New System.Windows.Forms.Label()
        Me.top_menu_tab_report_page = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.top_menu_data_check_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_data_check_bt_lbl = New System.Windows.Forms.Label()
        Me.top_menu_customer_release_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_customer_release_bt_lbl = New System.Windows.Forms.Label()
        Me.top_menu_hours_stat_bt_lbl = New System.Windows.Forms.Label()
        Me.top_menu_hours_stat_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_tab_admin_page = New System.Windows.Forms.TabPage()
        Me.admin_menu_tab_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.top_menu_admin_bt = New System.Windows.Forms.PictureBox()
        Me.top_menu_admin_bt_lbl = New System.Windows.Forms.Label()
        Me.top_menu_left_layout = New System.Windows.Forms.FlowLayoutPanel()
        Me.top_menu_save = New System.Windows.Forms.PictureBox()
        Me.top_menu_save_status = New System.Windows.Forms.PictureBox()
        Me.pan_mainf_down_info = New System.Windows.Forms.Panel()
        Me.DownStatusStrip = New System.Windows.Forms.StatusStrip()
        Me.status_strip_main_label = New System.Windows.Forms.ToolStripStatusLabel()
        Me.status_strip_sub_label = New System.Windows.Forms.ToolStripStatusLabel()
        Me.status_strip_progress_bar = New System.Windows.Forms.ToolStripProgressBar()
        Me.status_strip_latest_update = New System.Windows.Forms.ToolStripStatusLabel()
        Me.status_strip_dgrid_selected_sum = New System.Windows.Forms.ToolStripStatusLabel()
        Me.calculation_label = New System.Windows.Forms.ToolStripStatusLabel()
        Me.calculation_progress_bar = New System.Windows.Forms.ToolStripProgressBar()
        Me.tab_ctrl_main = New System.Windows.Forms.TabControl()
        Me.tab_main_page_config = New System.Windows.Forms.TabPage()
        Me.tab_ctrl_config = New System.Windows.Forms.TabControl()
        Me.tab_ctrl_config_page_proj = New System.Windows.Forms.TabPage()
        Me.conf_project_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_prj_det_cust_email_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_cust_email = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_cust_rep_name_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_cust_rep_name = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_awq_specialist = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_acgroup_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_projdef = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_bill_grp_val = New System.Windows.Forms.ComboBox()
        Me.lbl_cnf_prj_det_acgroup = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_acrmast_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_acmast = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_acreg = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_acreg_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_projlist_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_projlist = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_projdesc_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_projdesc = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_curr_in_date = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_curr_fcst_date = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_cust_po = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_cust_po_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_quot_nr_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_quot_nr = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_cust = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_bill_grp = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_work_accpt_form = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_cust_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_work_accpt_form_val = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_ac_flight_hrs = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_ac_flight_cycl = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_ac_flight_hrs_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_ac_flight_cycl_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_dass_sales_or_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_dass_sales_or = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_curr_in_date_val = New System.Windows.Forms.DateTimePicker()
        Me.lbl_cnf_prj_det_curr_fcst_date_val = New System.Windows.Forms.DateTimePicker()
        Me.lbl_cnf_prj_det_projdef_val = New System.Windows.Forms.TextBox()
        Me.awq_specialist_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_prj_det_awq_specialist_email_val = New System.Windows.Forms.TextBox()
        Me.lbl_cnf_prj_det_awq_specialist_email = New System.Windows.Forms.Label()
        Me.lbl_cnf_prj_det_awq_specialist_val = New System.Windows.Forms.TextBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.tab_ctrl_config_page_billing = New System.Windows.Forms.TabPage()
        Me.tab_ctrl_config_page_bill_flow_panel = New System.Windows.Forms.FlowLayoutPanel()
        Me.conf_bill_curr_grp = New System.Windows.Forms.GroupBox()
        Me.conf_bill_exchg_rate_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_exchg_rate_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_curr_layount = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_bill_tagk_date = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_lab_curr = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_mat_curr = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_rep_curr = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_3party_curr = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_freight_curr = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_lab_curr_val = New System.Windows.Forms.ComboBox()
        Me.lbl_cnf_bill_mat_curr_val = New System.Windows.Forms.ComboBox()
        Me.lbl_cnf_bill_rep_curr_val = New System.Windows.Forms.ComboBox()
        Me.lbl_cnf_bill_3party_curr_val = New System.Windows.Forms.ComboBox()
        Me.lbl_cnf_bill_freight_curr_val = New System.Windows.Forms.ComboBox()
        Me.lbl_cnf_bill_tagk_date_val = New System.Windows.Forms.DateTimePicker()
        Me.conf_bill_markup_grp = New System.Windows.Forms.GroupBox()
        Me.conf_bill_markup_grp_cap_p_grp = New System.Windows.Forms.GroupBox()
        Me.cb_cnf_bill_markup_cap_proj_curr = New System.Windows.Forms.ComboBox()
        Me.cb_cnf_bill_markup_cap_proj_value = New System.Windows.Forms.TextBox()
        Me.conf_bill_markup_grp_cap_l_grp = New System.Windows.Forms.GroupBox()
        Me.cb_cnf_bill_markup_cap_line_curr = New System.Windows.Forms.ComboBox()
        Me.cb_cnf_bill_markup_cap_line_value = New System.Windows.Forms.TextBox()
        Me.chk_b_conf_bill_markup_is_proj_cap = New System.Windows.Forms.CheckBox()
        Me.chk_b_conf_bill_markup_is_line_cap = New System.Windows.Forms.CheckBox()
        Me.conf_bill_mat_markup_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_mat_markup_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_markup_layount = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_bill_markup = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_markup_val = New System.Windows.Forms.ComboBox()
        Me.conf_bill_freight_grp = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_bill_freight_rate = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_freight_rate_val = New System.Windows.Forms.TextBox()
        Me.conf_bill_freight_grp_cap_p_grp = New System.Windows.Forms.GroupBox()
        Me.cb_cnf_bill_freight_cap_proj_curr = New System.Windows.Forms.ComboBox()
        Me.cb_cnf_bill_freight_cap_proj_value = New System.Windows.Forms.TextBox()
        Me.conf_bill_freight_grp_cap_l_grp = New System.Windows.Forms.GroupBox()
        Me.cb_cnf_bill_freight_cap_line_curr = New System.Windows.Forms.ComboBox()
        Me.cb_cnf_bill_freight_cap_line_value = New System.Windows.Forms.TextBox()
        Me.chk_b_conf_bill_freight_is_proj_cap = New System.Windows.Forms.CheckBox()
        Me.chk_b_conf_bill_freight_is_line_cap = New System.Windows.Forms.CheckBox()
        Me.conf_bill_mat_freight_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_mat_freight_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_surcharge_grp = New System.Windows.Forms.GroupBox()
        Me.conf_bill_surch_eco_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_surch_eco_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_surch_eco_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_bill_eco = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_eco_val = New System.Windows.Forms.ComboBox()
        Me.conf_bill_surch_gum_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_surch_gum_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_surch_gum_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_cnf_bill_gum = New System.Windows.Forms.Label()
        Me.lbl_cnf_bill_gum_val = New System.Windows.Forms.ComboBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.conf_bill_down_pay_grp = New System.Windows.Forms.GroupBox()
        Me.conf_bill_down_payment_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_down_payment_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_discount_grp = New System.Windows.Forms.GroupBox()
        Me.conf_bill_discount_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_discount_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_nte_grp = New System.Windows.Forms.GroupBox()
        Me.conf_bill_nte_grid_p = New System.Windows.Forms.Panel()
        Me.conf_bill_nte_grid = New System.Windows.Forms.DataGridView()
        Me.conf_bill_top_menu_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.conf_bill_menu_downpay_lbl = New System.Windows.Forms.Label()
        Me.conf_bill_menu_surcharge_lbl = New System.Windows.Forms.Label()
        Me.conf_bill_menu_curr_lbl = New System.Windows.Forms.Label()
        Me.conf_bill_menu_freight_lbl = New System.Windows.Forms.Label()
        Me.conf_bill_menu_markup_lbl = New System.Windows.Forms.Label()
        Me.conf_bill_menu_nte_lbl = New System.Windows.Forms.Label()
        Me.conf_bill_menu_discount_lbl = New System.Windows.Forms.Label()
        Me.tab_ctrl_config_page_rates = New System.Windows.Forms.TabPage()
        Me.tab_ctrl_config_page_labrate_panel = New System.Windows.Forms.Panel()
        Me.conf_labrate_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.conf_labrate_task_rate_grp = New System.Windows.Forms.GroupBox()
        Me.conf_labrate_task_rate_grid = New System.Windows.Forms.DataGridView()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.conf_labrate_labrate_std_grp = New System.Windows.Forms.GroupBox()
        Me.conf_labrate_labrate_std_grid = New System.Windows.Forms.DataGridView()
        Me.conf_labrate_std_choice_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl_conf_labrate_std_choice = New System.Windows.Forms.Label()
        Me.lbl_conf_labrate_std_choice_val = New System.Windows.Forms.ComboBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.conf_labrate_labrate_oem_grp = New System.Windows.Forms.GroupBox()
        Me.conf_labrate_labrate_oem_grid = New System.Windows.Forms.DataGridView()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.conf_labrate_labrate_nte_grp = New System.Windows.Forms.GroupBox()
        Me.conf_labrate_labrate_nte_grid = New System.Windows.Forms.DataGridView()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.conf_lab_rate_top_menu_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.conf_labrate_menu_task_rate_lbl = New System.Windows.Forms.Label()
        Me.conf_labrate_menu_std_lbl = New System.Windows.Forms.Label()
        Me.conf_labrate_menu_nte_lbl = New System.Windows.Forms.Label()
        Me.conf_labrate_menu_oem_lbl = New System.Windows.Forms.Label()
        Me.tab_ctrl_config_page_editable_lov = New System.Windows.Forms.TabPage()
        Me.conf_lov_main_panel = New System.Windows.Forms.Panel()
        Me.conf_lov_main_ly = New System.Windows.Forms.FlowLayoutPanel()
        Me.conf_lov_edit_group = New System.Windows.Forms.GroupBox()
        Me.conf_lov_edit_datagrid = New System.Windows.Forms.DataGridView()
        Me.conf_lov_edit_lov_selection_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.conf_lov_edit_lov_selected_lbl = New System.Windows.Forms.Label()
        Me.conf_lov_edit_lov_selected_val = New System.Windows.Forms.ComboBox()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.tab_ctrl_config_page_lov_top_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.conf_lov_edit_menu = New System.Windows.Forms.Label()
        Me.tab_main_page_init_scope = New System.Windows.Forms.TabPage()
        Me.tab_main_page_init_scope_p = New System.Windows.Forms.Panel()
        Me.init_scope_main_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.init_scope_menu_quots_grp = New System.Windows.Forms.GroupBox()
        Me.init_scope_quotes_grid = New System.Windows.Forms.DataGridView()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.init_scope_menu_pricing_sum_grp = New System.Windows.Forms.GroupBox()
        Me.init_scope_pricing_sum_grid = New System.Windows.Forms.DataGridView()
        Me.init_scope_top_menu_layout = New System.Windows.Forms.TableLayoutPanel()
        Me.init_scope_menu_pricing_sum = New System.Windows.Forms.Label()
        Me.init_scope_menu_quots = New System.Windows.Forms.Label()
        Me.tab_main_page_add_scope = New System.Windows.Forms.TabPage()
        Me.add_scope_main_panel = New System.Windows.Forms.Panel()
        Me.add_scope_main_datagrid = New System.Windows.Forms.DataGridView()
        Me.add_scope_top_panel = New System.Windows.Forms.Panel()
        Me.tab_main_page_materials = New System.Windows.Forms.TabPage()
        Me.material_main_panel = New System.Windows.Forms.Panel()
        Me.material_main_dgrid = New System.Windows.Forms.DataGridView()
        Me.mat_page_top_panel = New System.Windows.Forms.Panel()
        Me.tab_main_page_awq = New System.Windows.Forms.TabPage()
        Me.awq_main_panel = New System.Windows.Forms.Panel()
        Me.awq_main_dgrid = New System.Windows.Forms.DataGridView()
        Me.awq_top_panel = New System.Windows.Forms.Panel()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.new_awq_no_activity = New System.Windows.Forms.Button()
        Me.awq_mass_closure_bt = New System.Windows.Forms.Button()
        Me.tab_main_proj_ctrl = New System.Windows.Forms.TabPage()
        Me.proj_ctrl_tab_ctrl = New System.Windows.Forms.TabControl()
        Me.proj_ctrl_conf_tab = New System.Windows.Forms.TabPage()
        Me.proj_ctrl_conf_ly = New System.Windows.Forms.FlowLayoutPanel()
        Me.proj_ctrl_transfer_cc_grp = New System.Windows.Forms.GroupBox()
        Me.proj_ctrl_transfer_cc_p = New System.Windows.Forms.Panel()
        Me.proj_ctrl_transfer_cc_dgrid = New System.Windows.Forms.DataGridView()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.proj_ctrl_config_top_ly = New System.Windows.Forms.TableLayoutPanel()
        Me.proj_ctrl_config_top_transfer_cc = New System.Windows.Forms.Label()
        Me.labor_controlling_tab = New System.Windows.Forms.TabPage()
        Me.labor_ctrl_main_panel = New System.Windows.Forms.Panel()
        Me.labor_ctrl_main_dgrid = New System.Windows.Forms.DataGridView()
        Me.labor_ctrl_top_panel = New System.Windows.Forms.Panel()
        Me.open_file_dialog = New System.Windows.Forms.OpenFileDialog()
        Me.pan_mainf_top_menu.SuspendLayout()
        Me.top_menu_layout.SuspendLayout()
        Me.top_menu_tab_ctrl.SuspendLayout()
        Me.top_menu_tab_home_page.SuspendLayout()
        Me.top_menu_tab_home_page_ly.SuspendLayout()
        CType(Me.top_menu_home_tab_info_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_menu_tab_import_page.SuspendLayout()
        Me.top_menu_import_page_ly.SuspendLayout()
        CType(Me.top_menu_import_zimro_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.top_menu_import_CN47N_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.top_menu_import_zmel_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.top_menu_import_CIJ3_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_menu_tab_report_page.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.top_menu_data_check_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.top_menu_customer_release_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.top_menu_hours_stat_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_menu_tab_admin_page.SuspendLayout()
        Me.admin_menu_tab_ly.SuspendLayout()
        CType(Me.top_menu_admin_bt, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.top_menu_left_layout.SuspendLayout()
        CType(Me.top_menu_save, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.top_menu_save_status, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pan_mainf_down_info.SuspendLayout()
        Me.DownStatusStrip.SuspendLayout()
        Me.tab_ctrl_main.SuspendLayout()
        Me.tab_main_page_config.SuspendLayout()
        Me.tab_ctrl_config.SuspendLayout()
        Me.tab_ctrl_config_page_proj.SuspendLayout()
        Me.conf_project_layout.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.awq_specialist_ly.SuspendLayout()
        Me.tab_ctrl_config_page_billing.SuspendLayout()
        Me.tab_ctrl_config_page_bill_flow_panel.SuspendLayout()
        Me.conf_bill_curr_grp.SuspendLayout()
        Me.conf_bill_exchg_rate_grid_p.SuspendLayout()
        CType(Me.conf_bill_exchg_rate_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_curr_layount.SuspendLayout()
        Me.conf_bill_markup_grp.SuspendLayout()
        Me.conf_bill_markup_grp_cap_p_grp.SuspendLayout()
        Me.conf_bill_markup_grp_cap_l_grp.SuspendLayout()
        Me.conf_bill_mat_markup_grid_p.SuspendLayout()
        CType(Me.conf_bill_mat_markup_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_markup_layount.SuspendLayout()
        Me.conf_bill_freight_grp.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.conf_bill_freight_grp_cap_p_grp.SuspendLayout()
        Me.conf_bill_freight_grp_cap_l_grp.SuspendLayout()
        Me.conf_bill_mat_freight_grid_p.SuspendLayout()
        CType(Me.conf_bill_mat_freight_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_surcharge_grp.SuspendLayout()
        Me.conf_bill_surch_eco_grid_p.SuspendLayout()
        CType(Me.conf_bill_surch_eco_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_surch_eco_layout.SuspendLayout()
        Me.conf_bill_surch_gum_grid_p.SuspendLayout()
        CType(Me.conf_bill_surch_gum_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_surch_gum_layout.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_down_pay_grp.SuspendLayout()
        Me.conf_bill_down_payment_grid_p.SuspendLayout()
        CType(Me.conf_bill_down_payment_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_discount_grp.SuspendLayout()
        Me.conf_bill_discount_grid_p.SuspendLayout()
        CType(Me.conf_bill_discount_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_nte_grp.SuspendLayout()
        Me.conf_bill_nte_grid_p.SuspendLayout()
        CType(Me.conf_bill_nte_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_bill_top_menu_layout.SuspendLayout()
        Me.tab_ctrl_config_page_rates.SuspendLayout()
        Me.tab_ctrl_config_page_labrate_panel.SuspendLayout()
        Me.conf_labrate_layout.SuspendLayout()
        Me.conf_labrate_task_rate_grp.SuspendLayout()
        CType(Me.conf_labrate_task_rate_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_labrate_labrate_std_grp.SuspendLayout()
        CType(Me.conf_labrate_labrate_std_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_labrate_std_choice_layout.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_labrate_labrate_oem_grp.SuspendLayout()
        CType(Me.conf_labrate_labrate_oem_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_labrate_labrate_nte_grp.SuspendLayout()
        CType(Me.conf_labrate_labrate_nte_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_lab_rate_top_menu_layout.SuspendLayout()
        Me.tab_ctrl_config_page_editable_lov.SuspendLayout()
        Me.conf_lov_main_panel.SuspendLayout()
        Me.conf_lov_main_ly.SuspendLayout()
        Me.conf_lov_edit_group.SuspendLayout()
        CType(Me.conf_lov_edit_datagrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.conf_lov_edit_lov_selection_ly.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab_ctrl_config_page_lov_top_ly.SuspendLayout()
        Me.tab_main_page_init_scope.SuspendLayout()
        Me.tab_main_page_init_scope_p.SuspendLayout()
        Me.init_scope_main_layout.SuspendLayout()
        Me.init_scope_menu_quots_grp.SuspendLayout()
        CType(Me.init_scope_quotes_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.init_scope_menu_pricing_sum_grp.SuspendLayout()
        CType(Me.init_scope_pricing_sum_grid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.init_scope_top_menu_layout.SuspendLayout()
        Me.tab_main_page_add_scope.SuspendLayout()
        Me.add_scope_main_panel.SuspendLayout()
        CType(Me.add_scope_main_datagrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab_main_page_materials.SuspendLayout()
        Me.material_main_panel.SuspendLayout()
        CType(Me.material_main_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tab_main_page_awq.SuspendLayout()
        Me.awq_main_panel.SuspendLayout()
        CType(Me.awq_main_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.awq_top_panel.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.tab_main_proj_ctrl.SuspendLayout()
        Me.proj_ctrl_tab_ctrl.SuspendLayout()
        Me.proj_ctrl_conf_tab.SuspendLayout()
        Me.proj_ctrl_conf_ly.SuspendLayout()
        Me.proj_ctrl_transfer_cc_grp.SuspendLayout()
        Me.proj_ctrl_transfer_cc_p.SuspendLayout()
        CType(Me.proj_ctrl_transfer_cc_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.proj_ctrl_config_top_ly.SuspendLayout()
        Me.labor_controlling_tab.SuspendLayout()
        Me.labor_ctrl_main_panel.SuspendLayout()
        CType(Me.labor_ctrl_main_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pan_mainf_top_menu
        '
        Me.pan_mainf_top_menu.AutoScroll = True
        Me.pan_mainf_top_menu.Controls.Add(Me.top_menu_layout)
        Me.pan_mainf_top_menu.Dock = System.Windows.Forms.DockStyle.Top
        Me.pan_mainf_top_menu.Location = New System.Drawing.Point(0, 0)
        Me.pan_mainf_top_menu.Margin = New System.Windows.Forms.Padding(0)
        Me.pan_mainf_top_menu.Name = "pan_mainf_top_menu"
        Me.pan_mainf_top_menu.Size = New System.Drawing.Size(1924, 149)
        Me.pan_mainf_top_menu.TabIndex = 0
        '
        'top_menu_layout
        '
        Me.top_menu_layout.AutoScroll = True
        Me.top_menu_layout.ColumnCount = 2
        Me.top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.754482!))
        Me.top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.24552!))
        Me.top_menu_layout.Controls.Add(Me.top_menu_tab_ctrl, 1, 0)
        Me.top_menu_layout.Controls.Add(Me.top_menu_left_layout, 0, 0)
        Me.top_menu_layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.top_menu_layout.Location = New System.Drawing.Point(0, 0)
        Me.top_menu_layout.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_layout.Name = "top_menu_layout"
        Me.top_menu_layout.RowCount = 1
        Me.top_menu_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.top_menu_layout.Size = New System.Drawing.Size(1924, 149)
        Me.top_menu_layout.TabIndex = 1
        '
        'top_menu_tab_ctrl
        '
        Me.top_menu_tab_ctrl.Controls.Add(Me.top_menu_tab_home_page)
        Me.top_menu_tab_ctrl.Controls.Add(Me.top_menu_tab_import_page)
        Me.top_menu_tab_ctrl.Controls.Add(Me.top_menu_tab_report_page)
        Me.top_menu_tab_ctrl.Controls.Add(Me.top_menu_tab_admin_page)
        Me.top_menu_tab_ctrl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.top_menu_tab_ctrl.Location = New System.Drawing.Point(91, 0)
        Me.top_menu_tab_ctrl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_tab_ctrl.Name = "top_menu_tab_ctrl"
        Me.top_menu_tab_ctrl.SelectedIndex = 0
        Me.top_menu_tab_ctrl.Size = New System.Drawing.Size(1833, 149)
        Me.top_menu_tab_ctrl.TabIndex = 1
        '
        'top_menu_tab_home_page
        '
        Me.top_menu_tab_home_page.AutoScroll = True
        Me.top_menu_tab_home_page.Controls.Add(Me.top_menu_tab_home_page_ly)
        Me.top_menu_tab_home_page.Location = New System.Drawing.Point(4, 29)
        Me.top_menu_tab_home_page.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_tab_home_page.Name = "top_menu_tab_home_page"
        Me.top_menu_tab_home_page.Size = New System.Drawing.Size(1825, 116)
        Me.top_menu_tab_home_page.TabIndex = 0
        Me.top_menu_tab_home_page.Text = "HOME"
        Me.top_menu_tab_home_page.UseVisualStyleBackColor = True
        '
        'top_menu_tab_home_page_ly
        '
        Me.top_menu_tab_home_page_ly.ColumnCount = 4
        Me.top_menu_tab_home_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.601318!))
        Me.top_menu_tab_home_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.354201!))
        Me.top_menu_tab_home_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.12685!))
        Me.top_menu_tab_home_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 103.0!))
        Me.top_menu_tab_home_page_ly.Controls.Add(Me.top_menu_home_tab_info_bt, 3, 0)
        Me.top_menu_tab_home_page_ly.Controls.Add(Me.top_menu_home_tab_info_lbl, 3, 1)
        Me.top_menu_tab_home_page_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.top_menu_tab_home_page_ly.Location = New System.Drawing.Point(0, 0)
        Me.top_menu_tab_home_page_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_tab_home_page_ly.Name = "top_menu_tab_home_page_ly"
        Me.top_menu_tab_home_page_ly.RowCount = 2
        Me.top_menu_tab_home_page_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.41558!))
        Me.top_menu_tab_home_page_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.58442!))
        Me.top_menu_tab_home_page_ly.Size = New System.Drawing.Size(1825, 116)
        Me.top_menu_tab_home_page_ly.TabIndex = 2
        '
        'top_menu_home_tab_info_bt
        '
        Me.top_menu_home_tab_info_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_home_tab_info_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_home_tab_info_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.admin_info
        Me.top_menu_home_tab_info_bt.Location = New System.Drawing.Point(1738, 7)
        Me.top_menu_home_tab_info_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_home_tab_info_bt.Name = "top_menu_home_tab_info_bt"
        Me.top_menu_home_tab_info_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_home_tab_info_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_home_tab_info_bt.TabIndex = 4
        Me.top_menu_home_tab_info_bt.TabStop = False
        '
        'top_menu_home_tab_info_lbl
        '
        Me.top_menu_home_tab_info_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_home_tab_info_lbl.AutoSize = True
        Me.top_menu_home_tab_info_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_home_tab_info_lbl.Location = New System.Drawing.Point(1751, 98)
        Me.top_menu_home_tab_info_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_home_tab_info_lbl.Name = "top_menu_home_tab_info_lbl"
        Me.top_menu_home_tab_info_lbl.Size = New System.Drawing.Size(43, 16)
        Me.top_menu_home_tab_info_lbl.TabIndex = 5
        Me.top_menu_home_tab_info_lbl.Text = "About"
        '
        'top_menu_tab_import_page
        '
        Me.top_menu_tab_import_page.AutoScroll = True
        Me.top_menu_tab_import_page.Controls.Add(Me.top_menu_import_page_ly)
        Me.top_menu_tab_import_page.Location = New System.Drawing.Point(4, 29)
        Me.top_menu_tab_import_page.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_tab_import_page.Name = "top_menu_tab_import_page"
        Me.top_menu_tab_import_page.Size = New System.Drawing.Size(1825, 116)
        Me.top_menu_tab_import_page.TabIndex = 1
        Me.top_menu_tab_import_page.Text = "IMPORT"
        Me.top_menu_tab_import_page.UseVisualStyleBackColor = True
        '
        'top_menu_import_page_ly
        '
        Me.top_menu_import_page_ly.ColumnCount = 5
        Me.top_menu_import_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.top_menu_import_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.top_menu_import_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 99.0!))
        Me.top_menu_import_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88.0!))
        Me.top_menu_import_page_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1498.0!))
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_zimro_bt, 0, 0)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_zimro_lbl, 0, 1)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_CN47N_bt, 1, 0)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_zmel_bt, 2, 0)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_item_import_cost_coll, 1, 1)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_zmel_lbl, 2, 1)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_CIJ3_bt, 3, 0)
        Me.top_menu_import_page_ly.Controls.Add(Me.top_menu_import_actuals, 3, 1)
        Me.top_menu_import_page_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.top_menu_import_page_ly.Location = New System.Drawing.Point(0, 0)
        Me.top_menu_import_page_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_page_ly.Name = "top_menu_import_page_ly"
        Me.top_menu_import_page_ly.RowCount = 2
        Me.top_menu_import_page_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.41558!))
        Me.top_menu_import_page_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.58442!))
        Me.top_menu_import_page_ly.Size = New System.Drawing.Size(1825, 116)
        Me.top_menu_import_page_ly.TabIndex = 0
        '
        'top_menu_import_zimro_bt
        '
        Me.top_menu_import_zimro_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_zimro_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_import_zimro_bt.Image = CType(resources.GetObject("top_menu_import_zimro_bt.Image"), System.Drawing.Image)
        Me.top_menu_import_zimro_bt.Location = New System.Drawing.Point(10, 7)
        Me.top_menu_import_zimro_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_zimro_bt.Name = "top_menu_import_zimro_bt"
        Me.top_menu_import_zimro_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_import_zimro_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_import_zimro_bt.TabIndex = 2
        Me.top_menu_import_zimro_bt.TabStop = False
        '
        'top_menu_import_zimro_lbl
        '
        Me.top_menu_import_zimro_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_zimro_lbl.AutoSize = True
        Me.top_menu_import_zimro_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_import_zimro_lbl.Location = New System.Drawing.Point(0, 98)
        Me.top_menu_import_zimro_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_zimro_lbl.Name = "top_menu_import_zimro_lbl"
        Me.top_menu_import_zimro_lbl.Size = New System.Drawing.Size(90, 16)
        Me.top_menu_import_zimro_lbl.TabIndex = 3
        Me.top_menu_import_zimro_lbl.Text = "Import ZIMRO"
        '
        'top_menu_import_CN47N_bt
        '
        Me.top_menu_import_CN47N_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_CN47N_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_import_CN47N_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.wo_collector_import
        Me.top_menu_import_CN47N_bt.Location = New System.Drawing.Point(100, 7)
        Me.top_menu_import_CN47N_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_CN47N_bt.Name = "top_menu_import_CN47N_bt"
        Me.top_menu_import_CN47N_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_import_CN47N_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_import_CN47N_bt.TabIndex = 6
        Me.top_menu_import_CN47N_bt.TabStop = False
        '
        'top_menu_import_zmel_bt
        '
        Me.top_menu_import_zmel_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_zmel_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_import_zmel_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.import_zmel
        Me.top_menu_import_zmel_bt.Location = New System.Drawing.Point(195, 7)
        Me.top_menu_import_zmel_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_zmel_bt.Name = "top_menu_import_zmel_bt"
        Me.top_menu_import_zmel_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_import_zmel_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_import_zmel_bt.TabIndex = 3
        Me.top_menu_import_zmel_bt.TabStop = False
        '
        'top_menu_item_import_cost_coll
        '
        Me.top_menu_item_import_cost_coll.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_item_import_cost_coll.AutoSize = True
        Me.top_menu_item_import_cost_coll.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_item_import_cost_coll.Location = New System.Drawing.Point(90, 98)
        Me.top_menu_item_import_cost_coll.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_item_import_cost_coll.Name = "top_menu_item_import_cost_coll"
        Me.top_menu_item_import_cost_coll.Size = New System.Drawing.Size(91, 16)
        Me.top_menu_item_import_cost_coll.TabIndex = 5
        Me.top_menu_item_import_cost_coll.Text = "Import CN47N"
        '
        'top_menu_import_zmel_lbl
        '
        Me.top_menu_import_zmel_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_zmel_lbl.AutoSize = True
        Me.top_menu_import_zmel_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_import_zmel_lbl.Location = New System.Drawing.Point(189, 98)
        Me.top_menu_import_zmel_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_zmel_lbl.Name = "top_menu_import_zmel_lbl"
        Me.top_menu_import_zmel_lbl.Size = New System.Drawing.Size(83, 16)
        Me.top_menu_import_zmel_lbl.TabIndex = 4
        Me.top_menu_import_zmel_lbl.Text = "Import ZMEL"
        '
        'top_menu_import_CIJ3_bt
        '
        Me.top_menu_import_CIJ3_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_CIJ3_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_import_CIJ3_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.import_hours
        Me.top_menu_import_CIJ3_bt.Location = New System.Drawing.Point(289, 7)
        Me.top_menu_import_CIJ3_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_CIJ3_bt.Name = "top_menu_import_CIJ3_bt"
        Me.top_menu_import_CIJ3_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_import_CIJ3_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_import_CIJ3_bt.TabIndex = 7
        Me.top_menu_import_CIJ3_bt.TabStop = False
        '
        'top_menu_import_actuals
        '
        Me.top_menu_import_actuals.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_import_actuals.AutoSize = True
        Me.top_menu_import_actuals.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_import_actuals.Location = New System.Drawing.Point(287, 98)
        Me.top_menu_import_actuals.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_import_actuals.Name = "top_menu_import_actuals"
        Me.top_menu_import_actuals.Size = New System.Drawing.Size(74, 16)
        Me.top_menu_import_actuals.TabIndex = 8
        Me.top_menu_import_actuals.Text = "Import CJI3"
        '
        'top_menu_tab_report_page
        '
        Me.top_menu_tab_report_page.Controls.Add(Me.TableLayoutPanel2)
        Me.top_menu_tab_report_page.Location = New System.Drawing.Point(4, 29)
        Me.top_menu_tab_report_page.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_menu_tab_report_page.Name = "top_menu_tab_report_page"
        Me.top_menu_tab_report_page.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_menu_tab_report_page.Size = New System.Drawing.Size(1825, 116)
        Me.top_menu_tab_report_page.TabIndex = 3
        Me.top_menu_tab_report_page.Text = "REPORTING"
        Me.top_menu_tab_report_page.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 4
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 98.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 98.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 98.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.top_menu_data_check_bt, 2, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.top_menu_data_check_bt_lbl, 2, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.top_menu_customer_release_bt, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.top_menu_customer_release_bt_lbl, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.top_menu_hours_stat_bt_lbl, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.top_menu_hours_stat_bt, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(4, 5)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.41558!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.58442!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1817, 106)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'top_menu_data_check_bt
        '
        Me.top_menu_data_check_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_data_check_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_data_check_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.datacheck
        Me.top_menu_data_check_bt.Location = New System.Drawing.Point(210, 3)
        Me.top_menu_data_check_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_data_check_bt.Name = "top_menu_data_check_bt"
        Me.top_menu_data_check_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_data_check_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_data_check_bt.TabIndex = 6
        Me.top_menu_data_check_bt.TabStop = False
        '
        'top_menu_data_check_bt_lbl
        '
        Me.top_menu_data_check_bt_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_data_check_bt_lbl.AutoSize = True
        Me.top_menu_data_check_bt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_data_check_bt_lbl.Location = New System.Drawing.Point(206, 89)
        Me.top_menu_data_check_bt_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_data_check_bt_lbl.Name = "top_menu_data_check_bt_lbl"
        Me.top_menu_data_check_bt_lbl.Size = New System.Drawing.Size(78, 16)
        Me.top_menu_data_check_bt_lbl.TabIndex = 5
        Me.top_menu_data_check_bt_lbl.Text = "Data Check"
        '
        'top_menu_customer_release_bt
        '
        Me.top_menu_customer_release_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_customer_release_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_customer_release_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.release_to_customer
        Me.top_menu_customer_release_bt.Location = New System.Drawing.Point(14, 3)
        Me.top_menu_customer_release_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_customer_release_bt.Name = "top_menu_customer_release_bt"
        Me.top_menu_customer_release_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_customer_release_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_customer_release_bt.TabIndex = 2
        Me.top_menu_customer_release_bt.TabStop = False
        '
        'top_menu_customer_release_bt_lbl
        '
        Me.top_menu_customer_release_bt_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_customer_release_bt_lbl.AutoSize = True
        Me.top_menu_customer_release_bt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_customer_release_bt_lbl.Location = New System.Drawing.Point(3, 89)
        Me.top_menu_customer_release_bt_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_customer_release_bt_lbl.Name = "top_menu_customer_release_bt_lbl"
        Me.top_menu_customer_release_bt_lbl.Size = New System.Drawing.Size(92, 16)
        Me.top_menu_customer_release_bt_lbl.TabIndex = 3
        Me.top_menu_customer_release_bt_lbl.Text = "Cust. Release"
        '
        'top_menu_hours_stat_bt_lbl
        '
        Me.top_menu_hours_stat_bt_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_hours_stat_bt_lbl.AutoSize = True
        Me.top_menu_hours_stat_bt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_hours_stat_bt_lbl.Location = New System.Drawing.Point(118, 89)
        Me.top_menu_hours_stat_bt_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_hours_stat_bt_lbl.Name = "top_menu_hours_stat_bt_lbl"
        Me.top_menu_hours_stat_bt_lbl.Size = New System.Drawing.Size(58, 16)
        Me.top_menu_hours_stat_bt_lbl.TabIndex = 4
        Me.top_menu_hours_stat_bt_lbl.Text = "Hrs Stat."
        '
        'top_menu_hours_stat_bt
        '
        Me.top_menu_hours_stat_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_hours_stat_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_hours_stat_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.hours_statement_report
        Me.top_menu_hours_stat_bt.Location = New System.Drawing.Point(112, 3)
        Me.top_menu_hours_stat_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_hours_stat_bt.Name = "top_menu_hours_stat_bt"
        Me.top_menu_hours_stat_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_hours_stat_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_hours_stat_bt.TabIndex = 5
        Me.top_menu_hours_stat_bt.TabStop = False
        '
        'top_menu_tab_admin_page
        '
        Me.top_menu_tab_admin_page.Controls.Add(Me.admin_menu_tab_ly)
        Me.top_menu_tab_admin_page.Location = New System.Drawing.Point(4, 29)
        Me.top_menu_tab_admin_page.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_menu_tab_admin_page.Name = "top_menu_tab_admin_page"
        Me.top_menu_tab_admin_page.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_menu_tab_admin_page.Size = New System.Drawing.Size(1825, 116)
        Me.top_menu_tab_admin_page.TabIndex = 2
        Me.top_menu_tab_admin_page.Text = "ADMIN"
        Me.top_menu_tab_admin_page.UseVisualStyleBackColor = True
        '
        'admin_menu_tab_ly
        '
        Me.admin_menu_tab_ly.ColumnCount = 4
        Me.admin_menu_tab_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.601318!))
        Me.admin_menu_tab_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 5.354201!))
        Me.admin_menu_tab_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 89.12685!))
        Me.admin_menu_tab_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 59.0!))
        Me.admin_menu_tab_ly.Controls.Add(Me.top_menu_admin_bt, 0, 0)
        Me.admin_menu_tab_ly.Controls.Add(Me.top_menu_admin_bt_lbl, 0, 1)
        Me.admin_menu_tab_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.admin_menu_tab_ly.Location = New System.Drawing.Point(4, 5)
        Me.admin_menu_tab_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.admin_menu_tab_ly.Name = "admin_menu_tab_ly"
        Me.admin_menu_tab_ly.RowCount = 2
        Me.admin_menu_tab_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 84.41558!))
        Me.admin_menu_tab_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 15.58442!))
        Me.admin_menu_tab_ly.Size = New System.Drawing.Size(1817, 106)
        Me.admin_menu_tab_ly.TabIndex = 1
        '
        'top_menu_admin_bt
        '
        Me.top_menu_admin_bt.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_admin_bt.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_admin_bt.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.admin_icon
        Me.top_menu_admin_bt.Location = New System.Drawing.Point(14, 3)
        Me.top_menu_admin_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_admin_bt.Name = "top_menu_admin_bt"
        Me.top_menu_admin_bt.Size = New System.Drawing.Size(70, 83)
        Me.top_menu_admin_bt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_admin_bt.TabIndex = 2
        Me.top_menu_admin_bt.TabStop = False
        '
        'top_menu_admin_bt_lbl
        '
        Me.top_menu_admin_bt_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.top_menu_admin_bt_lbl.AutoSize = True
        Me.top_menu_admin_bt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 6.5!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.top_menu_admin_bt_lbl.Location = New System.Drawing.Point(10, 89)
        Me.top_menu_admin_bt_lbl.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_admin_bt_lbl.Name = "top_menu_admin_bt_lbl"
        Me.top_menu_admin_bt_lbl.Size = New System.Drawing.Size(78, 16)
        Me.top_menu_admin_bt_lbl.TabIndex = 3
        Me.top_menu_admin_bt_lbl.Text = "Admin View"
        '
        'top_menu_left_layout
        '
        Me.top_menu_left_layout.Controls.Add(Me.top_menu_save)
        Me.top_menu_left_layout.Controls.Add(Me.top_menu_save_status)
        Me.top_menu_left_layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.top_menu_left_layout.Location = New System.Drawing.Point(0, 0)
        Me.top_menu_left_layout.Margin = New System.Windows.Forms.Padding(0)
        Me.top_menu_left_layout.Name = "top_menu_left_layout"
        Me.top_menu_left_layout.Size = New System.Drawing.Size(91, 149)
        Me.top_menu_left_layout.TabIndex = 2
        '
        'top_menu_save
        '
        Me.top_menu_save.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_save.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.save
        Me.top_menu_save.Location = New System.Drawing.Point(4, 5)
        Me.top_menu_save.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_menu_save.Name = "top_menu_save"
        Me.top_menu_save.Size = New System.Drawing.Size(30, 31)
        Me.top_menu_save.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_save.TabIndex = 3
        Me.top_menu_save.TabStop = False
        '
        'top_menu_save_status
        '
        Me.top_menu_save_status.Cursor = System.Windows.Forms.Cursors.Hand
        Me.top_menu_save_status.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.no_errors
        Me.top_menu_save_status.Location = New System.Drawing.Point(42, 5)
        Me.top_menu_save_status.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.top_menu_save_status.Name = "top_menu_save_status"
        Me.top_menu_save_status.Size = New System.Drawing.Size(30, 31)
        Me.top_menu_save_status.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.top_menu_save_status.TabIndex = 2
        Me.top_menu_save_status.TabStop = False
        '
        'pan_mainf_down_info
        '
        Me.pan_mainf_down_info.Controls.Add(Me.DownStatusStrip)
        Me.pan_mainf_down_info.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pan_mainf_down_info.Location = New System.Drawing.Point(0, 1014)
        Me.pan_mainf_down_info.Margin = New System.Windows.Forms.Padding(0)
        Me.pan_mainf_down_info.Name = "pan_mainf_down_info"
        Me.pan_mainf_down_info.Size = New System.Drawing.Size(1924, 35)
        Me.pan_mainf_down_info.TabIndex = 3
        '
        'DownStatusStrip
        '
        Me.DownStatusStrip.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DownStatusStrip.ImageScalingSize = New System.Drawing.Size(24, 24)
        Me.DownStatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.status_strip_main_label, Me.status_strip_sub_label, Me.status_strip_progress_bar, Me.status_strip_latest_update, Me.status_strip_dgrid_selected_sum, Me.calculation_label, Me.calculation_progress_bar})
        Me.DownStatusStrip.Location = New System.Drawing.Point(0, 0)
        Me.DownStatusStrip.Name = "DownStatusStrip"
        Me.DownStatusStrip.Padding = New System.Windows.Forms.Padding(2, 0, 21, 0)
        Me.DownStatusStrip.Size = New System.Drawing.Size(1924, 35)
        Me.DownStatusStrip.TabIndex = 0
        Me.DownStatusStrip.Text = "status_strip_down"
        '
        'status_strip_main_label
        '
        Me.status_strip_main_label.AutoSize = False
        Me.status_strip_main_label.Name = "status_strip_main_label"
        Me.status_strip_main_label.Size = New System.Drawing.Size(150, 30)
        Me.status_strip_main_label.Text = "Main Status text"
        '
        'status_strip_sub_label
        '
        Me.status_strip_sub_label.AutoSize = False
        Me.status_strip_sub_label.Name = "status_strip_sub_label"
        Me.status_strip_sub_label.Size = New System.Drawing.Size(350, 30)
        Me.status_strip_sub_label.Text = "Sub Status text"
        '
        'status_strip_progress_bar
        '
        Me.status_strip_progress_bar.AutoSize = False
        Me.status_strip_progress_bar.Name = "status_strip_progress_bar"
        Me.status_strip_progress_bar.Size = New System.Drawing.Size(150, 29)
        '
        'status_strip_latest_update
        '
        Me.status_strip_latest_update.Name = "status_strip_latest_update"
        Me.status_strip_latest_update.Size = New System.Drawing.Size(163, 30)
        Me.status_strip_latest_update.Text = "Latest Update Date"
        '
        'status_strip_dgrid_selected_sum
        '
        Me.status_strip_dgrid_selected_sum.Name = "status_strip_dgrid_selected_sum"
        Me.status_strip_dgrid_selected_sum.Size = New System.Drawing.Size(138, 30)
        Me.status_strip_dgrid_selected_sum.Text = "Count Sum Avg"
        '
        'calculation_label
        '
        Me.calculation_label.Name = "calculation_label"
        Me.calculation_label.Size = New System.Drawing.Size(107, 30)
        Me.calculation_label.Text = "Calculation :"
        '
        'calculation_progress_bar
        '
        Me.calculation_progress_bar.Name = "calculation_progress_bar"
        Me.calculation_progress_bar.Size = New System.Drawing.Size(150, 29)
        '
        'tab_ctrl_main
        '
        Me.tab_ctrl_main.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.tab_ctrl_main.Controls.Add(Me.tab_main_page_config)
        Me.tab_ctrl_main.Controls.Add(Me.tab_main_page_init_scope)
        Me.tab_ctrl_main.Controls.Add(Me.tab_main_page_add_scope)
        Me.tab_ctrl_main.Controls.Add(Me.tab_main_page_materials)
        Me.tab_ctrl_main.Controls.Add(Me.tab_main_page_awq)
        Me.tab_ctrl_main.Controls.Add(Me.tab_main_proj_ctrl)
        Me.tab_ctrl_main.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab_ctrl_main.Location = New System.Drawing.Point(0, 149)
        Me.tab_ctrl_main.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_main.Multiline = True
        Me.tab_ctrl_main.Name = "tab_ctrl_main"
        Me.tab_ctrl_main.SelectedIndex = 0
        Me.tab_ctrl_main.Size = New System.Drawing.Size(1924, 865)
        Me.tab_ctrl_main.TabIndex = 0
        '
        'tab_main_page_config
        '
        Me.tab_main_page_config.Controls.Add(Me.tab_ctrl_config)
        Me.tab_main_page_config.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.tab_main_page_config.Location = New System.Drawing.Point(4, 4)
        Me.tab_main_page_config.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_config.Name = "tab_main_page_config"
        Me.tab_main_page_config.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_config.Size = New System.Drawing.Size(1916, 832)
        Me.tab_main_page_config.TabIndex = 0
        Me.tab_main_page_config.Text = "Project Configuration"
        Me.tab_main_page_config.UseVisualStyleBackColor = True
        '
        'tab_ctrl_config
        '
        Me.tab_ctrl_config.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.tab_ctrl_config.Controls.Add(Me.tab_ctrl_config_page_proj)
        Me.tab_ctrl_config.Controls.Add(Me.tab_ctrl_config_page_billing)
        Me.tab_ctrl_config.Controls.Add(Me.tab_ctrl_config_page_rates)
        Me.tab_ctrl_config.Controls.Add(Me.tab_ctrl_config_page_editable_lov)
        Me.tab_ctrl_config.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab_ctrl_config.Location = New System.Drawing.Point(4, 5)
        Me.tab_ctrl_config.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config.Name = "tab_ctrl_config"
        Me.tab_ctrl_config.SelectedIndex = 0
        Me.tab_ctrl_config.Size = New System.Drawing.Size(1908, 822)
        Me.tab_ctrl_config.TabIndex = 0
        '
        'tab_ctrl_config_page_proj
        '
        Me.tab_ctrl_config_page_proj.AutoScroll = True
        Me.tab_ctrl_config_page_proj.Controls.Add(Me.conf_project_layout)
        Me.tab_ctrl_config_page_proj.Controls.Add(Me.RichTextBox1)
        Me.tab_ctrl_config_page_proj.Location = New System.Drawing.Point(4, 4)
        Me.tab_ctrl_config_page_proj.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_proj.Name = "tab_ctrl_config_page_proj"
        Me.tab_ctrl_config_page_proj.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_proj.Size = New System.Drawing.Size(1900, 789)
        Me.tab_ctrl_config_page_proj.TabIndex = 0
        Me.tab_ctrl_config_page_proj.Text = "1 - Project Details"
        Me.tab_ctrl_config_page_proj.UseVisualStyleBackColor = True
        '
        'conf_project_layout
        '
        Me.conf_project_layout.ColumnCount = 3
        Me.conf_project_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.19305!))
        Me.conf_project_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.05184!))
        Me.conf_project_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.75511!))
        Me.conf_project_layout.Controls.Add(Me.TableLayoutPanel3, 1, 11)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_cust_rep_name, 0, 11)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_awq_specialist, 0, 12)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_acgroup_val, 1, 2)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_projdef, 0, 4)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_bill_grp_val, 1, 13)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_acgroup, 0, 2)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_acrmast_val, 1, 1)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_acmast, 0, 1)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_acreg, 0, 0)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_acreg_val, 1, 0)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_projlist_val, 1, 3)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_projlist, 0, 3)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_projdesc_val, 1, 5)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_projdesc, 0, 5)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_curr_in_date, 0, 6)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_curr_fcst_date, 0, 7)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_cust_po, 0, 8)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_cust_po_val, 1, 8)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_quot_nr_val, 1, 9)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_quot_nr, 0, 9)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_cust, 0, 10)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_bill_grp, 0, 13)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_work_accpt_form, 0, 14)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_cust_val, 1, 10)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_work_accpt_form_val, 1, 14)
        Me.conf_project_layout.Controls.Add(Me.Label12, 0, 17)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_ac_flight_hrs, 0, 18)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_ac_flight_cycl, 0, 19)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_ac_flight_hrs_val, 1, 18)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_ac_flight_cycl_val, 1, 19)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_dass_sales_or_val, 1, 20)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_dass_sales_or, 0, 20)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_curr_in_date_val, 1, 6)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_curr_fcst_date_val, 1, 7)
        Me.conf_project_layout.Controls.Add(Me.lbl_cnf_prj_det_projdef_val, 1, 4)
        Me.conf_project_layout.Controls.Add(Me.awq_specialist_ly, 1, 12)
        Me.conf_project_layout.Location = New System.Drawing.Point(711, 55)
        Me.conf_project_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_project_layout.Name = "conf_project_layout"
        Me.conf_project_layout.RowCount = 21
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.720529!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.775469!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.520136!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.896813!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.780042!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.896813!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.520136!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 4.777863!))
        Me.conf_project_layout.Size = New System.Drawing.Size(1770, 823)
        Me.conf_project_layout.TabIndex = 1
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 3
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.05994!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.50158!))
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel3.Controls.Add(Me.lbl_cnf_prj_det_cust_email_val, 2, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.lbl_cnf_prj_det_cust_email, 1, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.lbl_cnf_prj_det_cust_rep_name_val, 0, 0)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(357, 427)
        Me.TableLayoutPanel3.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 1
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(951, 37)
        Me.TableLayoutPanel3.TabIndex = 45
        '
        'lbl_cnf_prj_det_cust_email_val
        '
        Me.lbl_cnf_prj_det_cust_email_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_email_val.Location = New System.Drawing.Point(636, 5)
        Me.lbl_cnf_prj_det_cust_email_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_cust_email_val.Name = "lbl_cnf_prj_det_cust_email_val"
        Me.lbl_cnf_prj_det_cust_email_val.Size = New System.Drawing.Size(307, 26)
        Me.lbl_cnf_prj_det_cust_email_val.TabIndex = 24
        '
        'lbl_cnf_prj_det_cust_email
        '
        Me.lbl_cnf_prj_det_cust_email.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_email.AutoSize = True
        Me.lbl_cnf_prj_det_cust_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_cust_email.Location = New System.Drawing.Point(413, 8)
        Me.lbl_cnf_prj_det_cust_email.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_cust_email.Name = "lbl_cnf_prj_det_cust_email"
        Me.lbl_cnf_prj_det_cust_email.Size = New System.Drawing.Size(173, 20)
        Me.lbl_cnf_prj_det_cust_email.TabIndex = 17
        Me.lbl_cnf_prj_det_cust_email.Text = "Cust. Rep. email  *:"
        '
        'lbl_cnf_prj_det_cust_rep_name_val
        '
        Me.lbl_cnf_prj_det_cust_rep_name_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_rep_name_val.Location = New System.Drawing.Point(4, 5)
        Me.lbl_cnf_prj_det_cust_rep_name_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_cust_rep_name_val.Name = "lbl_cnf_prj_det_cust_rep_name_val"
        Me.lbl_cnf_prj_det_cust_rep_name_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_cust_rep_name_val.TabIndex = 26
        '
        'lbl_cnf_prj_det_cust_rep_name
        '
        Me.lbl_cnf_prj_det_cust_rep_name.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_rep_name.AutoSize = True
        Me.lbl_cnf_prj_det_cust_rep_name.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_cust_rep_name.Location = New System.Drawing.Point(4, 436)
        Me.lbl_cnf_prj_det_cust_rep_name.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_cust_rep_name.Name = "lbl_cnf_prj_det_cust_rep_name"
        Me.lbl_cnf_prj_det_cust_rep_name.Size = New System.Drawing.Size(235, 20)
        Me.lbl_cnf_prj_det_cust_rep_name.TabIndex = 19
        Me.lbl_cnf_prj_det_cust_rep_name.Text = "Customer Representative*:"
        '
        'lbl_cnf_prj_det_awq_specialist
        '
        Me.lbl_cnf_prj_det_awq_specialist.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_awq_specialist.AutoSize = True
        Me.lbl_cnf_prj_det_awq_specialist.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_awq_specialist.Location = New System.Drawing.Point(4, 475)
        Me.lbl_cnf_prj_det_awq_specialist.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_awq_specialist.Name = "lbl_cnf_prj_det_awq_specialist"
        Me.lbl_cnf_prj_det_awq_specialist.Size = New System.Drawing.Size(208, 20)
        Me.lbl_cnf_prj_det_awq_specialist.TabIndex = 18
        Me.lbl_cnf_prj_det_awq_specialist.Text = "AWQ Specialist Name*:"
        '
        'lbl_cnf_prj_det_acgroup_val
        '
        Me.lbl_cnf_prj_det_acgroup_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_acgroup_val.Enabled = False
        Me.lbl_cnf_prj_det_acgroup_val.Location = New System.Drawing.Point(361, 84)
        Me.lbl_cnf_prj_det_acgroup_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_acgroup_val.Name = "lbl_cnf_prj_det_acgroup_val"
        Me.lbl_cnf_prj_det_acgroup_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_acgroup_val.TabIndex = 4
        '
        'lbl_cnf_prj_det_projdef
        '
        Me.lbl_cnf_prj_det_projdef.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_projdef.AutoSize = True
        Me.lbl_cnf_prj_det_projdef.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_projdef.Location = New System.Drawing.Point(4, 164)
        Me.lbl_cnf_prj_det_projdef.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_projdef.Name = "lbl_cnf_prj_det_projdef"
        Me.lbl_cnf_prj_det_projdef.Size = New System.Drawing.Size(175, 20)
        Me.lbl_cnf_prj_det_projdef.TabIndex = 5
        Me.lbl_cnf_prj_det_projdef.Text = "Project Definition *:"
        '
        'lbl_cnf_prj_det_bill_grp_val
        '
        Me.lbl_cnf_prj_det_bill_grp_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_bill_grp_val.FormattingEnabled = True
        Me.lbl_cnf_prj_det_bill_grp_val.Location = New System.Drawing.Point(361, 510)
        Me.lbl_cnf_prj_det_bill_grp_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_bill_grp_val.Name = "lbl_cnf_prj_det_bill_grp_val"
        Me.lbl_cnf_prj_det_bill_grp_val.Size = New System.Drawing.Size(298, 28)
        Me.lbl_cnf_prj_det_bill_grp_val.TabIndex = 43
        '
        'lbl_cnf_prj_det_acgroup
        '
        Me.lbl_cnf_prj_det_acgroup.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_acgroup.AutoSize = True
        Me.lbl_cnf_prj_det_acgroup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_acgroup.Location = New System.Drawing.Point(4, 87)
        Me.lbl_cnf_prj_det_acgroup.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_acgroup.Name = "lbl_cnf_prj_det_acgroup"
        Me.lbl_cnf_prj_det_acgroup.Size = New System.Drawing.Size(148, 20)
        Me.lbl_cnf_prj_det_acgroup.TabIndex = 3
        Me.lbl_cnf_prj_det_acgroup.Text = "Aircraft Group *:"
        '
        'lbl_cnf_prj_det_acrmast_val
        '
        Me.lbl_cnf_prj_det_acrmast_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_acrmast_val.Enabled = False
        Me.lbl_cnf_prj_det_acrmast_val.Location = New System.Drawing.Point(361, 45)
        Me.lbl_cnf_prj_det_acrmast_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_acrmast_val.Name = "lbl_cnf_prj_det_acrmast_val"
        Me.lbl_cnf_prj_det_acrmast_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_acrmast_val.TabIndex = 3
        '
        'lbl_cnf_prj_det_acmast
        '
        Me.lbl_cnf_prj_det_acmast.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_acmast.AutoSize = True
        Me.lbl_cnf_prj_det_acmast.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_acmast.Location = New System.Drawing.Point(4, 48)
        Me.lbl_cnf_prj_det_acmast.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_acmast.Name = "lbl_cnf_prj_det_acmast"
        Me.lbl_cnf_prj_det_acmast.Size = New System.Drawing.Size(155, 20)
        Me.lbl_cnf_prj_det_acmast.TabIndex = 2
        Me.lbl_cnf_prj_det_acmast.Text = "Aircraft Master *:"
        '
        'lbl_cnf_prj_det_acreg
        '
        Me.lbl_cnf_prj_det_acreg.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_acreg.AutoSize = True
        Me.lbl_cnf_prj_det_acreg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_acreg.Location = New System.Drawing.Point(4, 9)
        Me.lbl_cnf_prj_det_acreg.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_acreg.Name = "lbl_cnf_prj_det_acreg"
        Me.lbl_cnf_prj_det_acreg.Size = New System.Drawing.Size(199, 20)
        Me.lbl_cnf_prj_det_acreg.TabIndex = 0
        Me.lbl_cnf_prj_det_acreg.Text = "Aircraft Registration *:"
        '
        'lbl_cnf_prj_det_acreg_val
        '
        Me.lbl_cnf_prj_det_acreg_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_acreg_val.Enabled = False
        Me.lbl_cnf_prj_det_acreg_val.Location = New System.Drawing.Point(361, 6)
        Me.lbl_cnf_prj_det_acreg_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_acreg_val.Name = "lbl_cnf_prj_det_acreg_val"
        Me.lbl_cnf_prj_det_acreg_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_acreg_val.TabIndex = 1
        '
        'lbl_cnf_prj_det_projlist_val
        '
        Me.lbl_cnf_prj_det_projlist_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_projlist_val.Enabled = False
        Me.lbl_cnf_prj_det_projlist_val.Location = New System.Drawing.Point(361, 122)
        Me.lbl_cnf_prj_det_projlist_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_projlist_val.Name = "lbl_cnf_prj_det_projlist_val"
        Me.lbl_cnf_prj_det_projlist_val.Size = New System.Drawing.Size(944, 26)
        Me.lbl_cnf_prj_det_projlist_val.TabIndex = 5
        '
        'lbl_cnf_prj_det_projlist
        '
        Me.lbl_cnf_prj_det_projlist.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_projlist.AutoSize = True
        Me.lbl_cnf_prj_det_projlist.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_projlist.Location = New System.Drawing.Point(4, 125)
        Me.lbl_cnf_prj_det_projlist.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_projlist.Name = "lbl_cnf_prj_det_projlist"
        Me.lbl_cnf_prj_det_projlist.Size = New System.Drawing.Size(126, 20)
        Me.lbl_cnf_prj_det_projlist.TabIndex = 4
        Me.lbl_cnf_prj_det_projlist.Text = "Project List *:"
        '
        'lbl_cnf_prj_det_projdesc_val
        '
        Me.lbl_cnf_prj_det_projdesc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_projdesc_val.Location = New System.Drawing.Point(361, 200)
        Me.lbl_cnf_prj_det_projdesc_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_projdesc_val.Name = "lbl_cnf_prj_det_projdesc_val"
        Me.lbl_cnf_prj_det_projdesc_val.Size = New System.Drawing.Size(944, 26)
        Me.lbl_cnf_prj_det_projdesc_val.TabIndex = 7
        '
        'lbl_cnf_prj_det_projdesc
        '
        Me.lbl_cnf_prj_det_projdesc.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_projdesc.AutoSize = True
        Me.lbl_cnf_prj_det_projdesc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_projdesc.Location = New System.Drawing.Point(4, 203)
        Me.lbl_cnf_prj_det_projdesc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_projdesc.Name = "lbl_cnf_prj_det_projdesc"
        Me.lbl_cnf_prj_det_projdesc.Size = New System.Drawing.Size(191, 20)
        Me.lbl_cnf_prj_det_projdesc.TabIndex = 6
        Me.lbl_cnf_prj_det_projdesc.Text = "Project Description *:"
        '
        'lbl_cnf_prj_det_curr_in_date
        '
        Me.lbl_cnf_prj_det_curr_in_date.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_curr_in_date.AutoSize = True
        Me.lbl_cnf_prj_det_curr_in_date.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_curr_in_date.Location = New System.Drawing.Point(4, 242)
        Me.lbl_cnf_prj_det_curr_in_date.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_curr_in_date.Name = "lbl_cnf_prj_det_curr_in_date"
        Me.lbl_cnf_prj_det_curr_in_date.Size = New System.Drawing.Size(287, 20)
        Me.lbl_cnf_prj_det_curr_in_date.TabIndex = 8
        Me.lbl_cnf_prj_det_curr_in_date.Text = "CURRENT INPUT DATE (SAP) *:"
        '
        'lbl_cnf_prj_det_curr_fcst_date
        '
        Me.lbl_cnf_prj_det_curr_fcst_date.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_curr_fcst_date.AutoSize = True
        Me.lbl_cnf_prj_det_curr_fcst_date.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_curr_fcst_date.Location = New System.Drawing.Point(4, 281)
        Me.lbl_cnf_prj_det_curr_fcst_date.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_curr_fcst_date.Name = "lbl_cnf_prj_det_curr_fcst_date"
        Me.lbl_cnf_prj_det_curr_fcst_date.Size = New System.Drawing.Size(269, 20)
        Me.lbl_cnf_prj_det_curr_fcst_date.TabIndex = 10
        Me.lbl_cnf_prj_det_curr_fcst_date.Text = "CURRENT CRS FCST (SAP) *:"
        '
        'lbl_cnf_prj_det_cust_po
        '
        Me.lbl_cnf_prj_det_cust_po.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_po.AutoSize = True
        Me.lbl_cnf_prj_det_cust_po.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_cust_po.Location = New System.Drawing.Point(4, 319)
        Me.lbl_cnf_prj_det_cust_po.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_cust_po.Name = "lbl_cnf_prj_det_cust_po"
        Me.lbl_cnf_prj_det_cust_po.Size = New System.Drawing.Size(235, 20)
        Me.lbl_cnf_prj_det_cust_po.TabIndex = 11
        Me.lbl_cnf_prj_det_cust_po.Text = "Customer Purchase Order:"
        '
        'lbl_cnf_prj_det_cust_po_val
        '
        Me.lbl_cnf_prj_det_cust_po_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_po_val.Location = New System.Drawing.Point(361, 316)
        Me.lbl_cnf_prj_det_cust_po_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_cust_po_val.Name = "lbl_cnf_prj_det_cust_po_val"
        Me.lbl_cnf_prj_det_cust_po_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_cust_po_val.TabIndex = 13
        '
        'lbl_cnf_prj_det_quot_nr_val
        '
        Me.lbl_cnf_prj_det_quot_nr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_quot_nr_val.Enabled = False
        Me.lbl_cnf_prj_det_quot_nr_val.Location = New System.Drawing.Point(361, 355)
        Me.lbl_cnf_prj_det_quot_nr_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_quot_nr_val.Name = "lbl_cnf_prj_det_quot_nr_val"
        Me.lbl_cnf_prj_det_quot_nr_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_quot_nr_val.TabIndex = 14
        '
        'lbl_cnf_prj_det_quot_nr
        '
        Me.lbl_cnf_prj_det_quot_nr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_quot_nr.AutoSize = True
        Me.lbl_cnf_prj_det_quot_nr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_quot_nr.Location = New System.Drawing.Point(4, 358)
        Me.lbl_cnf_prj_det_quot_nr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_quot_nr.Name = "lbl_cnf_prj_det_quot_nr"
        Me.lbl_cnf_prj_det_quot_nr.Size = New System.Drawing.Size(191, 20)
        Me.lbl_cnf_prj_det_quot_nr.TabIndex = 15
        Me.lbl_cnf_prj_det_quot_nr.Text = "Quotation(s) Number:"
        '
        'lbl_cnf_prj_det_cust
        '
        Me.lbl_cnf_prj_det_cust.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust.AutoSize = True
        Me.lbl_cnf_prj_det_cust.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_cust.Location = New System.Drawing.Point(4, 397)
        Me.lbl_cnf_prj_det_cust.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_cust.Name = "lbl_cnf_prj_det_cust"
        Me.lbl_cnf_prj_det_cust.Size = New System.Drawing.Size(192, 20)
        Me.lbl_cnf_prj_det_cust.TabIndex = 16
        Me.lbl_cnf_prj_det_cust.Text = "Customer Company *:"
        '
        'lbl_cnf_prj_det_bill_grp
        '
        Me.lbl_cnf_prj_det_bill_grp.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_bill_grp.AutoSize = True
        Me.lbl_cnf_prj_det_bill_grp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_bill_grp.Location = New System.Drawing.Point(4, 514)
        Me.lbl_cnf_prj_det_bill_grp.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_bill_grp.Name = "lbl_cnf_prj_det_bill_grp"
        Me.lbl_cnf_prj_det_bill_grp.Size = New System.Drawing.Size(138, 20)
        Me.lbl_cnf_prj_det_bill_grp.TabIndex = 18
        Me.lbl_cnf_prj_det_bill_grp.Text = "Billing Group *:"
        '
        'lbl_cnf_prj_det_work_accpt_form
        '
        Me.lbl_cnf_prj_det_work_accpt_form.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_work_accpt_form.AutoSize = True
        Me.lbl_cnf_prj_det_work_accpt_form.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_work_accpt_form.Location = New System.Drawing.Point(4, 553)
        Me.lbl_cnf_prj_det_work_accpt_form.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_work_accpt_form.Name = "lbl_cnf_prj_det_work_accpt_form"
        Me.lbl_cnf_prj_det_work_accpt_form.Size = New System.Drawing.Size(211, 20)
        Me.lbl_cnf_prj_det_work_accpt_form.TabIndex = 19
        Me.lbl_cnf_prj_det_work_accpt_form.Text = "Work Acceptance Form:"
        '
        'lbl_cnf_prj_det_cust_val
        '
        Me.lbl_cnf_prj_det_cust_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_cust_val.Location = New System.Drawing.Point(361, 394)
        Me.lbl_cnf_prj_det_cust_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_cust_val.Name = "lbl_cnf_prj_det_cust_val"
        Me.lbl_cnf_prj_det_cust_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_cust_val.TabIndex = 23
        '
        'lbl_cnf_prj_det_work_accpt_form_val
        '
        Me.lbl_cnf_prj_det_work_accpt_form_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_work_accpt_form_val.Location = New System.Drawing.Point(361, 550)
        Me.lbl_cnf_prj_det_work_accpt_form_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_work_accpt_form_val.Name = "lbl_cnf_prj_det_work_accpt_form_val"
        Me.lbl_cnf_prj_det_work_accpt_form_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_work_accpt_form_val.TabIndex = 26
        '
        'Label12
        '
        Me.Label12.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(4, 669)
        Me.Label12.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(208, 20)
        Me.Label12.TabIndex = 30
        Me.Label12.Text = "Required for Dassault only"
        '
        'lbl_cnf_prj_det_ac_flight_hrs
        '
        Me.lbl_cnf_prj_det_ac_flight_hrs.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_ac_flight_hrs.AutoSize = True
        Me.lbl_cnf_prj_det_ac_flight_hrs.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_ac_flight_hrs.Location = New System.Drawing.Point(4, 708)
        Me.lbl_cnf_prj_det_ac_flight_hrs.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_ac_flight_hrs.Name = "lbl_cnf_prj_det_ac_flight_hrs"
        Me.lbl_cnf_prj_det_ac_flight_hrs.Size = New System.Drawing.Size(147, 20)
        Me.lbl_cnf_prj_det_ac_flight_hrs.TabIndex = 31
        Me.lbl_cnf_prj_det_ac_flight_hrs.Text = "A/C flight hours:"
        '
        'lbl_cnf_prj_det_ac_flight_cycl
        '
        Me.lbl_cnf_prj_det_ac_flight_cycl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_ac_flight_cycl.AutoSize = True
        Me.lbl_cnf_prj_det_ac_flight_cycl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_ac_flight_cycl.Location = New System.Drawing.Point(4, 747)
        Me.lbl_cnf_prj_det_ac_flight_cycl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_ac_flight_cycl.Name = "lbl_cnf_prj_det_ac_flight_cycl"
        Me.lbl_cnf_prj_det_ac_flight_cycl.Size = New System.Drawing.Size(106, 20)
        Me.lbl_cnf_prj_det_ac_flight_cycl.TabIndex = 32
        Me.lbl_cnf_prj_det_ac_flight_cycl.Text = "A/C cycles:"
        '
        'lbl_cnf_prj_det_ac_flight_hrs_val
        '
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.Enabled = False
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.Location = New System.Drawing.Point(361, 705)
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.Name = "lbl_cnf_prj_det_ac_flight_hrs_val"
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_ac_flight_hrs_val.TabIndex = 34
        '
        'lbl_cnf_prj_det_ac_flight_cycl_val
        '
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.Enabled = False
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.Location = New System.Drawing.Point(361, 744)
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.Name = "lbl_cnf_prj_det_ac_flight_cycl_val"
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_ac_flight_cycl_val.TabIndex = 35
        '
        'lbl_cnf_prj_det_dass_sales_or_val
        '
        Me.lbl_cnf_prj_det_dass_sales_or_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_dass_sales_or_val.Enabled = False
        Me.lbl_cnf_prj_det_dass_sales_or_val.Location = New System.Drawing.Point(361, 787)
        Me.lbl_cnf_prj_det_dass_sales_or_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_dass_sales_or_val.Name = "lbl_cnf_prj_det_dass_sales_or_val"
        Me.lbl_cnf_prj_det_dass_sales_or_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_dass_sales_or_val.TabIndex = 37
        '
        'lbl_cnf_prj_det_dass_sales_or
        '
        Me.lbl_cnf_prj_det_dass_sales_or.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_dass_sales_or.AutoSize = True
        Me.lbl_cnf_prj_det_dass_sales_or.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_dass_sales_or.Location = New System.Drawing.Point(4, 790)
        Me.lbl_cnf_prj_det_dass_sales_or.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_dass_sales_or.Name = "lbl_cnf_prj_det_dass_sales_or"
        Me.lbl_cnf_prj_det_dass_sales_or.Size = New System.Drawing.Size(226, 20)
        Me.lbl_cnf_prj_det_dass_sales_or.TabIndex = 36
        Me.lbl_cnf_prj_det_dass_sales_or.Text = "Dassault Sales Order nr::"
        '
        'lbl_cnf_prj_det_curr_in_date_val
        '
        Me.lbl_cnf_prj_det_curr_in_date_val.Location = New System.Drawing.Point(361, 238)
        Me.lbl_cnf_prj_det_curr_in_date_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_curr_in_date_val.Name = "lbl_cnf_prj_det_curr_in_date_val"
        Me.lbl_cnf_prj_det_curr_in_date_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_curr_in_date_val.TabIndex = 38
        '
        'lbl_cnf_prj_det_curr_fcst_date_val
        '
        Me.lbl_cnf_prj_det_curr_fcst_date_val.Location = New System.Drawing.Point(361, 277)
        Me.lbl_cnf_prj_det_curr_fcst_date_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_curr_fcst_date_val.Name = "lbl_cnf_prj_det_curr_fcst_date_val"
        Me.lbl_cnf_prj_det_curr_fcst_date_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_curr_fcst_date_val.TabIndex = 39
        '
        'lbl_cnf_prj_det_projdef_val
        '
        Me.lbl_cnf_prj_det_projdef_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_projdef_val.Location = New System.Drawing.Point(361, 161)
        Me.lbl_cnf_prj_det_projdef_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_projdef_val.Name = "lbl_cnf_prj_det_projdef_val"
        Me.lbl_cnf_prj_det_projdef_val.Size = New System.Drawing.Size(944, 26)
        Me.lbl_cnf_prj_det_projdef_val.TabIndex = 6
        '
        'awq_specialist_ly
        '
        Me.awq_specialist_ly.ColumnCount = 3
        Me.awq_specialist_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.05994!))
        Me.awq_specialist_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.50158!))
        Me.awq_specialist_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.awq_specialist_ly.Controls.Add(Me.lbl_cnf_prj_det_awq_specialist_email_val, 2, 0)
        Me.awq_specialist_ly.Controls.Add(Me.lbl_cnf_prj_det_awq_specialist_email, 1, 0)
        Me.awq_specialist_ly.Controls.Add(Me.lbl_cnf_prj_det_awq_specialist_val, 0, 0)
        Me.awq_specialist_ly.Location = New System.Drawing.Point(357, 466)
        Me.awq_specialist_ly.Margin = New System.Windows.Forms.Padding(0)
        Me.awq_specialist_ly.Name = "awq_specialist_ly"
        Me.awq_specialist_ly.RowCount = 1
        Me.awq_specialist_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.awq_specialist_ly.Size = New System.Drawing.Size(951, 37)
        Me.awq_specialist_ly.TabIndex = 44
        '
        'lbl_cnf_prj_det_awq_specialist_email_val
        '
        Me.lbl_cnf_prj_det_awq_specialist_email_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_awq_specialist_email_val.Location = New System.Drawing.Point(636, 5)
        Me.lbl_cnf_prj_det_awq_specialist_email_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_awq_specialist_email_val.Name = "lbl_cnf_prj_det_awq_specialist_email_val"
        Me.lbl_cnf_prj_det_awq_specialist_email_val.Size = New System.Drawing.Size(307, 26)
        Me.lbl_cnf_prj_det_awq_specialist_email_val.TabIndex = 26
        '
        'lbl_cnf_prj_det_awq_specialist_email
        '
        Me.lbl_cnf_prj_det_awq_specialist_email.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_awq_specialist_email.AutoSize = True
        Me.lbl_cnf_prj_det_awq_specialist_email.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_prj_det_awq_specialist_email.Location = New System.Drawing.Point(413, 8)
        Me.lbl_cnf_prj_det_awq_specialist_email.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_prj_det_awq_specialist_email.Name = "lbl_cnf_prj_det_awq_specialist_email"
        Me.lbl_cnf_prj_det_awq_specialist_email.Size = New System.Drawing.Size(207, 20)
        Me.lbl_cnf_prj_det_awq_specialist_email.TabIndex = 19
        Me.lbl_cnf_prj_det_awq_specialist_email.Text = "AWQ Specialist Email*:"
        '
        'lbl_cnf_prj_det_awq_specialist_val
        '
        Me.lbl_cnf_prj_det_awq_specialist_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_prj_det_awq_specialist_val.Location = New System.Drawing.Point(4, 5)
        Me.lbl_cnf_prj_det_awq_specialist_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_prj_det_awq_specialist_val.Name = "lbl_cnf_prj_det_awq_specialist_val"
        Me.lbl_cnf_prj_det_awq_specialist_val.Size = New System.Drawing.Size(298, 26)
        Me.lbl_cnf_prj_det_awq_specialist_val.TabIndex = 25
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Enabled = False
        Me.RichTextBox1.Location = New System.Drawing.Point(36, 55)
        Me.RichTextBox1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(625, 821)
        Me.RichTextBox1.TabIndex = 0
        Me.RichTextBox1.Text = ""
        '
        'tab_ctrl_config_page_billing
        '
        Me.tab_ctrl_config_page_billing.AutoScroll = True
        Me.tab_ctrl_config_page_billing.Controls.Add(Me.tab_ctrl_config_page_bill_flow_panel)
        Me.tab_ctrl_config_page_billing.Controls.Add(Me.conf_bill_top_menu_layout)
        Me.tab_ctrl_config_page_billing.Location = New System.Drawing.Point(4, 4)
        Me.tab_ctrl_config_page_billing.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_billing.Name = "tab_ctrl_config_page_billing"
        Me.tab_ctrl_config_page_billing.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_billing.Size = New System.Drawing.Size(1900, 789)
        Me.tab_ctrl_config_page_billing.TabIndex = 1
        Me.tab_ctrl_config_page_billing.Text = "2-Billing Data"
        Me.tab_ctrl_config_page_billing.UseVisualStyleBackColor = True
        '
        'tab_ctrl_config_page_bill_flow_panel
        '
        Me.tab_ctrl_config_page_bill_flow_panel.AutoScroll = True
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_curr_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_markup_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_freight_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_surcharge_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_down_pay_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_discount_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Controls.Add(Me.conf_bill_nte_grp)
        Me.tab_ctrl_config_page_bill_flow_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab_ctrl_config_page_bill_flow_panel.Location = New System.Drawing.Point(4, 50)
        Me.tab_ctrl_config_page_bill_flow_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_bill_flow_panel.Name = "tab_ctrl_config_page_bill_flow_panel"
        Me.tab_ctrl_config_page_bill_flow_panel.Size = New System.Drawing.Size(1892, 734)
        Me.tab_ctrl_config_page_bill_flow_panel.TabIndex = 5
        '
        'conf_bill_curr_grp
        '
        Me.conf_bill_curr_grp.AutoSize = True
        Me.conf_bill_curr_grp.Controls.Add(Me.conf_bill_exchg_rate_grid_p)
        Me.conf_bill_curr_grp.Controls.Add(Me.conf_bill_curr_layount)
        Me.conf_bill_curr_grp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_curr_grp.Location = New System.Drawing.Point(4, 5)
        Me.conf_bill_curr_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_curr_grp.Name = "conf_bill_curr_grp"
        Me.conf_bill_curr_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_curr_grp.Size = New System.Drawing.Size(1762, 548)
        Me.conf_bill_curr_grp.TabIndex = 1
        Me.conf_bill_curr_grp.TabStop = False
        Me.conf_bill_curr_grp.Text = "1-Currencies and Exchange Rates"
        '
        'conf_bill_exchg_rate_grid_p
        '
        Me.conf_bill_exchg_rate_grid_p.AutoSize = True
        Me.conf_bill_exchg_rate_grid_p.Controls.Add(Me.conf_bill_exchg_rate_grid)
        Me.conf_bill_exchg_rate_grid_p.Location = New System.Drawing.Point(1074, 5)
        Me.conf_bill_exchg_rate_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_exchg_rate_grid_p.Name = "conf_bill_exchg_rate_grid_p"
        Me.conf_bill_exchg_rate_grid_p.Size = New System.Drawing.Size(680, 514)
        Me.conf_bill_exchg_rate_grid_p.TabIndex = 4
        '
        'conf_bill_exchg_rate_grid
        '
        Me.conf_bill_exchg_rate_grid.AllowUserToAddRows = False
        Me.conf_bill_exchg_rate_grid.AllowUserToDeleteRows = False
        Me.conf_bill_exchg_rate_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_exchg_rate_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_exchg_rate_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_exchg_rate_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_exchg_rate_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_exchg_rate_grid.Name = "conf_bill_exchg_rate_grid"
        Me.conf_bill_exchg_rate_grid.Size = New System.Drawing.Size(680, 514)
        Me.conf_bill_exchg_rate_grid.TabIndex = 3
        '
        'conf_bill_curr_layount
        '
        Me.conf_bill_curr_layount.ColumnCount = 2
        Me.conf_bill_curr_layount.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.conf_bill_curr_layount.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_tagk_date, 0, 5)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_lab_curr, 0, 0)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_mat_curr, 0, 1)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_rep_curr, 0, 2)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_3party_curr, 0, 3)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_freight_curr, 0, 4)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_lab_curr_val, 1, 0)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_mat_curr_val, 1, 1)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_rep_curr_val, 1, 2)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_3party_curr_val, 1, 3)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_freight_curr_val, 1, 4)
        Me.conf_bill_curr_layount.Controls.Add(Me.lbl_cnf_bill_tagk_date_val, 1, 5)
        Me.conf_bill_curr_layount.Location = New System.Drawing.Point(232, 29)
        Me.conf_bill_curr_layount.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_curr_layount.Name = "conf_bill_curr_layount"
        Me.conf_bill_curr_layount.RowCount = 6
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.11686!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.03172!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.6177!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.11686!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.11686!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.conf_bill_curr_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.conf_bill_curr_layount.Size = New System.Drawing.Size(402, 297)
        Me.conf_bill_curr_layount.TabIndex = 2
        '
        'lbl_cnf_bill_tagk_date
        '
        Me.lbl_cnf_bill_tagk_date.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_tagk_date.AutoSize = True
        Me.lbl_cnf_bill_tagk_date.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_tagk_date.Location = New System.Drawing.Point(4, 270)
        Me.lbl_cnf_bill_tagk_date.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_tagk_date.Name = "lbl_cnf_bill_tagk_date"
        Me.lbl_cnf_bill_tagk_date.Size = New System.Drawing.Size(123, 20)
        Me.lbl_cnf_bill_tagk_date.TabIndex = 49
        Me.lbl_cnf_bill_tagk_date.Text = "TAGK Date *:"
        '
        'lbl_cnf_bill_lab_curr
        '
        Me.lbl_cnf_bill_lab_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_lab_curr.AutoSize = True
        Me.lbl_cnf_bill_lab_curr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_lab_curr.Location = New System.Drawing.Point(4, 16)
        Me.lbl_cnf_bill_lab_curr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_lab_curr.Name = "lbl_cnf_bill_lab_curr"
        Me.lbl_cnf_bill_lab_curr.Size = New System.Drawing.Size(158, 20)
        Me.lbl_cnf_bill_lab_curr.TabIndex = 4
        Me.lbl_cnf_bill_lab_curr.Text = "Labor Currency *:"
        '
        'lbl_cnf_bill_mat_curr
        '
        Me.lbl_cnf_bill_mat_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_mat_curr.AutoSize = True
        Me.lbl_cnf_bill_mat_curr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_mat_curr.Location = New System.Drawing.Point(4, 68)
        Me.lbl_cnf_bill_mat_curr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_mat_curr.Name = "lbl_cnf_bill_mat_curr"
        Me.lbl_cnf_bill_mat_curr.Size = New System.Drawing.Size(178, 20)
        Me.lbl_cnf_bill_mat_curr.TabIndex = 11
        Me.lbl_cnf_bill_mat_curr.Text = "Material Currency *:"
        '
        'lbl_cnf_bill_rep_curr
        '
        Me.lbl_cnf_bill_rep_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_rep_curr.AutoSize = True
        Me.lbl_cnf_bill_rep_curr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_rep_curr.Location = New System.Drawing.Point(4, 120)
        Me.lbl_cnf_bill_rep_curr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_rep_curr.Name = "lbl_cnf_bill_rep_curr"
        Me.lbl_cnf_bill_rep_curr.Size = New System.Drawing.Size(199, 20)
        Me.lbl_cnf_bill_rep_curr.TabIndex = 15
        Me.lbl_cnf_bill_rep_curr.Text = "Small Mat. Currency *:"
        '
        'lbl_cnf_bill_3party_curr
        '
        Me.lbl_cnf_bill_3party_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_3party_curr.AutoSize = True
        Me.lbl_cnf_bill_3party_curr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_3party_curr.Location = New System.Drawing.Point(4, 173)
        Me.lbl_cnf_bill_3party_curr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_3party_curr.Name = "lbl_cnf_bill_3party_curr"
        Me.lbl_cnf_bill_3party_curr.Size = New System.Drawing.Size(187, 20)
        Me.lbl_cnf_bill_3party_curr.TabIndex = 16
        Me.lbl_cnf_bill_3party_curr.Text = "3rd Party Currency *:"
        '
        'lbl_cnf_bill_freight_curr
        '
        Me.lbl_cnf_bill_freight_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_freight_curr.AutoSize = True
        Me.lbl_cnf_bill_freight_curr.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_freight_curr.Location = New System.Drawing.Point(4, 226)
        Me.lbl_cnf_bill_freight_curr.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_freight_curr.Name = "lbl_cnf_bill_freight_curr"
        Me.lbl_cnf_bill_freight_curr.Size = New System.Drawing.Size(169, 20)
        Me.lbl_cnf_bill_freight_curr.TabIndex = 17
        Me.lbl_cnf_bill_freight_curr.Text = "Freight Currency *:"
        '
        'lbl_cnf_bill_lab_curr_val
        '
        Me.lbl_cnf_bill_lab_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_lab_curr_val.FormattingEnabled = True
        Me.lbl_cnf_bill_lab_curr_val.Location = New System.Drawing.Point(220, 12)
        Me.lbl_cnf_bill_lab_curr_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_lab_curr_val.Name = "lbl_cnf_bill_lab_curr_val"
        Me.lbl_cnf_bill_lab_curr_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_lab_curr_val.TabIndex = 44
        '
        'lbl_cnf_bill_mat_curr_val
        '
        Me.lbl_cnf_bill_mat_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_mat_curr_val.FormattingEnabled = True
        Me.lbl_cnf_bill_mat_curr_val.Location = New System.Drawing.Point(220, 64)
        Me.lbl_cnf_bill_mat_curr_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_mat_curr_val.Name = "lbl_cnf_bill_mat_curr_val"
        Me.lbl_cnf_bill_mat_curr_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_mat_curr_val.TabIndex = 45
        '
        'lbl_cnf_bill_rep_curr_val
        '
        Me.lbl_cnf_bill_rep_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_rep_curr_val.Enabled = False
        Me.lbl_cnf_bill_rep_curr_val.FormattingEnabled = True
        Me.lbl_cnf_bill_rep_curr_val.Location = New System.Drawing.Point(220, 116)
        Me.lbl_cnf_bill_rep_curr_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_rep_curr_val.Name = "lbl_cnf_bill_rep_curr_val"
        Me.lbl_cnf_bill_rep_curr_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_rep_curr_val.TabIndex = 46
        '
        'lbl_cnf_bill_3party_curr_val
        '
        Me.lbl_cnf_bill_3party_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_3party_curr_val.Enabled = False
        Me.lbl_cnf_bill_3party_curr_val.FormattingEnabled = True
        Me.lbl_cnf_bill_3party_curr_val.Location = New System.Drawing.Point(220, 169)
        Me.lbl_cnf_bill_3party_curr_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_3party_curr_val.Name = "lbl_cnf_bill_3party_curr_val"
        Me.lbl_cnf_bill_3party_curr_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_3party_curr_val.TabIndex = 47
        '
        'lbl_cnf_bill_freight_curr_val
        '
        Me.lbl_cnf_bill_freight_curr_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_freight_curr_val.Enabled = False
        Me.lbl_cnf_bill_freight_curr_val.FormattingEnabled = True
        Me.lbl_cnf_bill_freight_curr_val.Location = New System.Drawing.Point(220, 222)
        Me.lbl_cnf_bill_freight_curr_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_freight_curr_val.Name = "lbl_cnf_bill_freight_curr_val"
        Me.lbl_cnf_bill_freight_curr_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_freight_curr_val.TabIndex = 48
        '
        'lbl_cnf_bill_tagk_date_val
        '
        Me.lbl_cnf_bill_tagk_date_val.Location = New System.Drawing.Point(220, 268)
        Me.lbl_cnf_bill_tagk_date_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_tagk_date_val.Name = "lbl_cnf_bill_tagk_date_val"
        Me.lbl_cnf_bill_tagk_date_val.Size = New System.Drawing.Size(164, 26)
        Me.lbl_cnf_bill_tagk_date_val.TabIndex = 50
        '
        'conf_bill_markup_grp
        '
        Me.conf_bill_markup_grp.Controls.Add(Me.conf_bill_markup_grp_cap_p_grp)
        Me.conf_bill_markup_grp.Controls.Add(Me.conf_bill_markup_grp_cap_l_grp)
        Me.conf_bill_markup_grp.Controls.Add(Me.chk_b_conf_bill_markup_is_proj_cap)
        Me.conf_bill_markup_grp.Controls.Add(Me.chk_b_conf_bill_markup_is_line_cap)
        Me.conf_bill_markup_grp.Controls.Add(Me.conf_bill_mat_markup_grid_p)
        Me.conf_bill_markup_grp.Controls.Add(Me.conf_bill_markup_layount)
        Me.conf_bill_markup_grp.Location = New System.Drawing.Point(4, 563)
        Me.conf_bill_markup_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_grp.Name = "conf_bill_markup_grp"
        Me.conf_bill_markup_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_grp.Size = New System.Drawing.Size(2204, 629)
        Me.conf_bill_markup_grp.TabIndex = 2
        Me.conf_bill_markup_grp.TabStop = False
        Me.conf_bill_markup_grp.Text = "2-Material Markup"
        '
        'conf_bill_markup_grp_cap_p_grp
        '
        Me.conf_bill_markup_grp_cap_p_grp.Controls.Add(Me.cb_cnf_bill_markup_cap_proj_curr)
        Me.conf_bill_markup_grp_cap_p_grp.Controls.Add(Me.cb_cnf_bill_markup_cap_proj_value)
        Me.conf_bill_markup_grp_cap_p_grp.Location = New System.Drawing.Point(1310, 434)
        Me.conf_bill_markup_grp_cap_p_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_grp_cap_p_grp.Name = "conf_bill_markup_grp_cap_p_grp"
        Me.conf_bill_markup_grp_cap_p_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_grp_cap_p_grp.Size = New System.Drawing.Size(450, 123)
        Me.conf_bill_markup_grp_cap_p_grp.TabIndex = 8
        Me.conf_bill_markup_grp_cap_p_grp.TabStop = False
        '
        'cb_cnf_bill_markup_cap_proj_curr
        '
        Me.cb_cnf_bill_markup_cap_proj_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cb_cnf_bill_markup_cap_proj_curr.FormattingEnabled = True
        Me.cb_cnf_bill_markup_cap_proj_curr.Location = New System.Drawing.Point(226, 45)
        Me.cb_cnf_bill_markup_cap_proj_curr.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_markup_cap_proj_curr.Name = "cb_cnf_bill_markup_cap_proj_curr"
        Me.cb_cnf_bill_markup_cap_proj_curr.Size = New System.Drawing.Size(164, 28)
        Me.cb_cnf_bill_markup_cap_proj_curr.TabIndex = 47
        '
        'cb_cnf_bill_markup_cap_proj_value
        '
        Me.cb_cnf_bill_markup_cap_proj_value.Location = New System.Drawing.Point(10, 46)
        Me.cb_cnf_bill_markup_cap_proj_value.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_markup_cap_proj_value.Name = "cb_cnf_bill_markup_cap_proj_value"
        Me.cb_cnf_bill_markup_cap_proj_value.Size = New System.Drawing.Size(146, 26)
        Me.cb_cnf_bill_markup_cap_proj_value.TabIndex = 46
        '
        'conf_bill_markup_grp_cap_l_grp
        '
        Me.conf_bill_markup_grp_cap_l_grp.Controls.Add(Me.cb_cnf_bill_markup_cap_line_curr)
        Me.conf_bill_markup_grp_cap_l_grp.Controls.Add(Me.cb_cnf_bill_markup_cap_line_value)
        Me.conf_bill_markup_grp_cap_l_grp.Location = New System.Drawing.Point(1310, 195)
        Me.conf_bill_markup_grp_cap_l_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_grp_cap_l_grp.Name = "conf_bill_markup_grp_cap_l_grp"
        Me.conf_bill_markup_grp_cap_l_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_grp_cap_l_grp.Size = New System.Drawing.Size(444, 123)
        Me.conf_bill_markup_grp_cap_l_grp.TabIndex = 7
        Me.conf_bill_markup_grp_cap_l_grp.TabStop = False
        '
        'cb_cnf_bill_markup_cap_line_curr
        '
        Me.cb_cnf_bill_markup_cap_line_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cb_cnf_bill_markup_cap_line_curr.FormattingEnabled = True
        Me.cb_cnf_bill_markup_cap_line_curr.Location = New System.Drawing.Point(226, 46)
        Me.cb_cnf_bill_markup_cap_line_curr.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_markup_cap_line_curr.Name = "cb_cnf_bill_markup_cap_line_curr"
        Me.cb_cnf_bill_markup_cap_line_curr.Size = New System.Drawing.Size(164, 28)
        Me.cb_cnf_bill_markup_cap_line_curr.TabIndex = 45
        '
        'cb_cnf_bill_markup_cap_line_value
        '
        Me.cb_cnf_bill_markup_cap_line_value.Location = New System.Drawing.Point(10, 48)
        Me.cb_cnf_bill_markup_cap_line_value.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_markup_cap_line_value.Name = "cb_cnf_bill_markup_cap_line_value"
        Me.cb_cnf_bill_markup_cap_line_value.Size = New System.Drawing.Size(146, 26)
        Me.cb_cnf_bill_markup_cap_line_value.TabIndex = 0
        '
        'chk_b_conf_bill_markup_is_proj_cap
        '
        Me.chk_b_conf_bill_markup_is_proj_cap.AutoSize = True
        Me.chk_b_conf_bill_markup_is_proj_cap.Location = New System.Drawing.Point(1310, 411)
        Me.chk_b_conf_bill_markup_is_proj_cap.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.chk_b_conf_bill_markup_is_proj_cap.Name = "chk_b_conf_bill_markup_is_proj_cap"
        Me.chk_b_conf_bill_markup_is_proj_cap.Size = New System.Drawing.Size(279, 24)
        Me.chk_b_conf_bill_markup_is_proj_cap.TabIndex = 6
        Me.chk_b_conf_bill_markup_is_proj_cap.Text = "Cap Material Markup Per Project"
        Me.chk_b_conf_bill_markup_is_proj_cap.UseVisualStyleBackColor = True
        '
        'chk_b_conf_bill_markup_is_line_cap
        '
        Me.chk_b_conf_bill_markup_is_line_cap.AutoSize = True
        Me.chk_b_conf_bill_markup_is_line_cap.Location = New System.Drawing.Point(1310, 172)
        Me.chk_b_conf_bill_markup_is_line_cap.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.chk_b_conf_bill_markup_is_line_cap.Name = "chk_b_conf_bill_markup_is_line_cap"
        Me.chk_b_conf_bill_markup_is_line_cap.Size = New System.Drawing.Size(258, 24)
        Me.chk_b_conf_bill_markup_is_line_cap.TabIndex = 5
        Me.chk_b_conf_bill_markup_is_line_cap.Text = "Cap Material Markup Per Line"
        Me.chk_b_conf_bill_markup_is_line_cap.UseVisualStyleBackColor = True
        '
        'conf_bill_mat_markup_grid_p
        '
        Me.conf_bill_mat_markup_grid_p.Controls.Add(Me.conf_bill_mat_markup_grid)
        Me.conf_bill_mat_markup_grid_p.Location = New System.Drawing.Point(10, 118)
        Me.conf_bill_mat_markup_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_mat_markup_grid_p.Name = "conf_bill_mat_markup_grid_p"
        Me.conf_bill_mat_markup_grid_p.Size = New System.Drawing.Size(1035, 511)
        Me.conf_bill_mat_markup_grid_p.TabIndex = 4
        '
        'conf_bill_mat_markup_grid
        '
        Me.conf_bill_mat_markup_grid.AllowUserToAddRows = False
        Me.conf_bill_mat_markup_grid.AllowUserToDeleteRows = False
        Me.conf_bill_mat_markup_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_mat_markup_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_mat_markup_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_mat_markup_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_mat_markup_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_mat_markup_grid.Name = "conf_bill_mat_markup_grid"
        Me.conf_bill_mat_markup_grid.Size = New System.Drawing.Size(1035, 511)
        Me.conf_bill_mat_markup_grid.TabIndex = 3
        '
        'conf_bill_markup_layount
        '
        Me.conf_bill_markup_layount.ColumnCount = 2
        Me.conf_bill_markup_layount.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.conf_bill_markup_layount.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.conf_bill_markup_layount.Controls.Add(Me.lbl_cnf_bill_markup, 0, 0)
        Me.conf_bill_markup_layount.Controls.Add(Me.lbl_cnf_bill_markup_val, 1, 0)
        Me.conf_bill_markup_layount.Location = New System.Drawing.Point(302, 54)
        Me.conf_bill_markup_layount.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_markup_layount.Name = "conf_bill_markup_layount"
        Me.conf_bill_markup_layount.RowCount = 1
        Me.conf_bill_markup_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_bill_markup_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_markup_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_markup_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_markup_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_markup_layount.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_markup_layount.Size = New System.Drawing.Size(381, 54)
        Me.conf_bill_markup_layount.TabIndex = 2
        '
        'lbl_cnf_bill_markup
        '
        Me.lbl_cnf_bill_markup.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_markup.AutoSize = True
        Me.lbl_cnf_bill_markup.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_markup.Location = New System.Drawing.Point(4, 17)
        Me.lbl_cnf_bill_markup.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_markup.Name = "lbl_cnf_bill_markup"
        Me.lbl_cnf_bill_markup.Size = New System.Drawing.Size(163, 20)
        Me.lbl_cnf_bill_markup.TabIndex = 4
        Me.lbl_cnf_bill_markup.Text = "Material Markup *:"
        '
        'lbl_cnf_bill_markup_val
        '
        Me.lbl_cnf_bill_markup_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_markup_val.FormattingEnabled = True
        Me.lbl_cnf_bill_markup_val.Location = New System.Drawing.Point(209, 13)
        Me.lbl_cnf_bill_markup_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_markup_val.Name = "lbl_cnf_bill_markup_val"
        Me.lbl_cnf_bill_markup_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_markup_val.TabIndex = 44
        '
        'conf_bill_freight_grp
        '
        Me.conf_bill_freight_grp.Controls.Add(Me.TableLayoutPanel1)
        Me.conf_bill_freight_grp.Controls.Add(Me.conf_bill_freight_grp_cap_p_grp)
        Me.conf_bill_freight_grp.Controls.Add(Me.conf_bill_freight_grp_cap_l_grp)
        Me.conf_bill_freight_grp.Controls.Add(Me.chk_b_conf_bill_freight_is_proj_cap)
        Me.conf_bill_freight_grp.Controls.Add(Me.chk_b_conf_bill_freight_is_line_cap)
        Me.conf_bill_freight_grp.Controls.Add(Me.conf_bill_mat_freight_grid_p)
        Me.conf_bill_freight_grp.Location = New System.Drawing.Point(4, 1202)
        Me.conf_bill_freight_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_freight_grp.Name = "conf_bill_freight_grp"
        Me.conf_bill_freight_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_freight_grp.Size = New System.Drawing.Size(2204, 629)
        Me.conf_bill_freight_grp.TabIndex = 3
        Me.conf_bill_freight_grp.TabStop = False
        Me.conf_bill_freight_grp.Text = "3-Freight"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.TableLayoutPanel1.Controls.Add(Me.lbl_cnf_bill_freight_rate, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.lbl_cnf_bill_freight_rate_val, 1, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(1310, 48)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(444, 54)
        Me.TableLayoutPanel1.TabIndex = 9
        '
        'lbl_cnf_bill_freight_rate
        '
        Me.lbl_cnf_bill_freight_rate.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_freight_rate.AutoSize = True
        Me.lbl_cnf_bill_freight_rate.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_freight_rate.Location = New System.Drawing.Point(4, 17)
        Me.lbl_cnf_bill_freight_rate.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_freight_rate.Name = "lbl_cnf_bill_freight_rate"
        Me.lbl_cnf_bill_freight_rate.Size = New System.Drawing.Size(199, 20)
        Me.lbl_cnf_bill_freight_rate.TabIndex = 4
        Me.lbl_cnf_bill_freight_rate.Text = "Default Freight Rate *:"
        '
        'lbl_cnf_bill_freight_rate_val
        '
        Me.lbl_cnf_bill_freight_rate_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_freight_rate_val.Location = New System.Drawing.Point(243, 14)
        Me.lbl_cnf_bill_freight_rate_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_freight_rate_val.Name = "lbl_cnf_bill_freight_rate_val"
        Me.lbl_cnf_bill_freight_rate_val.Size = New System.Drawing.Size(148, 26)
        Me.lbl_cnf_bill_freight_rate_val.TabIndex = 5
        '
        'conf_bill_freight_grp_cap_p_grp
        '
        Me.conf_bill_freight_grp_cap_p_grp.Controls.Add(Me.cb_cnf_bill_freight_cap_proj_curr)
        Me.conf_bill_freight_grp_cap_p_grp.Controls.Add(Me.cb_cnf_bill_freight_cap_proj_value)
        Me.conf_bill_freight_grp_cap_p_grp.Location = New System.Drawing.Point(1310, 402)
        Me.conf_bill_freight_grp_cap_p_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_freight_grp_cap_p_grp.Name = "conf_bill_freight_grp_cap_p_grp"
        Me.conf_bill_freight_grp_cap_p_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_freight_grp_cap_p_grp.Size = New System.Drawing.Size(450, 123)
        Me.conf_bill_freight_grp_cap_p_grp.TabIndex = 8
        Me.conf_bill_freight_grp_cap_p_grp.TabStop = False
        '
        'cb_cnf_bill_freight_cap_proj_curr
        '
        Me.cb_cnf_bill_freight_cap_proj_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cb_cnf_bill_freight_cap_proj_curr.FormattingEnabled = True
        Me.cb_cnf_bill_freight_cap_proj_curr.Location = New System.Drawing.Point(226, 45)
        Me.cb_cnf_bill_freight_cap_proj_curr.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_freight_cap_proj_curr.Name = "cb_cnf_bill_freight_cap_proj_curr"
        Me.cb_cnf_bill_freight_cap_proj_curr.Size = New System.Drawing.Size(164, 28)
        Me.cb_cnf_bill_freight_cap_proj_curr.TabIndex = 47
        '
        'cb_cnf_bill_freight_cap_proj_value
        '
        Me.cb_cnf_bill_freight_cap_proj_value.Location = New System.Drawing.Point(10, 46)
        Me.cb_cnf_bill_freight_cap_proj_value.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_freight_cap_proj_value.Name = "cb_cnf_bill_freight_cap_proj_value"
        Me.cb_cnf_bill_freight_cap_proj_value.Size = New System.Drawing.Size(146, 26)
        Me.cb_cnf_bill_freight_cap_proj_value.TabIndex = 46
        '
        'conf_bill_freight_grp_cap_l_grp
        '
        Me.conf_bill_freight_grp_cap_l_grp.Controls.Add(Me.cb_cnf_bill_freight_cap_line_curr)
        Me.conf_bill_freight_grp_cap_l_grp.Controls.Add(Me.cb_cnf_bill_freight_cap_line_value)
        Me.conf_bill_freight_grp_cap_l_grp.Location = New System.Drawing.Point(1310, 211)
        Me.conf_bill_freight_grp_cap_l_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_freight_grp_cap_l_grp.Name = "conf_bill_freight_grp_cap_l_grp"
        Me.conf_bill_freight_grp_cap_l_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_freight_grp_cap_l_grp.Size = New System.Drawing.Size(444, 123)
        Me.conf_bill_freight_grp_cap_l_grp.TabIndex = 7
        Me.conf_bill_freight_grp_cap_l_grp.TabStop = False
        '
        'cb_cnf_bill_freight_cap_line_curr
        '
        Me.cb_cnf_bill_freight_cap_line_curr.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.cb_cnf_bill_freight_cap_line_curr.FormattingEnabled = True
        Me.cb_cnf_bill_freight_cap_line_curr.Location = New System.Drawing.Point(226, 46)
        Me.cb_cnf_bill_freight_cap_line_curr.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_freight_cap_line_curr.Name = "cb_cnf_bill_freight_cap_line_curr"
        Me.cb_cnf_bill_freight_cap_line_curr.Size = New System.Drawing.Size(164, 28)
        Me.cb_cnf_bill_freight_cap_line_curr.TabIndex = 45
        '
        'cb_cnf_bill_freight_cap_line_value
        '
        Me.cb_cnf_bill_freight_cap_line_value.Location = New System.Drawing.Point(10, 48)
        Me.cb_cnf_bill_freight_cap_line_value.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.cb_cnf_bill_freight_cap_line_value.Name = "cb_cnf_bill_freight_cap_line_value"
        Me.cb_cnf_bill_freight_cap_line_value.Size = New System.Drawing.Size(146, 26)
        Me.cb_cnf_bill_freight_cap_line_value.TabIndex = 0
        '
        'chk_b_conf_bill_freight_is_proj_cap
        '
        Me.chk_b_conf_bill_freight_is_proj_cap.AutoSize = True
        Me.chk_b_conf_bill_freight_is_proj_cap.Location = New System.Drawing.Point(1310, 378)
        Me.chk_b_conf_bill_freight_is_proj_cap.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.chk_b_conf_bill_freight_is_proj_cap.Name = "chk_b_conf_bill_freight_is_proj_cap"
        Me.chk_b_conf_bill_freight_is_proj_cap.Size = New System.Drawing.Size(211, 24)
        Me.chk_b_conf_bill_freight_is_proj_cap.TabIndex = 6
        Me.chk_b_conf_bill_freight_is_proj_cap.Text = "Cap Freight Per Project"
        Me.chk_b_conf_bill_freight_is_proj_cap.UseVisualStyleBackColor = True
        '
        'chk_b_conf_bill_freight_is_line_cap
        '
        Me.chk_b_conf_bill_freight_is_line_cap.AutoSize = True
        Me.chk_b_conf_bill_freight_is_line_cap.Location = New System.Drawing.Point(1310, 188)
        Me.chk_b_conf_bill_freight_is_line_cap.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.chk_b_conf_bill_freight_is_line_cap.Name = "chk_b_conf_bill_freight_is_line_cap"
        Me.chk_b_conf_bill_freight_is_line_cap.Size = New System.Drawing.Size(190, 24)
        Me.chk_b_conf_bill_freight_is_line_cap.TabIndex = 5
        Me.chk_b_conf_bill_freight_is_line_cap.Text = "Cap Freight Per Line"
        Me.chk_b_conf_bill_freight_is_line_cap.UseVisualStyleBackColor = True
        '
        'conf_bill_mat_freight_grid_p
        '
        Me.conf_bill_mat_freight_grid_p.Controls.Add(Me.conf_bill_mat_freight_grid)
        Me.conf_bill_mat_freight_grid_p.Location = New System.Drawing.Point(10, 29)
        Me.conf_bill_mat_freight_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_mat_freight_grid_p.Name = "conf_bill_mat_freight_grid_p"
        Me.conf_bill_mat_freight_grid_p.Size = New System.Drawing.Size(1035, 589)
        Me.conf_bill_mat_freight_grid_p.TabIndex = 4
        '
        'conf_bill_mat_freight_grid
        '
        Me.conf_bill_mat_freight_grid.AllowUserToAddRows = False
        Me.conf_bill_mat_freight_grid.AllowUserToDeleteRows = False
        Me.conf_bill_mat_freight_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_mat_freight_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_mat_freight_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_mat_freight_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_mat_freight_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_mat_freight_grid.Name = "conf_bill_mat_freight_grid"
        Me.conf_bill_mat_freight_grid.Size = New System.Drawing.Size(1035, 589)
        Me.conf_bill_mat_freight_grid.TabIndex = 3
        '
        'conf_bill_surcharge_grp
        '
        Me.conf_bill_surcharge_grp.Controls.Add(Me.conf_bill_surch_eco_grid_p)
        Me.conf_bill_surcharge_grp.Controls.Add(Me.conf_bill_surch_eco_layout)
        Me.conf_bill_surcharge_grp.Controls.Add(Me.conf_bill_surch_gum_grid_p)
        Me.conf_bill_surcharge_grp.Controls.Add(Me.conf_bill_surch_gum_layout)
        Me.conf_bill_surcharge_grp.Controls.Add(Me.PictureBox4)
        Me.conf_bill_surcharge_grp.Location = New System.Drawing.Point(4, 1841)
        Me.conf_bill_surcharge_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surcharge_grp.Name = "conf_bill_surcharge_grp"
        Me.conf_bill_surcharge_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surcharge_grp.Size = New System.Drawing.Size(2204, 629)
        Me.conf_bill_surcharge_grp.TabIndex = 4
        Me.conf_bill_surcharge_grp.TabStop = False
        Me.conf_bill_surcharge_grp.Text = "4-Surcharge"
        '
        'conf_bill_surch_eco_grid_p
        '
        Me.conf_bill_surch_eco_grid_p.Controls.Add(Me.conf_bill_surch_eco_grid)
        Me.conf_bill_surch_eco_grid_p.Location = New System.Drawing.Point(459, 445)
        Me.conf_bill_surch_eco_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surch_eco_grid_p.Name = "conf_bill_surch_eco_grid_p"
        Me.conf_bill_surch_eco_grid_p.Size = New System.Drawing.Size(854, 175)
        Me.conf_bill_surch_eco_grid_p.TabIndex = 6
        '
        'conf_bill_surch_eco_grid
        '
        Me.conf_bill_surch_eco_grid.AllowUserToAddRows = False
        Me.conf_bill_surch_eco_grid.AllowUserToDeleteRows = False
        Me.conf_bill_surch_eco_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_surch_eco_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_surch_eco_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_surch_eco_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_surch_eco_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surch_eco_grid.Name = "conf_bill_surch_eco_grid"
        Me.conf_bill_surch_eco_grid.Size = New System.Drawing.Size(854, 175)
        Me.conf_bill_surch_eco_grid.TabIndex = 3
        '
        'conf_bill_surch_eco_layout
        '
        Me.conf_bill_surch_eco_layout.ColumnCount = 2
        Me.conf_bill_surch_eco_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.conf_bill_surch_eco_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.conf_bill_surch_eco_layout.Controls.Add(Me.lbl_cnf_bill_eco, 0, 0)
        Me.conf_bill_surch_eco_layout.Controls.Add(Me.lbl_cnf_bill_eco_val, 1, 0)
        Me.conf_bill_surch_eco_layout.Location = New System.Drawing.Point(686, 382)
        Me.conf_bill_surch_eco_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surch_eco_layout.Name = "conf_bill_surch_eco_layout"
        Me.conf_bill_surch_eco_layout.RowCount = 1
        Me.conf_bill_surch_eco_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_bill_surch_eco_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_eco_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_eco_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_eco_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_eco_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_eco_layout.Size = New System.Drawing.Size(381, 54)
        Me.conf_bill_surch_eco_layout.TabIndex = 5
        '
        'lbl_cnf_bill_eco
        '
        Me.lbl_cnf_bill_eco.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_eco.AutoSize = True
        Me.lbl_cnf_bill_eco.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_eco.Location = New System.Drawing.Point(4, 17)
        Me.lbl_cnf_bill_eco.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_eco.Name = "lbl_cnf_bill_eco"
        Me.lbl_cnf_bill_eco.Size = New System.Drawing.Size(108, 20)
        Me.lbl_cnf_bill_eco.TabIndex = 4
        Me.lbl_cnf_bill_eco.Text = "ECO TAX *:"
        '
        'lbl_cnf_bill_eco_val
        '
        Me.lbl_cnf_bill_eco_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_eco_val.FormattingEnabled = True
        Me.lbl_cnf_bill_eco_val.Location = New System.Drawing.Point(209, 13)
        Me.lbl_cnf_bill_eco_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_eco_val.Name = "lbl_cnf_bill_eco_val"
        Me.lbl_cnf_bill_eco_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_eco_val.TabIndex = 44
        '
        'conf_bill_surch_gum_grid_p
        '
        Me.conf_bill_surch_gum_grid_p.Controls.Add(Me.conf_bill_surch_gum_grid)
        Me.conf_bill_surch_gum_grid_p.Location = New System.Drawing.Point(459, 109)
        Me.conf_bill_surch_gum_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surch_gum_grid_p.Name = "conf_bill_surch_gum_grid_p"
        Me.conf_bill_surch_gum_grid_p.Size = New System.Drawing.Size(854, 175)
        Me.conf_bill_surch_gum_grid_p.TabIndex = 4
        '
        'conf_bill_surch_gum_grid
        '
        Me.conf_bill_surch_gum_grid.AllowUserToAddRows = False
        Me.conf_bill_surch_gum_grid.AllowUserToDeleteRows = False
        Me.conf_bill_surch_gum_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_surch_gum_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_surch_gum_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_surch_gum_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_surch_gum_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surch_gum_grid.Name = "conf_bill_surch_gum_grid"
        Me.conf_bill_surch_gum_grid.Size = New System.Drawing.Size(854, 175)
        Me.conf_bill_surch_gum_grid.TabIndex = 3
        '
        'conf_bill_surch_gum_layout
        '
        Me.conf_bill_surch_gum_layout.ColumnCount = 2
        Me.conf_bill_surch_gum_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.conf_bill_surch_gum_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.conf_bill_surch_gum_layout.Controls.Add(Me.lbl_cnf_bill_gum, 0, 0)
        Me.conf_bill_surch_gum_layout.Controls.Add(Me.lbl_cnf_bill_gum_val, 1, 0)
        Me.conf_bill_surch_gum_layout.Location = New System.Drawing.Point(686, 46)
        Me.conf_bill_surch_gum_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_surch_gum_layout.Name = "conf_bill_surch_gum_layout"
        Me.conf_bill_surch_gum_layout.RowCount = 1
        Me.conf_bill_surch_gum_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_bill_surch_gum_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_gum_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_gum_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_gum_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_gum_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_bill_surch_gum_layout.Size = New System.Drawing.Size(381, 54)
        Me.conf_bill_surch_gum_layout.TabIndex = 2
        '
        'lbl_cnf_bill_gum
        '
        Me.lbl_cnf_bill_gum.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_gum.AutoSize = True
        Me.lbl_cnf_bill_gum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_cnf_bill_gum.Location = New System.Drawing.Point(4, 17)
        Me.lbl_cnf_bill_gum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_cnf_bill_gum.Name = "lbl_cnf_bill_gum"
        Me.lbl_cnf_bill_gum.Size = New System.Drawing.Size(70, 20)
        Me.lbl_cnf_bill_gum.TabIndex = 4
        Me.lbl_cnf_bill_gum.Text = "GUM *:"
        '
        'lbl_cnf_bill_gum_val
        '
        Me.lbl_cnf_bill_gum_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_cnf_bill_gum_val.FormattingEnabled = True
        Me.lbl_cnf_bill_gum_val.Location = New System.Drawing.Point(209, 13)
        Me.lbl_cnf_bill_gum_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_cnf_bill_gum_val.Name = "lbl_cnf_bill_gum_val"
        Me.lbl_cnf_bill_gum_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_cnf_bill_gum_val.TabIndex = 44
        '
        'PictureBox4
        '
        Me.PictureBox4.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox4.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(418, 589)
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'conf_bill_down_pay_grp
        '
        Me.conf_bill_down_pay_grp.Controls.Add(Me.conf_bill_down_payment_grid_p)
        Me.conf_bill_down_pay_grp.Location = New System.Drawing.Point(4, 2480)
        Me.conf_bill_down_pay_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_down_pay_grp.Name = "conf_bill_down_pay_grp"
        Me.conf_bill_down_pay_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_down_pay_grp.Size = New System.Drawing.Size(2204, 337)
        Me.conf_bill_down_pay_grp.TabIndex = 6
        Me.conf_bill_down_pay_grp.TabStop = False
        Me.conf_bill_down_pay_grp.Text = "5-Downpayments"
        '
        'conf_bill_down_payment_grid_p
        '
        Me.conf_bill_down_payment_grid_p.Controls.Add(Me.conf_bill_down_payment_grid)
        Me.conf_bill_down_payment_grid_p.Location = New System.Drawing.Point(10, 29)
        Me.conf_bill_down_payment_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_down_payment_grid_p.Name = "conf_bill_down_payment_grid_p"
        Me.conf_bill_down_payment_grid_p.Size = New System.Drawing.Size(1736, 283)
        Me.conf_bill_down_payment_grid_p.TabIndex = 6
        '
        'conf_bill_down_payment_grid
        '
        Me.conf_bill_down_payment_grid.AllowUserToAddRows = False
        Me.conf_bill_down_payment_grid.AllowUserToDeleteRows = False
        Me.conf_bill_down_payment_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_down_payment_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_down_payment_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_down_payment_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_down_payment_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_down_payment_grid.Name = "conf_bill_down_payment_grid"
        Me.conf_bill_down_payment_grid.Size = New System.Drawing.Size(1736, 283)
        Me.conf_bill_down_payment_grid.TabIndex = 3
        '
        'conf_bill_discount_grp
        '
        Me.conf_bill_discount_grp.Controls.Add(Me.conf_bill_discount_grid_p)
        Me.conf_bill_discount_grp.Location = New System.Drawing.Point(4, 2827)
        Me.conf_bill_discount_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_discount_grp.Name = "conf_bill_discount_grp"
        Me.conf_bill_discount_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_discount_grp.Size = New System.Drawing.Size(2204, 622)
        Me.conf_bill_discount_grp.TabIndex = 5
        Me.conf_bill_discount_grp.TabStop = False
        Me.conf_bill_discount_grp.Text = "6-Discount"
        '
        'conf_bill_discount_grid_p
        '
        Me.conf_bill_discount_grid_p.Controls.Add(Me.conf_bill_discount_grid)
        Me.conf_bill_discount_grid_p.Location = New System.Drawing.Point(10, 29)
        Me.conf_bill_discount_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_discount_grid_p.Name = "conf_bill_discount_grid_p"
        Me.conf_bill_discount_grid_p.Size = New System.Drawing.Size(1736, 555)
        Me.conf_bill_discount_grid_p.TabIndex = 6
        '
        'conf_bill_discount_grid
        '
        Me.conf_bill_discount_grid.AllowUserToAddRows = False
        Me.conf_bill_discount_grid.AllowUserToDeleteRows = False
        Me.conf_bill_discount_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_discount_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_discount_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_discount_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_discount_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_discount_grid.Name = "conf_bill_discount_grid"
        Me.conf_bill_discount_grid.Size = New System.Drawing.Size(1736, 555)
        Me.conf_bill_discount_grid.TabIndex = 3
        '
        'conf_bill_nte_grp
        '
        Me.conf_bill_nte_grp.Controls.Add(Me.conf_bill_nte_grid_p)
        Me.conf_bill_nte_grp.Location = New System.Drawing.Point(4, 3459)
        Me.conf_bill_nte_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_nte_grp.Name = "conf_bill_nte_grp"
        Me.conf_bill_nte_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_nte_grp.Size = New System.Drawing.Size(2212, 408)
        Me.conf_bill_nte_grp.TabIndex = 6
        Me.conf_bill_nte_grp.TabStop = False
        Me.conf_bill_nte_grp.Text = "7-NTE Discount"
        '
        'conf_bill_nte_grid_p
        '
        Me.conf_bill_nte_grid_p.Controls.Add(Me.conf_bill_nte_grid)
        Me.conf_bill_nte_grid_p.Location = New System.Drawing.Point(10, 29)
        Me.conf_bill_nte_grid_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_nte_grid_p.Name = "conf_bill_nte_grid_p"
        Me.conf_bill_nte_grid_p.Size = New System.Drawing.Size(1744, 335)
        Me.conf_bill_nte_grid_p.TabIndex = 6
        '
        'conf_bill_nte_grid
        '
        Me.conf_bill_nte_grid.AllowUserToAddRows = False
        Me.conf_bill_nte_grid.AllowUserToDeleteRows = False
        Me.conf_bill_nte_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.conf_bill_nte_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_bill_nte_grid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_bill_nte_grid.Location = New System.Drawing.Point(0, 0)
        Me.conf_bill_nte_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_nte_grid.Name = "conf_bill_nte_grid"
        Me.conf_bill_nte_grid.Size = New System.Drawing.Size(1744, 335)
        Me.conf_bill_nte_grid.TabIndex = 3
        '
        'conf_bill_top_menu_layout
        '
        Me.conf_bill_top_menu_layout.ColumnCount = 8
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_bill_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1060.0!))
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_downpay_lbl, 4, 0)
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_surcharge_lbl, 3, 0)
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_curr_lbl, 0, 0)
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_freight_lbl, 2, 0)
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_markup_lbl, 1, 0)
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_nte_lbl, 6, 0)
        Me.conf_bill_top_menu_layout.Controls.Add(Me.conf_bill_menu_discount_lbl, 5, 0)
        Me.conf_bill_top_menu_layout.Dock = System.Windows.Forms.DockStyle.Top
        Me.conf_bill_top_menu_layout.Location = New System.Drawing.Point(4, 5)
        Me.conf_bill_top_menu_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_bill_top_menu_layout.Name = "conf_bill_top_menu_layout"
        Me.conf_bill_top_menu_layout.RowCount = 1
        Me.conf_bill_top_menu_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_bill_top_menu_layout.Size = New System.Drawing.Size(1892, 45)
        Me.conf_bill_top_menu_layout.TabIndex = 0
        '
        'conf_bill_menu_downpay_lbl
        '
        Me.conf_bill_menu_downpay_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_downpay_lbl.AutoSize = True
        Me.conf_bill_menu_downpay_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_downpay_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_downpay_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_downpay_lbl.Location = New System.Drawing.Point(733, 12)
        Me.conf_bill_menu_downpay_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_downpay_lbl.Name = "conf_bill_menu_downpay_lbl"
        Me.conf_bill_menu_downpay_lbl.Size = New System.Drawing.Size(153, 20)
        Me.conf_bill_menu_downpay_lbl.TabIndex = 10
        Me.conf_bill_menu_downpay_lbl.Text = "5-Downpayments"
        Me.conf_bill_menu_downpay_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_bill_menu_surcharge_lbl
        '
        Me.conf_bill_menu_surcharge_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_surcharge_lbl.AutoSize = True
        Me.conf_bill_menu_surcharge_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_surcharge_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_surcharge_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_surcharge_lbl.Location = New System.Drawing.Point(574, 12)
        Me.conf_bill_menu_surcharge_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_surcharge_lbl.Name = "conf_bill_menu_surcharge_lbl"
        Me.conf_bill_menu_surcharge_lbl.Size = New System.Drawing.Size(112, 20)
        Me.conf_bill_menu_surcharge_lbl.TabIndex = 5
        Me.conf_bill_menu_surcharge_lbl.Text = "4-Surcharge"
        Me.conf_bill_menu_surcharge_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_bill_menu_curr_lbl
        '
        Me.conf_bill_menu_curr_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_curr_lbl.AutoSize = True
        Me.conf_bill_menu_curr_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_curr_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_curr_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_curr_lbl.Location = New System.Drawing.Point(10, 12)
        Me.conf_bill_menu_curr_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_curr_lbl.Name = "conf_bill_menu_curr_lbl"
        Me.conf_bill_menu_curr_lbl.Size = New System.Drawing.Size(160, 20)
        Me.conf_bill_menu_curr_lbl.TabIndex = 0
        Me.conf_bill_menu_curr_lbl.Text = "1-Curr and  Rates"
        Me.conf_bill_menu_curr_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_bill_menu_freight_lbl
        '
        Me.conf_bill_menu_freight_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_freight_lbl.AutoSize = True
        Me.conf_bill_menu_freight_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_freight_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_freight_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_freight_lbl.Location = New System.Drawing.Point(407, 12)
        Me.conf_bill_menu_freight_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_freight_lbl.Name = "conf_bill_menu_freight_lbl"
        Me.conf_bill_menu_freight_lbl.Size = New System.Drawing.Size(85, 20)
        Me.conf_bill_menu_freight_lbl.TabIndex = 2
        Me.conf_bill_menu_freight_lbl.Text = "3-Freight"
        Me.conf_bill_menu_freight_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_bill_menu_markup_lbl
        '
        Me.conf_bill_menu_markup_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_markup_lbl.AutoSize = True
        Me.conf_bill_menu_markup_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_markup_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_markup_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_markup_lbl.Location = New System.Drawing.Point(189, 12)
        Me.conf_bill_menu_markup_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_markup_lbl.Name = "conf_bill_menu_markup_lbl"
        Me.conf_bill_menu_markup_lbl.Size = New System.Drawing.Size(161, 20)
        Me.conf_bill_menu_markup_lbl.TabIndex = 1
        Me.conf_bill_menu_markup_lbl.Text = "2-Material markup"
        Me.conf_bill_menu_markup_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_bill_menu_nte_lbl
        '
        Me.conf_bill_menu_nte_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_nte_lbl.AutoSize = True
        Me.conf_bill_menu_nte_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_nte_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_nte_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_nte_lbl.Location = New System.Drawing.Point(1139, 12)
        Me.conf_bill_menu_nte_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_nte_lbl.Name = "conf_bill_menu_nte_lbl"
        Me.conf_bill_menu_nte_lbl.Size = New System.Drawing.Size(62, 20)
        Me.conf_bill_menu_nte_lbl.TabIndex = 10
        Me.conf_bill_menu_nte_lbl.Text = "7-NTE"
        Me.conf_bill_menu_nte_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_bill_menu_discount_lbl
        '
        Me.conf_bill_menu_discount_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_bill_menu_discount_lbl.AutoSize = True
        Me.conf_bill_menu_discount_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_bill_menu_discount_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_bill_menu_discount_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_bill_menu_discount_lbl.Location = New System.Drawing.Point(939, 12)
        Me.conf_bill_menu_discount_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_bill_menu_discount_lbl.Name = "conf_bill_menu_discount_lbl"
        Me.conf_bill_menu_discount_lbl.Size = New System.Drawing.Size(101, 20)
        Me.conf_bill_menu_discount_lbl.TabIndex = 9
        Me.conf_bill_menu_discount_lbl.Text = "6-Discount"
        Me.conf_bill_menu_discount_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tab_ctrl_config_page_rates
        '
        Me.tab_ctrl_config_page_rates.AutoScroll = True
        Me.tab_ctrl_config_page_rates.Controls.Add(Me.tab_ctrl_config_page_labrate_panel)
        Me.tab_ctrl_config_page_rates.Controls.Add(Me.conf_lab_rate_top_menu_layout)
        Me.tab_ctrl_config_page_rates.Location = New System.Drawing.Point(4, 4)
        Me.tab_ctrl_config_page_rates.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_rates.Name = "tab_ctrl_config_page_rates"
        Me.tab_ctrl_config_page_rates.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_rates.Size = New System.Drawing.Size(1900, 789)
        Me.tab_ctrl_config_page_rates.TabIndex = 2
        Me.tab_ctrl_config_page_rates.Text = "3-Labor Rates"
        Me.tab_ctrl_config_page_rates.UseVisualStyleBackColor = True
        '
        'tab_ctrl_config_page_labrate_panel
        '
        Me.tab_ctrl_config_page_labrate_panel.Controls.Add(Me.conf_labrate_layout)
        Me.tab_ctrl_config_page_labrate_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab_ctrl_config_page_labrate_panel.Location = New System.Drawing.Point(4, 50)
        Me.tab_ctrl_config_page_labrate_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_labrate_panel.Name = "tab_ctrl_config_page_labrate_panel"
        Me.tab_ctrl_config_page_labrate_panel.Size = New System.Drawing.Size(1892, 734)
        Me.tab_ctrl_config_page_labrate_panel.TabIndex = 2
        '
        'conf_labrate_layout
        '
        Me.conf_labrate_layout.AutoScroll = True
        Me.conf_labrate_layout.ColumnCount = 1
        Me.conf_labrate_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_labrate_layout.Controls.Add(Me.conf_labrate_task_rate_grp, 0, 1)
        Me.conf_labrate_layout.Controls.Add(Me.conf_labrate_labrate_std_grp, 0, 0)
        Me.conf_labrate_layout.Controls.Add(Me.conf_labrate_labrate_oem_grp, 0, 3)
        Me.conf_labrate_layout.Controls.Add(Me.conf_labrate_labrate_nte_grp, 0, 2)
        Me.conf_labrate_layout.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_labrate_layout.Location = New System.Drawing.Point(0, 0)
        Me.conf_labrate_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_layout.Name = "conf_labrate_layout"
        Me.conf_labrate_layout.RowCount = 4
        Me.conf_labrate_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.conf_labrate_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.conf_labrate_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.conf_labrate_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.conf_labrate_layout.Size = New System.Drawing.Size(1892, 734)
        Me.conf_labrate_layout.TabIndex = 0
        '
        'conf_labrate_task_rate_grp
        '
        Me.conf_labrate_task_rate_grp.AutoSize = True
        Me.conf_labrate_task_rate_grp.Controls.Add(Me.conf_labrate_task_rate_grid)
        Me.conf_labrate_task_rate_grp.Controls.Add(Me.PictureBox10)
        Me.conf_labrate_task_rate_grp.Location = New System.Drawing.Point(4, 709)
        Me.conf_labrate_task_rate_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_task_rate_grp.Name = "conf_labrate_task_rate_grp"
        Me.conf_labrate_task_rate_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_task_rate_grp.Size = New System.Drawing.Size(1799, 569)
        Me.conf_labrate_task_rate_grp.TabIndex = 6
        Me.conf_labrate_task_rate_grp.TabStop = False
        Me.conf_labrate_task_rate_grp.Text = "2-Task Rates"
        '
        'conf_labrate_task_rate_grid
        '
        Me.conf_labrate_task_rate_grid.AllowUserToAddRows = False
        Me.conf_labrate_task_rate_grid.AllowUserToDeleteRows = False
        Me.conf_labrate_task_rate_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_labrate_task_rate_grid.Location = New System.Drawing.Point(468, 29)
        Me.conf_labrate_task_rate_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_task_rate_grid.Name = "conf_labrate_task_rate_grid"
        Me.conf_labrate_task_rate_grid.Size = New System.Drawing.Size(1323, 511)
        Me.conf_labrate_task_rate_grid.TabIndex = 3
        '
        'PictureBox10
        '
        Me.PictureBox10.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox10.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(418, 509)
        Me.PictureBox10.TabIndex = 0
        Me.PictureBox10.TabStop = False
        '
        'conf_labrate_labrate_std_grp
        '
        Me.conf_labrate_labrate_std_grp.AutoSize = True
        Me.conf_labrate_labrate_std_grp.Controls.Add(Me.conf_labrate_labrate_std_grid)
        Me.conf_labrate_labrate_std_grp.Controls.Add(Me.conf_labrate_std_choice_layout)
        Me.conf_labrate_labrate_std_grp.Controls.Add(Me.PictureBox7)
        Me.conf_labrate_labrate_std_grp.Location = New System.Drawing.Point(4, 5)
        Me.conf_labrate_labrate_std_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_std_grp.Name = "conf_labrate_labrate_std_grp"
        Me.conf_labrate_labrate_std_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_std_grp.Size = New System.Drawing.Size(1799, 694)
        Me.conf_labrate_labrate_std_grp.TabIndex = 3
        Me.conf_labrate_labrate_std_grp.TabStop = False
        Me.conf_labrate_labrate_std_grp.Text = "1-Customer Rates"
        '
        'conf_labrate_labrate_std_grid
        '
        Me.conf_labrate_labrate_std_grid.AllowUserToAddRows = False
        Me.conf_labrate_labrate_std_grid.AllowUserToDeleteRows = False
        Me.conf_labrate_labrate_std_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_labrate_labrate_std_grid.Location = New System.Drawing.Point(459, 129)
        Me.conf_labrate_labrate_std_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_std_grid.Name = "conf_labrate_labrate_std_grid"
        Me.conf_labrate_labrate_std_grid.Size = New System.Drawing.Size(1332, 535)
        Me.conf_labrate_labrate_std_grid.TabIndex = 3
        '
        'conf_labrate_std_choice_layout
        '
        Me.conf_labrate_std_choice_layout.ColumnCount = 2
        Me.conf_labrate_std_choice_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.conf_labrate_std_choice_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.conf_labrate_std_choice_layout.Controls.Add(Me.lbl_conf_labrate_std_choice, 0, 0)
        Me.conf_labrate_std_choice_layout.Controls.Add(Me.lbl_conf_labrate_std_choice_val, 1, 0)
        Me.conf_labrate_std_choice_layout.Location = New System.Drawing.Point(459, 46)
        Me.conf_labrate_std_choice_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_std_choice_layout.Name = "conf_labrate_std_choice_layout"
        Me.conf_labrate_std_choice_layout.RowCount = 1
        Me.conf_labrate_std_choice_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_labrate_std_choice_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_labrate_std_choice_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_labrate_std_choice_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_labrate_std_choice_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_labrate_std_choice_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_labrate_std_choice_layout.Size = New System.Drawing.Size(381, 54)
        Me.conf_labrate_std_choice_layout.TabIndex = 2
        '
        'lbl_conf_labrate_std_choice
        '
        Me.lbl_conf_labrate_std_choice.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_conf_labrate_std_choice.AutoSize = True
        Me.lbl_conf_labrate_std_choice.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl_conf_labrate_std_choice.Location = New System.Drawing.Point(4, 17)
        Me.lbl_conf_labrate_std_choice.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.lbl_conf_labrate_std_choice.Name = "lbl_conf_labrate_std_choice"
        Me.lbl_conf_labrate_std_choice.Size = New System.Drawing.Size(121, 20)
        Me.lbl_conf_labrate_std_choice.TabIndex = 4
        Me.lbl_conf_labrate_std_choice.Text = "Labor Rate *:"
        '
        'lbl_conf_labrate_std_choice_val
        '
        Me.lbl_conf_labrate_std_choice_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.lbl_conf_labrate_std_choice_val.FormattingEnabled = True
        Me.lbl_conf_labrate_std_choice_val.Location = New System.Drawing.Point(209, 13)
        Me.lbl_conf_labrate_std_choice_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.lbl_conf_labrate_std_choice_val.Name = "lbl_conf_labrate_std_choice_val"
        Me.lbl_conf_labrate_std_choice_val.Size = New System.Drawing.Size(164, 28)
        Me.lbl_conf_labrate_std_choice_val.TabIndex = 44
        '
        'PictureBox7
        '
        Me.PictureBox7.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox7.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(418, 634)
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'conf_labrate_labrate_oem_grp
        '
        Me.conf_labrate_labrate_oem_grp.AutoSize = True
        Me.conf_labrate_labrate_oem_grp.Controls.Add(Me.conf_labrate_labrate_oem_grid)
        Me.conf_labrate_labrate_oem_grp.Controls.Add(Me.PictureBox8)
        Me.conf_labrate_labrate_oem_grp.Location = New System.Drawing.Point(4, 2036)
        Me.conf_labrate_labrate_oem_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_oem_grp.Name = "conf_labrate_labrate_oem_grp"
        Me.conf_labrate_labrate_oem_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_oem_grp.Size = New System.Drawing.Size(1799, 1675)
        Me.conf_labrate_labrate_oem_grp.TabIndex = 4
        Me.conf_labrate_labrate_oem_grp.TabStop = False
        Me.conf_labrate_labrate_oem_grp.Text = "4-OEM Rates"
        '
        'conf_labrate_labrate_oem_grid
        '
        Me.conf_labrate_labrate_oem_grid.AllowUserToAddRows = False
        Me.conf_labrate_labrate_oem_grid.AllowUserToDeleteRows = False
        Me.conf_labrate_labrate_oem_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_labrate_labrate_oem_grid.Location = New System.Drawing.Point(438, 29)
        Me.conf_labrate_labrate_oem_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_oem_grid.Name = "conf_labrate_labrate_oem_grid"
        Me.conf_labrate_labrate_oem_grid.Size = New System.Drawing.Size(1353, 1617)
        Me.conf_labrate_labrate_oem_grid.TabIndex = 3
        '
        'PictureBox8
        '
        Me.PictureBox8.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox8.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(418, 1615)
        Me.PictureBox8.TabIndex = 0
        Me.PictureBox8.TabStop = False
        '
        'conf_labrate_labrate_nte_grp
        '
        Me.conf_labrate_labrate_nte_grp.AutoSize = True
        Me.conf_labrate_labrate_nte_grp.Controls.Add(Me.conf_labrate_labrate_nte_grid)
        Me.conf_labrate_labrate_nte_grp.Controls.Add(Me.PictureBox9)
        Me.conf_labrate_labrate_nte_grp.Location = New System.Drawing.Point(4, 1288)
        Me.conf_labrate_labrate_nte_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_nte_grp.Name = "conf_labrate_labrate_nte_grp"
        Me.conf_labrate_labrate_nte_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_nte_grp.Size = New System.Drawing.Size(1799, 738)
        Me.conf_labrate_labrate_nte_grp.TabIndex = 5
        Me.conf_labrate_labrate_nte_grp.TabStop = False
        Me.conf_labrate_labrate_nte_grp.Text = "3-NTE Rates"
        '
        'conf_labrate_labrate_nte_grid
        '
        Me.conf_labrate_labrate_nte_grid.AllowUserToAddRows = False
        Me.conf_labrate_labrate_nte_grid.AllowUserToDeleteRows = False
        Me.conf_labrate_labrate_nte_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_labrate_labrate_nte_grid.Location = New System.Drawing.Point(438, 31)
        Me.conf_labrate_labrate_nte_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_labrate_labrate_nte_grid.Name = "conf_labrate_labrate_nte_grid"
        Me.conf_labrate_labrate_nte_grid.Size = New System.Drawing.Size(1353, 678)
        Me.conf_labrate_labrate_nte_grid.TabIndex = 3
        '
        'PictureBox9
        '
        Me.PictureBox9.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox9.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(418, 678)
        Me.PictureBox9.TabIndex = 0
        Me.PictureBox9.TabStop = False
        '
        'conf_lab_rate_top_menu_layout
        '
        Me.conf_lab_rate_top_menu_layout.ColumnCount = 7
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.conf_lab_rate_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1215.0!))
        Me.conf_lab_rate_top_menu_layout.Controls.Add(Me.conf_labrate_menu_task_rate_lbl, 0, 0)
        Me.conf_lab_rate_top_menu_layout.Controls.Add(Me.conf_labrate_menu_std_lbl, 0, 0)
        Me.conf_lab_rate_top_menu_layout.Controls.Add(Me.conf_labrate_menu_nte_lbl, 2, 0)
        Me.conf_lab_rate_top_menu_layout.Controls.Add(Me.conf_labrate_menu_oem_lbl, 3, 0)
        Me.conf_lab_rate_top_menu_layout.Dock = System.Windows.Forms.DockStyle.Top
        Me.conf_lab_rate_top_menu_layout.Location = New System.Drawing.Point(4, 5)
        Me.conf_lab_rate_top_menu_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lab_rate_top_menu_layout.Name = "conf_lab_rate_top_menu_layout"
        Me.conf_lab_rate_top_menu_layout.RowCount = 1
        Me.conf_lab_rate_top_menu_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_lab_rate_top_menu_layout.Size = New System.Drawing.Size(1892, 45)
        Me.conf_lab_rate_top_menu_layout.TabIndex = 1
        '
        'conf_labrate_menu_task_rate_lbl
        '
        Me.conf_labrate_menu_task_rate_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_labrate_menu_task_rate_lbl.AutoSize = True
        Me.conf_labrate_menu_task_rate_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_labrate_menu_task_rate_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_labrate_menu_task_rate_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_labrate_menu_task_rate_lbl.Location = New System.Drawing.Point(209, 12)
        Me.conf_labrate_menu_task_rate_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_labrate_menu_task_rate_lbl.Name = "conf_labrate_menu_task_rate_lbl"
        Me.conf_labrate_menu_task_rate_lbl.Size = New System.Drawing.Size(121, 20)
        Me.conf_labrate_menu_task_rate_lbl.TabIndex = 3
        Me.conf_labrate_menu_task_rate_lbl.Text = "2-Task Rates"
        Me.conf_labrate_menu_task_rate_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_labrate_menu_std_lbl
        '
        Me.conf_labrate_menu_std_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_labrate_menu_std_lbl.AutoSize = True
        Me.conf_labrate_menu_std_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_labrate_menu_std_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_labrate_menu_std_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_labrate_menu_std_lbl.Location = New System.Drawing.Point(12, 12)
        Me.conf_labrate_menu_std_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_labrate_menu_std_lbl.Name = "conf_labrate_menu_std_lbl"
        Me.conf_labrate_menu_std_lbl.Size = New System.Drawing.Size(156, 20)
        Me.conf_labrate_menu_std_lbl.TabIndex = 0
        Me.conf_labrate_menu_std_lbl.Text = "1-Standard Rates"
        Me.conf_labrate_menu_std_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_labrate_menu_nte_lbl
        '
        Me.conf_labrate_menu_nte_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_labrate_menu_nte_lbl.AutoSize = True
        Me.conf_labrate_menu_nte_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_labrate_menu_nte_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_labrate_menu_nte_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_labrate_menu_nte_lbl.Location = New System.Drawing.Point(391, 12)
        Me.conf_labrate_menu_nte_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_labrate_menu_nte_lbl.Name = "conf_labrate_menu_nte_lbl"
        Me.conf_labrate_menu_nte_lbl.Size = New System.Drawing.Size(117, 20)
        Me.conf_labrate_menu_nte_lbl.TabIndex = 2
        Me.conf_labrate_menu_nte_lbl.Text = "3-NTE Rates"
        Me.conf_labrate_menu_nte_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'conf_labrate_menu_oem_lbl
        '
        Me.conf_labrate_menu_oem_lbl.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_labrate_menu_oem_lbl.AutoSize = True
        Me.conf_labrate_menu_oem_lbl.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_labrate_menu_oem_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_labrate_menu_oem_lbl.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_labrate_menu_oem_lbl.Location = New System.Drawing.Point(566, 12)
        Me.conf_labrate_menu_oem_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_labrate_menu_oem_lbl.Name = "conf_labrate_menu_oem_lbl"
        Me.conf_labrate_menu_oem_lbl.Size = New System.Drawing.Size(128, 20)
        Me.conf_labrate_menu_oem_lbl.TabIndex = 1
        Me.conf_labrate_menu_oem_lbl.Text = "4- OEM Rates"
        Me.conf_labrate_menu_oem_lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tab_ctrl_config_page_editable_lov
        '
        Me.tab_ctrl_config_page_editable_lov.AutoScroll = True
        Me.tab_ctrl_config_page_editable_lov.Controls.Add(Me.conf_lov_main_panel)
        Me.tab_ctrl_config_page_editable_lov.Controls.Add(Me.tab_ctrl_config_page_lov_top_ly)
        Me.tab_ctrl_config_page_editable_lov.Location = New System.Drawing.Point(4, 4)
        Me.tab_ctrl_config_page_editable_lov.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_editable_lov.Name = "tab_ctrl_config_page_editable_lov"
        Me.tab_ctrl_config_page_editable_lov.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_editable_lov.Size = New System.Drawing.Size(1900, 789)
        Me.tab_ctrl_config_page_editable_lov.TabIndex = 3
        Me.tab_ctrl_config_page_editable_lov.Text = "4-List Of Values"
        Me.tab_ctrl_config_page_editable_lov.UseVisualStyleBackColor = True
        '
        'conf_lov_main_panel
        '
        Me.conf_lov_main_panel.Controls.Add(Me.conf_lov_main_ly)
        Me.conf_lov_main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_lov_main_panel.Location = New System.Drawing.Point(4, 50)
        Me.conf_lov_main_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_main_panel.Name = "conf_lov_main_panel"
        Me.conf_lov_main_panel.Size = New System.Drawing.Size(1892, 734)
        Me.conf_lov_main_panel.TabIndex = 2
        '
        'conf_lov_main_ly
        '
        Me.conf_lov_main_ly.AutoScroll = True
        Me.conf_lov_main_ly.Controls.Add(Me.conf_lov_edit_group)
        Me.conf_lov_main_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.conf_lov_main_ly.Location = New System.Drawing.Point(0, 0)
        Me.conf_lov_main_ly.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_main_ly.Name = "conf_lov_main_ly"
        Me.conf_lov_main_ly.Size = New System.Drawing.Size(1892, 734)
        Me.conf_lov_main_ly.TabIndex = 4
        '
        'conf_lov_edit_group
        '
        Me.conf_lov_edit_group.AutoSize = True
        Me.conf_lov_edit_group.Controls.Add(Me.conf_lov_edit_datagrid)
        Me.conf_lov_edit_group.Controls.Add(Me.conf_lov_edit_lov_selection_ly)
        Me.conf_lov_edit_group.Controls.Add(Me.PictureBox14)
        Me.conf_lov_edit_group.Location = New System.Drawing.Point(4, 5)
        Me.conf_lov_edit_group.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_edit_group.Name = "conf_lov_edit_group"
        Me.conf_lov_edit_group.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_edit_group.Size = New System.Drawing.Size(1862, 1549)
        Me.conf_lov_edit_group.TabIndex = 3
        Me.conf_lov_edit_group.TabStop = False
        Me.conf_lov_edit_group.Text = "1-Edit List Of Values"
        '
        'conf_lov_edit_datagrid
        '
        Me.conf_lov_edit_datagrid.AllowUserToAddRows = False
        Me.conf_lov_edit_datagrid.AllowUserToDeleteRows = False
        Me.conf_lov_edit_datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.conf_lov_edit_datagrid.Location = New System.Drawing.Point(459, 140)
        Me.conf_lov_edit_datagrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_edit_datagrid.Name = "conf_lov_edit_datagrid"
        Me.conf_lov_edit_datagrid.Size = New System.Drawing.Size(1395, 1380)
        Me.conf_lov_edit_datagrid.TabIndex = 3
        '
        'conf_lov_edit_lov_selection_ly
        '
        Me.conf_lov_edit_lov_selection_ly.ColumnCount = 2
        Me.conf_lov_edit_lov_selection_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.93701!))
        Me.conf_lov_edit_lov_selection_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.06299!))
        Me.conf_lov_edit_lov_selection_ly.Controls.Add(Me.conf_lov_edit_lov_selected_lbl, 0, 0)
        Me.conf_lov_edit_lov_selection_ly.Controls.Add(Me.conf_lov_edit_lov_selected_val, 1, 0)
        Me.conf_lov_edit_lov_selection_ly.Location = New System.Drawing.Point(710, 51)
        Me.conf_lov_edit_lov_selection_ly.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_edit_lov_selection_ly.Name = "conf_lov_edit_lov_selection_ly"
        Me.conf_lov_edit_lov_selection_ly.RowCount = 1
        Me.conf_lov_edit_lov_selection_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.conf_lov_edit_lov_selection_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_lov_edit_lov_selection_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_lov_edit_lov_selection_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_lov_edit_lov_selection_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_lov_edit_lov_selection_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54.0!))
        Me.conf_lov_edit_lov_selection_ly.Size = New System.Drawing.Size(648, 54)
        Me.conf_lov_edit_lov_selection_ly.TabIndex = 2
        '
        'conf_lov_edit_lov_selected_lbl
        '
        Me.conf_lov_edit_lov_selected_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.conf_lov_edit_lov_selected_lbl.AutoSize = True
        Me.conf_lov_edit_lov_selected_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_lov_edit_lov_selected_lbl.Location = New System.Drawing.Point(4, 17)
        Me.conf_lov_edit_lov_selected_lbl.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_lov_edit_lov_selected_lbl.Name = "conf_lov_edit_lov_selected_lbl"
        Me.conf_lov_edit_lov_selected_lbl.Size = New System.Drawing.Size(199, 20)
        Me.conf_lov_edit_lov_selected_lbl.TabIndex = 4
        Me.conf_lov_edit_lov_selected_lbl.Text = "Filter List Of Values *:"
        '
        'conf_lov_edit_lov_selected_val
        '
        Me.conf_lov_edit_lov_selected_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.conf_lov_edit_lov_selected_val.FormattingEnabled = True
        Me.conf_lov_edit_lov_selected_val.Location = New System.Drawing.Point(353, 13)
        Me.conf_lov_edit_lov_selected_val.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.conf_lov_edit_lov_selected_val.Name = "conf_lov_edit_lov_selected_val"
        Me.conf_lov_edit_lov_selected_val.Size = New System.Drawing.Size(288, 28)
        Me.conf_lov_edit_lov_selected_val.TabIndex = 44
        '
        'PictureBox14
        '
        Me.PictureBox14.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox14.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(420, 634)
        Me.PictureBox14.TabIndex = 0
        Me.PictureBox14.TabStop = False
        '
        'tab_ctrl_config_page_lov_top_ly
        '
        Me.tab_ctrl_config_page_lov_top_ly.ColumnCount = 7
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218.0!))
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 194.0!))
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 188.0!))
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190.0!))
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225.0!))
        Me.tab_ctrl_config_page_lov_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1101.0!))
        Me.tab_ctrl_config_page_lov_top_ly.Controls.Add(Me.conf_lov_edit_menu, 0, 0)
        Me.tab_ctrl_config_page_lov_top_ly.Dock = System.Windows.Forms.DockStyle.Top
        Me.tab_ctrl_config_page_lov_top_ly.Location = New System.Drawing.Point(4, 5)
        Me.tab_ctrl_config_page_lov_top_ly.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_ctrl_config_page_lov_top_ly.Name = "tab_ctrl_config_page_lov_top_ly"
        Me.tab_ctrl_config_page_lov_top_ly.RowCount = 1
        Me.tab_ctrl_config_page_lov_top_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tab_ctrl_config_page_lov_top_ly.Size = New System.Drawing.Size(1892, 45)
        Me.tab_ctrl_config_page_lov_top_ly.TabIndex = 1
        '
        'conf_lov_edit_menu
        '
        Me.conf_lov_edit_menu.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.conf_lov_edit_menu.AutoSize = True
        Me.conf_lov_edit_menu.Cursor = System.Windows.Forms.Cursors.Hand
        Me.conf_lov_edit_menu.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.conf_lov_edit_menu.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.conf_lov_edit_menu.Location = New System.Drawing.Point(16, 12)
        Me.conf_lov_edit_menu.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.conf_lov_edit_menu.Name = "conf_lov_edit_menu"
        Me.conf_lov_edit_menu.Size = New System.Drawing.Size(186, 20)
        Me.conf_lov_edit_menu.TabIndex = 0
        Me.conf_lov_edit_menu.Text = "1-Edit List Of Values"
        Me.conf_lov_edit_menu.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tab_main_page_init_scope
        '
        Me.tab_main_page_init_scope.Controls.Add(Me.tab_main_page_init_scope_p)
        Me.tab_main_page_init_scope.Controls.Add(Me.init_scope_top_menu_layout)
        Me.tab_main_page_init_scope.Location = New System.Drawing.Point(4, 4)
        Me.tab_main_page_init_scope.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_init_scope.Name = "tab_main_page_init_scope"
        Me.tab_main_page_init_scope.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_init_scope.Size = New System.Drawing.Size(1916, 832)
        Me.tab_main_page_init_scope.TabIndex = 1
        Me.tab_main_page_init_scope.Text = "Initial Scope"
        Me.tab_main_page_init_scope.UseVisualStyleBackColor = True
        '
        'tab_main_page_init_scope_p
        '
        Me.tab_main_page_init_scope_p.AutoScroll = True
        Me.tab_main_page_init_scope_p.Controls.Add(Me.init_scope_main_layout)
        Me.tab_main_page_init_scope_p.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tab_main_page_init_scope_p.Location = New System.Drawing.Point(4, 50)
        Me.tab_main_page_init_scope_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_init_scope_p.Name = "tab_main_page_init_scope_p"
        Me.tab_main_page_init_scope_p.Size = New System.Drawing.Size(1908, 777)
        Me.tab_main_page_init_scope_p.TabIndex = 3
        '
        'init_scope_main_layout
        '
        Me.init_scope_main_layout.AutoSize = True
        Me.init_scope_main_layout.ColumnCount = 1
        Me.init_scope_main_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.init_scope_main_layout.Controls.Add(Me.init_scope_menu_quots_grp, 0, 0)
        Me.init_scope_main_layout.Controls.Add(Me.init_scope_menu_pricing_sum_grp, 0, 1)
        Me.init_scope_main_layout.Location = New System.Drawing.Point(0, 0)
        Me.init_scope_main_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_main_layout.Name = "init_scope_main_layout"
        Me.init_scope_main_layout.RowCount = 3
        Me.init_scope_main_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.init_scope_main_layout.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.init_scope_main_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31.0!))
        Me.init_scope_main_layout.Size = New System.Drawing.Size(2754, 1538)
        Me.init_scope_main_layout.TabIndex = 0
        '
        'init_scope_menu_quots_grp
        '
        Me.init_scope_menu_quots_grp.AutoSize = True
        Me.init_scope_menu_quots_grp.Controls.Add(Me.init_scope_quotes_grid)
        Me.init_scope_menu_quots_grp.Controls.Add(Me.PictureBox11)
        Me.init_scope_menu_quots_grp.Location = New System.Drawing.Point(4, 5)
        Me.init_scope_menu_quots_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_menu_quots_grp.Name = "init_scope_menu_quots_grp"
        Me.init_scope_menu_quots_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_menu_quots_grp.Size = New System.Drawing.Size(2103, 331)
        Me.init_scope_menu_quots_grp.TabIndex = 3
        Me.init_scope_menu_quots_grp.TabStop = False
        Me.init_scope_menu_quots_grp.Text = "1-Initial Quotes"
        '
        'init_scope_quotes_grid
        '
        Me.init_scope_quotes_grid.AllowUserToAddRows = False
        Me.init_scope_quotes_grid.AllowUserToDeleteRows = False
        Me.init_scope_quotes_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.init_scope_quotes_grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.init_scope_quotes_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.init_scope_quotes_grid.DefaultCellStyle = DataGridViewCellStyle8
        Me.init_scope_quotes_grid.Location = New System.Drawing.Point(435, 31)
        Me.init_scope_quotes_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_quotes_grid.Name = "init_scope_quotes_grid"
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.init_scope_quotes_grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle9
        Me.init_scope_quotes_grid.Size = New System.Drawing.Size(1660, 271)
        Me.init_scope_quotes_grid.TabIndex = 3
        '
        'PictureBox11
        '
        Me.PictureBox11.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox11.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(418, 271)
        Me.PictureBox11.TabIndex = 0
        Me.PictureBox11.TabStop = False
        '
        'init_scope_menu_pricing_sum_grp
        '
        Me.init_scope_menu_pricing_sum_grp.AutoSize = True
        Me.init_scope_menu_pricing_sum_grp.Controls.Add(Me.init_scope_pricing_sum_grid)
        Me.init_scope_menu_pricing_sum_grp.Location = New System.Drawing.Point(4, 346)
        Me.init_scope_menu_pricing_sum_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_menu_pricing_sum_grp.Name = "init_scope_menu_pricing_sum_grp"
        Me.init_scope_menu_pricing_sum_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_menu_pricing_sum_grp.Size = New System.Drawing.Size(2744, 1140)
        Me.init_scope_menu_pricing_sum_grp.TabIndex = 4
        Me.init_scope_menu_pricing_sum_grp.TabStop = False
        Me.init_scope_menu_pricing_sum_grp.Text = "2-Pricing Summary"
        '
        'init_scope_pricing_sum_grid
        '
        Me.init_scope_pricing_sum_grid.AllowUserToAddRows = False
        Me.init_scope_pricing_sum_grid.AllowUserToDeleteRows = False
        Me.init_scope_pricing_sum_grid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        DataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.init_scope_pricing_sum_grid.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle10
        Me.init_scope_pricing_sum_grid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.init_scope_pricing_sum_grid.DefaultCellStyle = DataGridViewCellStyle11
        Me.init_scope_pricing_sum_grid.Location = New System.Drawing.Point(0, 31)
        Me.init_scope_pricing_sum_grid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_pricing_sum_grid.Name = "init_scope_pricing_sum_grid"
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.init_scope_pricing_sum_grid.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.init_scope_pricing_sum_grid.Size = New System.Drawing.Size(2736, 1080)
        Me.init_scope_pricing_sum_grid.TabIndex = 3
        '
        'init_scope_top_menu_layout
        '
        Me.init_scope_top_menu_layout.ColumnCount = 7
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 231.0!))
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129.0!))
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.init_scope_top_menu_layout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1550.0!))
        Me.init_scope_top_menu_layout.Controls.Add(Me.init_scope_menu_pricing_sum, 1, 0)
        Me.init_scope_top_menu_layout.Controls.Add(Me.init_scope_menu_quots, 0, 0)
        Me.init_scope_top_menu_layout.Dock = System.Windows.Forms.DockStyle.Top
        Me.init_scope_top_menu_layout.Location = New System.Drawing.Point(4, 5)
        Me.init_scope_top_menu_layout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.init_scope_top_menu_layout.Name = "init_scope_top_menu_layout"
        Me.init_scope_top_menu_layout.RowCount = 1
        Me.init_scope_top_menu_layout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.init_scope_top_menu_layout.Size = New System.Drawing.Size(1908, 45)
        Me.init_scope_top_menu_layout.TabIndex = 2
        '
        'init_scope_menu_pricing_sum
        '
        Me.init_scope_menu_pricing_sum.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.init_scope_menu_pricing_sum.AutoSize = True
        Me.init_scope_menu_pricing_sum.Cursor = System.Windows.Forms.Cursors.Hand
        Me.init_scope_menu_pricing_sum.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.init_scope_menu_pricing_sum.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.init_scope_menu_pricing_sum.Location = New System.Drawing.Point(211, 12)
        Me.init_scope_menu_pricing_sum.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.init_scope_menu_pricing_sum.Name = "init_scope_menu_pricing_sum"
        Me.init_scope_menu_pricing_sum.Size = New System.Drawing.Size(169, 20)
        Me.init_scope_menu_pricing_sum.TabIndex = 1
        Me.init_scope_menu_pricing_sum.Text = "2-Pricing Summary"
        Me.init_scope_menu_pricing_sum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'init_scope_menu_quots
        '
        Me.init_scope_menu_quots.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.init_scope_menu_quots.AutoSize = True
        Me.init_scope_menu_quots.Cursor = System.Windows.Forms.Cursors.Hand
        Me.init_scope_menu_quots.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.init_scope_menu_quots.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.init_scope_menu_quots.Location = New System.Drawing.Point(21, 12)
        Me.init_scope_menu_quots.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.init_scope_menu_quots.Name = "init_scope_menu_quots"
        Me.init_scope_menu_quots.Size = New System.Drawing.Size(138, 20)
        Me.init_scope_menu_quots.TabIndex = 0
        Me.init_scope_menu_quots.Text = "1-Initial Quotes"
        Me.init_scope_menu_quots.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tab_main_page_add_scope
        '
        Me.tab_main_page_add_scope.AutoScroll = True
        Me.tab_main_page_add_scope.Controls.Add(Me.add_scope_main_panel)
        Me.tab_main_page_add_scope.Controls.Add(Me.add_scope_top_panel)
        Me.tab_main_page_add_scope.Location = New System.Drawing.Point(4, 4)
        Me.tab_main_page_add_scope.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_add_scope.Name = "tab_main_page_add_scope"
        Me.tab_main_page_add_scope.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_add_scope.Size = New System.Drawing.Size(1916, 832)
        Me.tab_main_page_add_scope.TabIndex = 2
        Me.tab_main_page_add_scope.Text = "Additionnal Scope"
        Me.tab_main_page_add_scope.UseVisualStyleBackColor = True
        '
        'add_scope_main_panel
        '
        Me.add_scope_main_panel.Controls.Add(Me.add_scope_main_datagrid)
        Me.add_scope_main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.add_scope_main_panel.Location = New System.Drawing.Point(4, 91)
        Me.add_scope_main_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_main_panel.Name = "add_scope_main_panel"
        Me.add_scope_main_panel.Size = New System.Drawing.Size(1908, 736)
        Me.add_scope_main_panel.TabIndex = 1
        '
        'add_scope_main_datagrid
        '
        Me.add_scope_main_datagrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.add_scope_main_datagrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.add_scope_main_datagrid.Location = New System.Drawing.Point(0, 0)
        Me.add_scope_main_datagrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_main_datagrid.Name = "add_scope_main_datagrid"
        Me.add_scope_main_datagrid.Size = New System.Drawing.Size(1908, 736)
        Me.add_scope_main_datagrid.TabIndex = 0
        '
        'add_scope_top_panel
        '
        Me.add_scope_top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.add_scope_top_panel.Location = New System.Drawing.Point(4, 5)
        Me.add_scope_top_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.add_scope_top_panel.Name = "add_scope_top_panel"
        Me.add_scope_top_panel.Size = New System.Drawing.Size(1908, 86)
        Me.add_scope_top_panel.TabIndex = 0
        '
        'tab_main_page_materials
        '
        Me.tab_main_page_materials.Controls.Add(Me.material_main_panel)
        Me.tab_main_page_materials.Controls.Add(Me.mat_page_top_panel)
        Me.tab_main_page_materials.Location = New System.Drawing.Point(4, 4)
        Me.tab_main_page_materials.Name = "tab_main_page_materials"
        Me.tab_main_page_materials.Padding = New System.Windows.Forms.Padding(3)
        Me.tab_main_page_materials.Size = New System.Drawing.Size(1916, 832)
        Me.tab_main_page_materials.TabIndex = 3
        Me.tab_main_page_materials.Text = "Materials"
        Me.tab_main_page_materials.UseVisualStyleBackColor = True
        '
        'material_main_panel
        '
        Me.material_main_panel.Controls.Add(Me.material_main_dgrid)
        Me.material_main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.material_main_panel.Location = New System.Drawing.Point(3, 89)
        Me.material_main_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.material_main_panel.Name = "material_main_panel"
        Me.material_main_panel.Size = New System.Drawing.Size(1910, 740)
        Me.material_main_panel.TabIndex = 2
        '
        'material_main_dgrid
        '
        Me.material_main_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.material_main_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.material_main_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.material_main_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.material_main_dgrid.Name = "material_main_dgrid"
        Me.material_main_dgrid.Size = New System.Drawing.Size(1910, 740)
        Me.material_main_dgrid.TabIndex = 0
        '
        'mat_page_top_panel
        '
        Me.mat_page_top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.mat_page_top_panel.Location = New System.Drawing.Point(3, 3)
        Me.mat_page_top_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.mat_page_top_panel.Name = "mat_page_top_panel"
        Me.mat_page_top_panel.Size = New System.Drawing.Size(1910, 86)
        Me.mat_page_top_panel.TabIndex = 1
        '
        'tab_main_page_awq
        '
        Me.tab_main_page_awq.Controls.Add(Me.awq_main_panel)
        Me.tab_main_page_awq.Controls.Add(Me.awq_top_panel)
        Me.tab_main_page_awq.Location = New System.Drawing.Point(4, 4)
        Me.tab_main_page_awq.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_awq.Name = "tab_main_page_awq"
        Me.tab_main_page_awq.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.tab_main_page_awq.Size = New System.Drawing.Size(1916, 832)
        Me.tab_main_page_awq.TabIndex = 4
        Me.tab_main_page_awq.Text = "Additional Work Quotation"
        Me.tab_main_page_awq.UseVisualStyleBackColor = True
        '
        'awq_main_panel
        '
        Me.awq_main_panel.Controls.Add(Me.awq_main_dgrid)
        Me.awq_main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awq_main_panel.Location = New System.Drawing.Point(4, 99)
        Me.awq_main_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awq_main_panel.Name = "awq_main_panel"
        Me.awq_main_panel.Size = New System.Drawing.Size(1908, 728)
        Me.awq_main_panel.TabIndex = 3
        '
        'awq_main_dgrid
        '
        Me.awq_main_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.awq_main_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.awq_main_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.awq_main_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awq_main_dgrid.Name = "awq_main_dgrid"
        Me.awq_main_dgrid.Size = New System.Drawing.Size(1908, 728)
        Me.awq_main_dgrid.TabIndex = 0
        '
        'awq_top_panel
        '
        Me.awq_top_panel.Controls.Add(Me.FlowLayoutPanel1)
        Me.awq_top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.awq_top_panel.Location = New System.Drawing.Point(4, 5)
        Me.awq_top_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awq_top_panel.Name = "awq_top_panel"
        Me.awq_top_panel.Size = New System.Drawing.Size(1908, 94)
        Me.awq_top_panel.TabIndex = 2
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.new_awq_no_activity)
        Me.FlowLayoutPanel1.Controls.Add(Me.awq_mass_closure_bt)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(-6, 5)
        Me.FlowLayoutPanel1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(1045, 85)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'new_awq_no_activity
        '
        Me.new_awq_no_activity.Location = New System.Drawing.Point(4, 5)
        Me.new_awq_no_activity.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.new_awq_no_activity.Name = "new_awq_no_activity"
        Me.new_awq_no_activity.Size = New System.Drawing.Size(210, 35)
        Me.new_awq_no_activity.TabIndex = 0
        Me.new_awq_no_activity.Text = "New AWQ With No Act."
        Me.new_awq_no_activity.UseVisualStyleBackColor = True
        '
        'awq_mass_closure_bt
        '
        Me.awq_mass_closure_bt.Location = New System.Drawing.Point(222, 5)
        Me.awq_mass_closure_bt.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.awq_mass_closure_bt.Name = "awq_mass_closure_bt"
        Me.awq_mass_closure_bt.Size = New System.Drawing.Size(210, 35)
        Me.awq_mass_closure_bt.TabIndex = 1
        Me.awq_mass_closure_bt.Text = "Mass Closure"
        Me.awq_mass_closure_bt.UseVisualStyleBackColor = True
        '
        'tab_main_proj_ctrl
        '
        Me.tab_main_proj_ctrl.Controls.Add(Me.proj_ctrl_tab_ctrl)
        Me.tab_main_proj_ctrl.Location = New System.Drawing.Point(4, 4)
        Me.tab_main_proj_ctrl.Name = "tab_main_proj_ctrl"
        Me.tab_main_proj_ctrl.Padding = New System.Windows.Forms.Padding(3)
        Me.tab_main_proj_ctrl.Size = New System.Drawing.Size(1916, 832)
        Me.tab_main_proj_ctrl.TabIndex = 5
        Me.tab_main_proj_ctrl.Text = "Project Controlling"
        Me.tab_main_proj_ctrl.UseVisualStyleBackColor = True
        '
        'proj_ctrl_tab_ctrl
        '
        Me.proj_ctrl_tab_ctrl.Alignment = System.Windows.Forms.TabAlignment.Bottom
        Me.proj_ctrl_tab_ctrl.Controls.Add(Me.proj_ctrl_conf_tab)
        Me.proj_ctrl_tab_ctrl.Controls.Add(Me.labor_controlling_tab)
        Me.proj_ctrl_tab_ctrl.Dock = System.Windows.Forms.DockStyle.Fill
        Me.proj_ctrl_tab_ctrl.Location = New System.Drawing.Point(3, 3)
        Me.proj_ctrl_tab_ctrl.Name = "proj_ctrl_tab_ctrl"
        Me.proj_ctrl_tab_ctrl.SelectedIndex = 0
        Me.proj_ctrl_tab_ctrl.Size = New System.Drawing.Size(1910, 826)
        Me.proj_ctrl_tab_ctrl.TabIndex = 0
        '
        'proj_ctrl_conf_tab
        '
        Me.proj_ctrl_conf_tab.Controls.Add(Me.proj_ctrl_conf_ly)
        Me.proj_ctrl_conf_tab.Controls.Add(Me.proj_ctrl_config_top_ly)
        Me.proj_ctrl_conf_tab.Location = New System.Drawing.Point(4, 4)
        Me.proj_ctrl_conf_tab.Name = "proj_ctrl_conf_tab"
        Me.proj_ctrl_conf_tab.Padding = New System.Windows.Forms.Padding(3)
        Me.proj_ctrl_conf_tab.Size = New System.Drawing.Size(1902, 793)
        Me.proj_ctrl_conf_tab.TabIndex = 0
        Me.proj_ctrl_conf_tab.Text = "Configuration"
        Me.proj_ctrl_conf_tab.UseVisualStyleBackColor = True
        '
        'proj_ctrl_conf_ly
        '
        Me.proj_ctrl_conf_ly.AutoScroll = True
        Me.proj_ctrl_conf_ly.Controls.Add(Me.proj_ctrl_transfer_cc_grp)
        Me.proj_ctrl_conf_ly.Dock = System.Windows.Forms.DockStyle.Fill
        Me.proj_ctrl_conf_ly.Location = New System.Drawing.Point(3, 48)
        Me.proj_ctrl_conf_ly.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.proj_ctrl_conf_ly.Name = "proj_ctrl_conf_ly"
        Me.proj_ctrl_conf_ly.Size = New System.Drawing.Size(1896, 742)
        Me.proj_ctrl_conf_ly.TabIndex = 7
        '
        'proj_ctrl_transfer_cc_grp
        '
        Me.proj_ctrl_transfer_cc_grp.AutoSize = True
        Me.proj_ctrl_transfer_cc_grp.Controls.Add(Me.proj_ctrl_transfer_cc_p)
        Me.proj_ctrl_transfer_cc_grp.Controls.Add(Me.PictureBox13)
        Me.proj_ctrl_transfer_cc_grp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.proj_ctrl_transfer_cc_grp.Location = New System.Drawing.Point(4, 5)
        Me.proj_ctrl_transfer_cc_grp.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.proj_ctrl_transfer_cc_grp.Name = "proj_ctrl_transfer_cc_grp"
        Me.proj_ctrl_transfer_cc_grp.Padding = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.proj_ctrl_transfer_cc_grp.Size = New System.Drawing.Size(1279, 574)
        Me.proj_ctrl_transfer_cc_grp.TabIndex = 1
        Me.proj_ctrl_transfer_cc_grp.TabStop = False
        Me.proj_ctrl_transfer_cc_grp.Text = "1-Cost Center Transfer"
        '
        'proj_ctrl_transfer_cc_p
        '
        Me.proj_ctrl_transfer_cc_p.AutoSize = True
        Me.proj_ctrl_transfer_cc_p.Controls.Add(Me.proj_ctrl_transfer_cc_dgrid)
        Me.proj_ctrl_transfer_cc_p.Location = New System.Drawing.Point(459, 31)
        Me.proj_ctrl_transfer_cc_p.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.proj_ctrl_transfer_cc_p.Name = "proj_ctrl_transfer_cc_p"
        Me.proj_ctrl_transfer_cc_p.Size = New System.Drawing.Size(812, 514)
        Me.proj_ctrl_transfer_cc_p.TabIndex = 4
        '
        'proj_ctrl_transfer_cc_dgrid
        '
        Me.proj_ctrl_transfer_cc_dgrid.AllowUserToAddRows = False
        Me.proj_ctrl_transfer_cc_dgrid.AllowUserToDeleteRows = False
        Me.proj_ctrl_transfer_cc_dgrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.proj_ctrl_transfer_cc_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.proj_ctrl_transfer_cc_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.proj_ctrl_transfer_cc_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.proj_ctrl_transfer_cc_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.proj_ctrl_transfer_cc_dgrid.Name = "proj_ctrl_transfer_cc_dgrid"
        Me.proj_ctrl_transfer_cc_dgrid.Size = New System.Drawing.Size(812, 514)
        Me.proj_ctrl_transfer_cc_dgrid.TabIndex = 3
        '
        'PictureBox13
        '
        Me.PictureBox13.Location = New System.Drawing.Point(10, 31)
        Me.PictureBox13.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(418, 512)
        Me.PictureBox13.TabIndex = 0
        Me.PictureBox13.TabStop = False
        '
        'proj_ctrl_config_top_ly
        '
        Me.proj_ctrl_config_top_ly.ColumnCount = 8
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 135.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 180.0!))
        Me.proj_ctrl_config_top_ly.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 1060.0!))
        Me.proj_ctrl_config_top_ly.Controls.Add(Me.proj_ctrl_config_top_transfer_cc, 0, 0)
        Me.proj_ctrl_config_top_ly.Dock = System.Windows.Forms.DockStyle.Top
        Me.proj_ctrl_config_top_ly.Location = New System.Drawing.Point(3, 3)
        Me.proj_ctrl_config_top_ly.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.proj_ctrl_config_top_ly.Name = "proj_ctrl_config_top_ly"
        Me.proj_ctrl_config_top_ly.RowCount = 1
        Me.proj_ctrl_config_top_ly.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.proj_ctrl_config_top_ly.Size = New System.Drawing.Size(1896, 45)
        Me.proj_ctrl_config_top_ly.TabIndex = 6
        '
        'proj_ctrl_config_top_transfer_cc
        '
        Me.proj_ctrl_config_top_transfer_cc.Anchor = System.Windows.Forms.AnchorStyles.None
        Me.proj_ctrl_config_top_transfer_cc.AutoSize = True
        Me.proj_ctrl_config_top_transfer_cc.Cursor = System.Windows.Forms.Cursors.Hand
        Me.proj_ctrl_config_top_transfer_cc.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Underline), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.proj_ctrl_config_top_transfer_cc.ForeColor = System.Drawing.SystemColors.MenuHighlight
        Me.proj_ctrl_config_top_transfer_cc.Location = New System.Drawing.Point(10, 12)
        Me.proj_ctrl_config_top_transfer_cc.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.proj_ctrl_config_top_transfer_cc.Name = "proj_ctrl_config_top_transfer_cc"
        Me.proj_ctrl_config_top_transfer_cc.Size = New System.Drawing.Size(204, 20)
        Me.proj_ctrl_config_top_transfer_cc.TabIndex = 0
        Me.proj_ctrl_config_top_transfer_cc.Text = "1-Cost Center Transfer"
        Me.proj_ctrl_config_top_transfer_cc.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'labor_controlling_tab
        '
        Me.labor_controlling_tab.Controls.Add(Me.labor_ctrl_main_panel)
        Me.labor_controlling_tab.Controls.Add(Me.labor_ctrl_top_panel)
        Me.labor_controlling_tab.Location = New System.Drawing.Point(4, 4)
        Me.labor_controlling_tab.Name = "labor_controlling_tab"
        Me.labor_controlling_tab.Padding = New System.Windows.Forms.Padding(3)
        Me.labor_controlling_tab.Size = New System.Drawing.Size(1902, 793)
        Me.labor_controlling_tab.TabIndex = 1
        Me.labor_controlling_tab.Text = "Labor Controlling"
        Me.labor_controlling_tab.UseVisualStyleBackColor = True
        '
        'labor_ctrl_main_panel
        '
        Me.labor_ctrl_main_panel.Controls.Add(Me.labor_ctrl_main_dgrid)
        Me.labor_ctrl_main_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.labor_ctrl_main_panel.Location = New System.Drawing.Point(3, 89)
        Me.labor_ctrl_main_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.labor_ctrl_main_panel.Name = "labor_ctrl_main_panel"
        Me.labor_ctrl_main_panel.Size = New System.Drawing.Size(1896, 701)
        Me.labor_ctrl_main_panel.TabIndex = 2
        '
        'labor_ctrl_main_dgrid
        '
        Me.labor_ctrl_main_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.labor_ctrl_main_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.labor_ctrl_main_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.labor_ctrl_main_dgrid.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.labor_ctrl_main_dgrid.Name = "labor_ctrl_main_dgrid"
        Me.labor_ctrl_main_dgrid.Size = New System.Drawing.Size(1896, 701)
        Me.labor_ctrl_main_dgrid.TabIndex = 0
        '
        'labor_ctrl_top_panel
        '
        Me.labor_ctrl_top_panel.Dock = System.Windows.Forms.DockStyle.Top
        Me.labor_ctrl_top_panel.Location = New System.Drawing.Point(3, 3)
        Me.labor_ctrl_top_panel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.labor_ctrl_top_panel.Name = "labor_ctrl_top_panel"
        Me.labor_ctrl_top_panel.Size = New System.Drawing.Size(1896, 86)
        Me.labor_ctrl_top_panel.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.ClientSize = New System.Drawing.Size(1924, 1049)
        Me.Controls.Add(Me.tab_ctrl_main)
        Me.Controls.Add(Me.pan_mainf_down_info)
        Me.Controls.Add(Me.pan_mainf_top_menu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.Text = "MRO Scope And Budget Controlling Tool"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.pan_mainf_top_menu.ResumeLayout(False)
        Me.top_menu_layout.ResumeLayout(False)
        Me.top_menu_tab_ctrl.ResumeLayout(False)
        Me.top_menu_tab_home_page.ResumeLayout(False)
        Me.top_menu_tab_home_page_ly.ResumeLayout(False)
        Me.top_menu_tab_home_page_ly.PerformLayout()
        CType(Me.top_menu_home_tab_info_bt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_menu_tab_import_page.ResumeLayout(False)
        Me.top_menu_import_page_ly.ResumeLayout(False)
        Me.top_menu_import_page_ly.PerformLayout()
        CType(Me.top_menu_import_zimro_bt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.top_menu_import_CN47N_bt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.top_menu_import_zmel_bt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.top_menu_import_CIJ3_bt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_menu_tab_report_page.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        CType(Me.top_menu_data_check_bt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.top_menu_customer_release_bt, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.top_menu_hours_stat_bt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_menu_tab_admin_page.ResumeLayout(False)
        Me.admin_menu_tab_ly.ResumeLayout(False)
        Me.admin_menu_tab_ly.PerformLayout()
        CType(Me.top_menu_admin_bt, System.ComponentModel.ISupportInitialize).EndInit()
        Me.top_menu_left_layout.ResumeLayout(False)
        CType(Me.top_menu_save, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.top_menu_save_status, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pan_mainf_down_info.ResumeLayout(False)
        Me.pan_mainf_down_info.PerformLayout()
        Me.DownStatusStrip.ResumeLayout(False)
        Me.DownStatusStrip.PerformLayout()
        Me.tab_ctrl_main.ResumeLayout(False)
        Me.tab_main_page_config.ResumeLayout(False)
        Me.tab_ctrl_config.ResumeLayout(False)
        Me.tab_ctrl_config_page_proj.ResumeLayout(False)
        Me.conf_project_layout.ResumeLayout(False)
        Me.conf_project_layout.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.awq_specialist_ly.ResumeLayout(False)
        Me.awq_specialist_ly.PerformLayout()
        Me.tab_ctrl_config_page_billing.ResumeLayout(False)
        Me.tab_ctrl_config_page_bill_flow_panel.ResumeLayout(False)
        Me.tab_ctrl_config_page_bill_flow_panel.PerformLayout()
        Me.conf_bill_curr_grp.ResumeLayout(False)
        Me.conf_bill_curr_grp.PerformLayout()
        Me.conf_bill_exchg_rate_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_exchg_rate_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_curr_layount.ResumeLayout(False)
        Me.conf_bill_curr_layount.PerformLayout()
        Me.conf_bill_markup_grp.ResumeLayout(False)
        Me.conf_bill_markup_grp.PerformLayout()
        Me.conf_bill_markup_grp_cap_p_grp.ResumeLayout(False)
        Me.conf_bill_markup_grp_cap_p_grp.PerformLayout()
        Me.conf_bill_markup_grp_cap_l_grp.ResumeLayout(False)
        Me.conf_bill_markup_grp_cap_l_grp.PerformLayout()
        Me.conf_bill_mat_markup_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_mat_markup_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_markup_layount.ResumeLayout(False)
        Me.conf_bill_markup_layount.PerformLayout()
        Me.conf_bill_freight_grp.ResumeLayout(False)
        Me.conf_bill_freight_grp.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.conf_bill_freight_grp_cap_p_grp.ResumeLayout(False)
        Me.conf_bill_freight_grp_cap_p_grp.PerformLayout()
        Me.conf_bill_freight_grp_cap_l_grp.ResumeLayout(False)
        Me.conf_bill_freight_grp_cap_l_grp.PerformLayout()
        Me.conf_bill_mat_freight_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_mat_freight_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_surcharge_grp.ResumeLayout(False)
        Me.conf_bill_surch_eco_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_surch_eco_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_surch_eco_layout.ResumeLayout(False)
        Me.conf_bill_surch_eco_layout.PerformLayout()
        Me.conf_bill_surch_gum_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_surch_gum_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_surch_gum_layout.ResumeLayout(False)
        Me.conf_bill_surch_gum_layout.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_down_pay_grp.ResumeLayout(False)
        Me.conf_bill_down_payment_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_down_payment_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_discount_grp.ResumeLayout(False)
        Me.conf_bill_discount_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_discount_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_nte_grp.ResumeLayout(False)
        Me.conf_bill_nte_grid_p.ResumeLayout(False)
        CType(Me.conf_bill_nte_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_bill_top_menu_layout.ResumeLayout(False)
        Me.conf_bill_top_menu_layout.PerformLayout()
        Me.tab_ctrl_config_page_rates.ResumeLayout(False)
        Me.tab_ctrl_config_page_labrate_panel.ResumeLayout(False)
        Me.conf_labrate_layout.ResumeLayout(False)
        Me.conf_labrate_layout.PerformLayout()
        Me.conf_labrate_task_rate_grp.ResumeLayout(False)
        CType(Me.conf_labrate_task_rate_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_labrate_labrate_std_grp.ResumeLayout(False)
        CType(Me.conf_labrate_labrate_std_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_labrate_std_choice_layout.ResumeLayout(False)
        Me.conf_labrate_std_choice_layout.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_labrate_labrate_oem_grp.ResumeLayout(False)
        CType(Me.conf_labrate_labrate_oem_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_labrate_labrate_nte_grp.ResumeLayout(False)
        CType(Me.conf_labrate_labrate_nte_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_lab_rate_top_menu_layout.ResumeLayout(False)
        Me.conf_lab_rate_top_menu_layout.PerformLayout()
        Me.tab_ctrl_config_page_editable_lov.ResumeLayout(False)
        Me.conf_lov_main_panel.ResumeLayout(False)
        Me.conf_lov_main_ly.ResumeLayout(False)
        Me.conf_lov_main_ly.PerformLayout()
        Me.conf_lov_edit_group.ResumeLayout(False)
        CType(Me.conf_lov_edit_datagrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.conf_lov_edit_lov_selection_ly.ResumeLayout(False)
        Me.conf_lov_edit_lov_selection_ly.PerformLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab_ctrl_config_page_lov_top_ly.ResumeLayout(False)
        Me.tab_ctrl_config_page_lov_top_ly.PerformLayout()
        Me.tab_main_page_init_scope.ResumeLayout(False)
        Me.tab_main_page_init_scope_p.ResumeLayout(False)
        Me.tab_main_page_init_scope_p.PerformLayout()
        Me.init_scope_main_layout.ResumeLayout(False)
        Me.init_scope_main_layout.PerformLayout()
        Me.init_scope_menu_quots_grp.ResumeLayout(False)
        CType(Me.init_scope_quotes_grid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.init_scope_menu_pricing_sum_grp.ResumeLayout(False)
        CType(Me.init_scope_pricing_sum_grid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.init_scope_top_menu_layout.ResumeLayout(False)
        Me.init_scope_top_menu_layout.PerformLayout()
        Me.tab_main_page_add_scope.ResumeLayout(False)
        Me.add_scope_main_panel.ResumeLayout(False)
        CType(Me.add_scope_main_datagrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab_main_page_materials.ResumeLayout(False)
        Me.material_main_panel.ResumeLayout(False)
        CType(Me.material_main_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tab_main_page_awq.ResumeLayout(False)
        Me.awq_main_panel.ResumeLayout(False)
        CType(Me.awq_main_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.awq_top_panel.ResumeLayout(False)
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.tab_main_proj_ctrl.ResumeLayout(False)
        Me.proj_ctrl_tab_ctrl.ResumeLayout(False)
        Me.proj_ctrl_conf_tab.ResumeLayout(False)
        Me.proj_ctrl_conf_ly.ResumeLayout(False)
        Me.proj_ctrl_conf_ly.PerformLayout()
        Me.proj_ctrl_transfer_cc_grp.ResumeLayout(False)
        Me.proj_ctrl_transfer_cc_grp.PerformLayout()
        Me.proj_ctrl_transfer_cc_p.ResumeLayout(False)
        CType(Me.proj_ctrl_transfer_cc_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.proj_ctrl_config_top_ly.ResumeLayout(False)
        Me.proj_ctrl_config_top_ly.PerformLayout()
        Me.labor_controlling_tab.ResumeLayout(False)
        Me.labor_ctrl_main_panel.ResumeLayout(False)
        CType(Me.labor_ctrl_main_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pan_mainf_top_menu As Panel
    Friend WithEvents pan_mainf_down_info As Panel
    Friend WithEvents tab_ctrl_main As TabControl
    Friend WithEvents tab_main_page_config As TabPage
    Friend WithEvents tab_ctrl_config As TabControl
    Friend WithEvents tab_ctrl_config_page_proj As TabPage
    Friend WithEvents tab_ctrl_config_page_billing As TabPage
    Friend WithEvents RichTextBox1 As RichTextBox
    Friend WithEvents conf_project_layout As TableLayoutPanel
    Friend WithEvents lbl_cnf_prj_det_acreg As Label
    Friend WithEvents lbl_cnf_prj_det_acreg_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_acmast As Label
    Friend WithEvents lbl_cnf_prj_det_projdesc As Label
    Friend WithEvents lbl_cnf_prj_det_acrmast_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_projlist_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_projlist As Label
    Friend WithEvents lbl_cnf_prj_det_projdesc_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_curr_in_date As Label
    Friend WithEvents lbl_cnf_prj_det_curr_fcst_date As Label
    Friend WithEvents lbl_cnf_prj_det_cust_po As Label
    Friend WithEvents lbl_cnf_prj_det_cust_po_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_quot_nr_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_quot_nr As Label
    Friend WithEvents lbl_cnf_prj_det_cust As Label
    Friend WithEvents lbl_cnf_prj_det_cust_email As Label
    Friend WithEvents lbl_cnf_prj_det_bill_grp As Label
    Friend WithEvents lbl_cnf_prj_det_work_accpt_form As Label
    Friend WithEvents lbl_cnf_prj_det_cust_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_cust_email_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_work_accpt_form_val As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents lbl_cnf_prj_det_ac_flight_hrs As Label
    Friend WithEvents lbl_cnf_prj_det_ac_flight_cycl As Label
    Friend WithEvents lbl_cnf_prj_det_ac_flight_hrs_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_ac_flight_cycl_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_dass_sales_or_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_dass_sales_or As Label
    Friend WithEvents lbl_cnf_prj_det_curr_in_date_val As DateTimePicker
    Friend WithEvents lbl_cnf_prj_det_curr_fcst_date_val As DateTimePicker
    Friend WithEvents lbl_cnf_prj_det_acgroup As Label
    Friend WithEvents lbl_cnf_prj_det_bill_grp_val As ComboBox
    Friend WithEvents DownStatusStrip As StatusStrip
    Friend WithEvents status_strip_main_label As ToolStripStatusLabel
    Friend WithEvents status_strip_sub_label As ToolStripStatusLabel
    Friend WithEvents status_strip_progress_bar As ToolStripProgressBar
    Friend WithEvents status_strip_latest_update As ToolStripStatusLabel
    Friend WithEvents conf_bill_top_menu_layout As TableLayoutPanel
    Friend WithEvents conf_bill_menu_curr_lbl As Label
    Friend WithEvents conf_bill_curr_grp As GroupBox
    Friend WithEvents conf_bill_curr_layount As TableLayoutPanel
    Friend WithEvents lbl_cnf_bill_lab_curr As Label
    Friend WithEvents lbl_cnf_bill_mat_curr As Label
    Friend WithEvents lbl_cnf_bill_rep_curr As Label
    Friend WithEvents lbl_cnf_bill_3party_curr As Label
    Friend WithEvents lbl_cnf_bill_freight_curr As Label
    Friend WithEvents lbl_cnf_bill_lab_curr_val As ComboBox
    Friend WithEvents lbl_cnf_bill_mat_curr_val As ComboBox
    Friend WithEvents lbl_cnf_bill_rep_curr_val As ComboBox
    Friend WithEvents lbl_cnf_bill_3party_curr_val As ComboBox
    Friend WithEvents lbl_cnf_bill_freight_curr_val As ComboBox
    Friend WithEvents conf_bill_exchg_rate_grid As DataGridView
    Friend WithEvents lbl_cnf_bill_tagk_date As Label
    Friend WithEvents lbl_cnf_bill_tagk_date_val As DateTimePicker
    Friend WithEvents conf_bill_exchg_rate_grid_p As Panel
    Friend WithEvents tab_ctrl_config_page_rates As TabPage
    Friend WithEvents conf_bill_menu_markup_lbl As Label
    Friend WithEvents conf_bill_markup_grp As GroupBox
    Friend WithEvents conf_bill_mat_markup_grid_p As Panel
    Friend WithEvents conf_bill_mat_markup_grid As DataGridView
    Friend WithEvents conf_bill_markup_layount As TableLayoutPanel
    Friend WithEvents lbl_cnf_bill_markup As Label
    Friend WithEvents lbl_cnf_bill_markup_val As ComboBox
    Friend WithEvents conf_bill_markup_grp_cap_p_grp As GroupBox
    Friend WithEvents conf_bill_markup_grp_cap_l_grp As GroupBox
    Friend WithEvents chk_b_conf_bill_markup_is_proj_cap As CheckBox
    Friend WithEvents chk_b_conf_bill_markup_is_line_cap As CheckBox
    Friend WithEvents cb_cnf_bill_markup_cap_proj_curr As ComboBox
    Friend WithEvents cb_cnf_bill_markup_cap_proj_value As TextBox
    Friend WithEvents cb_cnf_bill_markup_cap_line_curr As ComboBox
    Friend WithEvents cb_cnf_bill_markup_cap_line_value As TextBox
    Friend WithEvents conf_bill_freight_grp As GroupBox
    Friend WithEvents conf_bill_freight_grp_cap_p_grp As GroupBox
    Friend WithEvents cb_cnf_bill_freight_cap_proj_curr As ComboBox
    Friend WithEvents cb_cnf_bill_freight_cap_proj_value As TextBox
    Friend WithEvents conf_bill_freight_grp_cap_l_grp As GroupBox
    Friend WithEvents cb_cnf_bill_freight_cap_line_curr As ComboBox
    Friend WithEvents cb_cnf_bill_freight_cap_line_value As TextBox
    Friend WithEvents chk_b_conf_bill_freight_is_proj_cap As CheckBox
    Friend WithEvents chk_b_conf_bill_freight_is_line_cap As CheckBox
    Friend WithEvents conf_bill_mat_freight_grid_p As Panel
    Friend WithEvents conf_bill_mat_freight_grid As DataGridView
    Friend WithEvents conf_bill_menu_freight_lbl As Label
    Friend WithEvents conf_bill_surcharge_grp As GroupBox
    Friend WithEvents conf_bill_surch_gum_grid_p As Panel
    Friend WithEvents conf_bill_surch_gum_grid As DataGridView
    Friend WithEvents conf_bill_surch_gum_layout As TableLayoutPanel
    Friend WithEvents lbl_cnf_bill_gum As Label
    Friend WithEvents lbl_cnf_bill_gum_val As ComboBox
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents conf_bill_menu_surcharge_lbl As Label
    Friend WithEvents conf_bill_surch_eco_grid_p As Panel
    Friend WithEvents conf_bill_surch_eco_grid As DataGridView
    Friend WithEvents conf_bill_surch_eco_layout As TableLayoutPanel
    Friend WithEvents lbl_cnf_bill_eco As Label
    Friend WithEvents lbl_cnf_bill_eco_val As ComboBox
    Friend WithEvents conf_bill_menu_discount_lbl As Label
    Friend WithEvents conf_bill_discount_grp As GroupBox
    Friend WithEvents conf_bill_discount_grid_p As Panel
    Friend WithEvents conf_bill_discount_grid As DataGridView
    Friend WithEvents conf_bill_menu_nte_lbl As Label
    Friend WithEvents conf_bill_nte_grp As GroupBox
    Friend WithEvents conf_bill_nte_grid_p As Panel
    Friend WithEvents conf_bill_nte_grid As DataGridView
    Friend WithEvents conf_lab_rate_top_menu_layout As TableLayoutPanel
    Friend WithEvents conf_labrate_menu_std_lbl As Label
    Friend WithEvents conf_labrate_menu_nte_lbl As Label
    Friend WithEvents conf_labrate_menu_oem_lbl As Label
    Friend WithEvents tab_ctrl_config_page_labrate_panel As Panel
    Friend WithEvents conf_labrate_layout As TableLayoutPanel
    Friend WithEvents conf_labrate_labrate_std_grp As GroupBox
    Friend WithEvents conf_labrate_labrate_std_grid As DataGridView
    Friend WithEvents conf_labrate_std_choice_layout As TableLayoutPanel
    Friend WithEvents lbl_conf_labrate_std_choice As Label
    Friend WithEvents lbl_conf_labrate_std_choice_val As ComboBox
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents conf_labrate_labrate_nte_grp As GroupBox
    Friend WithEvents conf_labrate_labrate_nte_grid As DataGridView
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents conf_labrate_labrate_oem_grp As GroupBox
    Friend WithEvents conf_labrate_labrate_oem_grid As DataGridView
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents tab_main_page_init_scope As TabPage
    Friend WithEvents tab_main_page_init_scope_p As Panel
    Friend WithEvents init_scope_main_layout As TableLayoutPanel
    Friend WithEvents init_scope_menu_quots_grp As GroupBox
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents init_scope_menu_pricing_sum_grp As GroupBox
    Friend WithEvents init_scope_top_menu_layout As TableLayoutPanel
    Friend WithEvents init_scope_menu_quots As Label
    Friend WithEvents init_scope_menu_pricing_sum As Label
    Friend WithEvents top_menu_layout As TableLayoutPanel
    Friend WithEvents top_menu_tab_ctrl As TabControl
    Friend WithEvents top_menu_tab_home_page As TabPage
    Friend WithEvents top_menu_tab_import_page As TabPage
    Friend WithEvents top_menu_left_layout As FlowLayoutPanel
    Friend WithEvents top_menu_save As PictureBox
    Friend WithEvents top_menu_save_status As PictureBox
    Friend WithEvents top_menu_import_page_ly As TableLayoutPanel
    Friend WithEvents top_menu_import_zimro_bt As PictureBox
    Friend WithEvents top_menu_import_zimro_lbl As Label
    Friend WithEvents tab_main_page_add_scope As TabPage
    Friend WithEvents open_file_dialog As OpenFileDialog
    Friend WithEvents lbl_cnf_prj_det_projdef As Label
    Friend WithEvents lbl_cnf_prj_det_projdef_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_acgroup_val As TextBox
    Friend WithEvents add_scope_main_panel As Panel
    Friend WithEvents add_scope_main_datagrid As DataGridView
    Friend WithEvents add_scope_top_panel As Panel
    Friend WithEvents init_scope_quotes_grid As DataGridView
    Friend WithEvents init_scope_pricing_sum_grid As DataGridView
    Friend WithEvents top_menu_import_zmel_lbl As Label
    Friend WithEvents top_menu_import_zmel_bt As PictureBox
    Friend WithEvents tab_main_page_materials As TabPage
    Friend WithEvents material_main_panel As Panel
    Friend WithEvents material_main_dgrid As DataGridView
    Friend WithEvents mat_page_top_panel As Panel
    Friend WithEvents tab_ctrl_config_page_bill_flow_panel As FlowLayoutPanel
    Friend WithEvents conf_labrate_task_rate_grp As GroupBox
    Friend WithEvents conf_labrate_task_rate_grid As DataGridView
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents conf_labrate_menu_task_rate_lbl As Label
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents lbl_cnf_bill_freight_rate As Label
    Friend WithEvents lbl_cnf_bill_freight_rate_val As TextBox
    Friend WithEvents top_menu_tab_admin_page As TabPage
    Friend WithEvents admin_menu_tab_ly As TableLayoutPanel
    Friend WithEvents top_menu_admin_bt_lbl As Label
    Friend WithEvents top_menu_admin_bt As PictureBox
    Friend WithEvents tab_main_page_awq As TabPage
    Friend WithEvents awq_main_panel As Panel
    Friend WithEvents awq_main_dgrid As DataGridView
    Friend WithEvents awq_top_panel As Panel
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents new_awq_no_activity As Button
    Friend WithEvents top_menu_tab_report_page As TabPage
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents top_menu_customer_release_bt_lbl As Label
    Friend WithEvents top_menu_customer_release_bt As PictureBox
    Friend WithEvents lbl_cnf_prj_det_awq_specialist As Label
    Friend WithEvents status_strip_dgrid_selected_sum As ToolStripStatusLabel
    Friend WithEvents awq_specialist_ly As TableLayoutPanel
    Friend WithEvents lbl_cnf_prj_det_awq_specialist_email_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_awq_specialist_email As Label
    Friend WithEvents lbl_cnf_prj_det_awq_specialist_val As TextBox
    Friend WithEvents TableLayoutPanel3 As TableLayoutPanel
    Friend WithEvents lbl_cnf_prj_det_cust_rep_name_val As TextBox
    Friend WithEvents lbl_cnf_prj_det_cust_rep_name As Label
    Friend WithEvents conf_bill_menu_downpay_lbl As Label
    Friend WithEvents conf_bill_down_pay_grp As GroupBox
    Friend WithEvents conf_bill_down_payment_grid_p As Panel
    Friend WithEvents conf_bill_down_payment_grid As DataGridView
    Friend WithEvents tab_ctrl_config_page_editable_lov As TabPage
    Friend WithEvents conf_lov_main_panel As Panel
    Friend WithEvents conf_lov_main_ly As FlowLayoutPanel
    Friend WithEvents conf_lov_edit_group As GroupBox
    Friend WithEvents conf_lov_edit_datagrid As DataGridView
    Friend WithEvents conf_lov_edit_lov_selection_ly As TableLayoutPanel
    Friend WithEvents conf_lov_edit_lov_selected_lbl As Label
    Friend WithEvents conf_lov_edit_lov_selected_val As ComboBox
    Friend WithEvents PictureBox14 As PictureBox
    Friend WithEvents tab_ctrl_config_page_lov_top_ly As TableLayoutPanel
    Friend WithEvents conf_lov_edit_menu As Label
    Friend WithEvents calculation_label As ToolStripStatusLabel
    Friend WithEvents calculation_progress_bar As ToolStripProgressBar
    Friend WithEvents tab_main_proj_ctrl As TabPage
    Friend WithEvents proj_ctrl_tab_ctrl As TabControl
    Friend WithEvents proj_ctrl_conf_tab As TabPage
    Friend WithEvents labor_controlling_tab As TabPage
    Friend WithEvents top_menu_item_import_cost_coll As Label
    Friend WithEvents top_menu_import_CN47N_bt As PictureBox
    Friend WithEvents top_menu_import_CIJ3_bt As PictureBox
    Friend WithEvents top_menu_import_actuals As Label
    Friend WithEvents labor_ctrl_top_panel As Panel
    Friend WithEvents labor_ctrl_main_panel As Panel
    Friend WithEvents labor_ctrl_main_dgrid As DataGridView
    Friend WithEvents proj_ctrl_conf_ly As FlowLayoutPanel
    Friend WithEvents proj_ctrl_transfer_cc_grp As GroupBox
    Friend WithEvents proj_ctrl_transfer_cc_p As Panel
    Friend WithEvents proj_ctrl_transfer_cc_dgrid As DataGridView
    Friend WithEvents PictureBox13 As PictureBox
    Friend WithEvents proj_ctrl_config_top_ly As TableLayoutPanel
    Friend WithEvents proj_ctrl_config_top_transfer_cc As Label
    Friend WithEvents top_menu_hours_stat_bt_lbl As Label
    Friend WithEvents top_menu_hours_stat_bt As PictureBox
    Friend WithEvents top_menu_tab_home_page_ly As TableLayoutPanel
    Friend WithEvents top_menu_home_tab_info_bt As PictureBox
    Friend WithEvents top_menu_home_tab_info_lbl As Label
    Friend WithEvents top_menu_data_check_bt As PictureBox
    Friend WithEvents top_menu_data_check_bt_lbl As Label
    Friend WithEvents awq_mass_closure_bt As Button
End Class
