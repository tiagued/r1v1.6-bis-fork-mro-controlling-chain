﻿Public Class CDBSystemHandler

    Public latest_close_user As CConfProperty
    Public latest_open_user As CConfProperty
    Public latest_save_time As CConfProperty
    Public session_count As CConfProperty
    Public version_highlight As CConfProperty

    Public latest_close_user_key As String = "latest_close_user"
    Public latest_open_user_key As String = "latest_open_user"
    Public latest_save_time_key As String = "latest_save_time"
    Public session_count_key As String = "count_open_session"
    Public version_highlight_key As String = "versions_hilights"

    Public session_signature_log As String

    Public Sub loadSysData()
        Try
            latest_close_user = Nothing
            latest_open_user = Nothing
            session_count = Nothing


            Dim sys_viewModel As CViewModelMapList = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_SYSTEM_DATA)
            'read db entries
            Dim listO As List(Of Object) = GLB_MSTR_CTRL.csDB.performSelectObject(sys_viewModel.getDefaultSelect, sys_viewModel, True)

            For Each obj As CConfProperty In listO
                If obj.key = latest_close_user_key Then
                    latest_close_user = obj
                    latest_close_user.db_table = MConstants.MOD_VIEW_MAP_SYSTEM_DATA
                ElseIf obj.key = latest_open_user_key Then
                    latest_open_user = obj
                    latest_open_user.db_table = MConstants.MOD_VIEW_MAP_SYSTEM_DATA
                ElseIf obj.key = latest_save_time_key Then
                    latest_save_time = obj
                    latest_save_time.db_table = MConstants.MOD_VIEW_MAP_SYSTEM_DATA
                ElseIf obj.key = session_count_key Then
                    session_count = obj
                    session_count.db_table = MConstants.MOD_VIEW_MAP_SYSTEM_DATA
                ElseIf obj.key = version_highlight_key Then
                    version_highlight = obj
                    version_highlight.db_table = MConstants.MOD_VIEW_MAP_SYSTEM_DATA
                End If
            Next

        Catch ex As Exception
            MsgBox("An error occured while loading system data " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'opened data log
    Public Sub db_log_latest_open_user_time()
        Try
            session_signature_log = ""
            'get latest fresh data
            loadSysData()

            If Not IsNothing(session_count) Then
                Dim countLng As Long = 0
                Dim userName As String = ""
                Dim pcName As String = ""

                If Integer.TryParse(session_count.value, countLng) Then
                    If countLng = 0 Then
                        'increment count
                        session_count.value = 1
                        'log open user name
                        If Not IsNothing(latest_open_user) Then
                            'user name
                            userName = Environment.UserName
                            If String.IsNullOrWhiteSpace(userName) Then
                                userName = "unknown user"
                            End If
                            'pc name
                            pcName = Environment.MachineName
                            If String.IsNullOrWhiteSpace(pcName) Then
                                pcName = "unknown machine"
                            End If
                            session_signature_log = "User:" & userName & " PC:" & pcName & " Open Time:" & Now.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATE_TIME_FORMAT).value)
                            latest_open_user.value = session_signature_log
                        End If
                    Else
                        'increment count
                        session_count.value = countLng + 1
                        If Not IsNothing(latest_open_user) Then
                            'user name
                            userName = Environment.UserName
                            If String.IsNullOrWhiteSpace(userName) Then
                                userName = "unknown user"
                            End If
                            'pc name
                            pcName = Environment.MachineName
                            If String.IsNullOrWhiteSpace(pcName) Then
                                pcName = "unknown machine"
                            End If
                            session_signature_log = "User:" & userName & " PC:" & pcName & " Open Time:" & Now.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATE_TIME_FORMAT).value)
                            latest_open_user.value = latest_open_user.value & MConstants.SEP_DEFAULT & session_signature_log
                        End If
                    End If

                End If
            End If

            'add item to be updated
            If Not IsNothing(latest_open_user) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(latest_open_user)
            End If
            If Not IsNothing(session_count) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(session_count)
            End If
            'execute query now
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getAutomaticUpdateDeleteAll)

        Catch ex As Exception
            MsgBox("An error occured while writing latest open user in db " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'close data log
    Public Sub db_log_latest_close_user_time()
        Try
            'get latest fresh data
            loadSysData()

            If Not IsNothing(session_count) Then
                Dim countLng As Long = 0
                If Integer.TryParse(session_count.value, countLng) Then
                    If countLng = 1 Then
                        session_count.value = 0
                        'log close user closed time
                        If Not IsNothing(latest_close_user) Then
                            latest_close_user.value = session_signature_log & " Close Time:" & Now.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATE_TIME_FORMAT).value)
                        End If
                    Else
                        'decrement count
                        session_count.value = countLng - 1
                        'log close user closed time
                        If Not IsNothing(latest_close_user) Then
                            latest_close_user.value = session_signature_log & " Close Time:" & Now.ToString(MConstants.constantConf(MConstants.CONST_CONF_DATE_TIME_FORMAT).value)
                        End If
                        'remove open user name
                        If Not IsNothing(latest_open_user) Then
                            If latest_open_user.value.Contains(MConstants.SEP_DEFAULT & session_signature_log) Then
                                latest_open_user.value = latest_open_user.value.Replace(MConstants.SEP_DEFAULT & session_signature_log, "")
                            ElseIf latest_open_user.value.Contains(session_signature_log & MConstants.SEP_DEFAULT) Then
                                latest_open_user.value = latest_open_user.value.Replace(session_signature_log & MConstants.SEP_DEFAULT, "")
                            Else
                                latest_open_user.value = latest_open_user.value.Replace(session_signature_log, "")
                            End If
                        End If
                    End If

                End If
            End If

            'add item to be updated
            If Not IsNothing(latest_open_user) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(latest_open_user)
            End If
            If Not IsNothing(latest_close_user) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(latest_close_user)
            End If
            If Not IsNothing(session_count) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(session_count)
            End If
            'execute query now. Not needed as the application will perform latest save before closed
            'MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getAutomaticUpdateDeleteAll)
        Catch ex As Exception
            MsgBox("An error occured while writing latest closed user in db " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'reset session count
    Public Sub db_log_reset_session_data()
        Try
            'get latest fresh data
            loadSysData()

            If Not IsNothing(session_count) Then
                Dim countLng As Long = 0
                If Integer.TryParse(session_count.value, countLng) Then
                    If countLng <> 1 Then
                        session_count.value = 1
                    End If
                    If Not IsNothing(latest_open_user) Then
                        latest_open_user.value = session_signature_log
                    End If
                End If
            End If

            'add item to be updated
            If Not IsNothing(latest_open_user) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(latest_open_user)
            End If
            If Not IsNothing(session_count) Then
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(session_count)
            End If
            'execute query now
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(CSaveWorkerArgs.getAutomaticUpdateDeleteAll)

        Catch ex As Exception
            MsgBox("An error occured while performing session data reset " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class
