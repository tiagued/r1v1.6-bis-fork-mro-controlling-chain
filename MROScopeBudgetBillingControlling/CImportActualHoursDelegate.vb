﻿Public Class CImportActualHoursDelegate
    Private Shared instance As CImportActualHoursDelegate

    Public Shared Function getInstance() As CImportActualHoursDelegate
        If IsNothing(instance) Then
            instance = New CImportActualHoursDelegate
        End If
        Return instance
    End Function

    'list of items
    Private proj_list As List(Of String)

    Private cnt_added As Long
    Private cnt_updated As Long
    Private cnt_initial As Long
    Private cnt_import_file_total As Long

    Dim progressForm As FormProgressBar
    'log message
    Delegate Sub safeCallDelegate(message As String)
    Private Sub appendLogMessage(message As String)
        Try
            If progressForm.final_message_txt.InvokeRequired Then
                Dim deleg As New safeCallDelegate(AddressOf appendLogMessage)
                progressForm.final_message_txt.Invoke(deleg, New Object() {message})
            Else
                progressForm.final_message_txt.AppendText(message & Environment.NewLine & Environment.NewLine)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub launchActualImport()
        Dim dialogRes As DialogResult
        Dim filepath As String = ""

        cnt_added = 0
        cnt_initial = 0
        cnt_import_file_total = 0
        cnt_updated = 0
        Try

            GLB_MSTR_CTRL.statusBar.status_strip_main_label = "Import SAP CJI3..."
            'open file dialog
            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Select CJI3 file..."
            GLB_MSTR_CTRL.mainForm.open_file_dialog.AddExtension = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckFileExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.CheckPathExists = True
            GLB_MSTR_CTRL.mainForm.open_file_dialog.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Multiselect = False
            GLB_MSTR_CTRL.mainForm.open_file_dialog.DereferenceLinks = True
            'filter file types
            GLB_MSTR_CTRL.mainForm.open_file_dialog.Filter = "Excel Documents|*" & MConstants.constantConf(MConstants.CONST_CONF_CIJ3_IMP_FILE_EXT).value

            dialogRes = GLB_MSTR_CTRL.mainForm.open_file_dialog.ShowDialog()

            If dialogRes = DialogResult.OK Then
                'check file extention
                filepath = GLB_MSTR_CTRL.mainForm.open_file_dialog.FileName
                'if file is mhtml
                If Not filepath.EndsWith(MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value, StringComparison.OrdinalIgnoreCase) Then
                    MsgBox("The file you selected is not a " & MConstants.constantConf(MConstants.CONST_CONF_SCOPE_IMP_FILE_EXT).value & " file", MsgBoxStyle.Critical, "Wrong file type")
                    Exit Sub
                End If
            Else
                Exit Sub
            End If
        Catch ex As Exception
            MsgBox("An error occured when intializing the import " & Chr(10) & ex.Message, MsgBoxStyle.Critical, "Error when picking CJI3 file")
            Exit Sub
        End Try

        Dim xlHandler As CDBHandler = Nothing
        Try

            'new form
            progressForm = New FormProgressBar

            Dim conn As String
            Dim listO As List(Of Object)
            Dim warnMess As String = ""
            'block calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = True

            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Load CJI3 file..."
            'get file and query data
            conn = MConstants.constantConf(MConstants.CONST_CONF_CIJ3_IMP_FILE_CONN_STR).value.Replace(MConstants.constantConf(MConstants.CONST_CONF_PROJ_PARAM).value, filepath)
            xlHandler = New CDBHandler(conn, filepath)
            listO = xlHandler.performSelectObject(GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACTUAL_HOURS_IMPORT).getExcelDefaultSelect, GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACTUAL_HOURS_IMPORT), True)

            'check data consistency
            Dim checkResult As KeyValuePair(Of Boolean, String)
            checkResult = mergeSAPData(listO)
            If checkResult.Key Then
                MsgBox(checkResult.Value, MsgBoxStyle.Information, "Warning Errors Occured")
                Exit Sub
            End If

            'save in db
            Dim arg_save As CSaveWorkerArgs
            arg_save = CSaveWorkerArgs.getInsertAll
            arg_save.progBar = progressForm.progress_bar_new
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)
            'updated items.
            arg_save = CSaveWorkerArgs.getUpdateDeleteAll
            arg_save.progBar = progressForm.progress_bar_update
            arg_save.is_user_action = False
            MConstants.GLB_MSTR_CTRL.save_controller.startAsync(arg_save)

            appendLogMessage("CJI3 Import successfully done !" & Chr(10) & Chr(10))
            appendLogMessage("CJI3 File_ Bookings=" & cnt_import_file_total)
            appendLogMessage("Initial_______ Bookings=" & cnt_initial)
            appendLogMessage("Added______ Bookings=" & cnt_added)
            appendLogMessage("Updated____ Bookings=" & cnt_updated)

            progressForm.ShowDialog()
            progressForm.BringToFront()

            'refresh view if needed
            If MConstants.GLB_MSTR_CTRL.labor_controlling_controller.LABOR_CONTROL_VIEW_LOADABLE Then
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.loadControllingModule()
            End If
        Catch ex As Exception
            MsgBox("An error occured when reading data from CJI3 file. The file might not be in the right template, please check and try again" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical, "Error when loading Actual CJI3 file")
        Finally
            'allow calculation on sold hours and activity
            MConstants.GLOB_SAP_IMPOT_RUNNING = False
            If Not IsNothing(xlHandler) Then
                Try
                    xlHandler.closeConn()
                Catch ex As Exception
                End Try
            End If
        End Try
        GLB_MSTR_CTRL.statusBar.status_strip_main_label = "END Import SAP Scope"
    End Sub

    Private Function mergeSAPData(listO As List(Of Object)) As KeyValuePair(Of Boolean, String)

        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Merging SAP data with current customer statement data..."

        Dim errMessage As String
        Dim is_error As Boolean = False
        'actual existing data to be ignored
        Dim existingKeys As New List(Of String)
        proj_list = New List(Of String)

        'list of ctrl items to update
        Dim CtrlLaborViewList As New Dictionary(Of String, CLaborControlMainViewObj)

        GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Check CJI3 Data consistency ..."
        Dim allProjAct As CScopeActivity = Nothing
        errMessage = "Scope upload failed. Error details : "
        Try
            'get the list of existing keys to ignore them
            cnt_initial = MConstants.GLB_MSTR_CTRL.labor_controlling_controller.actualHoursList.Count
            cnt_import_file_total = listO.Count

            Dim notif_act_list As New Dictionary(Of String, CScopeActivity)
            Dim act_act_list As New Dictionary(Of String, CScopeActivity)

            'map to get activity from notification or activity number deodning of if hours are booked on prodo & act or on cost collector(activity)
            For Each act As CScopeActivity In MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource
                Dim key = ""
                If act.is_cost_collector Then
                    key = act.proj & MConstants.SEP_DEFAULT & act.act
                    If Not act_act_list.ContainsKey(key) Then
                        act_act_list.Add(key, act)
                    End If
                Else
                    key = act.proj & MConstants.SEP_DEFAULT & act.notification
                    If Not notif_act_list.ContainsKey(key) Then
                        notif_act_list.Add(key, act)
                    End If
                End If
                If IsNothing(allProjAct) AndAlso act.act = MConstants.ALL_PROJ_ACT_NUMBER Then
                    allProjAct = act
                End If
            Next

            'log error if no allproject
            If IsNothing(allProjAct) Then
                errMessage = errMessage & Chr(10) & "No All Poject Activity were found in the project to collect costs with no WBS"
                is_error = True
            End If

            'load actual hours
            MConstants.GLOB_APP_LOADED = False
            MConstants.GLB_MSTR_CTRL.labor_controlling_controller.model_read_actual_hours()
            MConstants.GLOB_APP_LOADED = True
            'list to get former actual hours with no wbs found and that were added to all_project activities
            Dim previousAllProjectActualHours As New List(Of CActualHours)
            For Each actHr As CActualHours In MConstants.GLB_MSTR_CTRL.labor_controlling_controller.actualHoursList
                If Not existingKeys.Contains(actHr.posting_number) Then
                    existingKeys.Add(actHr.posting_number)
                End If

                If Not IsNothing(allProjAct) AndAlso actHr.act_id = allProjAct.id Then
                    previousAllProjectActualHours.Add(actHr)
                End If
            Next

            For Each actHrImported As CImportActualHoursObject In listO
                If existingKeys.Contains(actHrImported.posting_number) Then
                    Continue For
                End If
                Dim actHr As CActualHours = actHrImported.getActualHoursObject()
                cnt_added = cnt_added + 1
                actHr.populateCalc()
                'check proj
                If Not MConstants.GLB_MSTR_CTRL.project_controller.project.project_list.Contains(actHr.calc_network) Then
                    If Not proj_list.Contains(actHr.calc_network) Then
                        proj_list.Add(actHr.calc_network)
                    End If
                    Continue For
                End If
                Dim act_found As Boolean = False
                Dim key As String
                If actHr.is_cost_collector Then
                    'activity number is the key to look for the activity
                    key = actHr.calc_network & MConstants.SEP_DEFAULT & actHr.calc_activity
                    If act_act_list.ContainsKey(key) Then
                        actHr.act_id = act_act_list(key).id
                        actHr.act_obj = act_act_list(key)
                        act_found = True
                    End If
                Else
                    'notification number is the key
                    key = actHr.calc_network & MConstants.SEP_DEFAULT & actHr.calc_notfication
                    If notif_act_list.ContainsKey(key) Then
                        actHr.act_id = notif_act_list(key).id
                        actHr.act_obj = notif_act_list(key)
                        act_found = True
                    End If
                End If
                'if act not found bind it to default collector activity
                If Not act_found AndAlso Not IsNothing(allProjAct) Then
                    actHr.act_id = allProjAct.id
                    actHr.act_obj = allProjAct
                End If

                'add to list for saving
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted(actHr)
            Next

            'handle former actual hours
            For Each prevActH As CActualHours In previousAllProjectActualHours
                Dim key As String
                Dim act_found As Boolean = False
                prevActH.populateCalc()

                If prevActH.is_cost_collector Then
                    'activity number is the key to look for the activity
                    key = prevActH.calc_network & MConstants.SEP_DEFAULT & prevActH.calc_activity
                    If act_act_list.ContainsKey(key) Then
                        prevActH.act_id = act_act_list(key).id
                        prevActH.act_obj = act_act_list(key)
                        act_found = True
                    End If
                Else
                    'notification number is the key
                    key = prevActH.calc_network & MConstants.SEP_DEFAULT & prevActH.calc_notfication
                    If notif_act_list.ContainsKey(key) Then
                        prevActH.act_id = notif_act_list(key).id
                        prevActH.act_obj = notif_act_list(key)
                        act_found = True
                    End If
                End If
                If act_found Then
                    MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(prevActH)
                    cnt_updated = cnt_updated + 1
                End If
            Next

            'free memory
            listO.Clear()
            listO = Nothing

            Return New KeyValuePair(Of Boolean, String)(is_error, errMessage)
            GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "END Merging SAP data with current customer statement data"
        Catch ex As Exception
            Throw New Exception("An error occured when merging CJI3 data" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try

    End Function

End Class