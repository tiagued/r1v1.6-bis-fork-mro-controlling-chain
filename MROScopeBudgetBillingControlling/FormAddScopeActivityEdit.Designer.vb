﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormAddScopeActivityEdit
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.top_panel_activity = New System.Windows.Forms.Panel()
        Me.activity_info_grp = New System.Windows.Forms.GroupBox()
        Me.activity_details_tab = New System.Windows.Forms.TabControl()
        Me.act_details_general_page = New System.Windows.Forms.TabPage()
        Me.product_details_lay = New System.Windows.Forms.TableLayoutPanel()
        Me.act_proj_val = New System.Windows.Forms.ComboBox()
        Me.act_proj_lbl = New System.Windows.Forms.Label()
        Me.act_desc_val = New System.Windows.Forms.TextBox()
        Me.act_activity_val = New System.Windows.Forms.TextBox()
        Me.act_desc_lbl = New System.Windows.Forms.Label()
        Me.act_activity_lbl = New System.Windows.Forms.Label()
        Me.act_mat_info_val = New System.Windows.Forms.TextBox()
        Me.act_mat_info_lbl = New System.Windows.Forms.Label()
        Me.act_sold_freight_val = New System.Windows.Forms.TextBox()
        Me.act_sold_freight_lbl = New System.Windows.Forms.Label()
        Me.act_service_val = New System.Windows.Forms.TextBox()
        Me.act_service_lbl = New System.Windows.Forms.Label()
        Me.act_repair_val = New System.Windows.Forms.TextBox()
        Me.act_repair_lbl = New System.Windows.Forms.Label()
        Me.act_sold_mat_val = New System.Windows.Forms.TextBox()
        Me.act_sold_mat_lbl = New System.Windows.Forms.Label()
        Me.act_lab_sold_val = New System.Windows.Forms.TextBox()
        Me.act_lab_sold_lbl = New System.Windows.Forms.Label()
        Me.act_sold_hrs_val = New System.Windows.Forms.TextBox()
        Me.act_sold_hrs_lbl = New System.Windows.Forms.Label()
        Me.act_is_init_val = New System.Windows.Forms.CheckBox()
        Me.act_is_init_lbl = New System.Windows.Forms.Label()
        Me.ct_rev_ctrl_txt_lbl = New System.Windows.Forms.Label()
        Me.act_rev_ctrl_txt_val = New System.Windows.Forms.TextBox()
        Me.act_rev_ctrl_status_val = New System.Windows.Forms.TextBox()
        Me.act_rev_ctrl_status_lbl = New System.Windows.Forms.Label()
        Me.act_operator_code_val = New System.Windows.Forms.TextBox()
        Me.act_operator_code_lbl = New System.Windows.Forms.Label()
        Me.act_warr_code_val = New System.Windows.Forms.TextBox()
        Me.act_warr_code_lbl = New System.Windows.Forms.Label()
        Me.act_check_type_val = New System.Windows.Forms.TextBox()
        Me.act_check_type_lbl = New System.Windows.Forms.Label()
        Me.act_long_desc_val = New System.Windows.Forms.TextBox()
        Me.act_long_desc_lbl = New System.Windows.Forms.Label()
        Me.act_status_lbl = New System.Windows.Forms.Label()
        Me.act_status_val = New System.Windows.Forms.TextBox()
        Me.act_notif_val = New System.Windows.Forms.TextBox()
        Me.act_notif_lbl = New System.Windows.Forms.Label()
        Me.act_details_editable_page = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.act_comment_val = New System.Windows.Forms.TextBox()
        Me.act_comment_lbl = New System.Windows.Forms.Label()
        Me.act_freight_mark_val = New System.Windows.Forms.ComboBox()
        Me.act_serv_mark_val = New System.Windows.Forms.ComboBox()
        Me.act_freight_mark_lbl = New System.Windows.Forms.Label()
        Me.act_rep_mark_val = New System.Windows.Forms.ComboBox()
        Me.act_mat_mark_val = New System.Windows.Forms.ComboBox()
        Me.act_serv_mark_lbl = New System.Windows.Forms.Label()
        Me.act_waste_cause_val = New System.Windows.Forms.ComboBox()
        Me.act_waste_cause_lbl = New System.Windows.Forms.Label()
        Me.act_rep_mark_lbl = New System.Windows.Forms.Label()
        Me.act_is_hidden_val = New System.Windows.Forms.CheckBox()
        Me.act_is_hidden_lbl = New System.Windows.Forms.Label()
        Me.act_mat_mark_lbl = New System.Windows.Forms.Label()
        Me.act_labor_mark_val = New System.Windows.Forms.ComboBox()
        Me.act_category_val = New System.Windows.Forms.ComboBox()
        Me.act_labor_mark_lbl = New System.Windows.Forms.Label()
        Me.act_category_lbl = New System.Windows.Forms.Label()
        Me.act_discount_val = New System.Windows.Forms.ComboBox()
        Me.act_oem_status_val = New System.Windows.Forms.ComboBox()
        Me.act_bill_work_status_val = New System.Windows.Forms.ComboBox()
        Me.act_discount_lbl = New System.Windows.Forms.Label()
        Me.act_bill_work_status_lbl = New System.Windows.Forms.Label()
        Me.act_oem_status_lbl = New System.Windows.Forms.Label()
        Me.act_fix_lab_val = New System.Windows.Forms.TextBox()
        Me.act_fix_lab_lbl = New System.Windows.Forms.Label()
        Me.act_is_small_mat_lbl = New System.Windows.Forms.Label()
        Me.act_is_small_mat_val = New System.Windows.Forms.CheckBox()
        Me.act_nte_lbl = New System.Windows.Forms.Label()
        Me.act_nte_val = New System.Windows.Forms.ComboBox()
        Me.act_value_anal_lbl = New System.Windows.Forms.Label()
        Me.act_value_anal_val = New System.Windows.Forms.ComboBox()
        Me.act_is_odi_lbl = New System.Windows.Forms.Label()
        Me.act_is_odi_val = New System.Windows.Forms.CheckBox()
        Me.act_details_calculation_page = New System.Windows.Forms.TabPage()
        Me.act_calc_dgridv_panel = New System.Windows.Forms.Panel()
        Me.act_calc_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_calc_filter_panel = New System.Windows.Forms.Panel()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.act_calc_calc_type_list_val = New System.Windows.Forms.ComboBox()
        Me.act_calc_calc_type_list_lbl = New System.Windows.Forms.Label()
        Me.act_calc_scope_list_val = New System.Windows.Forms.ComboBox()
        Me.act_calc_payer_list_val = New System.Windows.Forms.ComboBox()
        Me.act_calc_scope_list_lbl = New System.Windows.Forms.Label()
        Me.act_calc_payer_list_lbl = New System.Windows.Forms.Label()
        Me.select_activity_grp = New System.Windows.Forms.GroupBox()
        Me.map_sap_act_bt = New System.Windows.Forms.Button()
        Me.new_awq_bt = New System.Windows.Forms.Button()
        Me.activity_error_state_pic = New System.Windows.Forms.PictureBox()
        Me.activity_select_right_pic = New System.Windows.Forms.PictureBox()
        Me.activity_select_left_pic = New System.Windows.Forms.PictureBox()
        Me.cb_activity = New System.Windows.Forms.ComboBox()
        Me.act_lab_mat_panel = New System.Windows.Forms.Panel()
        Me.act_lab_mat_tab = New System.Windows.Forms.TabControl()
        Me.act_matlab_addlab_page = New System.Windows.Forms.TabPage()
        Me.act_add_lab_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_matlab_add_mat_page = New System.Windows.Forms.TabPage()
        Me.act_add_materials_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_matlab_initlab_page = New System.Windows.Forms.TabPage()
        Me.act_init_lab_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_matlab_init_mat_page = New System.Windows.Forms.TabPage()
        Me.act_init_materials_dgrid = New System.Windows.Forms.DataGridView()
        Me.act_lab_mat_grp = New System.Windows.Forms.GroupBox()
        Me.top_panel_activity.SuspendLayout()
        Me.activity_info_grp.SuspendLayout()
        Me.activity_details_tab.SuspendLayout()
        Me.act_details_general_page.SuspendLayout()
        Me.product_details_lay.SuspendLayout()
        Me.act_details_editable_page.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.act_details_calculation_page.SuspendLayout()
        Me.act_calc_dgridv_panel.SuspendLayout()
        CType(Me.act_calc_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_calc_filter_panel.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.select_activity_grp.SuspendLayout()
        CType(Me.activity_error_state_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.activity_select_right_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.activity_select_left_pic, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_lab_mat_panel.SuspendLayout()
        Me.act_lab_mat_tab.SuspendLayout()
        Me.act_matlab_addlab_page.SuspendLayout()
        CType(Me.act_add_lab_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_matlab_add_mat_page.SuspendLayout()
        CType(Me.act_add_materials_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_matlab_initlab_page.SuspendLayout()
        CType(Me.act_init_lab_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_matlab_init_mat_page.SuspendLayout()
        CType(Me.act_init_materials_dgrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.act_lab_mat_grp.SuspendLayout()
        Me.SuspendLayout()
        '
        'top_panel_activity
        '
        Me.top_panel_activity.Controls.Add(Me.activity_info_grp)
        Me.top_panel_activity.Controls.Add(Me.select_activity_grp)
        Me.top_panel_activity.Dock = System.Windows.Forms.DockStyle.Top
        Me.top_panel_activity.Location = New System.Drawing.Point(5, 5)
        Me.top_panel_activity.Name = "top_panel_activity"
        Me.top_panel_activity.Size = New System.Drawing.Size(1273, 323)
        Me.top_panel_activity.TabIndex = 1
        '
        'activity_info_grp
        '
        Me.activity_info_grp.Controls.Add(Me.activity_details_tab)
        Me.activity_info_grp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.activity_info_grp.Location = New System.Drawing.Point(0, 50)
        Me.activity_info_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.activity_info_grp.Name = "activity_info_grp"
        Me.activity_info_grp.Size = New System.Drawing.Size(1273, 273)
        Me.activity_info_grp.TabIndex = 1
        Me.activity_info_grp.TabStop = False
        Me.activity_info_grp.Text = "Activity Details Details"
        '
        'activity_details_tab
        '
        Me.activity_details_tab.Controls.Add(Me.act_details_general_page)
        Me.activity_details_tab.Controls.Add(Me.act_details_editable_page)
        Me.activity_details_tab.Controls.Add(Me.act_details_calculation_page)
        Me.activity_details_tab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.activity_details_tab.Location = New System.Drawing.Point(3, 16)
        Me.activity_details_tab.Name = "activity_details_tab"
        Me.activity_details_tab.SelectedIndex = 0
        Me.activity_details_tab.Size = New System.Drawing.Size(1267, 254)
        Me.activity_details_tab.TabIndex = 2
        '
        'act_details_general_page
        '
        Me.act_details_general_page.AutoScroll = True
        Me.act_details_general_page.Controls.Add(Me.product_details_lay)
        Me.act_details_general_page.Location = New System.Drawing.Point(4, 22)
        Me.act_details_general_page.Name = "act_details_general_page"
        Me.act_details_general_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_details_general_page.Size = New System.Drawing.Size(1259, 228)
        Me.act_details_general_page.TabIndex = 0
        Me.act_details_general_page.Text = "General"
        Me.act_details_general_page.UseVisualStyleBackColor = True
        '
        'product_details_lay
        '
        Me.product_details_lay.ColumnCount = 8
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.95552!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.50906!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.555189!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.56837!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.566722!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.81549!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.566722!))
        Me.product_details_lay.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.95717!))
        Me.product_details_lay.Controls.Add(Me.act_proj_val, 3, 0)
        Me.product_details_lay.Controls.Add(Me.act_proj_lbl, 2, 0)
        Me.product_details_lay.Controls.Add(Me.act_desc_val, 7, 0)
        Me.product_details_lay.Controls.Add(Me.act_activity_val, 1, 0)
        Me.product_details_lay.Controls.Add(Me.act_desc_lbl, 6, 0)
        Me.product_details_lay.Controls.Add(Me.act_activity_lbl, 0, 0)
        Me.product_details_lay.Controls.Add(Me.act_mat_info_val, 5, 4)
        Me.product_details_lay.Controls.Add(Me.act_mat_info_lbl, 4, 4)
        Me.product_details_lay.Controls.Add(Me.act_sold_freight_val, 3, 4)
        Me.product_details_lay.Controls.Add(Me.act_sold_freight_lbl, 2, 4)
        Me.product_details_lay.Controls.Add(Me.act_service_val, 1, 4)
        Me.product_details_lay.Controls.Add(Me.act_service_lbl, 0, 4)
        Me.product_details_lay.Controls.Add(Me.act_repair_val, 7, 3)
        Me.product_details_lay.Controls.Add(Me.act_repair_lbl, 6, 3)
        Me.product_details_lay.Controls.Add(Me.act_sold_mat_val, 5, 3)
        Me.product_details_lay.Controls.Add(Me.act_sold_mat_lbl, 4, 3)
        Me.product_details_lay.Controls.Add(Me.act_lab_sold_val, 3, 3)
        Me.product_details_lay.Controls.Add(Me.act_lab_sold_lbl, 2, 3)
        Me.product_details_lay.Controls.Add(Me.act_sold_hrs_val, 1, 3)
        Me.product_details_lay.Controls.Add(Me.act_sold_hrs_lbl, 0, 3)
        Me.product_details_lay.Controls.Add(Me.act_is_init_val, 7, 2)
        Me.product_details_lay.Controls.Add(Me.act_is_init_lbl, 6, 2)
        Me.product_details_lay.Controls.Add(Me.ct_rev_ctrl_txt_lbl, 4, 2)
        Me.product_details_lay.Controls.Add(Me.act_rev_ctrl_txt_val, 5, 2)
        Me.product_details_lay.Controls.Add(Me.act_rev_ctrl_status_val, 3, 2)
        Me.product_details_lay.Controls.Add(Me.act_rev_ctrl_status_lbl, 2, 2)
        Me.product_details_lay.Controls.Add(Me.act_operator_code_val, 1, 2)
        Me.product_details_lay.Controls.Add(Me.act_operator_code_lbl, 0, 2)
        Me.product_details_lay.Controls.Add(Me.act_warr_code_val, 7, 1)
        Me.product_details_lay.Controls.Add(Me.act_warr_code_lbl, 6, 1)
        Me.product_details_lay.Controls.Add(Me.act_check_type_val, 5, 1)
        Me.product_details_lay.Controls.Add(Me.act_check_type_lbl, 4, 1)
        Me.product_details_lay.Controls.Add(Me.act_long_desc_val, 3, 1)
        Me.product_details_lay.Controls.Add(Me.act_long_desc_lbl, 2, 1)
        Me.product_details_lay.Controls.Add(Me.act_status_lbl, 0, 1)
        Me.product_details_lay.Controls.Add(Me.act_status_val, 1, 1)
        Me.product_details_lay.Controls.Add(Me.act_notif_val, 5, 0)
        Me.product_details_lay.Controls.Add(Me.act_notif_lbl, 4, 0)
        Me.product_details_lay.Location = New System.Drawing.Point(7, 6)
        Me.product_details_lay.Name = "product_details_lay"
        Me.product_details_lay.RowCount = 5
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.product_details_lay.Size = New System.Drawing.Size(1401, 125)
        Me.product_details_lay.TabIndex = 0
        '
        'act_proj_val
        '
        Me.act_proj_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_proj_val.FormattingEnabled = True
        Me.act_proj_val.Location = New System.Drawing.Point(476, 3)
        Me.act_proj_val.Name = "act_proj_val"
        Me.act_proj_val.Size = New System.Drawing.Size(100, 21)
        Me.act_proj_val.TabIndex = 3
        '
        'act_proj_lbl
        '
        Me.act_proj_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_proj_lbl.AutoSize = True
        Me.act_proj_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_proj_lbl.Location = New System.Drawing.Point(343, 6)
        Me.act_proj_lbl.Name = "act_proj_lbl"
        Me.act_proj_lbl.Size = New System.Drawing.Size(55, 13)
        Me.act_proj_lbl.TabIndex = 3
        Me.act_proj_lbl.Text = "Project :"
        '
        'act_desc_val
        '
        Me.act_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_desc_val.Location = New System.Drawing.Point(1151, 3)
        Me.act_desc_val.Name = "act_desc_val"
        Me.act_desc_val.Size = New System.Drawing.Size(216, 20)
        Me.act_desc_val.TabIndex = 13
        '
        'act_activity_val
        '
        Me.act_activity_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_activity_val.Location = New System.Drawing.Point(155, 3)
        Me.act_activity_val.Name = "act_activity_val"
        Me.act_activity_val.Size = New System.Drawing.Size(100, 20)
        Me.act_activity_val.TabIndex = 1
        '
        'act_desc_lbl
        '
        Me.act_desc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_desc_lbl.AutoSize = True
        Me.act_desc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_desc_lbl.Location = New System.Drawing.Point(1032, 6)
        Me.act_desc_lbl.Name = "act_desc_lbl"
        Me.act_desc_lbl.Size = New System.Drawing.Size(79, 13)
        Me.act_desc_lbl.TabIndex = 6
        Me.act_desc_lbl.Text = "Description :"
        '
        'act_activity_lbl
        '
        Me.act_activity_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_activity_lbl.AutoSize = True
        Me.act_activity_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_activity_lbl.Location = New System.Drawing.Point(3, 6)
        Me.act_activity_lbl.Name = "act_activity_lbl"
        Me.act_activity_lbl.Size = New System.Drawing.Size(57, 13)
        Me.act_activity_lbl.TabIndex = 0
        Me.act_activity_lbl.Text = "Activity :"
        '
        'act_mat_info_val
        '
        Me.act_mat_info_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_mat_info_val.Location = New System.Drawing.Point(812, 103)
        Me.act_mat_info_val.Name = "act_mat_info_val"
        Me.act_mat_info_val.Size = New System.Drawing.Size(187, 20)
        Me.act_mat_info_val.TabIndex = 18
        '
        'act_mat_info_lbl
        '
        Me.act_mat_info_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_mat_info_lbl.AutoSize = True
        Me.act_mat_info_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_mat_info_lbl.Location = New System.Drawing.Point(693, 106)
        Me.act_mat_info_lbl.Name = "act_mat_info_lbl"
        Me.act_mat_info_lbl.Size = New System.Drawing.Size(92, 13)
        Me.act_mat_info_lbl.TabIndex = 17
        Me.act_mat_info_lbl.Text = "Material Infos :"
        '
        'act_sold_freight_val
        '
        Me.act_sold_freight_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_freight_val.Location = New System.Drawing.Point(476, 103)
        Me.act_sold_freight_val.Name = "act_sold_freight_val"
        Me.act_sold_freight_val.Size = New System.Drawing.Size(100, 20)
        Me.act_sold_freight_val.TabIndex = 2
        '
        'act_sold_freight_lbl
        '
        Me.act_sold_freight_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_freight_lbl.AutoSize = True
        Me.act_sold_freight_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_sold_freight_lbl.Location = New System.Drawing.Point(343, 106)
        Me.act_sold_freight_lbl.Name = "act_sold_freight_lbl"
        Me.act_sold_freight_lbl.Size = New System.Drawing.Size(83, 13)
        Me.act_sold_freight_lbl.TabIndex = 8
        Me.act_sold_freight_lbl.Text = "Sold Freight :"
        '
        'act_service_val
        '
        Me.act_service_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_service_val.Location = New System.Drawing.Point(155, 103)
        Me.act_service_val.Name = "act_service_val"
        Me.act_service_val.Size = New System.Drawing.Size(100, 20)
        Me.act_service_val.TabIndex = 4
        '
        'act_service_lbl
        '
        Me.act_service_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_service_lbl.AutoSize = True
        Me.act_service_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_service_lbl.Location = New System.Drawing.Point(3, 106)
        Me.act_service_lbl.Name = "act_service_lbl"
        Me.act_service_lbl.Size = New System.Drawing.Size(87, 13)
        Me.act_service_lbl.TabIndex = 9
        Me.act_service_lbl.Text = "Sold Service :"
        '
        'act_repair_val
        '
        Me.act_repair_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_repair_val.Location = New System.Drawing.Point(1151, 78)
        Me.act_repair_val.Name = "act_repair_val"
        Me.act_repair_val.Size = New System.Drawing.Size(100, 20)
        Me.act_repair_val.TabIndex = 3
        '
        'act_repair_lbl
        '
        Me.act_repair_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_repair_lbl.AutoSize = True
        Me.act_repair_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_repair_lbl.Location = New System.Drawing.Point(1032, 81)
        Me.act_repair_lbl.Name = "act_repair_lbl"
        Me.act_repair_lbl.Size = New System.Drawing.Size(103, 13)
        Me.act_repair_lbl.TabIndex = 10
        Me.act_repair_lbl.Text = "Sold Small Mat. :"
        '
        'act_sold_mat_val
        '
        Me.act_sold_mat_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_mat_val.Location = New System.Drawing.Point(812, 78)
        Me.act_sold_mat_val.Name = "act_sold_mat_val"
        Me.act_sold_mat_val.Size = New System.Drawing.Size(100, 20)
        Me.act_sold_mat_val.TabIndex = 3
        '
        'act_sold_mat_lbl
        '
        Me.act_sold_mat_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_mat_lbl.AutoSize = True
        Me.act_sold_mat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_sold_mat_lbl.Location = New System.Drawing.Point(693, 81)
        Me.act_sold_mat_lbl.Name = "act_sold_mat_lbl"
        Me.act_sold_mat_lbl.Size = New System.Drawing.Size(89, 13)
        Me.act_sold_mat_lbl.TabIndex = 9
        Me.act_sold_mat_lbl.Text = "Sold Material :"
        '
        'act_lab_sold_val
        '
        Me.act_lab_sold_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_lab_sold_val.Location = New System.Drawing.Point(476, 78)
        Me.act_lab_sold_val.Name = "act_lab_sold_val"
        Me.act_lab_sold_val.Size = New System.Drawing.Size(100, 20)
        Me.act_lab_sold_val.TabIndex = 4
        '
        'act_lab_sold_lbl
        '
        Me.act_lab_sold_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_lab_sold_lbl.AutoSize = True
        Me.act_lab_sold_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_lab_sold_lbl.Location = New System.Drawing.Point(343, 81)
        Me.act_lab_sold_lbl.Name = "act_lab_sold_lbl"
        Me.act_lab_sold_lbl.Size = New System.Drawing.Size(76, 13)
        Me.act_lab_sold_lbl.TabIndex = 10
        Me.act_lab_sold_lbl.Text = "Sold Labor :"
        '
        'act_sold_hrs_val
        '
        Me.act_sold_hrs_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_hrs_val.Location = New System.Drawing.Point(155, 78)
        Me.act_sold_hrs_val.Name = "act_sold_hrs_val"
        Me.act_sold_hrs_val.Size = New System.Drawing.Size(100, 20)
        Me.act_sold_hrs_val.TabIndex = 5
        '
        'act_sold_hrs_lbl
        '
        Me.act_sold_hrs_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_sold_hrs_lbl.AutoSize = True
        Me.act_sold_hrs_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_sold_hrs_lbl.Location = New System.Drawing.Point(3, 81)
        Me.act_sold_hrs_lbl.Name = "act_sold_hrs_lbl"
        Me.act_sold_hrs_lbl.Size = New System.Drawing.Size(92, 13)
        Me.act_sold_hrs_lbl.TabIndex = 11
        Me.act_sold_hrs_lbl.Text = "Budget Hours :"
        '
        'act_is_init_val
        '
        Me.act_is_init_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_init_val.AutoSize = True
        Me.act_is_init_val.Location = New System.Drawing.Point(1151, 55)
        Me.act_is_init_val.Name = "act_is_init_val"
        Me.act_is_init_val.Size = New System.Drawing.Size(15, 14)
        Me.act_is_init_val.TabIndex = 16
        Me.act_is_init_val.UseVisualStyleBackColor = True
        '
        'act_is_init_lbl
        '
        Me.act_is_init_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_init_lbl.AutoSize = True
        Me.act_is_init_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_is_init_lbl.Location = New System.Drawing.Point(1032, 56)
        Me.act_is_init_lbl.Name = "act_is_init_lbl"
        Me.act_is_init_lbl.Size = New System.Drawing.Size(89, 13)
        Me.act_is_init_lbl.TabIndex = 12
        Me.act_is_init_lbl.Text = "Initial Scope ?"
        '
        'ct_rev_ctrl_txt_lbl
        '
        Me.ct_rev_ctrl_txt_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.ct_rev_ctrl_txt_lbl.AutoSize = True
        Me.ct_rev_ctrl_txt_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ct_rev_ctrl_txt_lbl.Location = New System.Drawing.Point(693, 56)
        Me.ct_rev_ctrl_txt_lbl.Name = "ct_rev_ctrl_txt_lbl"
        Me.ct_rev_ctrl_txt_lbl.Size = New System.Drawing.Size(98, 13)
        Me.ct_rev_ctrl_txt_lbl.TabIndex = 7
        Me.ct_rev_ctrl_txt_lbl.Text = "Rev. Ctrl. Text :"
        '
        'act_rev_ctrl_txt_val
        '
        Me.act_rev_ctrl_txt_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rev_ctrl_txt_val.Location = New System.Drawing.Point(812, 53)
        Me.act_rev_ctrl_txt_val.Name = "act_rev_ctrl_txt_val"
        Me.act_rev_ctrl_txt_val.Size = New System.Drawing.Size(182, 20)
        Me.act_rev_ctrl_txt_val.TabIndex = 7
        '
        'act_rev_ctrl_status_val
        '
        Me.act_rev_ctrl_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rev_ctrl_status_val.Location = New System.Drawing.Point(476, 53)
        Me.act_rev_ctrl_status_val.Name = "act_rev_ctrl_status_val"
        Me.act_rev_ctrl_status_val.Size = New System.Drawing.Size(100, 20)
        Me.act_rev_ctrl_status_val.TabIndex = 8
        '
        'act_rev_ctrl_status_lbl
        '
        Me.act_rev_ctrl_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rev_ctrl_status_lbl.AutoSize = True
        Me.act_rev_ctrl_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_rev_ctrl_status_lbl.Location = New System.Drawing.Point(343, 56)
        Me.act_rev_ctrl_status_lbl.Name = "act_rev_ctrl_status_lbl"
        Me.act_rev_ctrl_status_lbl.Size = New System.Drawing.Size(109, 13)
        Me.act_rev_ctrl_status_lbl.TabIndex = 13
        Me.act_rev_ctrl_status_lbl.Text = "Rev. Ctrl. Status :"
        '
        'act_operator_code_val
        '
        Me.act_operator_code_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_operator_code_val.Location = New System.Drawing.Point(155, 53)
        Me.act_operator_code_val.Name = "act_operator_code_val"
        Me.act_operator_code_val.Size = New System.Drawing.Size(100, 20)
        Me.act_operator_code_val.TabIndex = 9
        '
        'act_operator_code_lbl
        '
        Me.act_operator_code_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_operator_code_lbl.AutoSize = True
        Me.act_operator_code_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_operator_code_lbl.Location = New System.Drawing.Point(3, 56)
        Me.act_operator_code_lbl.Name = "act_operator_code_lbl"
        Me.act_operator_code_lbl.Size = New System.Drawing.Size(93, 13)
        Me.act_operator_code_lbl.TabIndex = 5
        Me.act_operator_code_lbl.Text = "Operator Code:"
        '
        'act_warr_code_val
        '
        Me.act_warr_code_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_warr_code_val.Location = New System.Drawing.Point(1151, 28)
        Me.act_warr_code_val.Name = "act_warr_code_val"
        Me.act_warr_code_val.Size = New System.Drawing.Size(100, 20)
        Me.act_warr_code_val.TabIndex = 10
        '
        'act_warr_code_lbl
        '
        Me.act_warr_code_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_warr_code_lbl.AutoSize = True
        Me.act_warr_code_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_warr_code_lbl.Location = New System.Drawing.Point(1032, 31)
        Me.act_warr_code_lbl.Name = "act_warr_code_lbl"
        Me.act_warr_code_lbl.Size = New System.Drawing.Size(75, 13)
        Me.act_warr_code_lbl.TabIndex = 6
        Me.act_warr_code_lbl.Text = "Warr Code :"
        '
        'act_check_type_val
        '
        Me.act_check_type_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_check_type_val.Location = New System.Drawing.Point(812, 28)
        Me.act_check_type_val.Name = "act_check_type_val"
        Me.act_check_type_val.Size = New System.Drawing.Size(100, 20)
        Me.act_check_type_val.TabIndex = 11
        '
        'act_check_type_lbl
        '
        Me.act_check_type_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_check_type_lbl.AutoSize = True
        Me.act_check_type_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_check_type_lbl.Location = New System.Drawing.Point(693, 31)
        Me.act_check_type_lbl.Name = "act_check_type_lbl"
        Me.act_check_type_lbl.Size = New System.Drawing.Size(83, 13)
        Me.act_check_type_lbl.TabIndex = 12
        Me.act_check_type_lbl.Text = "Check Type :"
        '
        'act_long_desc_val
        '
        Me.act_long_desc_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_long_desc_val.Location = New System.Drawing.Point(476, 28)
        Me.act_long_desc_val.Name = "act_long_desc_val"
        Me.act_long_desc_val.Size = New System.Drawing.Size(161, 20)
        Me.act_long_desc_val.TabIndex = 12
        '
        'act_long_desc_lbl
        '
        Me.act_long_desc_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_long_desc_lbl.AutoSize = True
        Me.act_long_desc_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_long_desc_lbl.Location = New System.Drawing.Point(343, 31)
        Me.act_long_desc_lbl.Name = "act_long_desc_lbl"
        Me.act_long_desc_lbl.Size = New System.Drawing.Size(76, 13)
        Me.act_long_desc_lbl.TabIndex = 10
        Me.act_long_desc_lbl.Text = "Long Desc :"
        '
        'act_status_lbl
        '
        Me.act_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_status_lbl.AutoSize = True
        Me.act_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_status_lbl.Location = New System.Drawing.Point(3, 31)
        Me.act_status_lbl.Name = "act_status_lbl"
        Me.act_status_lbl.Size = New System.Drawing.Size(51, 13)
        Me.act_status_lbl.TabIndex = 4
        Me.act_status_lbl.Text = "Status :"
        '
        'act_status_val
        '
        Me.act_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_status_val.Location = New System.Drawing.Point(155, 28)
        Me.act_status_val.Name = "act_status_val"
        Me.act_status_val.Size = New System.Drawing.Size(100, 20)
        Me.act_status_val.TabIndex = 14
        '
        'act_notif_val
        '
        Me.act_notif_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_notif_val.Location = New System.Drawing.Point(812, 3)
        Me.act_notif_val.Name = "act_notif_val"
        Me.act_notif_val.Size = New System.Drawing.Size(100, 20)
        Me.act_notif_val.TabIndex = 15
        '
        'act_notif_lbl
        '
        Me.act_notif_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_notif_lbl.AutoSize = True
        Me.act_notif_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_notif_lbl.Location = New System.Drawing.Point(693, 6)
        Me.act_notif_lbl.Name = "act_notif_lbl"
        Me.act_notif_lbl.Size = New System.Drawing.Size(65, 13)
        Me.act_notif_lbl.TabIndex = 2
        Me.act_notif_lbl.Text = "Job Card :"
        '
        'act_details_editable_page
        '
        Me.act_details_editable_page.AutoScroll = True
        Me.act_details_editable_page.Controls.Add(Me.TableLayoutPanel1)
        Me.act_details_editable_page.Location = New System.Drawing.Point(4, 22)
        Me.act_details_editable_page.Name = "act_details_editable_page"
        Me.act_details_editable_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_details_editable_page.Size = New System.Drawing.Size(1259, 228)
        Me.act_details_editable_page.TabIndex = 1
        Me.act_details_editable_page.Text = "Editable"
        Me.act_details_editable_page.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 8
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10.95552!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 13.50906!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.555189!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.56837!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.566722!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.81549!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.566722!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.95717!))
        Me.TableLayoutPanel1.Controls.Add(Me.act_comment_val, 1, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.act_comment_lbl, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.act_freight_mark_val, 7, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_serv_mark_val, 5, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_freight_mark_lbl, 6, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_rep_mark_val, 3, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_mat_mark_val, 1, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_serv_mark_lbl, 4, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_waste_cause_val, 5, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_waste_cause_lbl, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_rep_mark_lbl, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_is_hidden_val, 3, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_is_hidden_lbl, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_mat_mark_lbl, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.act_labor_mark_val, 7, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_category_val, 1, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_labor_mark_lbl, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_category_lbl, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_discount_val, 5, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_oem_status_val, 3, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_bill_work_status_val, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_discount_lbl, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_bill_work_status_lbl, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_oem_status_lbl, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_fix_lab_val, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_fix_lab_lbl, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_is_small_mat_lbl, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_is_small_mat_val, 1, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_nte_lbl, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_nte_val, 7, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.act_value_anal_lbl, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_value_anal_val, 3, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.act_is_odi_lbl, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.act_is_odi_val, 5, 2)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 3)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 5
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1401, 125)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'act_comment_val
        '
        Me.act_comment_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_comment_val.Location = New System.Drawing.Point(155, 103)
        Me.act_comment_val.Name = "act_comment_val"
        Me.act_comment_val.Size = New System.Drawing.Size(162, 20)
        Me.act_comment_val.TabIndex = 3
        '
        'act_comment_lbl
        '
        Me.act_comment_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_comment_lbl.AutoSize = True
        Me.act_comment_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_comment_lbl.Location = New System.Drawing.Point(3, 106)
        Me.act_comment_lbl.Name = "act_comment_lbl"
        Me.act_comment_lbl.Size = New System.Drawing.Size(72, 13)
        Me.act_comment_lbl.TabIndex = 9
        Me.act_comment_lbl.Text = "Comments :"
        '
        'act_freight_mark_val
        '
        Me.act_freight_mark_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_freight_mark_val.FormattingEnabled = True
        Me.act_freight_mark_val.Location = New System.Drawing.Point(1151, 78)
        Me.act_freight_mark_val.Name = "act_freight_mark_val"
        Me.act_freight_mark_val.Size = New System.Drawing.Size(121, 21)
        Me.act_freight_mark_val.TabIndex = 11
        '
        'act_serv_mark_val
        '
        Me.act_serv_mark_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_serv_mark_val.FormattingEnabled = True
        Me.act_serv_mark_val.Location = New System.Drawing.Point(812, 78)
        Me.act_serv_mark_val.Name = "act_serv_mark_val"
        Me.act_serv_mark_val.Size = New System.Drawing.Size(121, 21)
        Me.act_serv_mark_val.TabIndex = 12
        '
        'act_freight_mark_lbl
        '
        Me.act_freight_mark_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_freight_mark_lbl.AutoSize = True
        Me.act_freight_mark_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_freight_mark_lbl.Location = New System.Drawing.Point(1032, 81)
        Me.act_freight_mark_lbl.Name = "act_freight_mark_lbl"
        Me.act_freight_mark_lbl.Size = New System.Drawing.Size(86, 13)
        Me.act_freight_mark_lbl.TabIndex = 5
        Me.act_freight_mark_lbl.Text = "Freight Mark :"
        '
        'act_rep_mark_val
        '
        Me.act_rep_mark_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rep_mark_val.FormattingEnabled = True
        Me.act_rep_mark_val.Location = New System.Drawing.Point(476, 78)
        Me.act_rep_mark_val.Name = "act_rep_mark_val"
        Me.act_rep_mark_val.Size = New System.Drawing.Size(121, 21)
        Me.act_rep_mark_val.TabIndex = 13
        '
        'act_mat_mark_val
        '
        Me.act_mat_mark_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_mat_mark_val.FormattingEnabled = True
        Me.act_mat_mark_val.Location = New System.Drawing.Point(155, 78)
        Me.act_mat_mark_val.Name = "act_mat_mark_val"
        Me.act_mat_mark_val.Size = New System.Drawing.Size(121, 21)
        Me.act_mat_mark_val.TabIndex = 14
        '
        'act_serv_mark_lbl
        '
        Me.act_serv_mark_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_serv_mark_lbl.AutoSize = True
        Me.act_serv_mark_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_serv_mark_lbl.Location = New System.Drawing.Point(693, 81)
        Me.act_serv_mark_lbl.Name = "act_serv_mark_lbl"
        Me.act_serv_mark_lbl.Size = New System.Drawing.Size(77, 13)
        Me.act_serv_mark_lbl.TabIndex = 6
        Me.act_serv_mark_lbl.Text = "Serv. Mark :"
        '
        'act_waste_cause_val
        '
        Me.act_waste_cause_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_waste_cause_val.FormattingEnabled = True
        Me.act_waste_cause_val.Location = New System.Drawing.Point(812, 28)
        Me.act_waste_cause_val.Name = "act_waste_cause_val"
        Me.act_waste_cause_val.Size = New System.Drawing.Size(121, 21)
        Me.act_waste_cause_val.TabIndex = 3
        '
        'act_waste_cause_lbl
        '
        Me.act_waste_cause_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_waste_cause_lbl.AutoSize = True
        Me.act_waste_cause_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_waste_cause_lbl.Location = New System.Drawing.Point(693, 31)
        Me.act_waste_cause_lbl.Name = "act_waste_cause_lbl"
        Me.act_waste_cause_lbl.Size = New System.Drawing.Size(90, 13)
        Me.act_waste_cause_lbl.TabIndex = 7
        Me.act_waste_cause_lbl.Text = "Waste Cause :"
        '
        'act_rep_mark_lbl
        '
        Me.act_rep_mark_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_rep_mark_lbl.AutoSize = True
        Me.act_rep_mark_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_rep_mark_lbl.Location = New System.Drawing.Point(343, 81)
        Me.act_rep_mark_lbl.Name = "act_rep_mark_lbl"
        Me.act_rep_mark_lbl.Size = New System.Drawing.Size(84, 13)
        Me.act_rep_mark_lbl.TabIndex = 7
        Me.act_rep_mark_lbl.Text = "S.Mat. Mark :"
        '
        'act_is_hidden_val
        '
        Me.act_is_hidden_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_hidden_val.AutoSize = True
        Me.act_is_hidden_val.Location = New System.Drawing.Point(476, 55)
        Me.act_is_hidden_val.Name = "act_is_hidden_val"
        Me.act_is_hidden_val.Size = New System.Drawing.Size(15, 14)
        Me.act_is_hidden_val.TabIndex = 18
        Me.act_is_hidden_val.UseVisualStyleBackColor = True
        '
        'act_is_hidden_lbl
        '
        Me.act_is_hidden_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_hidden_lbl.AutoSize = True
        Me.act_is_hidden_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_is_hidden_lbl.Location = New System.Drawing.Point(343, 56)
        Me.act_is_hidden_lbl.Name = "act_is_hidden_lbl"
        Me.act_is_hidden_lbl.Size = New System.Drawing.Size(72, 13)
        Me.act_is_hidden_lbl.TabIndex = 14
        Me.act_is_hidden_lbl.Text = "Is Hidden ?"
        '
        'act_mat_mark_lbl
        '
        Me.act_mat_mark_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_mat_mark_lbl.AutoSize = True
        Me.act_mat_mark_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_mat_mark_lbl.Location = New System.Drawing.Point(3, 81)
        Me.act_mat_mark_lbl.Name = "act_mat_mark_lbl"
        Me.act_mat_mark_lbl.Size = New System.Drawing.Size(72, 13)
        Me.act_mat_mark_lbl.TabIndex = 8
        Me.act_mat_mark_lbl.Text = "Mat. Mark :"
        '
        'act_labor_mark_val
        '
        Me.act_labor_mark_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_labor_mark_val.FormattingEnabled = True
        Me.act_labor_mark_val.Location = New System.Drawing.Point(1151, 53)
        Me.act_labor_mark_val.Name = "act_labor_mark_val"
        Me.act_labor_mark_val.Size = New System.Drawing.Size(121, 21)
        Me.act_labor_mark_val.TabIndex = 10
        '
        'act_category_val
        '
        Me.act_category_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_category_val.FormattingEnabled = True
        Me.act_category_val.Location = New System.Drawing.Point(155, 28)
        Me.act_category_val.Name = "act_category_val"
        Me.act_category_val.Size = New System.Drawing.Size(121, 21)
        Me.act_category_val.TabIndex = 2
        '
        'act_labor_mark_lbl
        '
        Me.act_labor_mark_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_labor_mark_lbl.AutoSize = True
        Me.act_labor_mark_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_labor_mark_lbl.Location = New System.Drawing.Point(1032, 56)
        Me.act_labor_mark_lbl.Name = "act_labor_mark_lbl"
        Me.act_labor_mark_lbl.Size = New System.Drawing.Size(79, 13)
        Me.act_labor_mark_lbl.TabIndex = 9
        Me.act_labor_mark_lbl.Text = "Labor Mark :"
        '
        'act_category_lbl
        '
        Me.act_category_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_category_lbl.AutoSize = True
        Me.act_category_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_category_lbl.Location = New System.Drawing.Point(3, 31)
        Me.act_category_lbl.Name = "act_category_lbl"
        Me.act_category_lbl.Size = New System.Drawing.Size(65, 13)
        Me.act_category_lbl.TabIndex = 18
        Me.act_category_lbl.Text = "Category :"
        '
        'act_discount_val
        '
        Me.act_discount_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_discount_val.FormattingEnabled = True
        Me.act_discount_val.Location = New System.Drawing.Point(812, 3)
        Me.act_discount_val.Name = "act_discount_val"
        Me.act_discount_val.Size = New System.Drawing.Size(121, 21)
        Me.act_discount_val.TabIndex = 3
        '
        'act_oem_status_val
        '
        Me.act_oem_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_oem_status_val.FormattingEnabled = True
        Me.act_oem_status_val.Location = New System.Drawing.Point(476, 3)
        Me.act_oem_status_val.Name = "act_oem_status_val"
        Me.act_oem_status_val.Size = New System.Drawing.Size(121, 21)
        Me.act_oem_status_val.TabIndex = 4
        '
        'act_bill_work_status_val
        '
        Me.act_bill_work_status_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_bill_work_status_val.FormattingEnabled = True
        Me.act_bill_work_status_val.Location = New System.Drawing.Point(155, 3)
        Me.act_bill_work_status_val.Name = "act_bill_work_status_val"
        Me.act_bill_work_status_val.Size = New System.Drawing.Size(121, 21)
        Me.act_bill_work_status_val.TabIndex = 1
        '
        'act_discount_lbl
        '
        Me.act_discount_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_discount_lbl.AutoSize = True
        Me.act_discount_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_discount_lbl.Location = New System.Drawing.Point(693, 6)
        Me.act_discount_lbl.Name = "act_discount_lbl"
        Me.act_discount_lbl.Size = New System.Drawing.Size(65, 13)
        Me.act_discount_lbl.TabIndex = 4
        Me.act_discount_lbl.Text = "Discount :"
        '
        'act_bill_work_status_lbl
        '
        Me.act_bill_work_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_bill_work_status_lbl.AutoSize = True
        Me.act_bill_work_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_bill_work_status_lbl.Location = New System.Drawing.Point(3, 6)
        Me.act_bill_work_status_lbl.Name = "act_bill_work_status_lbl"
        Me.act_bill_work_status_lbl.Size = New System.Drawing.Size(89, 13)
        Me.act_bill_work_status_lbl.TabIndex = 0
        Me.act_bill_work_status_lbl.Text = "Billing Status :"
        '
        'act_oem_status_lbl
        '
        Me.act_oem_status_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_oem_status_lbl.AutoSize = True
        Me.act_oem_status_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_oem_status_lbl.Location = New System.Drawing.Point(343, 6)
        Me.act_oem_status_lbl.Name = "act_oem_status_lbl"
        Me.act_oem_status_lbl.Size = New System.Drawing.Size(82, 13)
        Me.act_oem_status_lbl.TabIndex = 2
        Me.act_oem_status_lbl.Text = "OEM Status :"
        '
        'act_fix_lab_val
        '
        Me.act_fix_lab_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_fix_lab_val.Location = New System.Drawing.Point(1151, 28)
        Me.act_fix_lab_val.Name = "act_fix_lab_val"
        Me.act_fix_lab_val.Size = New System.Drawing.Size(100, 20)
        Me.act_fix_lab_val.TabIndex = 2
        '
        'act_fix_lab_lbl
        '
        Me.act_fix_lab_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_fix_lab_lbl.AutoSize = True
        Me.act_fix_lab_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_fix_lab_lbl.Location = New System.Drawing.Point(1032, 31)
        Me.act_fix_lab_lbl.Name = "act_fix_lab_lbl"
        Me.act_fix_lab_lbl.Size = New System.Drawing.Size(67, 13)
        Me.act_fix_lab_lbl.TabIndex = 12
        Me.act_fix_lab_lbl.Text = "Fix Labor :"
        '
        'act_is_small_mat_lbl
        '
        Me.act_is_small_mat_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_small_mat_lbl.AutoSize = True
        Me.act_is_small_mat_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_is_small_mat_lbl.Location = New System.Drawing.Point(3, 56)
        Me.act_is_small_mat_lbl.Name = "act_is_small_mat_lbl"
        Me.act_is_small_mat_lbl.Size = New System.Drawing.Size(95, 13)
        Me.act_is_small_mat_lbl.TabIndex = 19
        Me.act_is_small_mat_lbl.Text = "Is Small Mat ? :"
        '
        'act_is_small_mat_val
        '
        Me.act_is_small_mat_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_small_mat_val.AutoSize = True
        Me.act_is_small_mat_val.Location = New System.Drawing.Point(155, 55)
        Me.act_is_small_mat_val.Name = "act_is_small_mat_val"
        Me.act_is_small_mat_val.Size = New System.Drawing.Size(15, 14)
        Me.act_is_small_mat_val.TabIndex = 20
        Me.act_is_small_mat_val.UseVisualStyleBackColor = True
        '
        'act_nte_lbl
        '
        Me.act_nte_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_nte_lbl.AutoSize = True
        Me.act_nte_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_nte_lbl.Location = New System.Drawing.Point(1032, 6)
        Me.act_nte_lbl.Name = "act_nte_lbl"
        Me.act_nte_lbl.Size = New System.Drawing.Size(73, 13)
        Me.act_nte_lbl.TabIndex = 1
        Me.act_nte_lbl.Text = "NTE Disc. :"
        '
        'act_nte_val
        '
        Me.act_nte_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_nte_val.FormattingEnabled = True
        Me.act_nte_val.Location = New System.Drawing.Point(1151, 3)
        Me.act_nte_val.Name = "act_nte_val"
        Me.act_nte_val.Size = New System.Drawing.Size(121, 21)
        Me.act_nte_val.TabIndex = 2
        '
        'act_value_anal_lbl
        '
        Me.act_value_anal_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_value_anal_lbl.AutoSize = True
        Me.act_value_anal_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_value_anal_lbl.Location = New System.Drawing.Point(343, 31)
        Me.act_value_anal_lbl.Name = "act_value_anal_lbl"
        Me.act_value_anal_lbl.Size = New System.Drawing.Size(97, 13)
        Me.act_value_anal_lbl.TabIndex = 6
        Me.act_value_anal_lbl.Text = "Value Analysis :"
        '
        'act_value_anal_val
        '
        Me.act_value_anal_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_value_anal_val.FormattingEnabled = True
        Me.act_value_anal_val.Location = New System.Drawing.Point(476, 28)
        Me.act_value_anal_val.Name = "act_value_anal_val"
        Me.act_value_anal_val.Size = New System.Drawing.Size(121, 21)
        Me.act_value_anal_val.TabIndex = 2
        '
        'act_is_odi_lbl
        '
        Me.act_is_odi_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_odi_lbl.AutoSize = True
        Me.act_is_odi_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_is_odi_lbl.Location = New System.Drawing.Point(693, 56)
        Me.act_is_odi_lbl.Name = "act_is_odi_lbl"
        Me.act_is_odi_lbl.Size = New System.Drawing.Size(62, 13)
        Me.act_is_odi_lbl.TabIndex = 10
        Me.act_is_odi_lbl.Text = "Is ODI ? :"
        '
        'act_is_odi_val
        '
        Me.act_is_odi_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_is_odi_val.AutoSize = True
        Me.act_is_odi_val.Location = New System.Drawing.Point(812, 55)
        Me.act_is_odi_val.Name = "act_is_odi_val"
        Me.act_is_odi_val.Size = New System.Drawing.Size(15, 14)
        Me.act_is_odi_val.TabIndex = 17
        Me.act_is_odi_val.UseVisualStyleBackColor = True
        '
        'act_details_calculation_page
        '
        Me.act_details_calculation_page.AutoScroll = True
        Me.act_details_calculation_page.Controls.Add(Me.act_calc_dgridv_panel)
        Me.act_details_calculation_page.Controls.Add(Me.act_calc_filter_panel)
        Me.act_details_calculation_page.Location = New System.Drawing.Point(4, 22)
        Me.act_details_calculation_page.Name = "act_details_calculation_page"
        Me.act_details_calculation_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_details_calculation_page.Size = New System.Drawing.Size(1259, 228)
        Me.act_details_calculation_page.TabIndex = 2
        Me.act_details_calculation_page.Text = "Calculation Details"
        Me.act_details_calculation_page.UseVisualStyleBackColor = True
        '
        'act_calc_dgridv_panel
        '
        Me.act_calc_dgridv_panel.Controls.Add(Me.act_calc_dgrid)
        Me.act_calc_dgridv_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_calc_dgridv_panel.Location = New System.Drawing.Point(240, 3)
        Me.act_calc_dgridv_panel.Name = "act_calc_dgridv_panel"
        Me.act_calc_dgridv_panel.Size = New System.Drawing.Size(1016, 222)
        Me.act_calc_dgridv_panel.TabIndex = 2
        '
        'act_calc_dgrid
        '
        Me.act_calc_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_calc_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_calc_dgrid.Location = New System.Drawing.Point(0, 0)
        Me.act_calc_dgrid.Name = "act_calc_dgrid"
        Me.act_calc_dgrid.Size = New System.Drawing.Size(1016, 222)
        Me.act_calc_dgrid.TabIndex = 0
        '
        'act_calc_filter_panel
        '
        Me.act_calc_filter_panel.Controls.Add(Me.TableLayoutPanel2)
        Me.act_calc_filter_panel.Dock = System.Windows.Forms.DockStyle.Left
        Me.act_calc_filter_panel.Location = New System.Drawing.Point(3, 3)
        Me.act_calc_filter_panel.Name = "act_calc_filter_panel"
        Me.act_calc_filter_panel.Size = New System.Drawing.Size(237, 222)
        Me.act_calc_filter_panel.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.69231!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.30769!))
        Me.TableLayoutPanel2.Controls.Add(Me.act_calc_calc_type_list_val, 1, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.act_calc_calc_type_list_lbl, 0, 2)
        Me.TableLayoutPanel2.Controls.Add(Me.act_calc_scope_list_val, 1, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.act_calc_payer_list_val, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.act_calc_scope_list_lbl, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.act_calc_payer_list_lbl, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 3
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(231, 81)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'act_calc_calc_type_list_val
        '
        Me.act_calc_calc_type_list_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_calc_calc_type_list_val.FormattingEnabled = True
        Me.act_calc_calc_type_list_val.Location = New System.Drawing.Point(78, 57)
        Me.act_calc_calc_type_list_val.Name = "act_calc_calc_type_list_val"
        Me.act_calc_calc_type_list_val.Size = New System.Drawing.Size(150, 21)
        Me.act_calc_calc_type_list_val.TabIndex = 4
        '
        'act_calc_calc_type_list_lbl
        '
        Me.act_calc_calc_type_list_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_calc_calc_type_list_lbl.AutoSize = True
        Me.act_calc_calc_type_list_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_calc_calc_type_list_lbl.Location = New System.Drawing.Point(3, 61)
        Me.act_calc_calc_type_list_lbl.Name = "act_calc_calc_type_list_lbl"
        Me.act_calc_calc_type_list_lbl.Size = New System.Drawing.Size(55, 13)
        Me.act_calc_calc_type_list_lbl.TabIndex = 4
        Me.act_calc_calc_type_list_lbl.Text = "Disc. ? :"
        '
        'act_calc_scope_list_val
        '
        Me.act_calc_scope_list_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_calc_scope_list_val.FormattingEnabled = True
        Me.act_calc_scope_list_val.Location = New System.Drawing.Point(78, 30)
        Me.act_calc_scope_list_val.Name = "act_calc_scope_list_val"
        Me.act_calc_scope_list_val.Size = New System.Drawing.Size(150, 21)
        Me.act_calc_scope_list_val.TabIndex = 3
        '
        'act_calc_payer_list_val
        '
        Me.act_calc_payer_list_val.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_calc_payer_list_val.FormattingEnabled = True
        Me.act_calc_payer_list_val.Location = New System.Drawing.Point(78, 3)
        Me.act_calc_payer_list_val.Name = "act_calc_payer_list_val"
        Me.act_calc_payer_list_val.Size = New System.Drawing.Size(150, 21)
        Me.act_calc_payer_list_val.TabIndex = 2
        '
        'act_calc_scope_list_lbl
        '
        Me.act_calc_scope_list_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_calc_scope_list_lbl.AutoSize = True
        Me.act_calc_scope_list_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_calc_scope_list_lbl.Location = New System.Drawing.Point(3, 34)
        Me.act_calc_scope_list_lbl.Name = "act_calc_scope_list_lbl"
        Me.act_calc_scope_list_lbl.Size = New System.Drawing.Size(51, 13)
        Me.act_calc_scope_list_lbl.TabIndex = 3
        Me.act_calc_scope_list_lbl.Text = "Scope :"
        '
        'act_calc_payer_list_lbl
        '
        Me.act_calc_payer_list_lbl.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.act_calc_payer_list_lbl.AutoSize = True
        Me.act_calc_payer_list_lbl.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.act_calc_payer_list_lbl.Location = New System.Drawing.Point(3, 7)
        Me.act_calc_payer_list_lbl.Name = "act_calc_payer_list_lbl"
        Me.act_calc_payer_list_lbl.Size = New System.Drawing.Size(47, 13)
        Me.act_calc_payer_list_lbl.TabIndex = 1
        Me.act_calc_payer_list_lbl.Text = "Payer :"
        '
        'select_activity_grp
        '
        Me.select_activity_grp.Controls.Add(Me.map_sap_act_bt)
        Me.select_activity_grp.Controls.Add(Me.new_awq_bt)
        Me.select_activity_grp.Controls.Add(Me.activity_error_state_pic)
        Me.select_activity_grp.Controls.Add(Me.activity_select_right_pic)
        Me.select_activity_grp.Controls.Add(Me.activity_select_left_pic)
        Me.select_activity_grp.Controls.Add(Me.cb_activity)
        Me.select_activity_grp.Dock = System.Windows.Forms.DockStyle.Top
        Me.select_activity_grp.Location = New System.Drawing.Point(0, 0)
        Me.select_activity_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.select_activity_grp.Name = "select_activity_grp"
        Me.select_activity_grp.Padding = New System.Windows.Forms.Padding(5)
        Me.select_activity_grp.Size = New System.Drawing.Size(1273, 50)
        Me.select_activity_grp.TabIndex = 0
        Me.select_activity_grp.TabStop = False
        Me.select_activity_grp.Text = "Select Activity"
        '
        'map_sap_act_bt
        '
        Me.map_sap_act_bt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.map_sap_act_bt.Location = New System.Drawing.Point(1278, 13)
        Me.map_sap_act_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.map_sap_act_bt.Name = "map_sap_act_bt"
        Me.map_sap_act_bt.Size = New System.Drawing.Size(136, 32)
        Me.map_sap_act_bt.TabIndex = 6
        Me.map_sap_act_bt.Text = "Map With SAP Activity"
        Me.map_sap_act_bt.UseVisualStyleBackColor = True
        '
        'new_awq_bt
        '
        Me.new_awq_bt.Font = New System.Drawing.Font("Microsoft Sans Serif", 7.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.new_awq_bt.Location = New System.Drawing.Point(61, 12)
        Me.new_awq_bt.Margin = New System.Windows.Forms.Padding(0)
        Me.new_awq_bt.Name = "new_awq_bt"
        Me.new_awq_bt.Size = New System.Drawing.Size(48, 32)
        Me.new_awq_bt.TabIndex = 5
        Me.new_awq_bt.Text = "AWQ"
        Me.new_awq_bt.UseVisualStyleBackColor = True
        '
        'activity_error_state_pic
        '
        Me.activity_error_state_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.activity_error_state_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.no_errors
        Me.activity_error_state_pic.Location = New System.Drawing.Point(12, 12)
        Me.activity_error_state_pic.Name = "activity_error_state_pic"
        Me.activity_error_state_pic.Size = New System.Drawing.Size(36, 32)
        Me.activity_error_state_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.activity_error_state_pic.TabIndex = 4
        Me.activity_error_state_pic.TabStop = False
        Me.activity_error_state_pic.Tag = ""
        '
        'activity_select_right_pic
        '
        Me.activity_select_right_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.activity_select_right_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.right_chevron
        Me.activity_select_right_pic.Location = New System.Drawing.Point(1012, 12)
        Me.activity_select_right_pic.Name = "activity_select_right_pic"
        Me.activity_select_right_pic.Size = New System.Drawing.Size(36, 32)
        Me.activity_select_right_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.activity_select_right_pic.TabIndex = 2
        Me.activity_select_right_pic.TabStop = False
        '
        'activity_select_left_pic
        '
        Me.activity_select_left_pic.Cursor = System.Windows.Forms.Cursors.Hand
        Me.activity_select_left_pic.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.left_chevron
        Me.activity_select_left_pic.Location = New System.Drawing.Point(261, 12)
        Me.activity_select_left_pic.Name = "activity_select_left_pic"
        Me.activity_select_left_pic.Size = New System.Drawing.Size(36, 32)
        Me.activity_select_left_pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.activity_select_left_pic.TabIndex = 1
        Me.activity_select_left_pic.TabStop = False
        '
        'cb_activity
        '
        Me.cb_activity.FormattingEnabled = True
        Me.cb_activity.Location = New System.Drawing.Point(303, 19)
        Me.cb_activity.Name = "cb_activity"
        Me.cb_activity.Size = New System.Drawing.Size(689, 21)
        Me.cb_activity.TabIndex = 0
        '
        'act_lab_mat_panel
        '
        Me.act_lab_mat_panel.AutoScroll = True
        Me.act_lab_mat_panel.Controls.Add(Me.act_lab_mat_tab)
        Me.act_lab_mat_panel.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_lab_mat_panel.Location = New System.Drawing.Point(5, 18)
        Me.act_lab_mat_panel.Name = "act_lab_mat_panel"
        Me.act_lab_mat_panel.Size = New System.Drawing.Size(1263, 326)
        Me.act_lab_mat_panel.TabIndex = 2
        '
        'act_lab_mat_tab
        '
        Me.act_lab_mat_tab.Controls.Add(Me.act_matlab_addlab_page)
        Me.act_lab_mat_tab.Controls.Add(Me.act_matlab_add_mat_page)
        Me.act_lab_mat_tab.Controls.Add(Me.act_matlab_initlab_page)
        Me.act_lab_mat_tab.Controls.Add(Me.act_matlab_init_mat_page)
        Me.act_lab_mat_tab.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_lab_mat_tab.Location = New System.Drawing.Point(0, 0)
        Me.act_lab_mat_tab.Name = "act_lab_mat_tab"
        Me.act_lab_mat_tab.SelectedIndex = 0
        Me.act_lab_mat_tab.Size = New System.Drawing.Size(1263, 326)
        Me.act_lab_mat_tab.TabIndex = 3
        '
        'act_matlab_addlab_page
        '
        Me.act_matlab_addlab_page.Controls.Add(Me.act_add_lab_dgrid)
        Me.act_matlab_addlab_page.Location = New System.Drawing.Point(4, 22)
        Me.act_matlab_addlab_page.Name = "act_matlab_addlab_page"
        Me.act_matlab_addlab_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_matlab_addlab_page.Size = New System.Drawing.Size(1255, 300)
        Me.act_matlab_addlab_page.TabIndex = 0
        Me.act_matlab_addlab_page.Text = "Additional Labor"
        Me.act_matlab_addlab_page.UseVisualStyleBackColor = True
        '
        'act_add_lab_dgrid
        '
        Me.act_add_lab_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_add_lab_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_add_lab_dgrid.Location = New System.Drawing.Point(3, 3)
        Me.act_add_lab_dgrid.Name = "act_add_lab_dgrid"
        Me.act_add_lab_dgrid.Size = New System.Drawing.Size(1249, 294)
        Me.act_add_lab_dgrid.TabIndex = 0
        '
        'act_matlab_add_mat_page
        '
        Me.act_matlab_add_mat_page.Controls.Add(Me.act_add_materials_dgrid)
        Me.act_matlab_add_mat_page.Location = New System.Drawing.Point(4, 22)
        Me.act_matlab_add_mat_page.Name = "act_matlab_add_mat_page"
        Me.act_matlab_add_mat_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_matlab_add_mat_page.Size = New System.Drawing.Size(1255, 300)
        Me.act_matlab_add_mat_page.TabIndex = 2
        Me.act_matlab_add_mat_page.Text = "Additional Materials"
        Me.act_matlab_add_mat_page.UseVisualStyleBackColor = True
        '
        'act_add_materials_dgrid
        '
        Me.act_add_materials_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_add_materials_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_add_materials_dgrid.Location = New System.Drawing.Point(3, 3)
        Me.act_add_materials_dgrid.Name = "act_add_materials_dgrid"
        Me.act_add_materials_dgrid.Size = New System.Drawing.Size(1249, 294)
        Me.act_add_materials_dgrid.TabIndex = 1
        '
        'act_matlab_initlab_page
        '
        Me.act_matlab_initlab_page.Controls.Add(Me.act_init_lab_dgrid)
        Me.act_matlab_initlab_page.Location = New System.Drawing.Point(4, 22)
        Me.act_matlab_initlab_page.Name = "act_matlab_initlab_page"
        Me.act_matlab_initlab_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_matlab_initlab_page.Size = New System.Drawing.Size(1255, 300)
        Me.act_matlab_initlab_page.TabIndex = 1
        Me.act_matlab_initlab_page.Text = "Initial Labor"
        Me.act_matlab_initlab_page.UseVisualStyleBackColor = True
        '
        'act_init_lab_dgrid
        '
        Me.act_init_lab_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_init_lab_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_init_lab_dgrid.Location = New System.Drawing.Point(3, 3)
        Me.act_init_lab_dgrid.Name = "act_init_lab_dgrid"
        Me.act_init_lab_dgrid.Size = New System.Drawing.Size(1249, 294)
        Me.act_init_lab_dgrid.TabIndex = 1
        '
        'act_matlab_init_mat_page
        '
        Me.act_matlab_init_mat_page.Controls.Add(Me.act_init_materials_dgrid)
        Me.act_matlab_init_mat_page.Location = New System.Drawing.Point(4, 22)
        Me.act_matlab_init_mat_page.Name = "act_matlab_init_mat_page"
        Me.act_matlab_init_mat_page.Padding = New System.Windows.Forms.Padding(3)
        Me.act_matlab_init_mat_page.Size = New System.Drawing.Size(1255, 300)
        Me.act_matlab_init_mat_page.TabIndex = 3
        Me.act_matlab_init_mat_page.Text = "Initial Materials"
        Me.act_matlab_init_mat_page.UseVisualStyleBackColor = True
        '
        'act_init_materials_dgrid
        '
        Me.act_init_materials_dgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.act_init_materials_dgrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_init_materials_dgrid.Location = New System.Drawing.Point(3, 3)
        Me.act_init_materials_dgrid.Name = "act_init_materials_dgrid"
        Me.act_init_materials_dgrid.Size = New System.Drawing.Size(1249, 294)
        Me.act_init_materials_dgrid.TabIndex = 1
        '
        'act_lab_mat_grp
        '
        Me.act_lab_mat_grp.Controls.Add(Me.act_lab_mat_panel)
        Me.act_lab_mat_grp.Dock = System.Windows.Forms.DockStyle.Fill
        Me.act_lab_mat_grp.Location = New System.Drawing.Point(5, 328)
        Me.act_lab_mat_grp.Margin = New System.Windows.Forms.Padding(5)
        Me.act_lab_mat_grp.Name = "act_lab_mat_grp"
        Me.act_lab_mat_grp.Padding = New System.Windows.Forms.Padding(5)
        Me.act_lab_mat_grp.Size = New System.Drawing.Size(1273, 349)
        Me.act_lab_mat_grp.TabIndex = 3
        Me.act_lab_mat_grp.TabStop = False
        Me.act_lab_mat_grp.Text = "Labor And Material Details"
        '
        'FormAddScopeActivityEdit
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1283, 682)
        Me.Controls.Add(Me.act_lab_mat_grp)
        Me.Controls.Add(Me.top_panel_activity)
        Me.Name = "FormAddScopeActivityEdit"
        Me.Padding = New System.Windows.Forms.Padding(5)
        Me.Text = "Edit Activity Data"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.top_panel_activity.ResumeLayout(False)
        Me.activity_info_grp.ResumeLayout(False)
        Me.activity_details_tab.ResumeLayout(False)
        Me.act_details_general_page.ResumeLayout(False)
        Me.product_details_lay.ResumeLayout(False)
        Me.product_details_lay.PerformLayout()
        Me.act_details_editable_page.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.act_details_calculation_page.ResumeLayout(False)
        Me.act_calc_dgridv_panel.ResumeLayout(False)
        CType(Me.act_calc_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_calc_filter_panel.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.select_activity_grp.ResumeLayout(False)
        CType(Me.activity_error_state_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.activity_select_right_pic, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.activity_select_left_pic, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_lab_mat_panel.ResumeLayout(False)
        Me.act_lab_mat_tab.ResumeLayout(False)
        Me.act_matlab_addlab_page.ResumeLayout(False)
        CType(Me.act_add_lab_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_matlab_add_mat_page.ResumeLayout(False)
        CType(Me.act_add_materials_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_matlab_initlab_page.ResumeLayout(False)
        CType(Me.act_init_lab_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_matlab_init_mat_page.ResumeLayout(False)
        CType(Me.act_init_materials_dgrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me.act_lab_mat_grp.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents top_panel_activity As Panel
    Friend WithEvents activity_info_grp As GroupBox
    Friend WithEvents product_details_lay As TableLayoutPanel
    Friend WithEvents act_check_type_lbl As Label
    Friend WithEvents act_long_desc_lbl As Label
    Friend WithEvents act_desc_lbl As Label
    Friend WithEvents act_status_lbl As Label
    Friend WithEvents act_activity_lbl As Label
    Friend WithEvents act_notif_lbl As Label
    Friend WithEvents select_activity_grp As GroupBox
    Friend WithEvents activity_select_right_pic As PictureBox
    Friend WithEvents activity_select_left_pic As PictureBox
    Friend WithEvents cb_activity As ComboBox
    Friend WithEvents act_operator_code_lbl As Label
    Friend WithEvents act_warr_code_lbl As Label
    Friend WithEvents ct_rev_ctrl_txt_lbl As Label
    Friend WithEvents act_rev_ctrl_status_lbl As Label
    Friend WithEvents act_sold_freight_lbl As Label
    Friend WithEvents act_sold_mat_lbl As Label
    Friend WithEvents act_lab_sold_lbl As Label
    Friend WithEvents act_sold_hrs_lbl As Label
    Friend WithEvents act_is_init_lbl As Label
    Friend WithEvents act_repair_lbl As Label
    Friend WithEvents act_service_lbl As Label
    Friend WithEvents activity_details_tab As TabControl
    Friend WithEvents act_details_general_page As TabPage
    Friend WithEvents act_details_editable_page As TabPage
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents act_fix_lab_lbl As Label
    Friend WithEvents act_is_odi_lbl As Label
    Friend WithEvents act_value_anal_lbl As Label
    Friend WithEvents act_discount_lbl As Label
    Friend WithEvents act_bill_work_status_lbl As Label
    Friend WithEvents act_oem_status_lbl As Label
    Friend WithEvents act_lab_mat_panel As Panel
    Friend WithEvents act_lab_mat_grp As GroupBox
    Friend WithEvents act_lab_mat_tab As TabControl
    Friend WithEvents act_matlab_addlab_page As TabPage
    Friend WithEvents act_matlab_initlab_page As TabPage
    Friend WithEvents act_matlab_add_mat_page As TabPage
    Friend WithEvents act_add_lab_dgrid As DataGridView
    Friend WithEvents act_init_lab_dgrid As DataGridView
    Friend WithEvents act_add_materials_dgrid As DataGridView
    Friend WithEvents act_activity_val As TextBox
    Friend WithEvents act_sold_freight_val As TextBox
    Friend WithEvents act_repair_val As TextBox
    Friend WithEvents act_service_val As TextBox
    Friend WithEvents act_sold_mat_val As TextBox
    Friend WithEvents act_lab_sold_val As TextBox
    Friend WithEvents act_rev_ctrl_txt_val As TextBox
    Friend WithEvents act_rev_ctrl_status_val As TextBox
    Friend WithEvents act_operator_code_val As TextBox
    Friend WithEvents act_warr_code_val As TextBox
    Friend WithEvents act_check_type_val As TextBox
    Friend WithEvents act_long_desc_val As TextBox
    Friend WithEvents act_desc_val As TextBox
    Friend WithEvents act_status_val As TextBox
    Friend WithEvents act_notif_val As TextBox
    Friend WithEvents act_sold_hrs_val As TextBox
    Friend WithEvents act_is_init_val As CheckBox
    Friend WithEvents act_value_anal_val As ComboBox
    Friend WithEvents act_discount_val As ComboBox
    Friend WithEvents act_oem_status_val As ComboBox
    Friend WithEvents act_bill_work_status_val As ComboBox
    Friend WithEvents act_is_odi_val As CheckBox
    Friend WithEvents act_fix_lab_val As TextBox
    Friend WithEvents act_nte_val As ComboBox
    Friend WithEvents act_nte_lbl As Label
    Friend WithEvents act_category_val As ComboBox
    Friend WithEvents act_category_lbl As Label
    Friend WithEvents act_mat_info_lbl As Label
    Friend WithEvents act_mat_info_val As TextBox
    Friend WithEvents act_is_small_mat_lbl As Label
    Friend WithEvents act_is_small_mat_val As CheckBox
    Friend WithEvents act_details_calculation_page As TabPage
    Friend WithEvents act_calc_dgridv_panel As Panel
    Friend WithEvents act_calc_dgrid As DataGridView
    Friend WithEvents act_calc_filter_panel As Panel
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents act_calc_scope_list_val As ComboBox
    Friend WithEvents act_calc_payer_list_val As ComboBox
    Friend WithEvents act_calc_scope_list_lbl As Label
    Friend WithEvents act_calc_payer_list_lbl As Label
    Friend WithEvents act_calc_calc_type_list_val As ComboBox
    Friend WithEvents act_calc_calc_type_list_lbl As Label
    Friend WithEvents act_is_hidden_val As CheckBox
    Friend WithEvents act_is_hidden_lbl As Label
    Friend WithEvents act_waste_cause_val As ComboBox
    Friend WithEvents act_waste_cause_lbl As Label
    Friend WithEvents activity_error_state_pic As PictureBox
    Friend WithEvents new_awq_bt As Button
    Friend WithEvents act_proj_val As ComboBox
    Friend WithEvents act_proj_lbl As Label
    Friend WithEvents act_freight_mark_val As ComboBox
    Friend WithEvents act_serv_mark_val As ComboBox
    Friend WithEvents act_freight_mark_lbl As Label
    Friend WithEvents act_rep_mark_val As ComboBox
    Friend WithEvents act_mat_mark_val As ComboBox
    Friend WithEvents act_serv_mark_lbl As Label
    Friend WithEvents act_rep_mark_lbl As Label
    Friend WithEvents act_mat_mark_lbl As Label
    Friend WithEvents act_labor_mark_val As ComboBox
    Friend WithEvents act_labor_mark_lbl As Label
    Friend WithEvents act_comment_val As TextBox
    Friend WithEvents act_comment_lbl As Label
    Friend WithEvents map_sap_act_bt As Button
    Friend WithEvents act_matlab_init_mat_page As TabPage
    Friend WithEvents act_init_materials_dgrid As DataGridView
End Class
