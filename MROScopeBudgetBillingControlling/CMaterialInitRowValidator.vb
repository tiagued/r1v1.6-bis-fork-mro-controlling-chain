﻿Public Class CMaterialInitRowValidator
    Public Shared instance As CMaterialInitRowValidator
    Public Shared Function getInstance() As CMaterialInitRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CMaterialInitRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(mat As CMaterial)
        mat.calc_is_in_error_state = False
    End Sub

    Public Function activity_obj(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If IsNothing(mat.activity_obj) OrElse mat.activity_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value Then
            err = "Activity cannot be empty"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function activity_id(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Return Me.activity_obj(mat)
    End Function

    Public Function part_number(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If mat.is_sap_import Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If String.IsNullOrWhiteSpace(mat.part_number) Then
            err = "Part Number cannot be empty"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function description(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If mat.is_sap_import Then
            Return New KeyValuePair(Of Boolean, String)(True, "")
        End If

        If String.IsNullOrWhiteSpace(mat.description) Then
            err = "Description cannot be empty"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function material_type(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.material_type = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Material type cannot be empty"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function payer_obj(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Payer cannot be empty"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function payer(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Return payer_obj(mat)
    End Function


    'check calculation ran without error
    Public Function errorText(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""


        If Not String.IsNullOrWhiteSpace(mat.calc_error_text) Then
            err = mat.calc_error_text
            res = False
        Else
            res = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'curr cannot be empty if value is set
    Public Function zprm_zstm_curr(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If (mat.total_sales_price_zprm > 0 OrElse mat.unit_list_price_zstm > 0) And mat.zprm_zstm_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Currency cannot be empty if ZPRM or ZSTM prices are set"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'curr cannot be empty if value is set
    Public Function handling_zcus_curr(mat As CMaterial) As KeyValuePair(Of Boolean, String)

        Dim res As Boolean = True
        Dim err As String = ""

        If mat.handling_zcus > 0 And mat.handling_zcus_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Currency cannot be empty if ZCUS price is set"
            res = False
            mat.calc_is_in_error_state = True
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'curr cannot be empty if value is set
    Public Function freight_zfrp_overwriten_curr(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.freight_zfrp_overwriten_value > 0 And mat.freight_zfrp_overwriten_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
            err = "Currency cannot be empty if ZFRP price is set"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'text 5 set where as the total material in CE value is not 0
    Public Function unit_list_price_zstm(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.unit_list_price_zstm > 0 And Not String.IsNullOrWhiteSpace(mat.text5) Then
            err = "Unit price list shall be null if Text5 is set"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'text 5 set where as the total material in CE value is not 0
    Public Function total_sales_price_zprm(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.total_sales_price_zprm > 0 And Not String.IsNullOrWhiteSpace(mat.text5) Then
            err = "Total Sales price list shall be null if Text5 is set"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'uom shall be filled if qty >0
    Public Function uom(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.qty > 0 And String.IsNullOrWhiteSpace(mat.uom) Then
            err = "UoM shall be set if qty is greather than 0"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'qty shall be >0 if uom fill
    Public Function qty(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If mat.qty = 0 And Not String.IsNullOrWhiteSpace(mat.uom) Then
            err = "qty cannot be equals to 0"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'is report, check if trying to hide item with values
    Public Function is_quote_sum(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not mat.is_scope_init AndAlso mat.is_not_null_price Then
            err = "Material is not mapped whereas it holds some not null price values"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'no discount on init items
    Public Function discount_obj(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not (IsNothing(mat.discount_obj) OrElse mat.discount_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value) AndAlso mat.is_scope_init Then
            err = "Cannot Apply Discount on Material part of init scope"
            res = False
            mat.calc_is_in_error_state = True
        ElseIf Not IsNothing(mat.discount_obj) AndAlso mat.discount_obj.id <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value AndAlso mat.payer_obj.payer <> mat.discount_obj.payer_obj.payer Then
            err = "Discount Payer and Material Payer are not matching"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function discount_id(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Return discount_obj(mat)
    End Function

    'no NTE on init items
    Public Function nte_obj(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If Not (IsNothing(mat.nte_obj) OrElse mat.nte_obj.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value) AndAlso mat.is_scope_init Then
            err = "Cannot Apply NTE on Material part of init scope"
            res = False
            mat.calc_is_in_error_state = True
        ElseIf Not IsNothing(mat.nte_obj) AndAlso mat.nte_obj.id <> MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value AndAlso mat.payer_obj.payer <> mat.nte_obj.payer_obj.payer Then
            err = "NTE Payer and Material Payer are not matching"
            res = False
            mat.calc_is_in_error_state = True
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function nte_id(mat As CMaterial) As KeyValuePair(Of Boolean, String)
        Return nte_obj(mat)
    End Function
End Class
