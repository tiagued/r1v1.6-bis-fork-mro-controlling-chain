﻿Public Class CLaborControlCostNodeObjRowValidator
    Public Shared instance As CLaborControlCostNodeObjRowValidator
    Public Shared Function getInstance() As CLaborControlCostNodeObjRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CLaborControlCostNodeObjRowValidator
            Return instance
        End If
    End Function


    Public Sub initErrorState(ctrlObj As CLaborControlMainViewObj)
        ctrlObj.calc_is_in_error_state = False
    End Sub

    'validate budget transferred
    Public Function tr_budget_total_qty(ctrlObj As CLaborControlMainViewObj) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If ctrlObj.budget_init_qty < ctrlObj.sent_item.budget_init_qty Then
            err = "The Init Budget Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_init_qty & ", Transferred : " & ctrlObj.sent_item.budget_init_qty
            res = False
        End If
        If ctrlObj.budget_add_below_thr_qty < ctrlObj.sent_item.budget_add_below_thr_qty Then
            err = "The budget_add_below_thr_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_add_below_thr_qty & ", Transferred : " & ctrlObj.sent_item.budget_add_below_thr_qty
            res = False
        End If
        If ctrlObj.budget_add_awq_inwork_qty < ctrlObj.sent_item.budget_add_awq_inwork_qty Then
            err = "The budget_add_awq_inwork_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_add_awq_inwork_qty & ", Transferred : " & ctrlObj.sent_item.budget_add_awq_inwork_qty
            res = False
        End If
        If ctrlObj.budget_add_awq_sent_qty < ctrlObj.sent_item.budget_add_awq_sent_qty Then
            err = "The budget_add_awq_sent_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_add_awq_sent_qty & ", Transferred : " & ctrlObj.sent_item.budget_add_awq_sent_qty
            res = False
        End If
        If ctrlObj.budget_add_awq_approved_qty < ctrlObj.sent_item.budget_add_awq_approved_qty Then
            err = "The budget_add_awq_approved_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_add_awq_approved_qty & ", Transferred : " & ctrlObj.sent_item.budget_add_awq_approved_qty
            res = False
        End If
        If ctrlObj.budget_foc_init_qty < ctrlObj.sent_item.budget_foc_init_qty Then
            err = "The budget_foc_init_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_foc_init_qty & ", Transferred : " & ctrlObj.sent_item.budget_foc_init_qty
            res = False
        End If
        If ctrlObj.budget_foc_add_blw_qty < ctrlObj.sent_item.budget_foc_add_blw_qty Then
            err = "The budget_foc_add_blw_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_foc_add_blw_qty & ", Transferred : " & ctrlObj.sent_item.budget_foc_add_blw_qty
            res = False
        End If
        If ctrlObj.budget_foc_add_appr_qty < ctrlObj.sent_item.budget_foc_add_appr_qty Then
            err = "The budget_foc_add_appr_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_foc_add_appr_qty & ", Transferred : " & ctrlObj.sent_item.budget_foc_add_appr_qty
            res = False
        End If
        If ctrlObj.budget_foc_add_pending_qty < ctrlObj.sent_item.budget_foc_add_pending_qty Then
            err = "The budget_foc_add_pending_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_foc_add_pending_qty & ", Transferred : " & ctrlObj.sent_item.budget_foc_add_pending_qty
            res = False
        End If
        If ctrlObj.budget_foc_add_inwork_qty < ctrlObj.sent_item.budget_foc_add_inwork_qty Then
            err = "The budget_foc_add_inwork_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_foc_add_inwork_qty & ", Transferred : " & ctrlObj.sent_item.budget_foc_add_inwork_qty
            res = False
        End If
        If ctrlObj.budget_odi_qty < ctrlObj.sent_item.budget_odi_qty Then
            err = "The budget_odi_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.budget_odi_qty & ", Transferred : " & ctrlObj.sent_item.budget_odi_qty
            res = False
        End If

        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    'validate actual transferred
    Public Function tr_actual_booked_qty(ctrlObj As CLaborControlMainViewObj) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""
        If ctrlObj.actual_booked_qty < ctrlObj.sent_item.actual_booked_qty Then
            err = "The actual_booked_qty Transferred to Other Controlling Objects is higher than the available budget. Budget : " & ctrlObj.actual_booked_qty & ", Transferred : " & ctrlObj.sent_item.actual_booked_qty
            res = False
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

End Class
