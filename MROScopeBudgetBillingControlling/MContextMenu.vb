﻿Imports Equin.ApplicationFramework

Module MContextMenu

    'generic context menu for dtagridviews and othrers controls
    Public GENERIC_CONTEXT_MENU As ContextMenuStrip
    Public TXTBOX_CONTEXT_MENU As ContextMenuStrip

    Public Sub initializeContextMenu()
        'init items
        createDgridContextMenu()
        createTextBoxContextMenu()
    End Sub

    Private Sub createDgridContextMenu()
        'dgrid menu strip
        GENERIC_CONTEXT_MENU = New ContextMenuStrip()
        GENERIC_CONTEXT_MENU.AutoSize = True
        AddHandler GENERIC_CONTEXT_MENU.Opening, AddressOf ContextMenu_Opening
        'copy items
        Dim copyItem As New ToolStripMenuItem
        copyItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.copy_icon
        copyItem.Name = "copy"
        copyItem.Text = "Copy"
        GENERIC_CONTEXT_MENU.Items.Add(copyItem)
        AddHandler copyItem.Click, AddressOf copyItem_Click

        'paste items
        Dim pasteItem As New ToolStripMenuItem
        pasteItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.paste_icon
        pasteItem.Name = "paste"
        pasteItem.Text = "Paste"
        GENERIC_CONTEXT_MENU.Items.Add(pasteItem)
        AddHandler pasteItem.Click, AddressOf pasteItem_Click

        'propagate items
        Dim propagateItem As New ToolStripMenuItem
        propagateItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.propagate_icon
        propagateItem.Name = "propagate"
        propagateItem.Text = "Propagate"
        GENERIC_CONTEXT_MENU.Items.Add(propagateItem)
        AddHandler propagateItem.Click, AddressOf propagateItem_Click

        'propagate clipboard items
        Dim propagateCBItem As New ToolStripMenuItem
        propagateCBItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.propagate_icon
        propagateCBItem.Name = "propagate_clipBoard"
        propagateCBItem.Text = "Propagate Clip Board"
        GENERIC_CONTEXT_MENU.Items.Add(propagateCBItem)
        AddHandler propagateCBItem.Click, AddressOf propagateCBItem_Click

        'clear content items
        Dim clearItem As New ToolStripMenuItem
        clearItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.clear_content_icon
        clearItem.Name = "clear"
        clearItem.Text = "Clear"
        GENERIC_CONTEXT_MENU.Items.Add(clearItem)
        AddHandler clearItem.Click, AddressOf clearItem_Click

        'delete items
        Dim deleteItem As New ToolStripMenuItem
        deleteItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.delete_icon
        deleteItem.Name = "delete"
        deleteItem.Text = "Delete"
        GENERIC_CONTEXT_MENU.Items.Add(deleteItem)
        AddHandler deleteItem.Click, AddressOf deleteItem_Click

        'reset sort
        Dim resetSort As New ToolStripMenuItem
        resetSort.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.clear_dgrid_sort_icon
        resetSort.Name = "resetSort"
        resetSort.Text = "Reset Sort"
        GENERIC_CONTEXT_MENU.Items.Add(resetSort)
        AddHandler resetSort.Click, AddressOf resetSort_Click

    End Sub

    Private Sub createTextBoxContextMenu()
        'dgrid menu strip
        TXTBOX_CONTEXT_MENU = New ContextMenuStrip()
        TXTBOX_CONTEXT_MENU.AutoSize = True
        'copy items
        Dim copyItem As New ToolStripMenuItem
        copyItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.copy_icon
        copyItem.Name = "copy"
        copyItem.Text = "Copy"
        TXTBOX_CONTEXT_MENU.Items.Add(copyItem)
        AddHandler copyItem.Click, AddressOf copyItem_Click

        'paste items
        Dim pasteItem As New ToolStripMenuItem
        pasteItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.paste_icon
        pasteItem.Name = "paste"
        pasteItem.Text = "Paste"
        TXTBOX_CONTEXT_MENU.Items.Add(pasteItem)
        AddHandler pasteItem.Click, AddressOf pasteItem_Click

        'clear content items
        Dim clearItem As New ToolStripMenuItem
        clearItem.Image = Global.MROScopeBudgetBillingControlling.My.Resources.Resources.clear_content_icon
        clearItem.Name = "clear"
        clearItem.Text = "Clear"
        TXTBOX_CONTEXT_MENU.Items.Add(clearItem)
        AddHandler clearItem.Click, AddressOf clearItem_Click

    End Sub

    Sub ContextMenu_Opening(ByVal sender As ContextMenuStrip, ByVal e As System.ComponentModel.CancelEventArgs)
        Dim s As String = sender.ToString

        If sender.SourceControl.GetType Is GetType(DataGridView) Then
            Dim dgrid As DataGridView = CType(sender.SourceControl, DataGridView)
            If Not dgrid.SelectedCells.Count > 0 Then
                e.Cancel = True
            End If
        End If

    End Sub



    'actions handlers
    Private Sub copyItem_Click(sender As ToolStripMenuItem, e As EventArgs)
        Try
            'handle copy
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                Dim selected As KeyValuePair(Of Boolean, List(Of DataGridViewCell))
                selected = MGeneralFuntionsViewControl.checkOneSelection(dgrid)
                If selected.Key Then
                    Dim cb_obj As New DataObject
                    Dim cb_text As String = ""
                    Dim minCell As DataGridViewCell = selected.Value(0)
                    Dim maxCell As DataGridViewCell = selected.Value(1)
                    For cp_row As Integer = minCell.RowIndex To maxCell.RowIndex
                        Dim isFirst As Boolean = True
                        For cp_col As Integer = minCell.ColumnIndex To maxCell.ColumnIndex
                            Dim cell As DataGridViewCell = dgrid(cp_col, cp_row)
                            If isFirst Then
                                cb_text = cb_text & cell.Value
                                isFirst = False
                            Else
                                cb_text = cb_text & vbTab & cell.Value
                            End If
                        Next
                        cb_text = cb_text & vbCrLf
                    Next
                    cb_obj.SetText(cb_text)
                    Clipboard.Clear()
                    Clipboard.SetDataObject(cb_obj)
                Else
                    MGeneralFuntionsViewControl.displayMessage("Error Copy", "The command cannot be used on multiple selections", MsgBoxStyle.Critical)
                    Exit Sub
                End If

            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Copy", "An error occured while copying to clipboard" & Chr(10) & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    Private Sub propagateItem_Click(sender As ToolStripMenuItem, e As EventArgs)
        'handle propagate
        Try
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                propagateItem(sender, e, False)
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Propagate", "An error occured while propagating data" & Chr(10) & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    'propagate clipboard
    Private Sub propagateCBItem_Click(sender As ToolStripMenuItem, e As EventArgs)
        'handle propagate
        Try
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                propagateItem(sender, e, True)
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Propagate", "An error occured while propagating data" & Chr(10) & ex.Message, MsgBoxStyle.Critical)
        End Try
    End Sub

    'generic propagate item
    Private Sub propagateItem(sender As ToolStripMenuItem, e As EventArgs, fromCP As Boolean)
        'handle propagate
        Try
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                'not on read only dgrid
                If dgrid.ReadOnly Then
                    MGeneralFuntionsViewControl.displayMessage("Error Propagate", "View is readonly and cannot be edited", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                'unique region
                Dim regionUnique As KeyValuePair(Of Boolean, List(Of DataGridViewCell)) = MGeneralFuntionsViewControl.checkOneSelection(dgrid)
                If Not regionUnique.Key Then
                    MGeneralFuntionsViewControl.displayMessage("Error Propagate", "The command cannot be used on multiple selections", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                Dim minCell As DataGridViewCell = regionUnique.Value(0)
                Dim maxCell As DataGridViewCell = regionUnique.Value(1)

                'only one cell to be selected
                If minCell.ColumnIndex <> maxCell.ColumnIndex Then
                    MGeneralFuntionsViewControl.displayMessage("Error Propagate", "Only one column column can be propagated at a time", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                Dim readOnlyCol As New List(Of String)
                Dim readOnlyRow As New List(Of String)
                Dim checkedCol As New List(Of String)
                Dim checkedRow As New List(Of Integer)
                Dim buttonCol As New List(Of String)
                Dim formatErrors As New List(Of String)

                Dim valueProp As Object

                If fromCP Then
                    Dim clipB As Dictionary(Of Integer, Dictionary(Of Integer, String)) = MGeneralFuntionsViewControl.getClipBoardValues()
                    If Not (clipB.Count = 1 AndAlso clipB(0).Count = 1) Then
                        MGeneralFuntionsViewControl.displayMessage("Error Propagate", "Only one cell value shall be in the clipboard to perform propagate", MsgBoxStyle.Critical)
                        Exit Sub
                    End If
                    valueProp = clipB(0)(0)
                Else
                    valueProp = minCell.Value
                End If

                Dim isCellValFormatted As KeyValuePair(Of Boolean, String)
                'check if col is editable and rows are editables
                For Each cell As DataGridViewCell In dgrid.SelectedCells
                    If cell.Equals(minCell) And Not fromCP Then
                        Continue For
                    End If
                    'check if any column is readonly
                    If Not checkedCol.Contains(cell.OwningColumn.Name) Then
                        If cell.OwningColumn.ReadOnly Then
                            readOnlyCol.Add(cell.OwningColumn.Name)
                        ElseIf cell.OwningColumn.GetType.Equals(GetType(DataGridViewButtonColumn)) OrElse cell.OwningColumn.GetType.Equals(GetType(DataGridViewImageColumn)) Then
                            buttonCol.Add(cell.OwningColumn.Name)
                        End If
                        checkedCol.Add(cell.OwningColumn.Name)
                    End If

                    'check if any row is readonly
                    If Not checkedRow.Contains(cell.RowIndex) Then
                        Dim rowObj As IGenericInterfaces.IDataGridViewEditable = Nothing
                        Try
                            rowObj = CType(cell.OwningRow.DataBoundItem.Object, IGenericInterfaces.IDataGridViewEditable)
                        Catch ex As Exception
                        End Try
                        If Not IsNothing(rowObj) AndAlso Not rowObj.isEditable.Key Then
                            readOnlyRow.Add(rowObj.ToString)
                        End If
                        checkedRow.Add(cell.RowIndex)
                    End If

                    'check formating
                    isCellValFormatted = checkCellFormatedValue(valueProp, cell)
                    If Not isCellValFormatted.Key Then
                        formatErrors.Add(isCellValFormatted.Value)
                    End If
                Next

                'check if errors 
                Dim errorMessage = "Cannot perform propagate for the following reasons :"
                Dim isError As Boolean = False

                If readOnlyCol.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Readonly column(s) cannot be modified : " & String.Join(",", readOnlyCol)
                    isError = True
                End If
                If buttonCol.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Cannot propagate value in button column(s) : " & String.Join(",", buttonCol)
                    isError = True
                End If
                If readOnlyRow.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Not Editable row(s) cannot be modified : " & Chr(10) & vbTab & "-" & String.Join(Chr(10) & vbTab & "-", readOnlyRow)
                    isError = True
                End If
                If formatErrors.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Wrong format of value(s) to be pasted : " & Chr(10) & vbTab & "-" & String.Join(Chr(10) & vbTab & "-", formatErrors)
                    isError = True
                End If

                If isError Then
                    MGeneralFuntionsViewControl.displayMessage("Error Propagate", errorMessage, MsgBoxStyle.Critical)
                    Exit Sub
                End If

                'ask for information
                Dim msgRes As MsgBoxResult
                msgRes = MsgBox("Do you really want to propagate value " & minCell.FormattedValue & " to others cells ?", MsgBoxStyle.YesNo)
                If msgRes = MsgBoxResult.No Then
                    Exit Sub
                End If

                'no errors
                Dim hdler As CGridViewRowValidationHandler = Nothing
                If CGridViewRowValidationHandler.handler_list.ContainsKey(dgrid.Name) Then
                    hdler = CGridViewRowValidationHandler.handler_list(dgrid.Name)
                End If

                'check if col is editable and rows are editables
                For Each cell As DataGridViewCell In dgrid.SelectedCells
                    If cell.Equals(minCell) And Not fromCP Then
                        Continue For
                    End If
                    cell.Value = valueProp
                    If Not IsNothing(hdler) Then
                        CGridViewRowValidationHandler.validateRow(dgrid, cell.RowIndex, hdler)
                    End If
                Next

            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Propagate", "An error occured while propagating data" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'paste handler
    Private Sub pasteItem_Click(sender As ToolStripMenuItem, e As EventArgs)
        'handle paste
        Try
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                'not on read only dgrid
                If dgrid.ReadOnly Then
                    MGeneralFuntionsViewControl.displayMessage("Error Paste", "View is readonly and cannot be edited", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                'only one cell to be selected
                If dgrid.SelectedCells.Count <> 1 Then
                    MGeneralFuntionsViewControl.displayMessage("Error Paste", "Only one cell shall be selected to perform Paste", MsgBoxStyle.Critical)
                    Exit Sub
                End If


                'unique region
                Dim regionUnique As KeyValuePair(Of Boolean, List(Of DataGridViewCell)) = MGeneralFuntionsViewControl.checkOneSelection(dgrid)
                If Not regionUnique.Key Then
                    MGeneralFuntionsViewControl.displayMessage("Error Paste", "The command cannot be used on multiple selections", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                Dim clipB As Dictionary(Of Integer, Dictionary(Of Integer, String)) = MGeneralFuntionsViewControl.getClipBoardValues()
                Dim clipBRows As Integer = clipB.Count
                Dim clipBCols As Integer = clipB(0).Count
                'dgrid rows depend on the latest new row
                Dim dgrid_rows As Integer
                If dgrid.AllowUserToAddRows Then
                    dgrid_rows = dgrid.RowCount - 1
                Else
                    dgrid_rows = dgrid.RowCount
                End If
                'do not use dgrid.currentcell because it ignores new added rows cells
                Dim dgrid_currentCell As DataGridViewCell = dgrid.SelectedCells(0)
                'check if enough rows for pasting
                If Not dgrid_currentCell.RowIndex + clipBRows <= dgrid_rows Then
                    MGeneralFuntionsViewControl.displayMessage("Error Paste", "The clipboard content is " & dgrid_currentCell.RowIndex + clipBRows - dgrid_rows & " row(s) greather than the available rows for pasting", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                'check if enough columns for pasting
                If Not dgrid_currentCell.ColumnIndex + clipBCols <= dgrid.ColumnCount Then
                    MGeneralFuntionsViewControl.displayMessage("Error Paste", "The clipboard content is " & dgrid_currentCell.ColumnIndex + clipBCols - dgrid.ColumnCount & " column(s) greather than the available columns for pasting", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                Dim readOnlyCol As New List(Of String)
                Dim readOnlyRow As New List(Of String)
                Dim checkedCol As New List(Of String)
                Dim checkedRow As New List(Of Integer)
                Dim buttonCol As New List(Of String)
                Dim formatErrors As New List(Of String)

                Dim isCellValFormatted As KeyValuePair(Of Boolean, String)
                For cp_row As Integer = 0 To clipBRows - 1
                    For cp_col As Integer = 0 To clipBCols - 1
                        Dim cell As DataGridViewCell = dgrid(dgrid_currentCell.ColumnIndex + cp_col, dgrid_currentCell.RowIndex + cp_row)
                        Dim pasteValue As String = clipB(cp_row)(cp_col)
                        cell.Selected = True
                        'check if any column is readonly
                        If Not checkedCol.Contains(cell.OwningColumn.Name) Then
                            If cell.OwningColumn.ReadOnly Then
                                readOnlyCol.Add(cell.OwningColumn.Name)
                            ElseIf cell.OwningColumn.GetType.Equals(GetType(DataGridViewButtonColumn)) OrElse cell.OwningColumn.GetType.Equals(GetType(DataGridViewImageColumn)) Then
                                buttonCol.Add(cell.OwningColumn.Name)
                            End If
                            checkedCol.Add(cell.OwningColumn.Name)
                        End If

                        'check if any row is readonly
                        If Not checkedRow.Contains(cell.RowIndex) Then
                            Dim rowObj As IGenericInterfaces.IDataGridViewEditable = Nothing
                            Try
                                rowObj = CType(cell.OwningRow.DataBoundItem.Object, IGenericInterfaces.IDataGridViewEditable)
                            Catch ex As Exception
                            End Try
                            If Not IsNothing(rowObj) AndAlso Not rowObj.isEditable.Key Then
                                readOnlyRow.Add(rowObj.ToString)
                            End If
                            checkedRow.Add(cell.RowIndex)
                        End If

                        'check formating
                        isCellValFormatted = checkCellFormatedValue(pasteValue, cell)
                        If Not isCellValFormatted.Key Then
                            formatErrors.Add(isCellValFormatted.Value)
                        End If
                    Next
                Next

                'check if errors 
                Dim errorMessage = "Cannot perform paste for the following reasons :"
                Dim isError As Boolean = False

                If readOnlyCol.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Readonly column(s) cannot be modified : " & String.Join(",", readOnlyCol)
                    isError = True
                End If
                If buttonCol.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Cannot paste value in button column(s) : " & String.Join(",", buttonCol)
                    isError = True
                End If
                If readOnlyRow.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Not Editable row(s) cannot be modified : " & Chr(10) & vbTab & "-" & String.Join(Chr(10) & vbTab & "-", readOnlyRow)
                    isError = True
                End If
                If formatErrors.Count > 0 Then
                    errorMessage = errorMessage & Chr(10) & Chr(10) & "--Wrong format of value(s) to be pasted : " & Chr(10) & vbTab & "-" & String.Join(Chr(10) & vbTab & "-", formatErrors)
                    isError = True
                End If

                If isError Then
                    MGeneralFuntionsViewControl.displayMessage("Error Paste", errorMessage, MsgBoxStyle.Critical)
                    Exit Sub
                End If

                'no errors
                'ask for confirmation
                Dim msgRes As MsgBoxResult
                msgRes = MsgBox("Do you really want to paste values on the selected range ?", MsgBoxStyle.YesNo)
                If msgRes = MsgBoxResult.No Then
                    Exit Sub
                End If

                Dim hdler As CGridViewRowValidationHandler = Nothing
                If CGridViewRowValidationHandler.handler_list.ContainsKey(dgrid.Name) Then
                    hdler = CGridViewRowValidationHandler.handler_list(dgrid.Name)
                End If
                For cp_row As Integer = 0 To clipBRows - 1
                    For cp_col As Integer = 0 To clipBCols - 1
                        Dim cell As DataGridViewCell = dgrid(dgrid_currentCell.ColumnIndex + cp_col, dgrid_currentCell.RowIndex + cp_row)
                        Dim pasteValue As String = clipB(cp_row)(cp_col)
                        cell.Value = pasteValue
                    Next
                    If Not IsNothing(hdler) Then
                        CGridViewRowValidationHandler.validateRow(dgrid, dgrid_currentCell.RowIndex + cp_row, hdler)
                    End If
                Next
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Paste", "An error occured while pasting from clipboard" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Function checkCellFormatedValue(value As String, cell As DataGridViewCell) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim mess As String = ""
        'check cell values
        If cell.OwningColumn.GetType.Equals(GetType(DataGridViewTextBoxColumn)) Then
            If cell.ValueType Is GetType(Date) Then
                Dim dateV As Date
                If Not Date.TryParse(value, dateV) Then
                    res = False
                    mess = value & " is not a date for " & cell.OwningColumn.Name & " column"
                End If
            ElseIf cell.ValueType Is GetType(Integer) Then
                Dim intV As Integer
                If Not Integer.TryParse(value, intV) Then
                    res = False
                    mess = value & " is not an integer for " & cell.OwningColumn.Name & " column"
                End If
            ElseIf cell.ValueType Is GetType(Double) Then
                Dim dblV As Double
                If Not Double.TryParse(value, dblV) Then
                    res = False
                    mess = value & " is not a numeric double value for " & cell.OwningColumn.Name & " column"
                End If
            End If
        ElseIf cell.OwningColumn.GetType.Equals(GetType(DataGridViewCheckBoxColumn)) Then
            Dim boolV As Boolean
            If Not Boolean.TryParse(value, boolV) Then
                res = False
                mess = value & " is not a boolean value for " & cell.OwningColumn.Name & " column"
            End If
        ElseIf cell.OwningColumn.GetType.Equals(GetType(DataGridViewComboBoxColumn)) Then
            Dim cellCb As DataGridViewComboBoxCell = cell
            Dim bds As BindingSource = CType(cellCb.DataSource, BindingSource)
            Dim listItem As IGenericInterfaces.IDataGridViewListable = bds.List.OfType(Of IGenericInterfaces.IDataGridViewListable)().ToList.Find(Function(obj As IGenericInterfaces.IDataGridViewListable) String.Equals(value, CStr(CallByName(obj, cellCb.ValueMember, CallType.Get))))
            If IsNothing(listItem) Then
                res = False
                mess = value & " is not in the combobox list for " & cell.OwningColumn.Name & " column"
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function
    'clear handler
    Private Sub clearItem_Click(sender As ToolStripMenuItem, e As EventArgs)
        'handle clear
        Try
            'handle clear
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)
            Dim listOfRowNotEditable As New List(Of Integer)
            Dim listOfRowNotEditableStr As New List(Of String)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                If dgrid.ReadOnly Then
                    MGeneralFuntionsViewControl.displayMessage("Error Clear", "ReadOnly Table View", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                For Each row As DataGridViewRow In dgrid.SelectedRows
                    Dim rowObj As IGenericInterfaces.IDataGridViewEditable = Nothing
                    Try
                        rowObj = CType(row.DataBoundItem.Object, IGenericInterfaces.IDataGridViewEditable)
                    Catch ex As Exception
                    End Try
                    If Not IsNothing(rowObj) AndAlso Not rowObj.isEditable.Key Then
                        listOfRowNotEditable.Add(row.Index)
                        listOfRowNotEditableStr.Add(rowObj.ToString)
                    End If
                Next
                For Each cell As DataGridViewCell In dgrid.SelectedCells
                    If Not listOfRowNotEditable.Contains(cell.RowIndex) Then
                        clearCellContent(cell)
                    End If
                Next

                If listOfRowNotEditableStr.Count > 0 Then
                    MGeneralFuntionsViewControl.displayMessage("Error Clear", "Cannot clear cells of the following rows cause they are blocked for edition" & Chr(10) & String.Join(Chr(10), listOfRowNotEditableStr), MsgBoxStyle.Critical)
                End If
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Clear", "An error occured while clearing cells content" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    Private Sub deleteItem_Click(sender As ToolStripMenuItem, e As EventArgs)
        'handle delete
        Try
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                If dgrid.ReadOnly Then
                    MGeneralFuntionsViewControl.displayMessage("Error Clear", "ReadOnly Table View", MsgBoxStyle.Critical)
                    Exit Sub
                End If
                Dim deletedSuccess As New List(Of String)
                Dim deletedFailed As New List(Of String)
                Dim listToDelete As New List(Of String)

                For Each row As DataGridViewRow In dgrid.SelectedRows
                    Dim rowObj As IGenericInterfaces.IDataGridViewDeletable = Nothing
                    Try
                        rowObj = CType(row.DataBoundItem.Object, IGenericInterfaces.IDataGridViewDeletable)
                    Catch ex As Exception
                    End Try
                    If Not IsNothing(rowObj) Then
                        listToDelete.Add(rowObj.ToString)
                    End If
                Next

                If listToDelete.Count = 0 Then
                    MGeneralFuntionsViewControl.displayMessage("Delete Operation Error", "Please select entire row to perform deletion", MsgBoxStyle.Critical)
                    Exit Sub
                End If

                'ask for confirmation
                Dim msgRes As MsgBoxResult
                msgRes = MsgBox("Do you really want to delete these " & listToDelete.Count & " item(s) ? " & Chr(10) & "-" & String.Join(Chr(10) & "-", listToDelete), MsgBoxStyle.YesNo)
                If msgRes = MsgBoxResult.No Then
                    Exit Sub
                End If

                Dim keyVal As KeyValuePair(Of Boolean, String)
                For Each row As DataGridViewRow In dgrid.SelectedRows
                    Dim rowObj As IGenericInterfaces.IDataGridViewDeletable = Nothing
                    Try
                        rowObj = CType(row.DataBoundItem.Object, IGenericInterfaces.IDataGridViewDeletable)
                    Catch ex As Exception
                    End Try
                    If Not IsNothing(rowObj) Then
                        keyVal = rowObj.deleteItem
                        If keyVal.Key Then
                            'delete success
                            deletedSuccess.Add(rowObj.ToString)
                        Else
                            deletedFailed.Add(rowObj.ToString & ", Failed Reason : " & Chr(10) & keyVal.Value)
                        End If
                    Else
                        deletedFailed.Add("Item at row " & row.Index & ", Failed Reason : No bound data was found")
                    End If
                Next
                Dim message As String
                If deletedSuccess.Count > 0 Then
                    dgrid.DataSource.refresh
                    message = deletedSuccess.Count & " items where deleted with success"
                    message = message & Chr(10) & Chr(10) & "Deleted with success : " & Chr(10) & "-" & String.Join(Chr(10) & "-", deletedSuccess)
                    MGeneralFuntionsViewControl.displayMessage("Delete Operation Result", message, MsgBoxStyle.Information)
                End If
                If deletedFailed.Count > 0 Then
                    message = deletedFailed.Count & " items failed to be deleted"
                    message = message & Chr(10) & Chr(10) & "Deletion failed : " & Chr(10) & "-" & String.Join(Chr(10) & "-", deletedFailed)
                    MGeneralFuntionsViewControl.displayMessage("Delete Operation Result", message, MsgBoxStyle.Critical)
                End If

            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error Delete", "An error occured while deletings rows" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'clear cell content
    Private Sub clearCellContent(cell As DataGridViewCell)
        Try
            If cell.OwningColumn.ReadOnly Then
                Exit Sub
            End If

            Dim colErros As New List(Of String)
            If cell.GetType Is GetType(DataGridViewComboBoxCell) Then
                Dim cellCb As DataGridViewComboBoxCell = CType(cell, DataGridViewComboBoxCell)
                Dim bds As BindingSource = CType(cellCb.DataSource, BindingSource)
                Dim listable As IGenericInterfaces.IDataGridViewListable = bds.List.OfType(Of IGenericInterfaces.IDataGridViewListable)().ToList.Find(Function(obj As IGenericInterfaces.IDataGridViewListable) obj.isEmptyItem)
                If Not IsNothing(listable) Then
                    cellCb.Value = CallByName(listable, cellCb.ValueMember, CallType.Get)
                Else
                    If Not colErros.Contains(cellCb.OwningColumn.Name) Then
                        colErros.Add(cellCb.OwningColumn.Name)
                    End If
                End If
            ElseIf cell.GetType Is GetType(DataGridViewTextBoxCell) Then
                If cell.ValueType Is GetType(Double) OrElse cell.ValueType Is GetType(Integer) Then
                    cell.Value = 0
                ElseIf cell.ValueType Is GetType(String) Then
                    cell.Value = ""
                ElseIf cell.ValueType Is GetType(Date) OrElse cell.ValueType Is GetType(DateTime) Then
                    cell.Value = MConstants.APP_NULL_DATE
                Else
                    If Not colErros.Contains(cell.OwningColumn.Name) Then
                        colErros.Add(cell.OwningColumn.Name)
                    End If
                End If
            Else
                If Not colErros.Contains(cell.OwningColumn.Name) Then
                    colErros.Add(cell.OwningColumn.Name)
                End If
            End If

            If colErros.Count > 0 Then
                MGeneralFuntionsViewControl.displayMessage("Error Clear", "No empty value for combobox column(s)" & Chr(10) & String.Join(",", colErros), MsgBoxStyle.Critical)
            End If
        Catch ex As Exception
            Throw New Exception("Clear Cell " & Chr(10) & ex.Message)
        End Try
    End Sub

    'reset sort
    Private Sub resetSort_Click(sender As ToolStripMenuItem, e As EventArgs)
        'handle delete
        Try

            Dim group As ToolStripMenuItem = CType(sender.OwnerItem, ToolStripMenuItem)
            Dim parent As ContextMenuStrip = CType(sender.Owner, ContextMenuStrip)

            If parent.SourceControl.GetType Is GetType(DataGridView) Then
                Dim dgrid As DataGridView = CType(parent.SourceControl, DataGridView)
                dgrid.DataSource.RemoveSort
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error clear sorting", "An error occured while clearing table sorting" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
End Module
