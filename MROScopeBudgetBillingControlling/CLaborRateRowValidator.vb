﻿
Public Class CLaborRateRowValidator
    Public Shared instance As CLaborRateRowValidator
    Public Shared Function getInstance() As CLaborRateRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CLaborRateRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(laborRate As CLaborRate)
        laborRate.calc_is_in_error_state = False
    End Sub

    Public Function rate(laborRate As CLaborRate) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If laborRate.is_editable Then
            If laborRate.rate <= 0 Then
                err = "Labor Rate Rate Value shall be between greather than 0"
                res = False
                laborRate.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function value_curr(laborRate As CLaborRate) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If laborRate.is_editable Then
            If String.IsNullOrWhiteSpace(laborRate.value_curr) OrElse laborRate.value_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value < 0 Then
                err = "Labor Rate Currecny Cannot be Empty"
                res = False
                laborRate.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class
