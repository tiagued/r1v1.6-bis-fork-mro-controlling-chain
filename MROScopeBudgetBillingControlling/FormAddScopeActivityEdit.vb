﻿Public Class FormAddScopeActivityEdit
    'navigate activity with arrows
    Private Sub activity_select_left_pic_Click(sender As Object, e As EventArgs) Handles activity_select_left_pic.Click
        GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.selectedActivityBackWard()
    End Sub
    'navigate activity with arrows
    Private Sub activity_select_right_pic_Click(sender As Object, e As EventArgs) Handles activity_select_right_pic.Click
        GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.selectedActivityForward()
    End Sub
    'activity change event
    Private Sub cb_activity_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cb_activity.SelectedIndexChanged
        GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.selectedActivityChanged()
    End Sub
    'activity error text
    Private Sub activity_error_state_pic_Click(sender As Object, e As EventArgs) Handles activity_error_state_pic.Click
        MsgBox(activity_error_state_pic.Tag)
    End Sub
    'launch AWQ Form
    Private Sub new_awq_bt_Click(sender As Object, e As EventArgs) Handles new_awq_bt.Click
        GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.launchAWQForm()
    End Sub

    'material main view, handle click on edit button
    Private Sub act_materials_dgrid_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles act_add_materials_dgrid.CellContentClick
        Try
            ' Ignore clicks that are not on button cells. 
            If e.RowIndex >= 0 AndAlso act_add_materials_dgrid.Columns(e.ColumnIndex).Name = MConstants.CONST_CONF_MAT_EDIT Then
                Try
                    Dim material As CMaterial
                    material = CType(act_add_materials_dgrid.Rows(e.RowIndex).DataBoundItem, Equin.ApplicationFramework.ObjectView(Of CMaterial)).Object
                    MConstants.GLB_MSTR_CTRL.material_edit_ctrl.launchEditForm(material, CMaterialAddRowValidator.getInstance, material.activity_obj)
                    CGridViewRowValidationHandler.initializeGridErrorState(act_add_materials_dgrid)
                Catch ex As Exception
                    MGeneralFuntionsViewControl.displayMessage("Error!", "Cannot launch the form to edit material, check bound has been done" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
                End Try
            Else
                Return
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub add_scope_main_datagrid_DataError(ByVal sender As Object, ByVal e As DataGridViewDataErrorEventArgs) Handles act_add_lab_dgrid.DataError
        sender = sender
        e = e
    End Sub

    Private Sub map_sap_act_bt_Click(sender As Object, e As EventArgs) Handles map_sap_act_bt.Click
        MConstants.GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.launchFakeActMap()
    End Sub
End Class