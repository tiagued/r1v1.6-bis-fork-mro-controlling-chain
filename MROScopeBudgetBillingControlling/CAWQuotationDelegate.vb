﻿
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CAWQuotationDelegate
    Private _awq_list As BindingListView(Of CAWQuotation)

    'get awq list
    Public ReadOnly Property awq_list() As BindingListView(Of CAWQuotation)
        Get
            Return _awq_list
        End Get
    End Property

    'read data from awq DB
    Public Sub model_read_awqDB()

        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform queries on awq tables..."
        Try
            model_read_awq_table()
        Catch ex As Exception
            Throw New Exception("Error occured when reading awq data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform queries on awq tables"
    End Sub

    'read data from awq DB
    Private Sub model_read_awq_table()
        Dim listO As List(Of Object)
        Try
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "perform query to awq table..."
            listO = MConstants.GLB_MSTR_CTRL.csDB.performSelectObject(MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_DB_READ).getDefaultSelect, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_DB_READ), True)
            _awq_list = New BindingListView(Of CAWQuotation)(listO)
            _awq_list.ApplySort("reference ASC, revision ASC")
        Catch ex As Exception
            Throw New Exception("Error occured when reading awq data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End perform query to awq table"
    End Sub


    'get quote reference from ID
    Public Function getAWQObject(id As Long) As CAWQuotation

        For Each awq As CAWQuotation In _awq_list.DataSource
            If awq.id = id Then
                Return awq
            End If
        Next
        If id = MConstants.EMPTY_AWQ.id Then
            Return MConstants.EMPTY_AWQ
        End If
        MGeneralFuntionsViewControl.displayMessage("AWQ Not found", "AWQ object with ID " & id & " does not exist", MsgBoxStyle.Critical)
        Return Nothing
    End Function

    Public Sub bind_awq_views()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "bind awq main views..."
        Try
            bind_awq_main_view()
        Catch ex As Exception
            Throw New Exception("Error occured when binding awq views" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End bind awq main views"
    End Sub


    'bind the controls items in the initial scope view view with the relevant data object
    Private Sub bind_awq_main_view()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Bind awq view ..."
        Try
            Dim mapAWQ As CViewModelMapList

            'get activity map view
            mapAWQ = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_AWQ_VIEW_DISPLAY)

            'quotation datagridview
            MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.AllowUserToAddRows = False
            MViewEventHandler.addDefaultDatagridEventHandler(MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid)

            For Each map As CViewModelMap In mapAWQ.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapAWQ.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_DATE).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                    Else
                        dgcol.DefaultCellStyle.Format = MConstants.constantConf(MConstants.CONST_CONF_DATE_FORMAT).value
                    End If
                    dgcol.ValueType = GetType(Date)
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapAWQ.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding AWQ view in AWQ tab. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_IMAGE).value Then
                    Dim dgcol As New DataGridViewImageColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Image = My.Resources.ResourceManager.GetObject(map.view_col)
                    MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns.Add(dgcol)
                End If
            Next

            'handle frozen col
            Dim i As Integer = 1
            While i <= CInt(MConstants.constantConf(MConstants.CONST_CONF_AWQ_VIEW_FREEZE_FIRST_COL_CNT).value)
                MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.Columns(i - 1).Frozen = True
                i = i + 1
            End While

            'bind datasource
            MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid.DataSource = _awq_list
            MGeneralFuntionsViewControl.resizeDataGridWidth(MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid)

            'refresh view when visible
            MViewEventHandler.addAWQListRefreshHandlerWhenVisible(MConstants.GLB_MSTR_CTRL.mainForm.awq_main_dgrid)
        Catch ex As Exception
            Throw New Exception("An error occured while loading awq main view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Bind awq view"
    End Sub

    Public Sub createAWQWithNoActivity()
        Try
            Dim dialogRes As DialogResult
            dialogRes = MessageBox.Show("You are about to create an AWQ for which the activity has not yet been created. This case must be avoided as much as possible. Do you want to continue ? " & Chr(10) & "Click OK to create, or Cancel to leave.", "Confirm new AWQ creation", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If
            Dim awq As New CAWQuotation
            Dim fakeAct As New CScopeActivity

            fakeAct.is_fake_activity = True
            fakeAct.awq_master_obj = awq
            fakeAct.status = ""
            fakeAct.job_card = "0000"
            fakeAct.value_analysis = MConstants.constantConf(MConstants.CONST_CONF_LOV_VAL_ANAL_ADD_V_KEY).value
            fakeAct.setFakeAct()
            'add to view
            MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource.Add(fakeAct)
            MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.Refresh()

            awq.is_latest_rev_calc = True
            awq.main_activity_obj = fakeAct
            awq.dependantActList.Add(fakeAct)

            'insert fake act to db
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(awq.main_activity_obj)
            MConstants.GLB_MSTR_CTRL.add_scope_hours_edit_ctrl.launchEditForm(awq.main_activity_obj)

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error While creating AWQ", "An error occured while creating AWQ with no binded activity " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'return all items sent to customer
    Public Function filterSentToCustomerDelegate(awq As CAWQuotation) As Boolean
        Return awq.has_been_sent()
    End Function

End Class
