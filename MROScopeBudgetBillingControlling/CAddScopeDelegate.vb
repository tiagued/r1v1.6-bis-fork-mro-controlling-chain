﻿
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CAddScopeDelegate
    Public mctrl As CMasterController
    Private _scopeList As BindingListView(Of CScopeActivity)
    Public _scopeListSorted_bds As BindingSource
    Public dbSoldHrsList As List(Of Object)

    Public Sub New(_ctrl As CMasterController)
        mctrl = _ctrl
    End Sub

    'get scope list
    Public ReadOnly Property scopeList() As BindingListView(Of CScopeActivity)
        Get
            Return _scopeList
        End Get
    End Property

    'read data from activity and hours DB
    Public Sub model_read_add_scopeDB()
        mctrl.statusBar.status_strip_sub_label = "perform queries on activity tables..."
        Try
            model_read_activity()
            model_read_sold_hours()
        Catch ex As Exception
            Throw New Exception("Error occured when reading activity data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform queries on activity tables"
    End Sub

    'read data from act DB
    Private Sub model_read_activity()
        Dim listO As List(Of Object)
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to activity table..."
            listO = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_DB_READ), True)

            _scopeList = New BindingListView(Of CScopeActivity)(listO)

            _scopeListSorted_bds = New BindingSource
            'init dependant bds
            refreshScopeDependantViews()

        Catch ex As Exception
            Throw New Exception("Error occured when reading activity data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to activity table"
    End Sub

    Public Sub refreshScopeDependantViews()
        Dim listO As New List(Of CScopeActivity)
        Dim actListHandler As New BindingListView(Of CScopeActivity)(scopeList.DataSource)
        actListHandler.ApplySort("act_notif_opcode ASC")
        listO = MGeneralFuntionsViewControl.getActFromListView(actListHandler)
        listO.Insert(0, MConstants.EMPTY_ACT)
        _scopeListSorted_bds.DataSource = listO
        _scopeListSorted_bds.ResetBindings(False)
    End Sub


    'read data from sold hours DB
    Private Sub model_read_sold_hours()
        Try
            mctrl.statusBar.status_strip_sub_label = "perform query to sold hours table..."
            dbSoldHrsList = mctrl.csDB.performSelectObject(mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_SOLD_HRS_DB_READ).getDefaultSelect, mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_SOLD_HRS_DB_READ), True)
        Catch ex As Exception
            Throw New Exception("Error occured when reading sold hours data from the DB" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End perform query to sold hours table"
    End Sub
    'get act reference from ID
    Public Function getActivityObject(id As Long) As CScopeActivity

        For Each act As CScopeActivity In _scopeList.DataSource
            If act.id = id Then
                Return act
            End If
        Next
        If id = MConstants.EMPTY_ACT.id Then
            Return MConstants.EMPTY_ACT
        End If
        MGeneralFuntionsViewControl.displayMessage("Activity Not found", "Activity object with ID " & id & " does not exist", MsgBoxStyle.Critical)
        Return Nothing
    End Function

    'get a list of all sold hours
    Public Function getSOldHours() As BindingListView(Of CSoldHours)
        Dim listSold As New List(Of CSoldHours)
        For Each act As CScopeActivity In _scopeList.DataSource
            listSold.AddRange(act.sold_hour_list)
        Next
        Return New BindingListView(Of CSoldHours)(listSold)
    End Function

    Public Sub bind_add_scope_views()
        mctrl.statusBar.status_strip_sub_label = "bind add scope views..."
        Try
            bind_act_view()
        Catch ex As Exception
            Throw New Exception("Error occured when binding add scope views" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End bind add scope views"
    End Sub


    'bind the controls items in the initial scope view view with the relevant data object
    Private Sub bind_act_view()
        mctrl.statusBar.status_strip_sub_label = "Bind activities to view controls..."
        Try
            Dim mapAct As CViewModelMapList

            'get activity map view
            mapAct = mctrl.modelViewMap(MConstants.MOD_VIEW_MAP_ADD_SCOP_ACT_VIEW_DISPLAY)

            'sold hours datagridview
            mctrl.mainForm.add_scope_main_datagrid.AllowUserToAddRows = False

            MViewEventHandler.addDefaultDatagridEventHandler(mctrl.mainForm.add_scope_main_datagrid)

            For Each map As CViewModelMap In mapAct.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapAct.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.add_scope_main_datagrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn

                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapAct.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_ACT_NTE_CAT.ToLower Then
                        dgcol.DataSource = MConstants.GLB_MSTR_CTRL.billing_cond_controller._nteList_lov_act
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "id"
                    ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_ACT_DISC.ToLower Then
                        dgcol.DataSource = MConstants.GLB_MSTR_CTRL.billing_cond_controller._discountList_lov_act
                        dgcol.DisplayMember = "label"
                        dgcol.ValueMember = "id"
                    Else
                        Throw New Exception("Error occured when binding activity view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                    End If

                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.add_scope_main_datagrid.Columns.Add(dgcol)
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    mctrl.mainForm.add_scope_main_datagrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    mctrl.mainForm.add_scope_main_datagrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_IMAGE).value Then
                    Dim dgcol As New DataGridViewImageColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Image = My.Resources.ResourceManager.GetObject(map.view_col)
                    mctrl.mainForm.add_scope_main_datagrid.Columns.Add(dgcol)
                End If
            Next

            'handle frozen col
            Dim i As Integer = 1
            While i <= CInt(MConstants.constantConf(MConstants.CONST_CONF_ACT_VIEW_FREEZE_FIRST_COL_CNT).value)
                mctrl.mainForm.add_scope_main_datagrid.Columns(i - 1).Frozen = True
                i = i + 1
            End While

            'bind datasource
            mctrl.mainForm.add_scope_main_datagrid.DataSource = _scopeList
            MGeneralFuntionsViewControl.resizeDataGridWidth(mctrl.mainForm.add_scope_main_datagrid)
            'filter
            _scopeList.ApplyFilter(AddressOf filterDeletedActivities)
            _scopeList.ApplySort("act_notif_opcode ASC")
            'validator
            If CBool(MConstants.constantConf(MConstants.CONST_CONF_ACT_VIEW_VALIDATOR_ACTIVE).value) Then
                'handle row validation events
                Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(mctrl.mainForm.add_scope_main_datagrid, CScopeActivityRowValidator.getInstance, mapAct)
                'dhler.validateAddDelegate = AddressOf quote_entries_view_ValidateAdd
                'dhler.cancelAddDelegate = AddressOf quote_entries_view_CancelAdd
            End If

        Catch ex As Exception
            Throw New Exception("An error occured while loading activities view" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace)
        End Try
        mctrl.statusBar.status_strip_sub_label = "End Bind activities data to view controls"
    End Sub

    Private Function filterDeletedActivities(ByVal act As CScopeActivity)
        'delegate for filtering
        Return Not act.system_status = MConstants.CONST_SYS_ROW_DELETED
    End Function

    'perform delete
    Public Sub deleteObjectSoldHours(soldH As CSoldHours)
        Try
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(soldH)
        Catch ex As Exception
        End Try
    End Sub

    'perform delete
    Public Sub deleteObjectActivity(act As CScopeActivity)
        Try
            For Each soldH As CSoldHours In act.sold_hour_list
                deleteObjectSoldHours(soldH)
            Next
            For Each mat As CMaterial In act.material_list
                MConstants.GLB_MSTR_CTRL.material_controller.deleteObjectMaterial(mat)
            Next
            If _scopeList.DataSource.Contains(act) Then
                _scopeList.DataSource.Remove(act)
            End If
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_deleted(act)
        Catch ex As Exception
        End Try
    End Sub

End Class
