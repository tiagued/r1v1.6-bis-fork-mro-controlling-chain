﻿Imports System.Globalization
Imports System.Text

Module MBackupHandler

    Public Sub backupFile(filepath As String)
        Try
            If System.IO.File.Exists(filepath) Then
                Dim sb As New StringBuilder()
                Dim saveYesterday As Boolean = True
                Dim saveLastWeek As Boolean = True
                Dim fileName = System.IO.Path.GetFileName(filepath)

                sb.Clear()
                Dim fileYesterday As String = sb.AppendFormat(MConstants.constantConf(MConstants.CONST_CONF_BACKUP_FILE_YESTERDAY).value, fileName).ToString
                fileYesterday = System.IO.Path.GetDirectoryName(filepath) & System.IO.Path.DirectorySeparatorChar & fileYesterday

                sb.Clear()
                Dim fileLastWeek As String = sb.AppendFormat(MConstants.constantConf(MConstants.CONST_CONF_BACKUP_FILE_LASTWEEK).value, fileName).ToString
                fileLastWeek = System.IO.Path.GetDirectoryName(filepath) & System.IO.Path.DirectorySeparatorChar & fileLastWeek

                If System.IO.File.Exists(fileYesterday) Then
                    Dim latestDate As Date = System.IO.File.GetLastWriteTime(filepath)
                    If Date.Now.Date = latestDate.Date Then
                        saveYesterday = False
                    Else
                        saveYesterday = True
                    End If
                Else
                    saveYesterday = True
                End If

                If System.IO.File.Exists(fileLastWeek) Then
                    Dim latestDate As Date = System.IO.File.GetLastWriteTime(fileLastWeek)
                    latestDate = System.IO.File.GetLastWriteTime(fileLastWeek)
                    If (Date.Now.Date - latestDate.Date).Days > 7 Then
                        saveLastWeek = True
                    Else
                        If DateTimeFormatInfo.CurrentInfo.Calendar.GetWeekOfYear(Date.Now, DateTimeFormatInfo.CurrentInfo.CalendarWeekRule, DayOfWeek.Thursday) _
                            = DateTimeFormatInfo.CurrentInfo.Calendar.GetWeekOfYear(latestDate, DateTimeFormatInfo.CurrentInfo.CalendarWeekRule, DayOfWeek.Thursday) Then
                            saveLastWeek = False
                        Else
                            saveLastWeek = True
                        End If
                    End If
                Else
                    saveLastWeek = True
                End If

                'save
                If saveLastWeek Then
                    Try
                        System.IO.File.Copy(filepath, fileLastWeek, True)
                    Catch ex As Exception
                    End Try
                End If
                If saveYesterday Then
                    Try
                        System.IO.File.Copy(filepath, fileYesterday, True)
                    Catch ex As Exception
                    End Try
                End If
            End If
        Catch ex As Exception
            'not a critical function, no error needed
        End Try
    End Sub
End Module
