﻿
Public Class CSurchargeRowValidator
    Public Shared instance As CSurchargeRowValidator
    Public Shared Function getInstance() As CSurchargeRowValidator
        If instance IsNot Nothing Then
            Return instance
        Else
            instance = New CSurchargeRowValidator
            Return instance
        End If
    End Function

    Public Sub initErrorState(surcharge As CSurcharge)
        surcharge.calc_is_in_error_state = False
    End Sub

    Public Function rate(surcharge As CSurcharge) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If surcharge.is_editable Then
            If surcharge.rate < 0 OrElse surcharge.rate > 1 Then
                err = "Surcharge Rate shall be between 0 and 1 (0% and 100%)"
                res = False
                surcharge.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function cap_value(surcharge As CSurcharge) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If surcharge.is_editable Then
            If surcharge.cap_value <= 0 Then
                err = "Surcharge Cap shall be between greather than 0"
                res = False
                surcharge.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function

    Public Function curr(surcharge As CSurcharge) As KeyValuePair(Of Boolean, String)
        Dim res As Boolean = True
        Dim err As String = ""

        If surcharge.is_editable Then
            If String.IsNullOrWhiteSpace(surcharge.curr) OrElse surcharge.curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value < 0 Then
                err = "Surcharge Cap Currecny Cannot be Empty"
                res = False
                surcharge.calc_is_in_error_state = True
            End If
        End If
        Return New KeyValuePair(Of Boolean, String)(res, err)
    End Function
End Class
