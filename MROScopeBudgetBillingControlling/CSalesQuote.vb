﻿Imports System.ComponentModel
Imports System.Runtime.CompilerServices

Public Class CSalesQuote
    Implements IGenericInterfaces.IDataGridViewEditable, IGenericInterfaces.IDataGridViewListable, IGenericInterfaces.IDataGridViewDeletable, IGenericInterfaces.IDataGridViewRecordable


    'used to created new ids
    Public Shared currentMaxID As Integer


    Public Sub New()
        'if data are created from DB, do nothing. If data are newly created during runtime, create a new ID
        If MConstants.GLOB_QUOTES_LOADED Then
            _lab_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _mat_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
            _payer = MConstants.listOfPayer.Values(0).payer
            _payer_obj = MConstants.listOfPayer.Values(0)
        End If
    End Sub

    Public Function isRecordable() As Boolean Implements IGenericInterfaces.IDataGridViewRecordable.isRecordable
        Return _id > 0
    End Function

    'get id
    Public Function getID() As Long Implements IGenericInterfaces.IDataGridViewRecordable.getID
        Return _id
    End Function
    'set id
    Public Sub setID(t_id As Long) Implements IGenericInterfaces.IDataGridViewRecordable.setID
        _id = t_id
        'refresh view of list of Quote cause when id is 0 item is not displayed
        MConstants.GLB_MSTR_CTRL.ini_scope_controller.refreshQuoteDependantViews()
    End Sub
    'get mapper
    Public Function getDBMapperName() As String Implements IGenericInterfaces.IDataGridViewRecordable.getDBMapperName
        Return MConstants.MOD_VIEW_MAP_QUOTE_DB_READ
    End Function

    Public Function isEditable() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewEditable.isEditable
        Return New KeyValuePair(Of Boolean, String)(True, "")
    End Function

    Public Function deleteItem() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewDeletable.deleteItem
        Dim res As Boolean = False
        Dim mess As String = ""

        Try
            Dim isUsed As KeyValuePair(Of Boolean, List(Of String)) = MDataConsistency.isSalesQuoteUsed(Me)
            If isUsed.Key Then
                res = False
                mess = "Item " & Me.tostring & " Cannot be deleted because it is used in " & isUsed.Value.Count & " quote summary objects " & Chr(10) & "-" & String.Join(Chr(10) & "-", isUsed.Value)
            Else
                Me.system_status = MConstants.CONST_SYS_ROW_DELETED
                MConstants.GLB_MSTR_CTRL.ini_scope_controller.deleteObjectQuote(Me)
                res = True
            End If
        Catch ex As Exception
            mess = "An error occured while deleting item " & Me.tostring & Chr(10) & ex.Message
        End Try
        Return New KeyValuePair(Of Boolean, String)(res, mess)
    End Function

    Public Function getErrorState() As KeyValuePair(Of Boolean, String) Implements IGenericInterfaces.IDataGridViewRecordable.getErrorState
        Return New KeyValuePair(Of Boolean, String)(_calc_is_in_error_state, _calc_error_text)
    End Function

    Public Sub setErrorState(isErr As Boolean, text As String) Implements IGenericInterfaces.IDataGridViewRecordable.setErrorState
        _calc_is_in_error_state = isErr
        _calc_error_text = text
    End Sub

    Public Function isEmptyItem() As Boolean Implements IGenericInterfaces.IDataGridViewListable.isEmptyItem
        Return _id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
    End Function

    Public Sub setID()
        If MConstants.GLOB_QUOTES_LOADED Then
            MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_inserted_and_run(Me)
        End If
    End Sub
    Public Shared Function getEmptyQuote()
        Dim currState As Boolean
        currState = MConstants.GLOB_QUOTES_LOADED
        MConstants.GLOB_QUOTES_LOADED = False
        Dim empty As New CSalesQuote
        empty.id = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_LNG_KEY).value
        'if label nothing it will cause issue in combobox selection
        empty.number = " "
        empty.lab_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        empty.mat_curr = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
        empty.payer_obj = MConstants.EMPTY_PAYER
        MConstants.GLOB_QUOTES_LOADED = currState
        Return empty
    End Function

    Private _calc_is_in_error_state As Boolean
    Public Property calc_is_in_error_state() As Boolean
        Get
            Return _calc_is_in_error_state
        End Get
        Set(ByVal value As Boolean)
            _calc_is_in_error_state = value
        End Set
    End Property

    'text to display any error
    Private _calc_error_text As String
    Public Property calc_error_text() As String
        Get
            Return _calc_error_text
        End Get
        Set(ByVal value As String)
            If Not value = _calc_error_text Then
                _calc_error_text = value
            End If
        End Set
    End Property

    Private _id As Integer
    Public Property id() As Integer
        Get
            Return _id
        End Get
        Set(ByVal value As Integer)
            If Not _id = value Then
                _id = value
                'handle ID uniqueness for new items
                If Not MConstants.GLOB_QUOTES_LOADED Then
                    CSalesQuote.currentMaxID = Math.Max(CSalesQuote.currentMaxID, _id)
                End If
            End If
        End Set
    End Property

    Private _number As String
    Public Property number() As String
        Get
            Return _number
        End Get
        Set(ByVal value As String)
            If Not _number = value Then
                _number = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _description As String
    Public Property description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            If Not _description = value Then
                _description = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property



    Private _lab_curr As String
    Public Property lab_curr() As String
        Get
            Return _lab_curr
        End Get
        Set(ByVal value As String)
            If Not _lab_curr = value Then
                _lab_curr = value
                If MConstants.GLOB_QUOTES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _mat_curr As String
    Public Property mat_curr() As String
        Get
            Return _mat_curr
        End Get
        Set(ByVal value As String)
            If Not _mat_curr = value Then
                _mat_curr = value
                If MConstants.GLOB_QUOTES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer As String
    Public Property payer() As String
        Get
            Return _payer
        End Get
        Set(ByVal value As String)
            If Not _payer = value Then
                _payer = value
                If _payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value Then
                    _payer_obj = Nothing
                Else
                    If MConstants.listOfPayer.ContainsKey(_payer) Then
                        _payer_obj = MConstants.listOfPayer(_payer)
                    Else
                        _payer_obj = Nothing
                    End If
                End If
                If MConstants.GLOB_QUOTES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _payer_obj As CPayer
    Public Property payer_obj() As CPayer
        Get
            Return _payer_obj
        End Get
        Set(ByVal value As CPayer)
            If Object.Equals(value, _payer_obj) Then
                _payer_obj = value
                If IsNothing(_payer_obj) Then
                    _payer = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
                Else
                    _payer = _payer_obj.payer
                End If
                If MConstants.GLOB_QUOTES_LOADED Then
                    MConstants.GLB_MSTR_CTRL.refreshCalculation()
                End If
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    Private _system_status As String
    Public Property system_status() As String
        Get
            Return _system_status
        End Get
        Set(ByVal value As String)
            If Not _system_status = value Then
                _system_status = value
                'add item to be updated
                MConstants.GLB_MSTR_CTRL.csDB.add_item_to_be_updated(Me)
            End If
        End Set
    End Property

    'to string
    Public Overrides Function tostring() As String
        Return "Quote number: " & _number & ", desc: " & _description
    End Function

End Class