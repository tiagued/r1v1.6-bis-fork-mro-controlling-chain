﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CLaborControllingCostNodeViewDelegate
    Private form As FormLaborControlCostNode

    Private latestEditedCtrlObj As CLaborControlMainViewObj

    Private filtered_cc As List(Of CCostCenter)
    Private filtered_act As Dictionary(Of String, CLaborControlActFilterObject)
    Private all_act_select As CLaborControlActFilterObject

    'launch form
    Public Sub launchEditForm(labor_ctrl As CLaborControlMainViewObj)
        Try
            filtered_cc = New List(Of CCostCenter)
            filtered_act = New Dictionary(Of String, CLaborControlActFilterObject)
            all_act_select = CLaborControlActFilterObject.getAllSelected

            latestEditedCtrlObj = Nothing
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "preparing cost node view..."
            form = New FormLaborControlCostNode

            'draw all the datagridviews
            MConstants.GLB_MSTR_CTRL.labor_controlling_controller.bind_controlling_main_view(form.labor_ctrl_main_dgrid)

            'prepare list of act and list of CC
            form.activities_list.DataSource = Nothing
            form.activities_list.DisplayMember = "display"
            form.activities_list.SelectionMode = SelectionMode.MultiExtended
            form.activities_list.HorizontalScrollbar = True
            form.activities_list.SelectedItems.Clear()

            form.cost_center_list.DataSource = Nothing
            form.cost_center_list.DisplayMember = "cc_desc_view"
            form.cost_center_list.SelectionMode = SelectionMode.MultiExtended
            form.cost_center_list.HorizontalScrollbar = True
            form.cost_center_list.SelectedItems.Clear()

            'initialize combobox selection combobox
            Dim costNodeView As New BindingListView(Of CLaborControlMainViewObj)(MConstants.GLB_MSTR_CTRL.labor_controlling_controller.laborControlCostNodeList.DataSource)
            costNodeView.ApplySort("view_str")
            'set combobox list
            form.cb_cost_nodes.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            form.cb_cost_nodes.DropDownStyle = ComboBoxStyle.DropDownList
            form.cb_cost_nodes.DataSource = costNodeView
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_cost_nodes)


            'add error handlers
            Dim ctrlObjMap As CViewModelMapList
            ctrlObjMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LAB_CTRL_OBJ_VIEW)
            For Each map As CViewModelMap In ctrlObjMap.list.Values
                Dim _ctrl As Control = Nothing
                _ctrl = form.Controls.Find(map.view_control, True).FirstOrDefault
                If Not IsNothing(_ctrl) Then
                    CEditPanelViewRowValidationHandler.addNew(_ctrl, CLaborControlCostNodeObjRowValidator.getInstance, ctrlObjMap, labor_ctrl.GetType, form.act_cc_error_state_pic, form)
                End If
            Next

            'set selected object. Do the selection at this level to force the activity changed event to refresh the view, especially the filtering panels
            form.cb_cost_nodes.SelectedItem = labor_ctrl

            'fill data in views
            updateView()

            'latest updated
            latestEditedCtrlObj = labor_ctrl

            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End preparing cost node view"

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MsgBox("An error occured while preparing the cost node view" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'if index changed selected activity
    Public Sub selectedCtrObjChanged()
        Dim ctrl_obj As CLaborControlMainViewObj = CType(form.cb_cost_nodes.SelectedItem, ObjectView(Of CLaborControlMainViewObj)).Object
        'update only if object changed
        If Not Object.Equals(ctrl_obj, latestEditedCtrlObj) Then
            updateView()
        End If

    End Sub


    'Update page
    Public Sub updateView()
        Try
            'ctrl obj details
            Dim ctrl_obj As CLaborControlMainViewObj = CType(form.cb_cost_nodes.SelectedItem, ObjectView(Of CLaborControlMainViewObj)).Object

            If Not Object.Equals(latestEditedCtrlObj, ctrl_obj) Then
                MConstants.GLB_MSTR_CTRL.labor_controlling_controller.updateCtrlObjView(ctrl_obj, form)
                updateGridViews(ctrl_obj)
                updateFilteringLists(ctrl_obj)
                latestEditedCtrlObj = ctrl_obj
            End If
        Catch ex As Exception
            MsgBox("An error occured while loading cost node view" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub



    'select via arrows
    Public Sub selectedObjectForward()
        If Not form.cb_cost_nodes.SelectedIndex + 1 > form.cb_cost_nodes.Items.Count - 1 Then
            form.cb_cost_nodes.SelectedIndex = form.cb_cost_nodes.SelectedIndex + 1
        End If
    End Sub

    'navigate backward 
    Public Sub selectedObjectBackWard()
        If Not form.cb_cost_nodes.SelectedIndex - 1 < 0 Then
            form.cb_cost_nodes.SelectedIndex = form.cb_cost_nodes.SelectedIndex - 1
        End If
    End Sub
    'update activity and cc list that are used to filter datagridview
    Public Sub updateFilteringLists(ctrl_obj As CLaborControlMainViewObj)
        Try
            'fill list of act and list of CC
            form.activities_list.DataSource = getActivityList(ctrl_obj)

            form.cost_center_list.DataSource = getCostCenterList(ctrl_obj)

        Catch ex As Exception
            MsgBox("An error occured while loading cost node filter lists table on item " & ctrl_obj.tostring & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    Public Sub selectedActivitiesChanged()
        Try
            filtered_act.Clear()
            For Each actItem As CLaborControlActFilterObject In form.activities_list.SelectedItems
                filtered_act.Add(actItem.getKey, actItem)
            Next
            Dim ctrl_obj As CLaborControlMainViewObj = CType(form.cb_cost_nodes.SelectedItem, ObjectView(Of CLaborControlMainViewObj)).Object
            form.cost_center_list.DataSource = getCostCenterList(ctrl_obj, filtered_act)
            form.cost_center_list.SelectedItems.Add(MConstants.LABOR_CTRL_SELECT_ALL_CC)
        Catch ex As Exception
            MsgBox("An error occured while refreshing cc filtering listsb after act has been selected " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'only cc changed trigger datagridview refresh
    Public Sub selectedCostCentersChanged()
        Try
            Dim ctrl_obj As CLaborControlMainViewObj = CType(form.cb_cost_nodes.SelectedItem, ObjectView(Of CLaborControlMainViewObj)).Object
            filtered_cc = form.cost_center_list.SelectedItems.OfType(Of CCostCenter).ToList
            filterGridView()
        Catch ex As Exception
            MsgBox("An error occured while refreshing gridview after cc has been selected" & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'get activity list
    Public Function getActivityList(ctrlO As CLaborControlMainViewObj) As List(Of CLaborControlActFilterObject)
        Dim res As New Dictionary(Of String, CLaborControlActFilterObject)
        res.Add(all_act_select.getKey, all_act_select)
        'just to get the cost node at the top of the list
        Dim costNode As CLaborControlActFilterObject = CLaborControlActFilterObject.getNewObj(ctrlO)
        res.Add(costNode.getKey, costNode)

        For Each child As CLaborControlMainViewObj In ctrlO.get_structure_children
            If Not IsNothing(child.act_obj) AndAlso Not res.ContainsKey(CLaborControlActFilterObject.getKey(child)) Then
                Dim obj_act_filt As CLaborControlActFilterObject = CLaborControlActFilterObject.getNewObj(child)
                res.Add(obj_act_filt.getKey, obj_act_filt)
            End If
        Next

        Dim filterView As New BindingListView(Of CLaborControlActFilterObject)(res.Values.ToList)
        filterView.ApplySort("display")
        Return getLabCtrlActFilterObjFromListView(filterView)
    End Function

    'get activity list
    Public Function getCostCenterList(ctrlO As CLaborControlMainViewObj, Optional selectedActList As Dictionary(Of String, CLaborControlActFilterObject) = Nothing) As List(Of CCostCenter)
        Dim res As New List(Of CCostCenter)
        res.Add(MConstants.LABOR_CTRL_SELECT_ALL_CC)
        For Each child As CLaborControlMainViewObj In ctrlO.get_structure_children
            'any restriction at act level ?
            If Not IsNothing(child.act_obj) AndAlso Not (IsNothing(selectedActList) OrElse selectedActList.Count = 0) Then
                'if all act is selected ignore restriction
                If Not selectedActList.ContainsKey(all_act_select.getKey) Then
                    If Not selectedActList.ContainsKey(CLaborControlActFilterObject.getKey(child)) Then
                        Continue For
                    End If
                End If
            End If

            If Not IsNothing(child.cc_object) AndAlso Not res.Contains(child.cc_object) Then
                res.Add(child.cc_object)
            End If
        Next
        Dim filterView As New BindingListView(Of CCostCenter)(res)
        filterView.ApplySort("cc_desc_view")
        Return getCCFromListView(filterView)
    End Function


    'update gridview when cost node selection changed
    Public Sub updateGridViews(ctrl_obj As CLaborControlMainViewObj)
        Try
            'fill views
            Dim costNodeChildren As New BindingListView(Of CLaborControlMainViewObj)(ctrl_obj.get_structure_children)
            form.labor_ctrl_main_dgrid.DataSource = costNodeChildren
        Catch ex As Exception
            MsgBox("An error occured while loading cost node children table view on item " & ctrl_obj.tostring & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'filger gird view according to selected activities and cc
    Public Sub filterGridView()
        Try
            'fill views
            Dim costNodeChildren As BindingListView(Of CLaborControlMainViewObj) = CType(form.labor_ctrl_main_dgrid.DataSource, BindingListView(Of CLaborControlMainViewObj))
            costNodeChildren.ApplyFilter(AddressOf filterGridViewDelegate)
        Catch ex As Exception
            MsgBox("An error occured while refreshing Dgrid view " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'filter delegate
    Public Function filterGridViewDelegate(ctrl_child As CLaborControlMainViewObj) As Boolean
        If (filtered_act.Count = 0 OrElse filtered_act.ContainsKey(all_act_select.getKey)) AndAlso (filtered_cc.Count = 0 OrElse filtered_cc.Contains(MConstants.LABOR_CTRL_SELECT_ALL_CC)) Then
            Return True
        ElseIf filtered_act.Count = 0 OrElse filtered_act.ContainsKey(all_act_select.getKey) Then
            Return filtered_cc.Contains(ctrl_child.cc_object)
        ElseIf filtered_cc.Count = 0 OrElse filtered_cc.Contains(MConstants.LABOR_CTRL_SELECT_ALL_CC) Then
            Return filtered_act.ContainsKey(CLaborControlActFilterObject.getKey(ctrl_child))
        Else
            Return filtered_cc.Contains(ctrl_child.cc_object) AndAlso filtered_act.ContainsKey(CLaborControlActFilterObject.getKey(ctrl_child))
        End If
    End Function
End Class


