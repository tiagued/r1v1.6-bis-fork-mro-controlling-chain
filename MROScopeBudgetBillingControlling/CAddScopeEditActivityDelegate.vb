﻿Imports System.ComponentModel
Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CAddScopeEditActivityDelegate
    Private form As FormAddScopeActivityEdit
    Private latestEditedActicity As CScopeActivity

    'launch form
    Public Sub launchEditForm(act As CScopeActivity)
        Try
            latestEditedActicity = Nothing
            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "preparing view to edit activity labor and material..."
            form = New FormAddScopeActivityEdit

            'draw hours grid views. Draw before selecting activity=> cause activity select will trigger whole view display
            drawInitHoursgrid()
            drawAddHoursgrid()
            drawMaterialgrid()
            drawInitMaterialgrid
            drawActivityCalcgrid()

            'initialize activity selection combobox
            Dim actListHandler As New BindingListView(Of CScopeActivity)(MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource)
            actListHandler.ApplySort("act_notif_opcode ASC")
            Dim bds As New BindingSource
            bds.DataSource = MGeneralFuntionsViewControl.getActFromListView(actListHandler)

            'set combobox list
            form.cb_activity.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            form.cb_activity.DropDownStyle = ComboBoxStyle.DropDownList
            form.cb_activity.DataSource = bds
            MViewEventHandler.addDefaultComboBoxEventHandler(form.cb_activity)
            'set selected object
            form.cb_activity.SelectedItem = act

            'event handler to refresh lists of calculation view filter
            AddHandler form.act_calc_scope_list_val.Click, AddressOf updateCalcViewFiltersCB
            AddHandler form.act_calc_payer_list_val.Click, AddressOf updateCalcViewFiltersCB

            'activity data validator
            'add error handlers
            Dim actEditMap As CViewModelMapList
            actEditMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_EDIT_VIEW_DISPLAY)
            For Each map As CViewModelMap In actEditMap.list.Values
                Dim _ctrl As Control = Nothing
                _ctrl = form.Controls.Find(map.view_control, True).FirstOrDefault
                If Not IsNothing(_ctrl) Then
                    CEditPanelViewRowValidationHandler.addNew(_ctrl, CScopeActivityRowValidator.getInstance, actEditMap, act.GetType, form.activity_error_state_pic, form)
                End If
            Next

            'fill data in views
            updateView()

            MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End preparing view to edit activity labor and material"

            form.ShowDialog()
            form.BringToFront()
        Catch ex As Exception
            MsgBox("An error occured while preparing the windows to edit labor and material " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'if index changed selected activity
    Public Sub selectedActivityChanged()
        Dim act As CScopeActivity = form.cb_activity.SelectedItem
        'use this to update view only when selected activity change. cause due to object binding, when any field of actiivity is updated, all the controls are updated even the selected activity combobox
        If latestEditedActicity IsNot Nothing AndAlso Not latestEditedActicity.Equals(act) Then
            updateView()
        End If

    End Sub
    'launch AWQ form
    Public Sub launchAWQForm()
        Dim act As CScopeActivity = form.cb_activity.SelectedItem
        Dim awq As CAWQuotation
        Dim dialogRes As DialogResult
        If act.is_fake_activity AndAlso Not CEditPanelViewRowValidationHandler.validateControl(form.act_proj_val) Then
            MsgBox("Cannot launch AWQ if current errors are not fixed !", MsgBoxStyle.Critical)
            Exit Sub
        End If

        If IsNothing(act.awq_master_obj) Then
            'not on Cost Collector
            If act.is_cost_collector Then
                MsgBox("The current activity is a Cost Collector and is not yet binded to an AWQ. Use a ZIMRO activity to create the AWQ", MsgBoxStyle.Critical)
                Exit Sub
            End If

            dialogRes = MessageBox.Show("The current activity is not yet binded to an AWQ. Do you want to create a new AWQ for this activity ? " & Chr(10) & "Click OK to create, or Cancel to leave.", "Confirm new AWQ creation", MessageBoxButtons.OKCancel)
            If dialogRes = DialogResult.Cancel Then
                Exit Sub
            End If
            'do not yet store awq data on act object, this will be done when awq will be validated
            awq = New CAWQuotation
            awq.is_latest_rev_calc = True
            awq.main_activity_obj = act
            awq.description = act.act_desc
            awq.dependantActList.Add(act)
        Else
            awq = act.awq_master_obj
            If Not IsNothing(act.awq_master_obj.latest_REV) Then
                awq = act.awq_master_obj.latest_REV
            End If
            'in case of fake activity creation set desc for first time
            If act.is_fake_activity AndAlso act.awq_master_obj.revision_list.Count = 0 Then
                act.awq_master_obj.description = act.act_desc
            End If
        End If
        MConstants.GLB_MSTR_CTRL.awq_edit_ctrl.launchEditForm(awq)
        updateView()
    End Sub

    'Update page
    Public Sub updateView()
        Try
            'activity details
            Dim act As CScopeActivity = form.cb_activity.SelectedItem

            'bind activity data
            Dim actEditMap As CViewModelMapList

            'Bind current activity to the view
            actEditMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACT_EDIT_VIEW_DISPLAY)
            updateActivityEditView(act, actEditMap, form)
            'map fake activity button
            If act.is_fake_activity Then
                form.map_sap_act_bt.Visible = True
                form.map_sap_act_bt.Enabled = True
            Else
                form.map_sap_act_bt.Enabled = False
                form.map_sap_act_bt.Visible = False
            End If

            updateHoursAndMaterialsViews()
            updateActivityCalucalionView()
        Catch ex As Exception
            MsgBox("An error occured while loading data of activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'update activity edit view
    Public Sub updateActivityEditView(act As CScopeActivity, actEditMap As CViewModelMapList, edit_form As Form)
        Try
            'bind activity data
            Dim bds As BindingSource

            'Bind current activity to the view
            Dim tbbd As Binding
            For Each map As CViewModelMap In actEditMap.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim textBox As TextBox
                    textBox = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, TextBox)
                    textBox.DataBindings.Clear()
                    tbbd = textBox.DataBindings.Add("Text", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    'format double
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        tbbd.FormatInfo = MConstants.CULT_INFO
                        tbbd.FormatString = map.col_format
                        tbbd.FormattingEnabled = True
                    End If
                    textBox.Enabled = map.col_editable
                    'description
                    If act.is_fake_activity AndAlso (map.class_field = "act_desc" OrElse map.class_field = "act_long_desc") Then
                        textBox.Enabled = True
                    End If
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                        Dim comboBox As ComboBox
                        comboBox = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, ComboBox)
                        comboBox.DataBindings.Clear()
                        comboBox.AutoCompleteMode = AutoCompleteMode.None
                        comboBox.DropDownStyle = ComboBoxStyle.DropDownList
                        comboBox.Enabled = map.col_editable
                        MViewEventHandler.addDefaultComboBoxEventHandler(comboBox)

                        bds = New BindingSource
                        If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                            bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                            comboBox.DataSource = bds
                            comboBox.DisplayMember = "value"
                            comboBox.ValueMember = "key"
                            comboBox.DataBindings.Add("SelectedValue", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                            bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                            comboBox.DataSource = bds
                            comboBox.DisplayMember = "value"
                            comboBox.ValueMember = "key"
                            comboBox.DataBindings.Add("SelectedValue", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        ElseIf map.view_control = MConstants.CONST_EDIT_ACT_DISC_CB Then
                            comboBox.DataSource = GLB_MSTR_CTRL.billing_cond_controller._discountList_lov_act
                            comboBox.DisplayMember = "label"
                            comboBox.DataBindings.Add("SelectedItem", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        ElseIf map.view_control = MConstants.CONST_EDIT_ACT_NTE_CB Then
                            comboBox.DataSource = GLB_MSTR_CTRL.billing_cond_controller._nteList_lov_act
                            comboBox.DisplayMember = "label"
                            comboBox.DataBindings.Add("SelectedItem", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                        ElseIf map.view_control = MConstants.CONST_EDIT_ACT_PROJ_CB Then
                            bds.DataSource = GLB_MSTR_CTRL.project_controller.project_cb_list
                            comboBox.DataSource = bds
                            comboBox.DisplayMember = "Value"
                            comboBox.ValueMember = "Key"
                            comboBox.DataBindings.Add("SelectedValue", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                            If act.is_fake_activity AndAlso (IsNothing(act.awq_master_obj) OrElse act.awq_master_obj.is_in_work) Then
                                comboBox.Enabled = True
                            Else
                                comboBox.Enabled = False
                            End If
                        Else
                            Throw New Exception("Error occured when binding activity view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                        End If
                    ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                        Dim chk As CheckBox
                    chk = CType(edit_form.Controls.Find(map.view_control, True).FirstOrDefault, CheckBox)
                    chk.DataBindings.Clear()
                    chk.DataBindings.Add("Checked", act, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    chk.Enabled = map.col_editable
                End If
            Next

        Catch ex As Exception
            MsgBox("An error occured while loading data of activity " & act.tostring & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'select new activity via arrows
    Public Sub selectedActivityForward()
        Try
            'if dirty then it is not possible to assign a new datasource
            form.act_add_lab_dgrid.DataSource = Nothing
        Catch ex As Exception
            Exit Sub
        End Try
        If Not form.cb_activity.SelectedIndex + 1 > MConstants.GLB_MSTR_CTRL.add_scope_controller.scopeList.DataSource.Count - 1 Then
            form.cb_activity.SelectedIndex = form.cb_activity.SelectedIndex + 1
        End If
    End Sub
    'navigate backward actv entries
    Public Sub selectedActivityBackWard()
        Try
            'if dirty then it is not possible to assign a new datasource
            form.act_add_lab_dgrid.DataSource = Nothing
            form.act_add_materials_dgrid.DataSource = Nothing
        Catch ex As Exception
            Exit Sub
        End Try
        If Not form.cb_activity.SelectedIndex - 1 < 0 Then
            form.cb_activity.SelectedIndex = form.cb_activity.SelectedIndex - 1
        End If
    End Sub


    'launch form to map fake activity with sap activty
    Public Sub launchFakeActMap()
        Try
            Dim act As CScopeActivity = form.cb_activity.SelectedItem
            If Not act.is_fake_activity Then
                Exit Sub
            End If
            Dim mapFakeActDelegate As New CAWQFakeActivityTransferDelegate
            mapFakeActDelegate.launchEditForm(act)
        Catch ex As Exception
            MsgBox("An error occured while loading form to map temporary activity" & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    Public Sub updateActivityCalucalionView()
        Try
            'reset views
            Dim act As CScopeActivity = form.cb_activity.SelectedItem
            Dim bds As BindingSource
            'fill views
            act.calculator.calc_entries_view.Refresh()
            form.act_calc_dgrid.DataSource = act.calculator.calc_entries_view
            act.calculator.calc_entries_view.ApplySort("entry_scope ASC, calc_type ASC ")
            form.act_calc_dgrid.Refresh()

            'bind filters: scope and payer
            form.act_calc_payer_list_val.DataBindings.Clear()
            bds = New BindingSource(act.calculator.calc_payer_list, Nothing)
            bds.DataSource = act.calculator.calc_payer_list
            form.act_calc_payer_list_val.AutoCompleteMode = AutoCompleteMode.None
            form.act_calc_payer_list_val.DropDownStyle = ComboBoxStyle.DropDownList
            form.act_calc_payer_list_val.DataSource = Nothing
            form.act_calc_payer_list_val.DataSource = bds
            form.act_calc_payer_list_val.DisplayMember = "label"
            form.act_calc_payer_list_val.DataBindings.Add("SelectedItem", act.calculator, "view_current_payer", False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(form.act_calc_payer_list_val)

            form.act_calc_scope_list_val.DataBindings.Clear()
            bds = New BindingSource(act.calculator.calc_payer_list, Nothing)
            bds.DataSource = act.calculator.calc_scope_list
            form.act_calc_scope_list_val.DataSource = bds
            form.act_calc_scope_list_val.AutoCompleteMode = AutoCompleteMode.None
            form.act_calc_scope_list_val.DropDownStyle = ComboBoxStyle.DropDownList
            form.act_calc_scope_list_val.DisplayMember = "Value"
            form.act_calc_scope_list_val.ValueMember = "Key"
            form.act_calc_scope_list_val.DataBindings.Add("SelectedItem", act.calculator, "view_current_scope", False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(form.act_calc_scope_list_val)

            'disc after disc bef disc
            bds = New BindingSource
            bds.DataSource = act.calculator.calc_entry_type_list
            form.act_calc_calc_type_list_val.DataSource = bds
            form.act_calc_calc_type_list_val.AutoCompleteMode = AutoCompleteMode.None
            form.act_calc_calc_type_list_val.DropDownStyle = ComboBoxStyle.DropDownList
            form.act_calc_calc_type_list_val.DisplayMember = "Value"
            form.act_calc_calc_type_list_val.ValueMember = "Key"
            form.act_calc_calc_type_list_val.DataBindings.Clear()
            form.act_calc_calc_type_list_val.DataBindings.Add("SelectedValue", act.calculator, "view_current_entry_type", False, DataSourceUpdateMode.OnPropertyChanged)
            MViewEventHandler.addDefaultComboBoxEventHandler(form.act_calc_calc_type_list_val)

        Catch ex As Exception
            MsgBox("An error occured while loading calculation data of the selected activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'refresh cb when list changed
    Public Sub updateCalcViewFiltersCB(ByVal sender As Object, ByVal e As EventArgs)
        Dim bds As BindingSource
        form.act_calc_scope_list_val.Refresh()
        bds = CType(form.act_calc_scope_list_val.DataSource, BindingSource)
        bds.ResetBindings(False)
        form.act_calc_payer_list_val.Refresh()
        bds = CType(form.act_calc_payer_list_val.DataSource, BindingSource)
        bds.ResetBindings(False)
    End Sub


    'update calculation view
    Public Sub updateHoursAndMaterialsViews()
        Try
            'reset views

            Dim act As CScopeActivity = form.cb_activity.SelectedItem
            'update previous act
            latestEditedActicity = act

            'fill views
            Dim addView As New BindingListView(Of CSoldHours)(act.sold_hour_list)
            'add new row handler
            RemoveHandler addView.AddingNew, AddressOf add_sold_hours_view_AddingNew
            AddHandler addView.AddingNew, AddressOf add_sold_hours_view_AddingNew
            addView.ApplyFilter(AddressOf filterAddScope)
            form.act_add_lab_dgrid.DataSource = addView
            form.act_add_lab_dgrid.Refresh()
            'error list if focus
            CGridViewRowValidationHandler.initializeGridErrorState(form.act_add_lab_dgrid)

            Dim initView As New BindingListView(Of CSoldHours)(act.sold_hour_list)
            initView.ApplyFilter(AddressOf filterInitScope)
            form.act_init_lab_dgrid.DataSource = initView

            Dim matView As New BindingListView(Of CMaterial)(act.material_list)
            'add new row handler=> Activity to be set on new rows, material to be added in material list
            RemoveHandler matView.AddingNew, AddressOf material_view_AddingNew
            AddHandler matView.AddingNew, AddressOf material_view_AddingNew
            matView.ApplyFilter(AddressOf filterAddMaterials)
            form.act_add_materials_dgrid.DataSource = matView
            form.act_add_materials_dgrid.Refresh()
            'error list if focus
            CGridViewRowValidationHandler.initializeGridErrorState(form.act_add_materials_dgrid)

            Dim matInitView As New BindingListView(Of CMaterial)(act.material_list)
            matInitView.ApplyFilter(AddressOf filterInitMaterials)
            form.act_init_materials_dgrid.DataSource = matInitView
            form.act_init_materials_dgrid.Refresh()
            'error list if focus
            CGridViewRowValidationHandler.initializeGridErrorState(form.act_init_materials_dgrid)

        Catch ex As Exception
            MsgBox("An error occured while loading working hours and materials of the selected activity " & Chr(10) & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'filter add materials
    Public Function filterAddMaterials(ByVal mat As CMaterial)
        'delegate for filtering
        If mat.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf IsNothing(mat.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function

    Public Function filterInitMaterials(ByVal mat As CMaterial)
        'delegate for filtering
        If mat.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf Not IsNothing(mat.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function

    'when added new row to hours view, set the activity object
    Private Sub add_sold_hours_view_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)
        Dim soldH As New CSoldHours
        Dim bds As BindingListView(Of CSoldHours) = sender
        soldH.is_reported = True
        soldH.act_object = form.cb_activity.SelectedItem
        e.NewObject = soldH
    End Sub

    'when added new row to material view, set the activity object
    Private Sub material_view_AddingNew(ByVal sender As Object, ByVal e As AddingNewEventArgs)
        Dim mat As New CMaterial
        Dim bds As BindingListView(Of CMaterial) = sender
        mat.is_reported = True
        mat.po_created = DateTime.Now
        mat.activity_obj = form.cb_activity.SelectedItem
        e.NewObject = mat
    End Sub

    'filter init scope
    Private Function filterInitScope(sold_hr As CSoldHours) As Boolean
        'delegate for filtering
        If sold_hr.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf Not IsNothing(sold_hr.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function
    'filter add scope
    Private Function filterAddScope(sold_hr As CSoldHours) As Boolean
        If sold_hr.system_status = MConstants.CONST_SYS_ROW_DELETED Then
            Return False
        ElseIf IsNothing(sold_hr.init_quote_entry_obj) Then
            Return True
        Else
            Return False
        End If
    End Function

    Private Sub drawAddHoursgrid()
        Dim gridMap As CViewModelMapList

        'get  map view
        gridMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ADD_HOURS_VIEW_DISPLAY)

        ' datagridview
        form.act_add_lab_dgrid.AllowUserToAddRows = True
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_add_lab_dgrid)

        drawGenericAddHoursgrid(form.act_add_lab_dgrid, gridMap)
        form.act_add_lab_dgrid.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged

        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_add_lab_dgrid)
        'add row validator
        Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(form.act_add_lab_dgrid, CSoldHoursAddRowValidator.getInstance, gridMap)
        dhler.validateAddDelegate = AddressOf add_hours_view_ValidateAdd
        dhler.cancelAddDelegate = AddressOf add_hours_view_CancelAdd
    End Sub

    'draw generic gridview for add labor
    Public Sub drawGenericAddHoursgrid(dgrid As DataGridView, gridMap As CViewModelMapList)
        Dim bds As BindingSource
        For Each map As CViewModelMap In gridMap.list.Values
            If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                Dim dgcol As New DataGridViewTextBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = gridMap.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "Double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgcol.ReadOnly = Not map.col_editable
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                Dim dgcol As New DataGridViewComboBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                If Not String.IsNullOrWhiteSpace(map.col_format) Then
                    dgcol.DefaultCellStyle.Format = map.col_format
                    Dim propInf As PropertyInfo
                    propInf = gridMap.object_class.GetProperty(map.class_field)
                    'format as numeric only for double
                    If propInf.PropertyType.Name = "Double" Then
                        dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                    End If
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                'values
                bds = New BindingSource
                If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                    bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                    bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "value"
                    dgcol.ValueMember = "key"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_CC.ToLower Then
                    bds.DataSource = MConstants.listOfCC.Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "cc"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_PAYER.ToLower Then
                    bds.DataSource = MConstants.listOfPayer.Values
                    dgcol.DataSource = bds
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "payer"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_NTE.ToLower Then
                    dgcol.DataSource = MConstants.GLB_MSTR_CTRL.billing_cond_controller._nteList_lov_lab_cc
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "id"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_DISC.ToLower Then
                    dgcol.DataSource = MConstants.GLB_MSTR_CTRL.billing_cond_controller._discountList_lov_lab_cc
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "id"
                ElseIf map.view_col_sys_name.ToLower = MConstants.CONST_CONF_SOLD_HRS_TASK_RATE.ToLower Then
                    dgcol.DataSource = MConstants.GLB_MSTR_CTRL.lab_rate_controller._task_rate_lov
                    dgcol.DisplayMember = "label"
                    dgcol.ValueMember = "id"
                Else
                    Throw New Exception("Error occured when binding activity view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                End If
                dgcol.ReadOnly = Not map.col_editable
                dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                dgrid.Columns.Add(dgcol)
            ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                Dim dgcol As New DataGridViewCheckBoxColumn
                'column name is the property name of binded object
                If map.col_width <> 0 Then
                    dgcol.Width = map.col_width
                End If
                dgcol.Name = map.view_col_sys_name
                dgcol.HeaderText = map.view_col
                dgcol.DataPropertyName = map.class_field
                dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                dgcol.ReadOnly = Not map.col_editable
                dgrid.Columns.Add(dgcol)
            End If
        Next
    End Sub
    'validate new
    Public Sub add_hours_view_ValidateAdd(soldHrs As ObjectView(Of CSoldHours), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(soldHrs.Object) Then
                Dim soldHrsListView As BindingListView(Of CSoldHours) = CType(form.act_add_lab_dgrid.DataSource, BindingListView(Of CSoldHours))
                'if new item
                soldHrs.Object.setID()
                If Not soldHrsListView.DataSource.Contains(soldHrs.Object) Then
                    soldHrsListView.EndNew(rowIndex)
                    soldHrsListView.Refresh()
                End If
                'update activity
                soldHrs.Object.act_object.updateCal()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new Add scope hour entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function add_hours_view_CancelAdd(soldHours As ObjectView(Of CSoldHours), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If soldHours.Object.id > 0 Then
                res = True
            Else
                Dim soldHrsListView As BindingListView(Of CSoldHours) = CType(form.act_add_lab_dgrid.DataSource, BindingListView(Of CSoldHours))
                If soldHrsListView.DataSource.Contains(soldHours.Object) Then
                    res = True
                Else
                    soldHours.CancelEdit()
                    soldHrsListView.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new Add scope hour entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function

    Private Sub drawMaterialgrid()
        'add mat datagrid
        form.act_add_materials_dgrid.AllowUserToAddRows = True
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_add_materials_dgrid)

        MConstants.GLB_MSTR_CTRL.material_controller.bind_mat_generic_view(form.act_add_materials_dgrid)
        'set activity view as read only
        Dim act_col As DataGridViewColumn = form.act_add_materials_dgrid.Columns(MConstants.CONST_CONF_MAT_MAIN_ACT)
        If Not IsNothing(act_col) Then
            act_col.ReadOnly = True
        End If

        'refresh activity dependant list to avoid that new activity not yet in list trigger an error
        MConstants.GLB_MSTR_CTRL.add_scope_controller.refreshScopeDependantViews()
        form.act_add_materials_dgrid.DataBindings.DefaultDataSourceUpdateMode = DataSourceUpdateMode.OnPropertyChanged
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_add_materials_dgrid)
        'row validator
        Dim dhler As CGridViewRowValidationHandler = CGridViewRowValidationHandler.addNew(form.act_add_materials_dgrid, CMaterialAddRowValidator.getInstance, MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_MAT_MAIN_VIEW_DISPLAY))
        dhler.validateAddDelegate = AddressOf material_view_ValidateAdd
        dhler.cancelAddDelegate = AddressOf material_view_CancelAdd
    End Sub

    'init
    Private Sub drawInitMaterialgrid()
        'ini mat datagrid
        form.act_init_materials_dgrid.ReadOnly = True
        form.act_init_materials_dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_init_materials_dgrid)
        MConstants.GLB_MSTR_CTRL.material_controller.bind_mat_generic_view(form.act_init_materials_dgrid)
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_init_materials_dgrid)
    End Sub

    'validate new
    Public Sub material_view_ValidateAdd(material As ObjectView(Of CMaterial), rowIndex As Integer)
        Try
            If Not MConstants.GLB_MSTR_CTRL.csDB.hasBeenValidated(material.Object) Then
                Dim matListView As BindingListView(Of CMaterial) = CType(form.act_add_materials_dgrid.DataSource, BindingListView(Of CMaterial))
                'if new item
                material.Object.setID()
                If Not matListView.DataSource.Contains(material.Object) Then
                    matListView.EndNew(rowIndex)
                End If

                'add to whole material list
                If Not MConstants.GLB_MSTR_CTRL.material_controller.matList.DataSource.Contains(material.Object) Then
                    MConstants.GLB_MSTR_CTRL.material_controller.matList.DataSource.Add(material.Object)
                End If
                'update activity
                material.Object.activity_obj.updateCal()
            End If
        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new material entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
    'cancel new
    Public Function material_view_CancelAdd(material As ObjectView(Of CMaterial), rowIndex As Integer) As Boolean
        'return true to block leaving and return false to allow leaving
        Dim res As Boolean
        Try
            'check if new
            If material.Object.id > 0 Then
                res = True
            Else
                Dim matListView As BindingListView(Of CMaterial) = CType(form.act_add_materials_dgrid.DataSource, BindingListView(Of CMaterial))
                If matListView.DataSource.Contains(material.Object) Then
                    res = True
                Else
                    material.CancelEdit()
                    matListView.CancelNew(rowIndex)
                    res = False
                End If
            End If

        Catch ex As Exception
            MGeneralFuntionsViewControl.displayMessage("Error!", "Error while validating new material entry " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
        Return res
    End Function

    Private Sub drawInitHoursgrid()
        Dim gridMap As CViewModelMapList
        'get  map view
        gridMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_INIT_HOURS_VIEW_DISPLAY)

        ' datagridview
        form.act_init_lab_dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_init_lab_dgrid)

        MConstants.GLB_MSTR_CTRL.init_scope_hours_edit_ctrl.drawGenericInitLaborGrid(form.act_init_lab_dgrid, gridMap)
        form.act_init_lab_dgrid.ReadOnly = True

        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_init_lab_dgrid)
    End Sub

    Private Sub drawActivityCalcgrid()
        Dim gridMap As CViewModelMapList
        'get  map view
        gridMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ACTT_CALC_VIEW_DISPLAY)
        ' datagridview
        form.act_calc_dgrid.AllowUserToAddRows = False
        MViewEventHandler.addDefaultDatagridEventHandler(form.act_calc_dgrid)

        'all as read only text
        For Each map As CViewModelMap In gridMap.list.Values
            Dim dgcol As New DataGridViewTextBoxColumn
            'column name is the property name of binded object
            If map.col_width <> 0 Then
                dgcol.Width = map.col_width
            End If
            If Not String.IsNullOrWhiteSpace(map.col_format) Then
                dgcol.DefaultCellStyle.Format = map.col_format
                Dim propInf As PropertyInfo
                propInf = gridMap.object_class.GetProperty(map.class_field)
                'format as numeric only for double
                If propInf.PropertyType.Name = "Double" Then
                    dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                End If
            End If
            dgcol.Name = map.view_col_sys_name
            dgcol.HeaderText = map.view_col
            dgcol.DataPropertyName = map.class_field
            dgcol.SortMode = DataGridViewColumnSortMode.Automatic
            'read only
            dgcol.ReadOnly = True
            form.act_calc_dgrid.Columns.Add(dgcol)
        Next
        MGeneralFuntionsViewControl.resizeDataGridWidth(form.act_calc_dgrid)
    End Sub
End Class
