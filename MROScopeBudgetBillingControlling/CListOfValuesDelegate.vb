﻿Imports System.Reflection
Imports Equin.ApplicationFramework

Public Class CListOfValuesDelegate

    Public lovEditView As BindingListView(Of CConfProperty)

    Public Sub bind_lov_views()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "bind LOV Conf views..."
        Try
            bind_editable_lov_view()
        Catch ex As Exception
            Throw New Exception("Error occured when binding LOV Conf views" & Chr(10) & ex.Message, ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End LOV Conf views"
    End Sub

    Private _selected_lov As String
    Public Property selected_lov() As String
        Get
            Return _selected_lov
        End Get
        Set(ByVal value As String)
            If Not _selected_lov = value Then
                _selected_lov = value
                lovEditView.Refresh()
            End If
        End Set
    End Property
    'view to edit lov items
    Private Sub bind_editable_lov_view()
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "Bind editable LOV to view controls..."
        Try
            Dim mapLOVEdit As CViewModelMapList

            'get lov map view
            mapLOVEdit = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_LOV_EDIT_VIEW_DISPLAY)

            'lov datagridview
            MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.AllowUserToAddRows = False

            MViewEventHandler.addDefaultDatagridEventHandler(MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid)

            For Each map As CViewModelMap In mapLOVEdit.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim dgcol As New DataGridViewTextBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLOVEdit.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "Double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim dgcol As New DataGridViewComboBoxColumn

                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        dgcol.DefaultCellStyle.Format = map.col_format
                        Dim propInf As PropertyInfo
                        propInf = mapLOVEdit.object_class.GetProperty(map.class_field)
                        'format as numeric only for double
                        If propInf.PropertyType.Name = "double" Then
                            dgcol.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
                        End If
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    'values
                    Dim bds As New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        dgcol.DataSource = bds
                        dgcol.DisplayMember = "value"
                        dgcol.ValueMember = "key"
                    Else
                        Throw New Exception("Error occured when binding activity view in additional scope tab. List " & map.view_list_name & " does not exist in LOV")
                    End If
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.Columns.Add(dgcol)
                    dgcol.DisplayStyle = DataGridViewComboBoxDisplayStyle.Nothing
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim dgcol As New DataGridViewCheckBoxColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = map.view_col
                    dgcol.DataPropertyName = map.class_field
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    dgcol.ReadOnly = Not map.col_editable
                    MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_BUTTON).value Then
                    Dim dgcol As New DataGridViewButtonColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Text = map.view_col
                    dgcol.UseColumnTextForButtonValue = True
                    dgcol.SortMode = DataGridViewColumnSortMode.Automatic
                    MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.Columns.Add(dgcol)
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_IMAGE).value Then
                    Dim dgcol As New DataGridViewImageColumn
                    'column name is the property name of binded object
                    If map.col_width <> 0 Then
                        dgcol.Width = map.col_width
                    End If
                    dgcol.Name = map.view_col_sys_name
                    dgcol.HeaderText = ""
                    dgcol.Image = My.Resources.ResourceManager.GetObject(map.view_col)
                    MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.Columns.Add(dgcol)
                End If
            Next

            Dim allLovValues As New List(Of CConfProperty)
            For Each list_dic As Dictionary(Of String, CConfProperty) In MConstants.listOfValueConf_edit.Values
                For Each entry As CConfProperty In list_dic.Values
                    allLovValues.Add(entry)
                Next
            Next
            lovEditView = New BindingListView(Of CConfProperty)(allLovValues)

            'bind datasource
            MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid.DataSource = lovEditView
            MGeneralFuntionsViewControl.resizeDataGridWidth(MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_datagrid)

            'lov list
            Dim lovList = New Dictionary(Of String, String)
            lovList.Add(MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value, " ")
            For Each key As String In MConstants.listOfValueConf_edit.Keys
                lovList.Add(key, key)
            Next

            'bind combobox
            Dim cb As ComboBox = MConstants.GLB_MSTR_CTRL.mainForm.conf_lov_edit_lov_selected_val
            Dim cb_bds As New BindingSource
            cb_bds.DataSource = lovList
            cb.DataSource = cb_bds
            cb.DisplayMember = "Value"
            cb.ValueMember = "Key"
            cb.DataBindings.Add("SelectedValue", Me, "selected_lov", False, DataSourceUpdateMode.OnPropertyChanged)
            cb.AutoCompleteMode = AutoCompleteMode.SuggestAppend
            cb.DropDownStyle = ComboBoxStyle.DropDownList
            MViewEventHandler.addDefaultComboBoxEventHandler(cb)

            'list fiter empty at the beginning
            _selected_lov = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value

            'filter
            lovEditView.ApplyFilter(AddressOf filterLOVEditViewDelegate)

        Catch ex As Exception
            Throw New Exception("An error occured while loading editable LOV view", ex)
        End Try
        MConstants.GLB_MSTR_CTRL.statusBar.status_strip_sub_label = "End Bind editable LOV to view controls"
    End Sub

    Private Function filterLOVEditViewDelegate(ByVal conf As CConfProperty)
        'delegate for filtering
        Return conf.list = _selected_lov OrElse _selected_lov = MConstants.constantConf(MConstants.CONST_CONF_LOV_EMPTY_KEY).value
    End Function

End Class
