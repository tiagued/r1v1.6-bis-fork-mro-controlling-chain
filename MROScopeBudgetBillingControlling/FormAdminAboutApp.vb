﻿
Public Class FormAdminAboutApp
    Public ctrl As CApplicationInformationViewDelegate
    Private Sub reset_session_bt_Click(sender As Object, e As EventArgs) Handles reset_session_bt.Click
        ctrl.resetSession()
    End Sub

    Private Sub version_info_bt_Click(sender As Object, e As EventArgs) Handles version_info_bt.Click
        ctrl.getVersionInfo()
    End Sub
End Class