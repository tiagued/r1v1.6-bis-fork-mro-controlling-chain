﻿Imports System.IO
Imports System.Reflection
Imports Equin.ApplicationFramework

Module MGeneralFuntionsViewControl

    'display a date picker control
    Public Sub setDatePickerValue(datePicker As DateTimePicker, value As String, Valueformat As String, format As String)
        Try
            If String.IsNullOrEmpty(Trim(value)) Then
                datePicker.Format = DateTimePickerFormat.Custom
                datePicker.CustomFormat = " "
                datePicker.Value = Date.Today
                datePicker.Enabled = True
                datePicker.Checked = False
            Else
                datePicker.Format = DateTimePickerFormat.Custom
                datePicker.CustomFormat = format
                datePicker.Value = DateTime.ParseExact(value, Valueformat, Globalization.CultureInfo.InvariantCulture)
                datePicker.Enabled = True
                datePicker.Checked = True
            End If
        Catch ex As Exception
            datePicker.Format = DateTimePickerFormat.Custom
            datePicker.CustomFormat = " "
            datePicker.Value = Date.Today
            datePicker.Enabled = True
            datePicker.Checked = False
        End Try
    End Sub

    'display set link label control
    Public Sub setFilePathtext(value As String, link As LinkLabel)
        Try
            If Not IsNothing(link.Links) Then
                link.Links.Clear()
            End If

            If File.Exists(value) Then
                Dim latest As Date
                latest = File.GetLastWriteTime(value)
                link.Text = "file updated:" & latest.ToString("dd-MMM-yyyy")
                link.Links.Add(0, link.Text.Length, value)
                link.LinkColor = Color.Blue
                link.Enabled = True
            Else
                link.Text = "no file found"
                link.Links.Add(0, link.Text.Length, value)
                link.LinkColor = Color.Red
                link.Enabled = True
            End If
        Catch ex As Exception
            Throw New Exception("Setting link label control of textbox " & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub

    'display cb control
    Public Sub setComboBoxValue(cb As ComboBox, lov As Dictionary(Of String, CConfProperty))
        Dim datasource As New Dictionary(Of String, String)
        Try
            For Each prop As CConfProperty In lov.Values
                datasource.Add(prop.key, prop.value)
            Next
            cb.DataSource = New BindingSource(datasource, Nothing)
            cb.DisplayMember = "Value"
            cb.ValueMember = "Key"

        Catch ex As Exception
            Throw New Exception("An error occured when filling combobox " & cb.Name & Chr(10) & ex.Message & Chr(10) & ex.StackTrace, ex)
        End Try
    End Sub


    'get clipboard values in tabular mode
    Public Function getClipBoardValues() As Dictionary(Of Integer, Dictionary(Of Integer, String))
        Dim res As New Dictionary(Of Integer, Dictionary(Of Integer, String))
        Dim clipBoardText As String
        Dim lines As String(), columns As String()
        Dim colWidth As Integer = 0

        clipBoardText = Clipboard.GetText
        If clipBoardText.EndsWith(vbCrLf) Then
            clipBoardText = clipBoardText.Remove(clipBoardText.Length - vbCrLf.Length)
        End If

        lines = System.Text.RegularExpressions.Regex.Split(clipBoardText, vbCrLf)
        Dim line_dict As Dictionary(Of Integer, String)
        For Each line As String In lines
            line_dict = New Dictionary(Of Integer, String)
            columns = line.Split(vbTab)
            If colWidth = 0 Then
                colWidth = columns.Count
            Else
                If colWidth <> columns.Count Then
                    Throw New Exception("The lines items in the clipboard don't have the same number of columns items")
                End If
            End If
            For Each col As String In columns
                line_dict.Add(line_dict.Count, col)
            Next
            res.Add(res.Count, line_dict)
        Next
        Return res
    End Function

    Public Function checkOneSelection(dgrid As DataGridView) As KeyValuePair(Of Boolean, List(Of DataGridViewCell))
        Dim res As Boolean
        Dim listMinMax As New List(Of DataGridViewCell)

        If dgrid.SelectedCells.Count > 0 Then
            Dim minCell As DataGridViewCell = dgrid.SelectedCells(0)
            Dim maxCell As DataGridViewCell = dgrid.SelectedCells(0)

            For Each cell As DataGridViewCell In dgrid.SelectedCells
                If cell.RowIndex <= minCell.RowIndex AndAlso cell.ColumnIndex <= minCell.ColumnIndex Then
                    minCell = cell
                End If

                If cell.RowIndex >= maxCell.RowIndex AndAlso cell.ColumnIndex >= maxCell.ColumnIndex Then
                    maxCell = cell
                End If
            Next
            If dgrid.SelectedCells.Count = (maxCell.RowIndex - minCell.RowIndex + 1) * (maxCell.ColumnIndex - minCell.ColumnIndex + 1) Then
                res = True
            Else
                res = False
            End If
            listMinMax.Add(minCell)
            listMinMax.Add(maxCell)
        Else
            res = False
        End If

        Return New KeyValuePair(Of Boolean, List(Of DataGridViewCell))(res, listMinMax)
    End Function

    'display critical error message
    Public Sub displayMessage(title As String, message As String, type As MsgBoxStyle)
        MsgBox(message, type, title)
    End Sub

    'adapt datagridview heiht
    Public Sub resizeDataGridHeight(dgrid As DataGridView, rows As Long, depth As Long)
        Dim delta As Long
        Dim count As Long
        delta = dgrid.RowTemplate.Height * (rows + 3) - dgrid.Height
        dgrid.Height = dgrid.RowTemplate.Height * (rows + 3)
        Dim ctrl As Control
        ctrl = dgrid
        While count < depth
            ctrl.Parent.Height = ctrl.Parent.Height + delta
            count = count + 1
            ctrl = ctrl.Parent
        End While

    End Sub

    Public Sub resizeDataGridWidth(dgrid As DataGridView)
        dgrid.Width = getSizeDataGridWidth(dgrid)
    End Sub
    Public Function getSizeDataGridWidth(dgrid As DataGridView) As Integer
        Dim width As Integer

        For Each dcol As DataGridViewColumn In dgrid.Columns
            width = width + dcol.Width
        Next
        width = width + 3 + dgrid.RowHeadersWidth
        Return width
    End Function

    'get the object from bindinglistview
    Public Function getDiscoutFromListView(view As BindingListView(Of CDiscount)) As List(Of CDiscount)
        Dim res As New List(Of CDiscount)

        For Each obj As ObjectView(Of CDiscount) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getNTEFromListView(view As BindingListView(Of CNotToExceed)) As List(Of CNotToExceed)
        Dim res As New List(Of CNotToExceed)

        For Each obj As ObjectView(Of CNotToExceed) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getTaskRateFromListView(view As BindingListView(Of CTaskRate)) As List(Of CTaskRate)
        Dim res As New List(Of CTaskRate)

        For Each obj As ObjectView(Of CTaskRate) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getActFromListView(view As BindingListView(Of CScopeActivity)) As List(Of CScopeActivity)
        Dim res As New List(Of CScopeActivity)

        For Each obj As ObjectView(Of CScopeActivity) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getAWQFromListView(view As BindingListView(Of CAWQuotation)) As List(Of CAWQuotation)
        Dim res As New List(Of CAWQuotation)

        For Each obj As ObjectView(Of CAWQuotation) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getMaterialFromListView(view As BindingListView(Of CMaterial)) As List(Of CMaterial)
        Dim res As New List(Of CMaterial)

        For Each obj As ObjectView(Of CMaterial) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getAWQMaterialViewFromListView(view As BindingListView(Of CAWQFormDetailsMat)) As List(Of CAWQFormDetailsMat)
        Dim res As New List(Of CAWQFormDetailsMat)

        For Each obj As ObjectView(Of CAWQFormDetailsMat) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function


    'get the object from bindinglistview
    Public Function getQuoteFromListView(view As BindingListView(Of CSalesQuote)) As List(Of CSalesQuote)
        Dim res As New List(Of CSalesQuote)

        For Each obj As ObjectView(Of CSalesQuote) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getMarkupFromListView(view As BindingListView(Of CMatMarkup)) As List(Of CMatMarkup)
        Dim res As New List(Of CMatMarkup)

        For Each obj As ObjectView(Of CMatMarkup) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getCCFromListView(view As BindingListView(Of CCostCenter)) As List(Of CCostCenter)
        Dim res As New List(Of CCostCenter)

        For Each obj As ObjectView(Of CCostCenter) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'get the object from bindinglistview
    Public Function getLabCtrlActFilterObjFromListView(view As BindingListView(Of CLaborControlActFilterObject)) As List(Of CLaborControlActFilterObject)
        Dim res As New List(Of CLaborControlActFilterObject)
        For Each obj As ObjectView(Of CLaborControlActFilterObject) In view
            res.Add(obj.Object)
        Next
        Return res
    End Function

    'use to force databinding on none visible controls
    Public Sub CreateControl(control As Control)
        Try
            Dim method As MethodInfo = control.GetType().GetMethod("CreateControl", BindingFlags.Instance Or BindingFlags.NonPublic)
            method.Invoke(control, New Object() {True})
        Catch ex As Exception
        End Try
    End Sub
End Module
