﻿Module MSpashScreenHandler
    Public splashScreen As SplashScreen
    Public incrementStep As Integer

    Public Sub init()
        Try
            splashScreen = CType(My.Application.SplashScreen, SplashScreen)
            incrementStep = 100 / 32 ' 31 is the number of call of setCurrentTaskLabel in the file CMasterControl
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub

    Delegate Sub setCurrentTaskLabelDelegate(task As String)
    Public Sub setCurrentTaskLabel(task As String)
        Try
            If splashScreen.Visible Then
                If splashScreen.current_task_lbl.InvokeRequired Then
                    Dim deleg As New setCurrentTaskLabelDelegate(AddressOf setCurrentTaskLabel)
                    splashScreen.current_task_lbl.Invoke(deleg, New Object() {task})
                Else
                    splashScreen.current_task_lbl.Text = task
                    'log previous task
                    addLog(task)
                    'log progress
                    incrementProgressBar(incrementStep)
                End If
            End If
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub

    Delegate Sub addLogDelegate(task As String)
    Public Sub addLog(task As String)
        Try
            If splashScreen.Visible Then
                If splashScreen.loading_logs_list_b.InvokeRequired Then
                    Dim deleg As New addLogDelegate(AddressOf addLog)
                    splashScreen.loading_logs_list_b.Invoke(deleg, New Object() {task})
                Else
                    splashScreen.loading_logs_list_b.Items.Add(DateTime.Now.ToString("hh:mm:ss") & " " & task)
                    splashScreen.loading_logs_list_b.ClearSelected()
                    splashScreen.loading_logs_list_b.SelectedIndex = splashScreen.loading_logs_list_b.Items.Count - 1
                End If
            End If
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub

    Delegate Sub incrementProgressBarDelegate(percent As Integer)
    Public Sub incrementProgressBar(percent As Integer)
        Try
            If splashScreen.Visible Then
                If splashScreen.progressBar.InvokeRequired Then
                    Dim deleg As New incrementProgressBarDelegate(AddressOf incrementProgressBar)
                    splashScreen.progressBar.Invoke(deleg, New Object() {percent})
                Else
                    If splashScreen.progressBar.Value + percent > 100 Then
                        Exit Sub
                    End If
                    splashScreen.progressBar.Increment(percent)
                End If
            End If
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub

    Delegate Sub setProgressBarValueDelegate(percent As Integer)
    Public Sub setProgressBarValue(percent As Integer)
        Try
            If splashScreen.Visible Then
                If splashScreen.progressBar.InvokeRequired Then
                    Dim deleg As New setProgressBarValueDelegate(AddressOf setProgressBarValue)
                    splashScreen.progressBar.Invoke(deleg, New Object() {percent})
                Else
                    If percent > 100 Then
                        Exit Sub
                    End If
                    splashScreen.progressBar.Value = percent
                End If
            End If
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub

    Delegate Sub bringSplashFrontDelegate()
    Public Sub bringSplashFront()
        Try
            If splashScreen.Visible Then
                If splashScreen.InvokeRequired Then
                    Dim deleg As New bringSplashFrontDelegate(AddressOf bringSplashFront)
                    splashScreen.Invoke(deleg, New Object() {})
                Else
                    splashScreen.BringToFront()
                End If
            End If
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub


    Public Sub copyToClipBoard()
        Try
            'TBD
        Catch ex As Exception
            'no need to catch error, spash screen not a critical function
        End Try
    End Sub

End Module
