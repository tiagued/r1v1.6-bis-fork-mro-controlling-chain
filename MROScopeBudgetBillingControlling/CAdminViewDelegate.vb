﻿Public Class CAdminViewDelegate
    Public adminConf As CAdminConf
    Private form As FormAdmin
    Public Sub New()
        adminConf = New CAdminConf
    End Sub
    Public Sub launch()
        form = New FormAdmin
        form.ctrl = Me
        loadAdminView()
        form.ShowDialog()
        form.BringToFront()
    End Sub

    Public Sub loadAdminView()
        Try
            Dim adminMap As CViewModelMapList
            adminMap = MConstants.GLB_MSTR_CTRL.modelViewMap(MConstants.MOD_VIEW_MAP_ADMIN_EDIT_VIEW_DISPLAY)
            'bind activity data
            Dim bds As BindingSource

            'Bind current activity to the view
            Dim tbbd As Binding
            For Each map As CViewModelMap In adminMap.list.Values
                If String.IsNullOrWhiteSpace(map.col_control_type) Or map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_TEXT).value Then
                    Dim textBox As TextBox
                    textBox = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, TextBox)
                    textBox.DataBindings.Clear()
                    tbbd = textBox.DataBindings.Add("Text", adminConf, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    'format double
                    If Not String.IsNullOrWhiteSpace(map.col_format) Then
                        tbbd.FormatInfo = MConstants.CULT_INFO
                        tbbd.FormatString = map.col_format
                        tbbd.FormattingEnabled = True
                    End If
                    textBox.Enabled = map.col_editable
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_COMBO).value Then
                    Dim comboBox As ComboBox
                    comboBox = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, ComboBox)
                    comboBox.DataBindings.Clear()
                    comboBox.AutoCompleteMode = AutoCompleteMode.None
                    comboBox.DropDownStyle = ComboBoxStyle.DropDownList
                    comboBox.Enabled = map.col_editable
                    MViewEventHandler.addDefaultComboBoxEventHandler(comboBox)

                    bds = New BindingSource
                    If MConstants.listOfValueConf.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf(map.view_list_name).Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "value"
                        comboBox.ValueMember = "key"
                        comboBox.DataBindings.Add("SelectedValue", adminConf, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    ElseIf MConstants.listOfValueConf_edit.ContainsKey(map.view_list_name) Then
                        bds.DataSource = MConstants.listOfValueConf_edit(map.view_list_name).Values
                        comboBox.DataSource = bds
                        comboBox.DisplayMember = "value"
                        comboBox.ValueMember = "key"
                        comboBox.DataBindings.Add("SelectedValue", adminConf, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    Else
                        Throw New Exception("Error occured when binding admin view. List " & map.view_list_name & " does not exist in LOV")
                    End If
                ElseIf map.col_control_type = MConstants.constantConf(MConstants.CONST_CONF_COL_TYPE_CHECK).value Then
                    Dim chk As CheckBox
                    chk = CType(form.Controls.Find(map.view_control, True).FirstOrDefault, CheckBox)
                    chk.DataBindings.Clear()
                    chk.DataBindings.Add("Checked", adminConf, map.class_field, False, DataSourceUpdateMode.OnPropertyChanged)
                    chk.Enabled = map.col_editable
                End If
            Next

            'add handler on buttons click here

        Catch ex As Exception
            MsgBox("An error occured while loading data of admin view " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub

    'get  ace versions
    Public Sub getACEVersion()
        Try
            Dim reader As OleDb.OleDbDataReader = OleDb.OleDbEnumerator.GetRootEnumerator
            Dim res As New List(Of String)
            While reader.Read
                For i As Integer = 0 To reader.FieldCount - 1
                    If reader.GetName(i) = "SOURCES_NAME" Then
                        res.Add(reader.GetValue(i).ToString)
                    End If
                Next
            End While
            reader.Close()
            MsgBox(String.Join(Chr(10), res), MsgBoxStyle.Information, "ACE Driver Version List")

        Catch ex As Exception
            MsgBox("An error occured while getting ACE Version " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub


    'get  ace versions
    Public Sub getCultureInfo()
        Try
            Dim cult As Globalization.CultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture
            Dim mess As String = "Culture Info Data : " & cult.ToString & ", " & cult.DisplayName

            MsgBox(mess, MsgBoxStyle.Information, "Culture Info")

        Catch ex As Exception
            MsgBox("An error occured while getting CUlture Info " & Chr(10) & ex.Message & ex.StackTrace, MsgBoxStyle.Critical)
        End Try
    End Sub
End Class
